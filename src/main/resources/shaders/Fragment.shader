#version 430 core
#define MAX_D_LIGHTS 10
#define MAX_P_LIGHTS 10
#define MAX_S_LIGHTS 10


struct DirLight
{
	vec4 direction;

	vec4 diffuse;
	vec4 specular;
};

struct PointLight
{
	vec4 position;

	float constant;
	float linear;
	float quadratic;

	vec4 diffuse;
	vec4 specular;
};

struct SpotLight
{
	vec4 position;
	vec4  direction;

	float cutOff;
	float outerCutOff;

	vec4 diffuse;
	vec4 specular;
};


uniform vec4 cameraPosition;
uniform DirLight dirLights[MAX_D_LIGHTS];
uniform PointLight pointLights[MAX_P_LIGHTS];
uniform SpotLight spotLights[MAX_S_LIGHTS];
uniform int DirLightCount;
uniform int PointLightCount;
uniform int SpotLightCount;
uniform vec4 ambient;

in VS_OUT
{
	vec3 fragPos;
	vec3 normal;
	vec4 materialAmbient;
	vec4 materialDiffuse;
	vec4 materialSpecular;
	float materialShininess;
}fs_in;

layout (location = 0) out vec4 colour;
layout (location = 1) out vec4 bloom;

vec4 calcDirLight(DirLight light)
{
	vec3 lightDir = -light.direction.xyz;
	lightDir = normalize(lightDir);
	vec3 normal =  fs_in.normal;

	//diffuse light, casts reflections on surfaces facing the light
	float diffuseIntensity = max(dot(normal, lightDir), 0.0);
	vec4 diffuse  = light.diffuse * (diffuseIntensity * fs_in.materialDiffuse);

	//specular light, casts a bright spot reltive to the veiwer's position
	vec3 viewDir = normalize(cameraPosition.xyz - fs_in.fragPos);
	vec3 halfDir = normalize(lightDir + viewDir);
	float specAngle = max(dot(halfDir, normal), 0.0);
	float specularIntensity = pow(specAngle, fs_in.materialShininess);
	vec4 specular = light.specular * (specularIntensity * fs_in.materialSpecular);

	return diffuse + specular;
}

vec4 calcPointLight(PointLight light)
{

	//simulated attenuation, see http://wiki.ogre3d.org/tiki-index.php?page=-Point+Light+Attenuation
	float distance = length(light.position.xyz - fs_in.fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));


	vec3 lightDir = light.position.xyz - fs_in.fragPos;
	lightDir = normalize(lightDir);
	vec3 normal =  fs_in.normal;

	//diffuse light, casts reflections on surfaces facing the light
	float diffuseIntesntiy = max(dot(normal, lightDir), 0.0);
	vec4 diffuse  = light.diffuse * (diffuseIntesntiy * fs_in.materialDiffuse);

	//specular light, casts a bright spot reltive to the veiwer's position
	vec3 viewDir = normalize(cameraPosition.xyz-fs_in.fragPos);
	vec3 halfDir = normalize(lightDir + viewDir);
	float specAngle = max(dot(halfDir, normal), 0.0);
	float specularIntensity = pow(specAngle, fs_in.materialShininess);
	vec4 specular = light.specular * (specularIntensity * fs_in.materialSpecular);

	diffuse  *= attenuation;
	specular *= attenuation;

	return diffuse + specular;

}

vec4 calcSpotLight(SpotLight light)
{

	vec3 lightDir = light.position.xyz - fs_in.fragPos;
	lightDir = normalize(lightDir);
	vec3 normal =  fs_in.normal;

	//adding a smooth cutoff for the light
	float cutOff = cos(light.cutOff);
	float outerCutOff = cos(light.outerCutOff);
	vec3 dir = light.direction.xyz;
	float theta     = dot(lightDir, normalize(-dir));
	float epsilon   = cutOff - outerCutOff;
	float intensity = clamp((theta - outerCutOff) / epsilon, 0.0, 1.0);

	//diffuse light, casts reflections on surfaces facing the light
	float diffuseIntesntiy = max(dot(normal, lightDir), 0.0);
	vec4 diffuse  = light.diffuse * (diffuseIntesntiy * fs_in.materialDiffuse);

	//specular light, casts a bright spot reltive to the veiwer's position
	vec3 viewDir = normalize(cameraPosition.xyz-fs_in.fragPos);
	vec3 halfDir = normalize(lightDir + viewDir);
	float specAngle = max(dot(halfDir, normal), 0.0);
	float specularIntensity = pow(specAngle, fs_in.materialShininess);
	vec4 specular = light.specular * (specularIntensity * fs_in.materialSpecular);


	specular *= intensity;
	diffuse  *= intensity;

	return diffuse + specular;
}

void main(void)
{
	vec4 result = vec4(0, 0, 0, 0);

	//ambient light, cast everywhere with no regard to direction
	vec4 ambientTotal = ambient * fs_in.materialAmbient;

	result += ambientTotal;

	for (int i = 0; i < DirLightCount; i++){
		int j = i;
		result += calcDirLight(dirLights[j]);
	}
	for (int i = 0; i < PointLightCount; i++){
		int j = i;
		result += calcPointLight(pointLights[j]);
	}
	for (int i = 0; i < SpotLightCount; i++){
		int j = i;
		result += calcSpotLight(spotLights[j]);
	}

	if (length(result.xyz) > sqrt(2.2) || fs_in.materialAmbient.w == 2){
		bloom = result;
	}
	else {
		bloom = vec4(0, 0, 0, 0);
	}

	colour = result;

}
