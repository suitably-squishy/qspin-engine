#version 430 core

layout (location = 0) in vec4 vertex;
layout (location = 1) in vec4 normal;
layout (location = 2) in vec4 materialAmbient;
layout (location = 3) in vec4 materialDiffuse;
layout (location = 4) in vec4 materialSpecular;
layout (location = 5) in float materialShininess;
layout (location = 6) in mat4 transform;
//location 7
//location 8
//location 9
layout (location = 10) in mat4 modelOffset;
//location 11
//location 12
//location 13

uniform mat4 cameraProjection;

out VS_OUT
{
	vec3 fragPos;
	vec3 normal;
	vec4 materialAmbient;
	vec4 materialDiffuse;
	vec4 materialSpecular;
	float materialShininess;
}vs_out;

void main(void)
{
	// Index into our array using gl_VertexID
	vec4 pos =  transform * modelOffset * vertex;
	vec4 projectedPos = cameraProjection * pos;
	gl_Position = projectedPos;


	vs_out.fragPos = pos.xyz;


	vec4 norm4d = transform * modelOffset * normal;
	vec3 norm3d = norm4d.xyz;
	vs_out.normal = normalize(norm4d.xyz);

	vs_out.materialAmbient = materialAmbient;
	vs_out.materialDiffuse = materialDiffuse;
	vs_out.materialSpecular = materialSpecular;
	vs_out.materialShininess = materialShininess;
}
