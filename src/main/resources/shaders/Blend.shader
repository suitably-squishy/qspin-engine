#version 430 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D scene;
uniform sampler2D blend;
uniform float exposure;

void main()
{
    vec3 sceneCol = texture(scene, TexCoords).rgb;
    vec3 blendCol = texture(blend, TexCoords).rgb;
    sceneCol += blendCol;// additive blending
    // tone mapping
    vec3 result = vec3(1.0) - exp(-sceneCol * exposure);
    FragColor = vec4(result, 1.0);
}