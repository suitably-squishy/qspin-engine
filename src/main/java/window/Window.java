package window;

import org.joml.Vector2i;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL43;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.opengl.GLUtil;
import settings.WindowSettings;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.glfw.GLFW.*;
import static settings.WindowSettings.*;

/**
 * A Class to hold the window properties.
 * Holds the openGl ID, the width and the height
 */
public class Window {
	private long window;
	private byte screenType;
	private int width, height;

	/**
	 * Create a new window
	 *
	 * @param windowSettings The settings of the window.
	 * @param name           The contextual name of the window which will be displayed on the window and task view.
	 */
	public Window(WindowSettings windowSettings, String name) {
		this.screenType = windowSettings.getScreenType();

		GLFWVidMode videoMode = windowSettings.getVideoMode();

		// Switch between fullscreen, borderless windowed and windowed. Windowed default size is half the screen size.
		switch (screenType) {
			case FULLSCREEN:
				width = videoMode.width();
				height = videoMode.height();
				glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
				glfwWindowHint(GLFW_REFRESH_RATE, videoMode.refreshRate());
				window = glfwCreateWindow(width, height, name, glfwGetPrimaryMonitor(), 0);
				break;
			case FULLSCREEN_WINDOWED:
				width = videoMode.width();
				height = videoMode.height();
				glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
				glfwWindowHint(GLFW_RED_BITS, videoMode.redBits());
				glfwWindowHint(GLFW_GREEN_BITS, videoMode.greenBits());
				glfwWindowHint(GLFW_BLUE_BITS, videoMode.blueBits());
				window = glfwCreateWindow(width, height, name, 0, 0);
				glfwSetWindowPos(window, 0, 0);
				glfwShowWindow(window);
				break;
			case WINDOWED:
				width = videoMode.width() / 2;
				height = videoMode.height() / 2;
				glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);
				window = glfwCreateWindow(width, height, name, 0, 0);
				glfwSetWindowPos(window, width / 2, height / 2);
				glfwShowWindow(window);
				break;
			//TODO: implement onResize
		}

		glfwMakeContextCurrent(window);

		if (windowSettings.isVSyncEnabled()) {
			glfwSwapInterval(1);
		}
	}

	/**
	 * Switch the buffers so that the buffer that was just drawn to is displayed
	 */
	public void swapBuffers() {
		glfwSwapBuffers(window); // swap the color buffers

	}

	/**
	 * Whether the window should close, closing openGl
	 *
	 * @return True if glfwWindowShouldClose, False otherwise
	 */
	public boolean shouldClose() {
		return glfwWindowShouldClose(window);
	}

	/**
	 * Get the Id of the window
	 *
	 * @return The openGl Id of the window
	 */
	public long getWindowId() {
		return window;
	}

	/**
	 * Get the width of the window.
	 *
	 * @return the width in pixels
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Get the height of the window.
	 *
	 * @return the height in pixels
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Gets the size of the screen as a vector.
	 *
	 * @return A vector with the x and y sizes of the window.
	 */
	public Vector2i getSize() {
		return new Vector2i(width, height);
	}

	/**
	 * Initialises GLFW.
	 */
	public static void initialiseGLFW() {
		if (!glfwInit()) {
			throw new IllegalStateException("Unable to initialize GLFW");
		}
	}

	/**
	 * Creates a capability test window, returning the capabilities of the graphics card.
	 *
	 * @return A GLCapabilities object with the capabilities of the graphics card.
	 */
	public static GLCapabilities createCapabilityTestWindow() {
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		// settings for if  the window will be resizable
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
		//Scale to monitor scaling
		glfwWindowHint(GLFW_SCALE_TO_MONITOR, GLFW_TRUE);
		// Creates a testing window to get the capabilities of the graphics card /
		// monitor.
		long temp = glfwCreateWindow(1, 1, "", 0, 0);
		glfwMakeContextCurrent(temp);
		GL.createCapabilities();
		GLCapabilities caps = GL.getCapabilities();
		glfwDestroyWindow(temp);
		return caps;
	}

	/**
	 * Sets up any GL debugging required.
	 */
	public static void setupDebugging() {
		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();
		// will print the error message in System.err.
		GLUtil.setupDebugMessageCallback(System.out);

		GL43.glDebugMessageControl(GL43.GL_DEBUG_SOURCE_API, GL43.GL_DEBUG_TYPE_OTHER, GL43.GL_DEBUG_SEVERITY_NOTIFICATION, (IntBuffer) null, true);
	}

	/**
	 * Gets the list of possible video modes the graphics card supports.
	 *
	 * @return A list of GLFWVidMode's.
	 */
	public static List<GLFWVidMode> getVideoModes() {
		GLFWVidMode.Buffer videoModes = glfwGetVideoModes(glfwGetPrimaryMonitor());
		if (videoModes == null) return null;
		List<GLFWVidMode> videoModeList = new ArrayList<>();
		for (GLFWVidMode mode : videoModes) {
			videoModeList.add(mode);
		}
		return videoModeList;
	}

	/**
	 * Gets the default video mode of the primary monitor.
	 *
	 * @return A GLFWVidMode for the default video mode.
	 */
	public static GLFWVidMode getDefaultVideoMode() {
		return glfwGetVideoMode(glfwGetPrimaryMonitor());
	}
}
