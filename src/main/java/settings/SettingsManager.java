package settings;

/**
 * A class for storing different types of settings wrappers.
 */
public class SettingsManager {
	// TODO Make more flexible and user extensible

	private WindowSettings windowSettings;

	public WindowSettings getWindowSettings() {
		return windowSettings;
	}

	public void setWindowSettings(WindowSettings windowSettings) {
		this.windowSettings = windowSettings;
	}
}
