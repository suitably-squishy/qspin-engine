package settings;

import java.util.List;

/**
 * A class for representing settings which can have a set of distinct values.
 *
 * @param <T> The type of the value the setting hold.
 */
public class OptionSetting<T> extends Setting<T> {

	/**
	 * The list of possible values the setting can hold.
	 */
	private List<T> possibleValues;

	/**
	 * Creates an option setting with the provided names and potential values.
	 *
	 * @param displayName    The human readable name of the setting.
	 * @param name           The internal name of the setting to reference it by.
	 * @param userEditable   Whether or not the value of the setting is changeable at runtime by the user.
	 * @param value          The current value of the setting.
	 * @param defaultValue   The default value of the setting.
	 * @param possibleValues The list of possible values the setting can hold, if either defaultValue or value are not present, they are added.
	 */
	public OptionSetting(String displayName, String name, boolean userEditable, T value, T defaultValue, List<T> possibleValues) {
		super(displayName, name, userEditable, value, defaultValue);
		this.possibleValues = possibleValues;
		if (!this.possibleValues.contains(value)) {
			this.possibleValues.add(value);
		}
		if (!this.possibleValues.contains(defaultValue)) {
			this.possibleValues.add(defaultValue);
		}
	}

	public List<T> getPossibleValues() {
		return possibleValues;
	}

	@Override
	public void setValue(T value) {
		if (possibleValues.contains(value)) {
			super.setValue(value);
		}
	}
}
