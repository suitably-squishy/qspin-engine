package settings;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 * A class for representing settings and their values.
 *
 * @param <T> The type of the value that the setting carries.
 */
public abstract class Setting<T> {
	// TODO Serialisation

	/**
	 * The user readable name of the setting.
	 */
	private String displayName;

	/**
	 * The internal name of the setting to reference.
	 */
	private String name;

	/**
	 * The current value of the setting.
	 */
	private T value;

	/**
	 * The default value of the setting.
	 */
	private T defaultValue;

	/**
	 * Whether or not the setting is editable at runtime by the user.
	 */
	private boolean userEditable;

	/**
	 * The set of listeners to be notified when the value of the setting changes.
	 */
	private Set<SettingChangeListener<T>> listeners;

	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/**
	 * Creates a new setting with the provided names and values.
	 *
	 * @param displayName  The human readable name of the setting.
	 * @param name         The internal name of the setting to reference it by.
	 * @param userEditable Whether or not the setting is editable by the user at runtime.
	 * @param defaultValue The default value of the setting.
	 * @param value        The current value of the setting.
	 */
	public Setting(String displayName, String name, boolean userEditable, T defaultValue, T value) {
		this.displayName = displayName;
		this.name = name;
		this.value = value;
		this.defaultValue = defaultValue;
		this.userEditable = userEditable;
		this.listeners = new HashSet<>();
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getName() {
		return name;
	}

	public T getValue() {
		return value;
	}

	/**
	 * Sets the value of the setting, notifying all listeners.
	 *
	 * @param value The value to assign to the setting.
	 */
	public void setValue(T value) {
		if (userEditable) {
			T oldValue = this.value;
			this.value = value;
			for (SettingChangeListener<T> listener : listeners) {
				listener.onValueChanged(this, oldValue);
			}
		} else {
			LOGGER.warning("An attempted change of setting \"" + name + "\" was denied.");
		}
	}

	public T getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(T defaultValue) {
		this.defaultValue = defaultValue;
	}

	public boolean isUserEditable() {
		return userEditable;
	}

	/**
	 * Subscribes the provided listener to be notified if the value of the setting changes.
	 *
	 * @param listener The listener to be subscribed.
	 */
	public void subscribe(SettingChangeListener<T> listener) {
		listeners.add(listener);
	}

	/**
	 * Unsubscribes the provided listener from being notified if the value of the setting changes.
	 *
	 * @param listener The listener to be unsubscribed.
	 */
	public void unsubscribe(SettingChangeListener<T> listener) {
		listeners.remove(listener);
	}
}
