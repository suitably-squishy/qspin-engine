package settings;

import org.lwjgl.glfw.GLFWVidMode;

import java.util.Arrays;
import java.util.List;

/**
 * A class for holding settings to do with the window configuration.
 */
public class WindowSettings {

	/**
	 * The setting for the list of possible video modes the monitor can support.
	 */
	private OptionSetting<GLFWVidMode> videoMode;

	/**
	 * The setting for the screen type, one of FULLSCREEN, FULLSCREEN_WINDOWED or WINDOWED.
	 */
	private OptionSetting<Byte> screenType;
	public static final byte FULLSCREEN = 0, FULLSCREEN_WINDOWED = 1, WINDOWED = 2;

	/**
	 * The setting for whether vSync should be enabled.
	 */
	private BooleanSetting vSync;

	/**
	 * The setting for whether the window should be resizeable, only applies if in windowed mode.
	 */
	private BooleanSetting resizeable;

	/**
	 * Creates window settings with vSync and resizeable window enabled by default.
	 *
	 * @param currentVideoMode    The current video mode being used by the monitor.
	 * @param availableVideoModes The list of available video modes.
	 */
	public WindowSettings(GLFWVidMode currentVideoMode, List<GLFWVidMode> availableVideoModes) {
		this.videoMode = new OptionSetting<>("Video Resolution", "resolution", true, currentVideoMode, currentVideoMode, availableVideoModes);
		this.screenType = new OptionSetting<>("Screen Type", "screenType", true, WINDOWED, WINDOWED, Arrays.asList(FULLSCREEN, FULLSCREEN_WINDOWED, WINDOWED));
		this.vSync = new BooleanSetting("Enable VSync", "vSync", true, true);
		this.resizeable = new BooleanSetting("Window Resizable", "resizeable", false, true);
	}

	public Byte getScreenType() {
		return screenType.getValue();
	}

	public OptionSetting<Byte> getScreenTypeSetting() {
		return screenType;
	}

	public GLFWVidMode getVideoMode() {
		return videoMode.getValue();
	}

	public OptionSetting<GLFWVidMode> getVideoModeSetting() {
		return videoMode;
	}

	public boolean isVSyncEnabled() {
		return vSync.getValue();
	}

	public BooleanSetting getVSyncSetting() {
		return vSync;
	}

	public boolean isResizeable() {
		return resizeable.getValue();
	}

	public BooleanSetting getResizeableSetting() {
		return resizeable;
	}
}
