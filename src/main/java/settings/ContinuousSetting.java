package settings;

/**
 * A class for representing settings which can have a value from a minimum to a maximum.
 *
 * @param <T>
 */
public class ContinuousSetting<T extends Number & Comparable<? super T>> extends Setting<T> {

	private T min;
	private T max;

	/**
	 * Creates a setting with the provided names and values, with a minimum and a maximum.
	 *
	 * @param displayName  The human readable name of the setting.
	 * @param name         The internal name of the setting to be referenced by.
	 * @param userEditable Whether or not the value of the setting can be changed at runtime by the user.
	 * @param defaultValue The default value of the setting.
	 * @param value        The current value of the setting.
	 * @param min          The minimum value of the setting, null if no minimum.
	 * @param max          The maximum value of the setting, null if no maximum.
	 */
	public ContinuousSetting(String displayName, String name, boolean userEditable, T defaultValue, T value, T min, T max) {
		super(displayName, name, userEditable, defaultValue, value);
		if (min != null && max != null && min.compareTo(max) > 0) {
			min = max;
		}
		this.min = min;
		this.max = max;
		if (min != null && value.compareTo(min) < 0) {
			setValue(min);
		}
		if (max != null && value.compareTo(max) > 0) {
			setValue(max);
		}
	}

	/**
	 * Creates a continuous setting without a minimum or maximum value.
	 *
	 * @param displayName  The human readable name of the setting.
	 * @param name         The internal name of the setting to be referenced by.
	 * @param userEditable Whether or not the value of the setting can be changed at runtime by the user.
	 * @param defaultValue The default value of the setting.
	 * @param value        The current value of the setting.
	 */
	public ContinuousSetting(String displayName, String name, boolean userEditable, T defaultValue, T value) {
		this(displayName, name, userEditable, defaultValue, value, null, null);
	}

	/**
	 * Sets the value of the setting, notifying any listeners, clamping the value to the min, max range.
	 *
	 * @param value The value to assign to the setting.
	 */
	@Override
	public void setValue(T value) {
		if (min != null && value.compareTo(min) < 0) {
			super.setValue(min);
		} else if (max != null && value.compareTo(max) > 0) {
			super.setValue(max);
		} else {
			super.setValue(value);
		}
	}
}
