package settings;

/**
 * An interface for objects wanting to be notified of changes to settings.
 *
 * @param <T> The type of the setting being changed.
 */
@FunctionalInterface
public interface SettingChangeListener<T> {
	void onValueChanged(Setting<T> setting, T oldValue);
}
