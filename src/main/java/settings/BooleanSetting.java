package settings;

import java.util.Arrays;

/**
 * A class for a setting which can be either true or false.
 */
public class BooleanSetting extends OptionSetting<Boolean> {

	/**
	 * Creates a new boolean setting with the provided names and default value.
	 *
	 * @param displayName  The user readable name of the setting.
	 * @param name         The internal name of the setting to be referenced by.
	 * @param userEditable Whether the setting can be changed by a user at runtime.
	 * @param defaultValue The default and current value of the setting.
	 */
	public BooleanSetting(String displayName, String name, boolean userEditable, Boolean defaultValue) {
		super(displayName, name, userEditable, defaultValue, defaultValue, Arrays.asList(true, false));
	}

	/**
	 * Creates a new boolean setting with the provided names and default value.
	 *
	 * @param displayName  The user readable name of the setting.
	 * @param name         The internal name of the setting to be referenced by.
	 * @param userEditable Whether the setting can be changed by a user at runtime.
	 * @param defaultValue The default value of the setting.
	 * @param value        The current value of the setting.
	 */
	public BooleanSetting(String displayName, String name, boolean userEditable, Boolean defaultValue, Boolean value) {
		super(displayName, name, userEditable, value, defaultValue, Arrays.asList(true, false));
	}
}
