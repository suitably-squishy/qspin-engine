package demo;

import entities.Entity;
import entities.LightEntity;
import entities.VoxelEntity;
import entities.filters.Class;
import entities.physics.collision.RayEntityIntersection;
import entities.physics.forces.ForceBehaviour;
import entities.physics.forces.ForceManager;
import io.input.binds.Bind;
import io.input.binds.BindContext;
import io.input.binds.BindReleaseSubscriber;
import io.input.mouse.MouseInputContext;
import org.joml.Vector2d;
import org.joml.Vector3f;
import render.camera.Camera;
import render.camera.PerspectiveCamera;
import render.model.VoxelModel;
import runner.Runner;
import sound.SoundBuffer;
import state.GameState;
import utils.RGBColour;
import utils.RGBMaterial;

import java.util.Collections;
import java.util.List;

import static org.lwjgl.glfw.GLFW.*;

/**
 * A small city tile scene
 *
 * @author Ryan Carreck
 * @version Pre-0.1.0
 * @since 28/02/2020
 */
public class TestSceneMain {

	public static void main(String[] args) {
		Runner runner = new Runner("Test");

		// Mouse manager and context setup
		glfwSetInputMode(runner.getWindow().getWindowId(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		MouseInputContext mouseInputContext = new MouseInputContext("default");
		runner.getMouseInputManager().addContextAndMakeCurrent(mouseInputContext);

		// Key bind manager and context setup
		BindContext bindContext = new BindContext("default");
		runner.getBindManager().addContextAndMakeCurrent(bindContext);

		bindContext.bind(GLFW_KEY_ESCAPE, new Bind("exit"));

		bindContext.getBind("exit").subscribe((BindReleaseSubscriber) (bindName, key, scancode, mods) -> {
			glfwSetWindowShouldClose(runner.getWindow().getWindowId(), true);
			return true;
		});

		GameState state = new GameState();

		// Get all the models required
		VoxelModel base = runner.getResourceManager().get(VoxelModel.class, "Test Scene/city tile base");
		VoxelModel building = runner.getResourceManager().get(VoxelModel.class, "Test Scene/building");
		VoxelModel cargo = runner.getResourceManager().get(VoxelModel.class, "Test Scene/cargo");
		VoxelModel cargoHover = runner.getResourceManager().get(VoxelModel.class, "Test Scene/cargo hover");
		VoxelModel topper = runner.getResourceManager().get(VoxelModel.class, "Test Scene/house topper");
		VoxelModel test = runner.getResourceManager().get(VoxelModel.class, "Test/ColourTest");

		//change the material of the voxel model post loading (does not work after creation of an entity)
		RGBMaterial testRgbMaterial = building.getPalette().getMaterial((short) 151);
		testRgbMaterial.setShininess(256);
		testRgbMaterial.setSpecular(RGBColour.WHITE);
//		testRgbMaterial.getAmbient().setAlpha(255 * 2);
		building.getPalette().setMaterials((short) 151, testRgbMaterial);


		// Our root entity
		Entity cityBase = new VoxelEntity("Baseplate", base);

		// Creates a building
		Entity cityBuilding1 = new VoxelEntity("Building1", Collections.singletonList("building"), building);
		Entity houseTopper1 = new VoxelEntity("Topper1", Collections.singletonList("topper"), topper);
		// Adds the building to the root entity
		cityBase.addChild(cityBuilding1);
		// Adds the topper as a child of the building
		cityBuilding1.addChild(houseTopper1);

		//repeat that four times
		Entity cityBuilding2 = new VoxelEntity("Building2", Collections.singletonList("building"), building);
		Entity houseTopper2 = new VoxelEntity("Topper2", Collections.singletonList("topper"), topper);
		cityBase.addChild(cityBuilding2);
		cityBuilding2.addChild(houseTopper2);

		Entity cityBuilding3 = new VoxelEntity("Building3", Collections.singletonList("building"), building);
		Entity houseTopper3 = new VoxelEntity("Topper3", Collections.singletonList("topper"), topper);
		cityBase.addChild(cityBuilding3);
		cityBuilding3.addChild(houseTopper3);

		Entity cityBuilding4 = new VoxelEntity("Building4", Collections.singletonList("building"), building);
		Entity houseTopper4 = new VoxelEntity("Topper4", Collections.singletonList("topper"), topper);
		cityBase.addChild(cityBuilding4);
		cityBuilding4.addChild(houseTopper4);


		// Creates a hovering car above the city
		VoxelEntity cityCargoHover = new VoxelEntity("HoverCar", Collections.singletonList("car"), cargoHover);
		cityBase.addChild(cityCargoHover);


		// Adds a car on the road outside
		VoxelEntity cityCargo = new VoxelEntity("MovingCar", Collections.singletonList("car"), cargo);
		cityBase.addChild(cityCargo);

		ForceManager cargoForceManager = cityCargo.getPhysicalProperties().getForceManager();

		cargoForceManager.addGravity(9.81f, new Vector3f(0, -1, 0));
		cargoForceManager.addContactForce("floor", 0.1f, new Vector3f(0, 1, 0));
		cargoForceManager.addImpulse(5000, new Vector3f(1, 0, 0));

		cityCargo.addBehaviour("forceSimulation", new ForceBehaviour(), 1);

		// Creates a new camera to view the scene from
		Camera camera = new PerspectiveCamera(50, runner.getWindow().getWidth(), runner.getWindow().getHeight(), 0.1f, 1000);

		// Creates our custom camera entity that can be controlled by a user
		FirstPersonCamera cameraEntity = new FirstPersonCamera(camera);
		cameraEntity.classList.add("camera");
		cameraEntity.setId("cameraEntity");

		// Adds the camera as a child of the root entity
		cityBase.addChild(cameraEntity);
		cameraEntity.setUpCamera(bindContext, mouseInputContext);

		//add 5 light entities and add a new spotlight to each
		Entity testLight1 = new LightEntity(0.4f, 0.42f);
		testLight1.classList.addAll(List.of("light", "spotLight"));
		testLight1.setId("testLight1");
		Entity testLight2 = new LightEntity(0.4f, 0.42f);
		testLight2.classList.addAll(List.of("light", "spotLight"));
		testLight2.setId("testLight2");
		Entity testLight3 = new LightEntity(0.4f, 0.42f);
		testLight3.classList.addAll(List.of("light", "spotLight"));
		testLight3.setId("testLight3");
		Entity testLight4 = new LightEntity(0.4f, 0.42f);
		testLight4.classList.addAll(List.of("light", "spotLight"));
		testLight4.setId("testLight4");
		Entity testLight5 = new LightEntity(0.4f, 0.42f);
		testLight5.classList.addAll(List.of("light", "spotLight"));
		testLight5.setId("testLight5");

		Entity marker = new VoxelEntity("marker", test);
		marker.getRelativeTransform().rotateY((float) Math.PI / 2).scale(0.5f);
		cityBase.addChild(marker);

		//add the lights to voxel models
		houseTopper1.addChild(testLight1);
		houseTopper2.addChild(testLight2);
		houseTopper3.addChild(testLight3);
		houseTopper4.addChild(testLight4);
		cityCargo.addChild(testLight5);
		// Sets the root entity as our city
		state.setRootEntity(cityBase);
		state.setCurrentCamera(camera);
		runner.setState(state);
		runner.startup();

		// Sets the transform of the buildings
		cityBuilding1.getRelativeTransform().translate(4, 0, -4);
		houseTopper1.getRelativeTransform().translate(-2, 25, -4);

		cityBuilding2.getRelativeTransform().translate(4, 0, 12);
		houseTopper2.getRelativeTransform().translate(-2, 25, -4);

		cityBuilding3.getRelativeTransform().translate(-12, 0, -4);
		houseTopper3.getRelativeTransform().translate(-2, 25, -4);

		cityBuilding4.getRelativeTransform().translate(-12, 0, 12);
		houseTopper4.getRelativeTransform().translate(-2, 25, -4);

		cityCargoHover.getRelativeTransform().translate(0, 30, 0);
		cityCargo.getRelativeTransform().translate(-10, -16, -17);

		cameraEntity.getRelativeTransform().translate(0, 0, -6);

		// Sets the cars to move every update and the toppers to rotate
		state.addBehaviour("moveAll", (delta, owner) -> {
			if (cityCargo.getAbsoluteTransform().getTranslation(new Vector3f()).x > 23) {
				cityCargo.getPhysicalProperties().getForceManager().removeContactForce("floor");
			}
			cityCargoHover.getRelativeTransform().rotateY(0.01f);
			houseTopper1.getRelativeTransform().rotateY(-0.02f);
			houseTopper2.getRelativeTransform().rotateY(-0.02f);
			houseTopper3.getRelativeTransform().rotateY(-0.02f);
			houseTopper4.getRelativeTransform().rotateY(-0.02f);
		});

		SoundBuffer soundBuffer = runner.getResourceManager().get(SoundBuffer.class, "giraffe");

		cityCargo.getSoundModule().addSound(soundBuffer, "giraffe");

		// Adds a simple bind to click to raycast from the center of the screen.
		bindContext.bind(GLFW_MOUSE_BUTTON_1, new Bind("raycast").subscribe(
				(BindReleaseSubscriber) (bindName, button, scancode, mods) -> {
					cityCargo.getSoundModule().play("giraffe");
					RayEntityIntersection.Result result = RayEntityIntersection.raycastFromScreenPosition(
							new Vector2d(runner.getWindow().getWidth() / 2f, runner.getWindow().getHeight() / 2f),
							new Vector2d(runner.getWindow().getSize()),
							camera,
							cameraEntity.cameraPitchController.getAbsoluteTransform(),
							state.getRootEntity()
					);

					if (result != null) {
						System.out.println("You clicked on " + result.collidedEntity);
						marker.getRelativeTransform().identity().translate(result.collisionPosition).rotateY((float) Math.PI / 2).scale(0.5f);
					}
					return true;
				}));

		// Adds a simple bind to move all buildings up by one block when the right mouse button is clicked.
		Bind moveBuildingsBind = new Bind("moveBuildings");
		moveBuildingsBind.subscribe((BindReleaseSubscriber) (bindName, button, scancode, mods) -> {
			cityBase.getDescendants(new Class("building")).forEach(b -> b.getRelativeTransform().translate(new Vector3f(0, 0, 1)));
			return true;
		});
		bindContext.bind(GLFW_MOUSE_BUTTON_2, moveBuildingsBind);

		runner.run();
		runner.shutdown();
	}
}

