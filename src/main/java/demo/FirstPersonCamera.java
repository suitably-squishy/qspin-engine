package demo;

import entities.CameraEntity;
import entities.Entity;
import io.input.binds.BindContext;
import io.input.mouse.MouseInputContext;
import render.camera.Camera;

/**
 * An example class to create a controllable camera.
 * Use WASDQE to move around and mouse to rotate the camera.
 * Two entities are used, one to deal with movement and yaw,
 * and one (that holds the camera) to adjust pitch
 * Esc quits the demo
 *
 * @author Ryan Carreck
 * @version Pre-0.1.0
 * @since 28/02/2020
 */
public class FirstPersonCamera extends Entity {

	public CameraEntity cameraPitchController;
	private final Camera camera;
	private float speed = 20;

	/**
	 * Create a new camera entity with the given type of camera
	 *
	 * @param camera The camera to be held,
	 *               different cameras could be different projection matrices
	 */
	public FirstPersonCamera(Camera camera) {
		this.camera = camera;
	}

	/**
	 * Creates the subscribers for WASD movement and looking around with the mouse
	 */
	public void setUpCamera(BindContext bindContext, MouseInputContext mouseContext) {
		this.addBehaviour("wasdqeMovement", new WASDQEMovement(this, bindContext, speed), 1);

		this.cameraPitchController = new CameraEntity(this.camera);
		this.addChild(cameraPitchController);
		cameraPitchController.getSoundModule().becomeListener();

		this.addBehaviour("look", new FirstPersonPivotLook(this, mouseContext, cameraPitchController), 50);
	}
}

