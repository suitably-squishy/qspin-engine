package demo;

import behaviour.BehaviourTemplate;
import behaviour.BehaviouralObject;
import entities.Entity;
import io.input.mouse.MouseInputContext;
import org.joml.Matrix4f;

/**
 * A behaviour template for providing first person look functionality.
 */
public class FirstPersonPivotLook extends BehaviourTemplate<BehaviouralObject> {

	/**
	 * A rotation matrix storing the current rotation of the vertical axis.
	 */
	private final Matrix4f verticalAxisRotationMatrix;

	/**
	 * The current rotation of the vertical axis in radians.
	 */
	private float verticalRotation = 0;

	/**
	 * Creates a first person look template with the provided owner to act as an x axis and child of the owner to act as the y axis.
	 *
	 * @param owner             The entity to rotate side to side with left and right movements of a mouse.
	 * @param mouseContext      The mouse context to use to subscribe to mouse move events.
	 * @param verticalAxisChild The entity to rotate up and down with vertical mouse movement.
	 */
	public FirstPersonPivotLook(Entity owner, MouseInputContext mouseContext, Entity verticalAxisChild) {
		super();

		// Aligns the vertical axis with the scene
		this.verticalAxisRotationMatrix = new Matrix4f();
		verticalAxisChild.getRelativeTransform().set(verticalAxisRotationMatrix);

		this.subscribe(mouseContext, ((mousePos, delta) -> {
			owner.getRelativeTransform().rotateY((float) delta.x / 100f);

			this.verticalRotation += delta.y / 100f;

			// Clamps pitch at looking straight up and down.
			this.verticalRotation = Float.min((float) (Math.PI / 2), this.verticalRotation);
			this.verticalRotation = Float.max((float) (-Math.PI / 2), this.verticalRotation);
			this.verticalAxisRotationMatrix.identity().rotateX(this.verticalRotation);
			verticalAxisChild.getRelativeTransform().set(verticalAxisRotationMatrix);
		}));
	}
}
