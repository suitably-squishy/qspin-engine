package demo;

import behaviour.BehaviourTemplate;
import behaviour.BehaviouralObject;
import entities.Entity;
import io.input.binds.Bind;
import io.input.binds.BindContext;
import io.input.binds.BindPressSubscriber;
import io.input.binds.BindReleaseSubscriber;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.*;

/**
 * A behaviour template for simple WASDQE movement of an entity.
 */
public class WASDQEMovement extends BehaviourTemplate<BehaviouralObject> {

	/**
	 * The current velocity of the entity, ignoring speed.
	 */
	private final Vector3f movement;

	/**
	 * The scaled velocity of the entity, including speed.
	 */
	private final Vector3f scaledMovement;


	/**
	 * The current transformation matrix to apply to the entity.
	 */
	private final Matrix4f movementMatrix;


	/**
	 * The speed at which the movement occurs.
	 */
	private float speed;


	/**
	 * Creates a movement template for the provided entity using binds in the provided bind context.
	 *
	 * @param owner       The owner to move with the WASDQE keys.
	 * @param bindContext The bind context to listen for key presses on.
	 * @param speed       The speed at which to move the owner.
	 */
	public WASDQEMovement(Entity owner, BindContext bindContext, float speed) {
		super();
		this.movement = new Vector3f();
		this.scaledMovement = new Vector3f();
		this.movementMatrix = new Matrix4f();
		this.speed = speed;
		this.setupBinds(bindContext);

		// Sets the update function of this behaviour template.
		this.updateFunction = (delta, updateOwner) -> {
			// Scales the movement and stores the result in scaledMovement.
			movement.mul(delta * speed, scaledMovement);

			// Resets the movement matrix, then translates by the scaled movement.
			this.movementMatrix.identity().translate(scaledMovement);

			// Applies the movement to the owner.
			owner.getRelativeTransform().mul(movementMatrix);
		};
	}

	/**
	 * Sets up the binds to WASDQE in order to change the movement.
	 *
	 * @param bindContext The bind context to apply the newly made binds to.
	 */
	private void setupBinds(BindContext bindContext) {
		// Forward
		Bind forward = new Bind("forward");

		this.subscribe(forward,
				(BindPressSubscriber) (bindName, button, scancode, mods) -> {
					movement.z = 1;
					return true;
				});
		this.subscribe(forward,
				(BindReleaseSubscriber) (bindName, button, scancode, mods) -> {
					movement.z = 0;
					return true;
				});

		bindContext.bind(GLFW_KEY_W, forward);

		// Left
		Bind left = new Bind("left");

		this.subscribe(left,
				(BindPressSubscriber) (bindName, button, scancode, mods) -> {
					movement.x = -1;
					return true;
				});
		this.subscribe(left,
				(BindReleaseSubscriber) (bindName, button, scancode, mods) -> {
					movement.x = 0;
					return true;
				});

		bindContext.bind(GLFW_KEY_A, left);

		// Back
		Bind back = new Bind("back");

		this.subscribe(back,
				(BindPressSubscriber) (bindName, button, scancode, mods) -> {
					movement.z = -1;
					return true;
				});
		this.subscribe(back,
				(BindReleaseSubscriber) (bindName, button, scancode, mods) -> {
					movement.z = 0;
					return true;
				});

		bindContext.bind(GLFW_KEY_S, back);

		// Right
		Bind right = new Bind("right");

		this.subscribe(right,
				(BindPressSubscriber) (bindName, button, scancode, mods) -> {
					movement.x = 1;
					return true;
				});
		this.subscribe(right,
				(BindReleaseSubscriber) (bindName, button, scancode, mods) -> {
					movement.x = 0;
					return true;
				});

		bindContext.bind(GLFW_KEY_D, right);

		// Down
		Bind down = new Bind("down");

		this.subscribe(down,
				(BindPressSubscriber) (bindName, button, scancode, mods) -> {
					movement.y = -1;
					return true;
				});
		this.subscribe(down,
				(BindReleaseSubscriber) (bindName, button, scancode, mods) -> {
					movement.y = 0;
					return true;
				});

		bindContext.bind(GLFW_KEY_Q, down);

		// Up
		Bind up = new Bind("up");

		this.subscribe(up,
				(BindPressSubscriber) (bindName, button, scancode, mods) -> {
					movement.y = 1;
					return true;
				});
		this.subscribe(up,
				(BindReleaseSubscriber) (bindName, button, scancode, mods) -> {
					movement.y = 0;
					return true;
				});

		bindContext.bind(GLFW_KEY_E, up);
	}
}
