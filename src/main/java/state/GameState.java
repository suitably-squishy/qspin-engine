package state;

import behaviour.BehaviouralObject;
import entities.Entity;
import entities.EntityRegistry;
import entities.selectors.Selector;
import render.camera.Camera;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Logger;

/**
 * A class representing the current state in the game,
 * Contains the tree of entities
 */
public class GameState extends BehaviouralObject {

	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/** The top level entity, all other entities are children of this. */
	private Entity rootEntity;
	private Camera currentCamera;
	private Set<BehaviouralObject> behaviouralObjects;
	private boolean disposed = false;

	private final EntityRegistry registry;

	/**
	 * Creates a new, empty, gamestate.
	 */
	public GameState() {
		behaviouralObjects = new HashSet<>();
		registry = new EntityRegistry();
	}

	/**
	 * Returns the root entity which contains all other entities
	 */
	public Entity getRootEntity() {
		return rootEntity;
	}

	/**
	 * Sets the state's root entity
	 *
	 * @param rootEntity The entity that holds all other entities and determines the coordinates
	 */
	public void setRootEntity(Entity rootEntity) {
		if (this.rootEntity != null) {
			behaviouralObjects.remove(this.rootEntity);
		}
		this.rootEntity = rootEntity;
		this.rootEntity.setRegistry(registry);
		behaviouralObjects.add(rootEntity);
	}

	/**
	 * Is run every render loop
	 *
	 * @param delta time passed between each loop
	 */
	@Override
	public void update(float delta) {
		if (this.disposed) {
			LOGGER.warning("Attempted to update a disposed game state, ignoring.");
			return;
		}
		super.update(delta);
		for (BehaviouralObject object : behaviouralObjects) {
			object.update(delta);
		}
	}

	/**
	 * Adds the provided behavioural object to the state's update loop.
	 *
	 * @param object The object to add.
	 */
	public void addBehaviouralObject(BehaviouralObject object) {
		behaviouralObjects.add(object);
	}

	/**
	 * Adds the provided behavioural objects to the state's update loop.
	 *
	 * @param objects The objects to add.
	 */
	public void addBehaviouralObjects(BehaviouralObject... objects) {
		behaviouralObjects.addAll(Arrays.asList(objects));
	}

	/**
	 * Removes the provided behavioural object from the state's update loop.
	 *
	 * @param object The object to remove.
	 */
	public void removeBehaviouralObject(BehaviouralObject object) {
		behaviouralObjects.remove(object);
	}

	/**
	 * Removes the provided behavioural objects from the state's update loop.
	 *
	 * @param objects The objects to remove.
	 */
	public void removeBehaviouralObjects(BehaviouralObject... objects) {
		behaviouralObjects.removeAll(Arrays.asList(objects));
	}

	public void setCurrentCamera(Camera currentCamera) {
		this.currentCamera = currentCamera;
	}

	public Camera getCurrentCamera() {
		return currentCamera;
	}

	public Selector getSelector() {
		if (this.getRootEntity() == null) {
			return null;
		}
		return this.getRootEntity().getSelector();
	}

	public Collection<Entity> getEntities() {
		if (this.getRootEntity() == null) return new HashSet<>();

		return this.getRootEntity().getDescendants();
	}

	public Collection<Entity> getEntities(Predicate<Entity> predicate) {
		if (this.getRootEntity() == null) return new HashSet<>();

		return this.getRootEntity().getDescendants(predicate);
	}

	/**
	 * Disposes the game state, free any resources held by any entities in the tree. Makes the game state unusable.
	 */
	public void dispose() {
		rootEntity.dispose(true);
		this.disposed = true;
	}

	/**
	 * Whether this game state has been disposed.
	 *
	 * @return True if the game state has been disposed, false otherwise.
	 */
	public boolean isDisposed() {
		return disposed;
	}
}
