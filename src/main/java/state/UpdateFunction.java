package state;

/**
 * An interface of a function that is run every update cycle
 */
@FunctionalInterface
public interface UpdateFunction<T> {

    /**
     * Run this every update loop.
     *
     * @param delta The time passed since the last update in seconds.
     * @param owner The owner of the update function.
     */
    void update(float delta, T owner);
}
