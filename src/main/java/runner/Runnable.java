package runner;

/**
 * An interface that can be run in the same style as the runner
 */
public interface Runnable {
	/**
	 * Prepare the code for running; Constructors should be run in the constructor method,
	 * this is instead for setup of elements such as creating the size of a render buffer,
	 * setting up openGl buffers, memory management, ect.
	 */
	void startup();

	/**
	 * The main loop of the game.
	 */
	void run();

	/**
	 * Cleanup and destroy objects before shutdown after a stop has been requested
	 */
	void shutdown();
}
