package runner;

import behaviour.BehaviouralObject;
import entities.iterators.PostOrderIterator;
import gui.GUI;
import io.input.binds.BindManager;
import io.input.mouse.MouseInputManager;
import io.resources.ResourceAtlas;
import io.resources.ResourceManager;
import io.resources.loaders.OggVorbisLoader;
import io.resources.loaders.VoxelModelLoader;
import org.liquidengine.legui.system.context.CallbackKeeper;
import org.liquidengine.legui.system.context.DefaultCallbackKeeper;
import org.liquidengine.legui.system.handler.processor.SystemEventProcessor;
import org.liquidengine.legui.system.handler.processor.SystemEventProcessorImpl;
import render.*;
import render.model.VoxelModel;
import settings.SettingsManager;
import settings.WindowSettings;
import sound.SoundBuffer;
import sound.SoundManager;
import state.GameState;
import utils.RGBColour;
import window.Window;

import java.util.logging.Logger;

import static org.lwjgl.glfw.GLFW.*;

/**
 * Handles the running of the engine and holds all state.
 */
public class Runner {

	/** The current game state loaded. */
	private GameState state;

	/** The current window being used by the engine. */
	private Window window;

	/** The resource manager to use to fetch all required resources. */
	private ResourceManager resourceManager;

	/** The settings manager to use to manage all settings. */
	private SettingsManager settingsManager;

	/** The current voxel renderer. */
	private VoxelRenderer voxelRenderer;

	/** The renderer for the end of the render pipeline */
	private ScreenRenderer screenRenderer;

	/** The shader to blend two textures */
	private Blender blender;

	/** The shader to add bloom */
	private BloomRenderer bloomRenderer;

	/** The renderer data to add all voxel information to. */
	private RendererData rendererData;

	private SoundManager soundManager;

	/** The handle to the GUI. */
	private final GUI gui;

	// TODO Replace with our own version w/ support for event absorption
	private final CallbackKeeper callbackKeeper;
	private final SystemEventProcessor systemEventProcessor;

	/** The bind manager responsible for handling any key or button binds. */
	private final BindManager bindManager;

	/** The manager responsible for handling any mouse movement or scroll events. */
	private final MouseInputManager mouseInputManager;

	/** Whether or not the render data has changed and therefore whether to rebuild the render tree. */
	private boolean rendererDataChanged = false;

	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/**
	 * Creates a new engine instance with the provided name.
	 *
	 * @param programName The name of the program to display on the taskbar.
	 */
	public Runner(String programName) {
		settingsManager = new SettingsManager();
		Window.initialiseGLFW();
		Window.createCapabilityTestWindow();
		Window.setupDebugging();
		WindowSettings windowSettings = new WindowSettings(Window.getDefaultVideoMode(), Window.getVideoModes());
		window = new Window(windowSettings, programName);

		rendererData = new RendererData();

		voxelRenderer = new VoxelRenderer(window);
		screenRenderer = new ScreenRenderer();
		blender = new Blender(window);
		bloomRenderer = new BloomRenderer(window);

		resourceManager = new ResourceManager();
		resourceManager.putAtlas(VoxelModel.class, new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel));

		ResourceAtlas<SoundBuffer> soundAtlas = new ResourceAtlas<>(new OggVorbisLoader(), null);
		resourceManager.putAtlas(SoundBuffer.class, soundAtlas);
		soundManager = new SoundManager();
		soundManager.init();

		systemEventProcessor = new SystemEventProcessorImpl();

		callbackKeeper = new DefaultCallbackKeeper();
		mouseInputManager = new MouseInputManager(window);
		bindManager = new BindManager(window);
		registerCallbacks();

		gui = new GUI(window, systemEventProcessor);
	}

	/**
	 * Starts the runner, setting up rendering.
	 */
	public void startup() {
		LOGGER.info("Runner Starting!");
		voxelRenderer.setData(rendererData);
		rendererData.fillFromEntities(new PostOrderIterator(state.getRootEntity()));
		voxelRenderer.startup();
		screenRenderer.startup();
		bloomRenderer.startup();
		blender.startup();
		if (state == null) {
			LOGGER.severe("Error, no state!");
			System.exit(-1);
			return;
		}
		if (window == null) {
			LOGGER.severe("Error, no Window!");
			System.exit(-1);
			return;
		}
	}

	/**
	 * Begins the main loop of the runner.
	 */
	public void run() {
		gui.updateWindowSize();
		double frameCap = 1.0 / 144.0;
		int frames = 0;
		double frameTime = 0;

		// Gets the starting time.
		double time = glfwGetTime();
		double unprocessed = 0;

		// Keeps looping until the window has to close.
		while (!window.shouldClose()) {

			boolean canRender = false;

			// Gets the time when the iteration of the loop happens.
			double time_2 = glfwGetTime();

			// Gets the delta between the previous loop.
			double timePassed = time_2 - time;

			// Adds the time that has passed to the time it has been since the last frame.
			unprocessed += timePassed;

			// Adds to the counter used for counting how many frames have been rendered in a
			// second.
			frameTime += timePassed;

			// Sets the time before the next render as the time of the last loop.
			time = time_2;

			// Performs updates whilst the amount of unprocessed time is greater than how
			// many times a second an update should be called.
			while (unprocessed >= frameCap) {
				// Sets the flag to render after all updates have been perfomed.
				canRender = true;
				// Removes the unprocessed time that has now been processed.
				unprocessed -= frameCap;
				// Performs one update.
				update((float) frameCap);

				if (frameTime >= 1.0) {
					// System.out.println("FPS: " + frames);
					frameTime = 0;
					frames = 0;
				}
			}

			// Performs a render if the update has requested it.
			if (canRender) {
				render(voxelRenderer, screenRenderer);
				frames++;
			}
			glfwPollEvents();
			gui.handleEvents();
		}
	}

	/**
	 * Shuts down the runner, closing the program.
	 */
	public void shutdown() {
		// TODO Add simple way of hooking to prevent explicit "dispose" calls
		// Gets rid of the window
		voxelRenderer.shutdown();
		screenRenderer.shutdown();
		blender.shutdown();
		bloomRenderer.shutdown();
		gui.destroy();
		glfwDestroyWindow(window.getWindowId());
		state.dispose();

		resourceManager.dispose();
		soundManager.dispose();

		// Exits glfw.
		glfwTerminate();

		System.exit(0);
	}

	/**
	 * Renders the current game state to the screen.
	 *
	 * @param voxelRenderer  The renderer to be used.
	 * @param screenRenderer The screen renderer to render to the screen with.
	 */
	public void render(VoxelRenderer voxelRenderer, ScreenRenderer screenRenderer) {
		if (state.isDisposed()) {
			LOGGER.warning("Attempting to render a disposed game state, this might get messy.");
		}
		if (rendererDataChanged || (state.getRootEntity() != null && state.getRootEntity().requiresRenderUpdate())) {
			if (state.getRootEntity() != null) {
				state.getRootEntity().clearRenderUpdate();
			}
			rendererData.elements.clear();
			//build the entity tree
			rendererData.fillFromEntities(new PostOrderIterator(state.getRootEntity()));
			//remake the buffers to pass to openGl
			voxelRenderer.remakeGLBuffer();
		}

		rendererData.currentCamera = state.getCurrentCamera();

		//runs the renderer

		FrameBufferObject voxels = voxelRenderer.renderScene();

		FrameBufferObject bloom = bloomRenderer.bloom(voxels, "BloomBuffer");

		screenRenderer.run(blender.blend(voxels, bloom));
		gui.render();

		//swaps the frame buffers of the window
		window.swapBuffers();
	}

	/**
	 * Registers the callback keeper as the owner of all callbacks.
	 */
	private void registerCallbacks() {
		CallbackKeeper.registerCallbacks(window.getWindowId(), callbackKeeper);

		SystemEventProcessor.addDefaultCallbacks(callbackKeeper, systemEventProcessor);

		callbackKeeper.getChainKeyCallback().add(bindManager::keyCallback);
		callbackKeeper.getChainMouseButtonCallback().add(bindManager::mouseButtonCallback);
		callbackKeeper.getChainCursorPosCallback().add(mouseInputManager::mousePosCallback);
		callbackKeeper.getChainScrollCallback().add(mouseInputManager::scrollCallback);
	}

	public void addBehavioursToUpdateLoop(BehaviouralObject object) {
		this.state.addBehaviouralObject(object);
	}

	public void removeBehavioursFromUpdateLoop(BehaviouralObject object) {
		this.state.removeBehaviouralObject(object);
	}

	public void update(float delta) {
		state.update(delta);
	}

	public GUI getGUI() {
		return gui;
	}

	public GameState getState() {
		return state;
	}

	public void setState(GameState state) {
		this.state = state;
	}

	public Window getWindow() {
		return window;
	}

	public void setWindow(Window window) {
		this.window = window;
	}

	public ResourceManager getResourceManager() {
		return resourceManager;
	}

	public BindManager getBindManager() {
		return bindManager;
	}

	public MouseInputManager getMouseInputManager() {
		return mouseInputManager;
	}

	public void setAmbient(RGBColour ambient) {
		rendererData.lightManager.setAmbientColour(ambient);
	}

	public SettingsManager getSettingsManager() {
		return settingsManager;
	}

	public void setSettingsManager(SettingsManager settingsManager) {
		this.settingsManager = settingsManager;
	}
}
