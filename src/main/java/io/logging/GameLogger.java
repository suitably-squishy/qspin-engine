package io.logging;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class GameLogger {
	private static FileHandler txtHandler;
	private static SimpleFormatter txtFormatter;
	
	public static void setup() throws IOException {
		System.setProperty("java.util.logging.SimpleFormatter.format",
				"%1$tF %1$tT [%4$s] %2$s %5$s%6$s%n");
		
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
		
		logger.setLevel(Level.INFO);
		
		File currentPath;
		try {
			// TODO log differently depending on in jar or not
			currentPath = new File(GameLogger.class.getProtectionDomain().getCodeSource().getLocation().toURI());
			String logFilePath = new File(currentPath.getParentFile(), "log.txt").getAbsolutePath();
			System.out.println("Log file at " + logFilePath);
			txtHandler = new FileHandler(logFilePath);
		} catch (SecurityException | URISyntaxException e) {
			e.printStackTrace();
		}
		
		txtFormatter = new SimpleFormatter();
		
		txtHandler.setFormatter(txtFormatter);
		logger.addHandler(txtHandler);
	}
	
	public static void closeLogs() {
		txtHandler.close();
	}
}
