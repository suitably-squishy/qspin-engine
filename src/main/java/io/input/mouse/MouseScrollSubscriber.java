package io.input.mouse;

/**
 * An interface for any objects wanting to be notified of mouse scroll operations.
 */
public interface MouseScrollSubscriber {

	/**
	 * The method called when a mouse scroll is detected.
	 *
	 * @param xoffset The amount of scroll in the x axis.
	 * @param yoffset The amount of scroll in the y axis.
	 * @return True if the scroll has been absorbed and should stop permeating, false otherwise.
	 */
	boolean onScroll(double xoffset, double yoffset);
}
