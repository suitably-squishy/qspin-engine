package io.input.mouse;

import org.joml.Vector2d;
import window.Window;

import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.glfw.GLFW.*;


/**
 * A class for holding mouse input (movement and scrolling) contexts and notifying the current context of any events.
 */
public class MouseInputManager {

	/**
	 * The current context to notify of any mouse events.
	 */
	private MouseInputContext currentContext;

	/**
	 * A mapping of context names to available contexts.
	 */
	private final Map<String, MouseInputContext> availableContexts;


	/**
	 * The last known position of the cursor.
	 */
	private Vector2d lastPos = null;

	/**
	 * The change in x,y position since the last update.
	 */
	private final Vector2d mouseDelta;

	/**
	 * The current position of the cursor.
	 */
	private Vector2d currentPos = null;

	/**
	 * The window handle currently in use.
	 */
	private long window;

	// Arrays for requesting mouse position from glfw.
	private final double[] x = new double[1];
	private final double[] y = new double[1];

	/**
	 * Creates a new manager which listens for mouse events (movement and scrolling) in the provided window.
	 *
	 * @param window The window to listen for mouse events in.
	 */
	public MouseInputManager(long window) {
		this.availableContexts = new HashMap<>();
		this.currentContext = null;
		this.mouseDelta = new Vector2d();
		this.window = window;
		glfwSetCursorPosCallback(window, this::mousePosCallback);
		glfwSetScrollCallback(window, this::scrollCallback);
	}

	/**
	 * Creates a new manager which listens for mouse events (movement and scrolling) in the provided window.
	 *
	 * @param window The window object to listen for mouse events in.
	 */
	public MouseInputManager(Window window) {
		this(window.getWindowId());
	}

	/**
	 * The function triggered when the mouse position changes.
	 *
	 * @param win  The window in which the movement happened.
	 * @param xpos The new x position of the mouse.
	 * @param ypos The new y position of the mouse.
	 */
	public void mousePosCallback(long win, double xpos, double ypos) {
		if (currentPos == null) {
			currentPos = new Vector2d(xpos, ypos);
		} else {
			currentPos.x = xpos;
			currentPos.y = ypos;
		}

		if (lastPos != null) {
			currentPos.sub(lastPos, mouseDelta);
		}

		if (currentContext != null) {
			currentContext.mousePos(win, currentPos, mouseDelta);
		}

		if (lastPos == null) {
			lastPos = new Vector2d(xpos, ypos);
		} else {
			lastPos.x = xpos;
			lastPos.y = ypos;
		}
	}

	/**
	 * The function triggered when the scroll wheel is moved.
	 *
	 * @param win     The window in which the event happened.
	 * @param xoffset The amount of change in the x axis.
	 * @param yoffset The amount of change in the y axis.
	 */
	public void scrollCallback(long win, double xoffset, double yoffset) {
		if (currentContext == null) return;

		currentContext.mouseScroll(win, xoffset, yoffset);
	}

	/**
	 * Sets the current context to the context with the provided name, if it is available.
	 *
	 * @param contextName The name of the context to set as current.
	 * @return False if the context is not available, true otherwise.
	 */
	public boolean setCurrentContext(String contextName) {
		MouseInputContext context = availableContexts.get(contextName);
		if (context == null) return false;

		this.currentContext = context;
		return true;
	}

	/**
	 * Gets the context associated with the provided name, if it is available.
	 *
	 * @param contextName The name of the context to fetch.
	 * @return Null if the context is not available, otherwise the context.s
	 */
	public MouseInputContext getContext(String contextName) {
		return availableContexts.get(contextName);
	}

	/**
	 * Gets the context currently stored in the manager as the current context.
	 *
	 * @return The context which is currently in use by the manager.
	 */
	public MouseInputContext getCurrentContext() {
		return currentContext;
	}

	/**
	 * Deletes the context with the provided name, removing it
	 *
	 * @param contextName The name of the context to delete. Sets the current context to null if the context deleted is current.
	 * @return False if the context does not exist, true otherwise.
	 */
	public boolean deleteContext(String contextName) {
		MouseInputContext context = availableContexts.remove(contextName);
		if (context == null) return false;

		if (currentContext == context) {
			currentContext = null;
		}
		return true;
	}

	/**
	 * Adds the provided context to the manager, storing it under it's name.
	 *
	 * @param context The context to add to the manager.
	 * @return The context added.
	 */
	public MouseInputContext addContext(MouseInputContext context) {
		availableContexts.put(context.getName(), context);
		return context;
	}

	/**
	 * Adds the provided context to the manager and sets it as the current context.
	 *
	 * @param context The context to add to the manager and make current.
	 * @return The context added.
	 */
	public MouseInputContext addContextAndMakeCurrent(MouseInputContext context) {
		this.addContext(context);
		this.setCurrentContext(context.getName());
		return context;
	}

	/**
	 * Gets the mouse position relative to the upper left corner in screen coordinates.
	 *
	 * @return A vector 2d representing the position of the cursor on the screen.
	 */
	public Vector2d getMousePosition() {
		glfwGetCursorPos(window, x, y);
		return new Vector2d(x[0], y[0]);
	}
}
