package io.input.mouse;

import org.joml.Vector2d;

import java.util.HashSet;
import java.util.Set;

/**
 * A class to hold subscribers of mouse events such as movement and scrolling.
 */
public class MouseInputContext {

	/**
	 * The user defined name of the context.
	 */
	private final String name;

	/**
	 * The set of subscribers for mouse move events.
	 */
	private final Set<MouseMoveSubscriber> moveSubscribers;

	/**
	 * The set of subscribers for mouse scroll events.
	 */
	private final Set<MouseScrollSubscriber> scrollSubscribers;

	/**
	 * Creates an empty mouse input context with the provided name.
	 *
	 * @param name The name to assign to the context.
	 */
	public MouseInputContext(String name) {
		this.name = name;
		this.moveSubscribers = new HashSet<>();
		this.scrollSubscribers = new HashSet<>();
	}

	/**
	 * Gets the user defined name of the context.
	 *
	 * @return The user defined name of the context.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Triggers the input context with the provided movement of the mouse.
	 *
	 * @param win      The window the mouse was moved inside.
	 * @param mousePos The x, y position of the mouse.
	 * @param delta    The change in x, y position since the last mousePos call.
	 */
	public void mousePos(long win, Vector2d mousePos, Vector2d delta) {
		for (MouseMoveSubscriber subscriber : moveSubscribers) {
			subscriber.onMove(mousePos, delta);
		}
	}

	/**
	 * Triggers the input context with the provided scroll event.
	 *
	 * @param win     The window the scroll occurred.
	 * @param xoffset The amount of scroll in the x axis.
	 * @param yoffset The amount of scroll in the y axis.
	 */
	public void mouseScroll(long win, double xoffset, double yoffset) {
		for (MouseScrollSubscriber subscriber : scrollSubscribers) {
			if (subscriber.onScroll(xoffset, yoffset)) break;
		}
	}

	/**
	 * Subscribes the provided object to be notified of any mouse move events.
	 *
	 * @param subscriber The subscriber to notify of any move events.
	 */
	public void subscribeToMove(MouseMoveSubscriber subscriber) {
		moveSubscribers.add(subscriber);
	}

	/**
	 * Subscribes the provided object to be notified of any mouse scroll events.
	 *
	 * @param subscriber The subscriber to notify of any scroll events.
	 */
	public void subscribeToScroll(MouseScrollSubscriber subscriber) {
		scrollSubscribers.add(subscriber);
	}

	/**
	 * Subscribes the provided object to be notified of any mouse move and scroll events.
	 *
	 * @param subscriber The subscriber to notify of any scroll or move events.
	 * @param <T>        A class that implements both MouseMoveSubscriber and MouseScrollSubscriber.
	 */
	public <T extends MouseMoveSubscriber & MouseScrollSubscriber> void subscribeToMoveAndScroll(T subscriber) {
		subscribeToMove(subscriber);
		subscribeToScroll(subscriber);
	}

	/**
	 * Unsubscribes the provided object from being notified of any mouse move events.
	 *
	 * @param subscriber The subscriber to unsubscribe.
	 */
	public void unsubscribeFromMove(MouseMoveSubscriber subscriber) {
		moveSubscribers.remove(subscriber);
	}

	/**
	 * Unsubscribes the provided object from being notified of any mouse scroll events.
	 *
	 * @param subscriber The subscriber to unsubscribe.
	 */
	public void unsubscribeFromScroll(MouseScrollSubscriber subscriber) {
		scrollSubscribers.remove(subscriber);
	}

	public boolean isSubscribed(MouseMoveSubscriber subscriber) {
		return moveSubscribers.contains(subscriber);
	}

	public boolean isSubscribed(MouseScrollSubscriber subscriber) {
		return scrollSubscribers.contains(subscriber);
	}
}
