package io.input.mouse;

import org.joml.Vector2d;

/**
 * An interface for any objects wanting to be notified of mouse movement.
 */
public interface MouseMoveSubscriber {

	/**
	 * The method called when the mouse is moved in the window.
	 *
	 * @param mousePos The x and y position of the mouse.
	 * @param delta    The x and y change since the last mouse move.
	 */
	void onMove(Vector2d mousePos, Vector2d delta);
}
