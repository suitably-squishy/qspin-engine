package io.input.binds;

import window.Window;

import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;

/**
 * A class for managing key binding contexts and the binds they contain.
 */
public class BindManager {

	/**
	 * The current bind context, ie the one which is notified when an event occurs.
	 */
	private BindContext currentContext;

	/**
	 * The currently available contexts stored under their names.
	 */
	private final Map<String, BindContext> availableContexts;

	/**
	 * The GLFW window id to look for events in.
	 */
	private long window;

	/**
	 * Creates an empty bind manager which checks for events in the provided window.
	 *
	 * @param window The GLFW window id to look for events in.
	 */
	public BindManager(long window) {
		this.availableContexts = new HashMap<>();
		this.currentContext = null;
		this.window = window;
		glfwSetKeyCallback(window, this::keyCallback);
		glfwSetMouseButtonCallback(window, this::mouseButtonCallback);
	}

	/**
	 * Creates an empty bind manager which checks for events in the provided window.
	 *
	 * @param window The window object to look for events in.
	 */
	public BindManager(Window window) {
		this(window.getWindowId());
	}

	/**
	 * Triggers the bind manager and associated current context with the provided key input.
	 *
	 * @param win      The GLFW window id the event occurred in.
	 * @param key      The GLFW key code the event refers to.
	 * @param scancode The platform specific scancode of the key.
	 * @param action   One of {@link org.lwjgl.glfw.GLFW#GLFW_PRESS}, {@link org.lwjgl.glfw.GLFW#GLFW_REPEAT} or {@link org.lwjgl.glfw.GLFW#GLFW_RELEASE}
	 * @param mods     The bitmask of any modifier keys held down on the event.
	 */
	public void keyCallback(long win, int key, int scancode, int action, int mods) {
		if (currentContext == null) return;

		currentContext.keyInput(win, key, scancode, action, mods);
	}

	/**
	 * Triggers the bind manager and associated current context with the provided mouse button input.
	 *
	 * @param win    The GLFW window id the event occurred in.
	 * @param button The GLFW mouse button code the event refers to.
	 * @param action One of {@link org.lwjgl.glfw.GLFW#GLFW_PRESS} or {@link org.lwjgl.glfw.GLFW#GLFW_RELEASE}
	 * @param mods   The bitmask of any modifier keys held down on the event.
	 */
	public void mouseButtonCallback(long win, int button, int action, int mods) {
		if (currentContext == null) return;

		currentContext.mouseInput(win, button, action, mods);
	}

	/**
	 * Sets the current context to the one with the provided name.
	 *
	 * @param contextName The name of the context to change to.
	 * @return False if the context does not exist, true otherwise.
	 */
	public boolean setCurrentContext(String contextName) {
		BindContext context = availableContexts.get(contextName);
		if (context == null) return false;

		this.currentContext = context;
		return true;
	}

	public BindContext getCurrentContext() {
		return currentContext;
	}


	/**
	 * Gets the context with the provided name.
	 *
	 * @param contextName The name of the context to fetch.
	 * @return The context with the provided name, null if not present.
	 */
	public BindContext getContext(String contextName) {
		return availableContexts.get(contextName);
	}

	/**
	 * Deletes the context from the manager with the provided name.
	 *
	 * @param contextName The name of the context to delete.
	 * @return False if the context does not exist, true otherwise.
	 */
	public boolean deleteContext(String contextName) {
		BindContext context = availableContexts.remove(contextName);
		if (context == null) return false;

		if (currentContext == context) {
			currentContext = null;
		}
		return true;
	}

	/**
	 * Adds the provided context to the manager. If the provided context has the same name as another context it will overwrite it.
	 *
	 * @param context The context to add to the manager.
	 * @return The context added.
	 */
	public BindContext addContext(BindContext context) {
		if (currentContext != null && currentContext.getName().equals(context.getName())) {
			currentContext = context;
		}
		availableContexts.put(context.getName(), context);
		return context;
	}

	/**
	 * Adds the provided context to the manager and sets it to the current context. If the provided context has the same name as another context it will overwrite it.
	 *
	 * @param context The context to add to the manager.
	 * @return The context added.
	 */
	public BindContext addContextAndMakeCurrent(BindContext context) {
		this.addContext(context);
		this.setCurrentContext(context.getName());
		return context;
	}

	/**
	 * Creates a new context and adds it with the provided name to the manager.
	 *
	 * @param contextName The name of the context to add.
	 * @return The context created.
	 */
	public BindContext createContext(String contextName) {
		BindContext context = new BindContext(contextName);
		return this.addContext(context);
	}

	/**
	 * Creates a new context and adds it with the provided name to the manager, then sets it to be current.
	 *
	 * @param contextName The name of the context to add.
	 * @return The context created.
	 */
	public BindContext createContextAndMakeCurrent(String contextName) {
		BindContext context = new BindContext(contextName);
		return this.addContextAndMakeCurrent(context);
	}

	/**
	 * Adds the provided bind to the current context under the given button.
	 *
	 * @param button The button which will trigger the bind.
	 * @param bind   The bind object to store.
	 * @return Null if the button ID is invalid, otherwise the bind provided.
	 * @see BindContext#bind(int, Bind)
	 */
	public Bind bindToCurrentContext(int button, Bind bind) {
		if (currentContext == null) return null;

		return currentContext.bind(button, bind);
	}

	/**
	 * Binds the provided button to a new bind object created with the provided name to the current context.
	 *
	 * @param button   The GLFW button id to bind to.
	 * @param bindName The name of the new bind.
	 * @return Null if the button ID is invalid, otherwise the bind created.
	 * @see BindContext#bind(int, String)
	 */
	public Bind bindToCurrentContext(int button, String bindName) {
		if (currentContext == null) return null;

		return currentContext.bind(button, bindName);
	}

	/**
	 * Binds the provided button to a new bind object created with the provided name to the current context.
	 *
	 * @param button             The GLFW button id to bind to.
	 * @param bindName           The name of the new bind.
	 * @param allowMultipleBinds Whether or not the bind object will allow more than one subscriber at one time.
	 * @return Null if the button ID is invalid, otherwise the bind created.
	 * @see BindContext#bind(int, String, boolean)
	 */
	public Bind bindToCurrentContext(int button, String bindName, boolean allowMultipleBinds) {
		if (currentContext == null) return null;

		return currentContext.bind(button, bindName, allowMultipleBinds);
	}

	/**
	 * Binds the provided button to the bind object in the context with the name provided.
	 *
	 * @param button      The GLFW button id to bind to.
	 * @param bind        The bind object to store.
	 * @param contextName The name of the context to bind this inside.
	 * @return Null if the context doesn't exist or the button ID is invalid, otherwise the bind object.
	 * @see BindContext#bind(int, Bind)
	 */
	public Bind bindToContext(int button, Bind bind, String contextName) {
		BindContext context = availableContexts.get(contextName);
		if (context == null) return null;

		return context.bind(button, bind);
	}

	/**
	 * Binds the provided button to a new bind object with the provided name in the context with the name provided.
	 *
	 * @param button      The GLFW button id to bind to.
	 * @param bindName    The name of the new bind.
	 * @param contextName The name of the context to bind this inside.
	 * @return Null if the context doesn't exist or the button ID is invalid, otherwise the created bind object.
	 * @see BindContext#bind(int, String)
	 */
	public Bind bindToContext(int button, String bindName, String contextName) {
		Bind bind = new Bind(bindName);
		return bindToContext(button, bind, contextName);
	}

	/**
	 * Binds the provided button to a new bind object with the provided name in the context with the name provided.
	 *
	 * @param button             The GLFW button id to bind to.
	 * @param bindName           The name of the new bind.
	 * @param contextName        The name of the context to bind this inside.
	 * @param allowMultipleBinds Whether or not the created bind will allow more than one subscriber at one time.
	 * @return Null if the context doesn't exist or the button ID is invalid, otherwise the created bind object.
	 * @see BindContext#bind(int, String, boolean)
	 */
	public Bind bindToContext(int button, String bindName, String contextName, boolean allowMultipleBinds) {
		Bind bind = new Bind(bindName, allowMultipleBinds);
		return bindToContext(button, bind, contextName);
	}

	/**
	 * Polls the bind with the provided name in the current context.
	 *
	 * @param bindName The name of the bind to poll.
	 * @return True if the bind is pressed, false if there is no current context, the bind doesn't exist or is released.
	 */
	public boolean pollBind(String bindName) {
		if (currentContext == null) return false;

		return currentContext.pollBind(bindName, window);
	}

	/**
	 * Sets the window handle of the bind manager.
	 *
	 * @param window The GLFW window id of the window.
	 */
	public void setWindow(long window) {
		this.window = window;
	}
}
