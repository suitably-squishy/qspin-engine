package io.input.binds;

/**
 * An object that is able to subscribe to the press event of a Bind object.
 */
@FunctionalInterface
public interface BindPressSubscriber {

	/**
	 * The method called when a bind is triggered by a press.
	 *
	 * @param bindName The user defined name of the bind triggered.
	 * @param button   The GLFW key or mouse button code which caused the bind to be triggered.
	 * @param scancode The platform specific scancode of the key, 0 if a mouse input.
	 * @param mods     The bitmask for any modifier keys held down when the bind was triggered.
	 * @return True if the press has been absorbed and should stop being permeated.
	 */
	boolean onPress(String bindName, int button, int scancode, int mods);
}
