package io.input.binds;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import static org.lwjgl.glfw.GLFW.*;

/**
 * A class to represent a key bind with subscribers to notify once the bind is triggered.
 */
public class Bind {

	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/**
	 * The user defined name for the bind.
	 */
	private final String name;

	/**
	 * The set of subscribers to notify when the bind is pressed.
	 */
	private final Set<BindPressSubscriber> pressSubscribers;

	/**
	 * The set of subscribers to notify when the bind is repeated.
	 */
	private final Set<BindRepeatSubscriber> repeatSubscribers;

	/**
	 * The set of subscribers to notify when the bind is released.
	 */
	private final Set<BindReleaseSubscriber> releaseSubscribers;

	/**
	 * Whether or not to allow more than one subscriber to be subscribed to the bind at a time.
	 */
	private boolean allowMultipleBinds;

	/**
	 * Creates an empty bind with the provided name and settings.
	 *
	 * @param name               The user defined name of the bind.
	 * @param allowMultipleBinds Whether or not to allow more than one subscriber to be subscribed to the bind at a time.
	 */
	public Bind(String name, boolean allowMultipleBinds) {
		this.name = name;
		this.pressSubscribers = new HashSet<>();
		this.repeatSubscribers = new HashSet<>();
		this.releaseSubscribers = new HashSet<>();
		this.allowMultipleBinds = allowMultipleBinds;
	}

	/**
	 * Creates an empty bind with the provided name, defaults to allowing multiple subscribers to be subscribed.
	 *
	 * @param name The user defined name of the bind.
	 */
	public Bind(String name) {
		this(name, true);
	}

	/**
	 * Subscribes the provided subscriber to the press of the bind. This means the subscriber will be notified whenever the bind is called.
	 *
	 * @param subscriber The press subscriber to subscribe to the bind.
	 * @return The same bind object.
	 */
	public Bind subscribe(BindPressSubscriber subscriber) {
		if (subscriber == null) return this;

		if (!allowMultipleBinds && !this.pressSubscribers.isEmpty()) {
			this.pressSubscribers.clear();
		}

		this.pressSubscribers.add(subscriber);
		return this;
	}

	/**
	 * Subscribes the provided subscriber to the repeat of the bind. This means the subscriber will be notified whenever the bind is called.
	 *
	 * @param subscriber The repeat subscriber to subscribe to the bind.
	 * @return The same bind object.
	 */
	public Bind subscribe(BindRepeatSubscriber subscriber) {
		if (subscriber == null) return this;

		if (!allowMultipleBinds && !this.repeatSubscribers.isEmpty()) {
			this.repeatSubscribers.clear();
		}

		this.repeatSubscribers.add(subscriber);
		return this;
	}

	/**
	 * Subscribes the provided subscriber to the release of the bind. This means the subscriber will be notified whenever the bind is called.
	 *
	 * @param subscriber The release subscriber to subscribe to the bind.
	 * @return The same bind object.
	 */
	public Bind subscribe(BindReleaseSubscriber subscriber) {
		if (subscriber == null) return this;

		if (!allowMultipleBinds && !this.releaseSubscribers.isEmpty()) {
			this.releaseSubscribers.clear();
		}

		this.releaseSubscribers.add(subscriber);
		return this;
	}

	public Bind subscribe(BindPressSubscriber pressSubscriber, BindReleaseSubscriber releaseSubscriber) {
		subscribe(pressSubscriber);
		subscribe(releaseSubscriber);
		return this;
	}

	public Bind subscribe(BindPressSubscriber pressSubscriber, BindRepeatSubscriber repeatSubscriber) {
		subscribe(pressSubscriber);
		subscribe(repeatSubscriber);
		return this;
	}

	public Bind subscribe(BindRepeatSubscriber repeatSubscriber, BindReleaseSubscriber releaseSubscriber) {
		subscribe(repeatSubscriber);
		subscribe(releaseSubscriber);
		return this;
	}

	public Bind subscribe(BindPressSubscriber pressSubscriber, BindRepeatSubscriber repeatSubscriber, BindReleaseSubscriber releaseSubscriber) {
		subscribe(pressSubscriber);
		subscribe(repeatSubscriber);
		subscribe(releaseSubscriber);
		return this;
	}

	/**
	 * Unsubscribes the provided press subscriber from the bind.
	 *
	 * @param subscriber The press subscriber to remove.
	 * @return True if the subscriber was removed, false if it was never present.
	 */
	public boolean unsubscribe(BindPressSubscriber subscriber) {
		return this.pressSubscribers.remove(subscriber);
	}

	/**
	 * Unsubscribes the provided press subscriber from the bind.
	 *
	 * @param subscriber The press subscriber to remove.
	 * @return True if the subscriber was removed, false if it was never present.
	 */
	public boolean unsubscribe(BindRepeatSubscriber subscriber) {
		return this.repeatSubscribers.remove(subscriber);
	}

	/**
	 * Unsubscribes the provided press subscriber from the bind.
	 *
	 * @param subscriber The press subscriber to remove.
	 * @return True if the subscriber was removed, false if it was never present.
	 */
	public boolean unsubscribe(BindReleaseSubscriber subscriber) {
		return this.releaseSubscribers.remove(subscriber);
	}

	public Bind clearSubscribers() {
		pressSubscribers.clear();
		repeatSubscribers.clear();
		releaseSubscribers.clear();
		return this;
	}


	/**
	 * Triggers the bind with the provided button input.
	 *
	 * @param window   The GLFW window id the event occurred in.
	 * @param key      The GLFW key or mouse button code the event refers to.
	 * @param scancode The platform specific scancode of the key, always 0 if a mouse button.
	 * @param action   One of {@link org.lwjgl.glfw.GLFW#GLFW_PRESS GLFW_PRESS}, {@link org.lwjgl.glfw.GLFW#GLFW_REPEAT GLFW_REPEAT} or {@link org.lwjgl.glfw.GLFW#GLFW_RELEASE GLFW_RELEASE}
	 * @param mods     The bitmask of any modifier keys held down on the event.
	 */
	public void input(long window, int key, int scancode, int action, int mods) {
		boolean absorbed;
		switch (action) {
			case GLFW_PRESS:
				for (BindPressSubscriber subscriber : this.pressSubscribers) {
					absorbed = subscriber.onPress(this.name, key, scancode, mods);
					if (absorbed) break;
				}
				break;
			case GLFW_REPEAT:
				for (BindRepeatSubscriber subscriber : this.repeatSubscribers) {
					absorbed = subscriber.onRepeat(this.name, key, scancode, mods);
					if (absorbed) break;
				}
				break;
			case GLFW_RELEASE:
				for (BindReleaseSubscriber subscriber : this.releaseSubscribers) {
					absorbed = subscriber.onRelease(this.name, key, scancode, mods);
					if (absorbed) break;
				}
				break;
			default:
				break;
		}
	}

	/**
	 * Gets the user defined name of the bind.
	 *
	 * @return The user defined name.
	 */
	public String getName() {
		return name;
	}
}
