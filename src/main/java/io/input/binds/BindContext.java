package io.input.binds;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.lwjgl.glfw.GLFW.*;

/**
 * A class to represent a bind context, ie a mapping of buttons to user defined binds.
 */
public class BindContext {

	/**
	 * The user defined name for the context.
	 */
	private final String name;

	/**
	 * The mapping of GLFW button (mouse or key) ids to bind names.
	 */
	private final String[] buttonsToNames;

	// TODO Convert the bind system from buttons to combinations of buttons (eg Ctrl + S), using modifier bitmasks.

	/**
	 * A map of bind names to Binds.
	 */
	private final Map<String, Bind> namesToBinds;

	private final Map<Bind, Set<Integer>> bindsToButtons;

	/**
	 * Creates an empty bind context with the provided name.
	 *
	 * @param name The user defined name for the context.
	 */
	public BindContext(String name) {
		this.name = name;
		this.buttonsToNames = new String[GLFW_KEY_LAST + 1];
		this.namesToBinds = new HashMap<>(GLFW_KEY_LAST + 1);
		this.bindsToButtons = new HashMap<>();
	}

	/**
	 * Triggers the bind context and associated binds with the provided key input.
	 *
	 * @param window   The GLFW window id the event occurred in.
	 * @param key      The GLFW key code the event refers to.
	 * @param scancode The platform specific scancode of the key.
	 * @param action   One of {@link org.lwjgl.glfw.GLFW#GLFW_PRESS}, {@link org.lwjgl.glfw.GLFW#GLFW_REPEAT} or {@link org.lwjgl.glfw.GLFW#GLFW_RELEASE}
	 * @param mods     The bitmask of any modifier keys held down on the event.
	 */
	public void keyInput(long window, int key, int scancode, int action, int mods) {
		if (key < 0) return;
		if (buttonsToNames[key] == null) return;

		Bind bindToTrigger = namesToBinds.get(buttonsToNames[key]);

		if (bindToTrigger == null) return;

		bindToTrigger.input(window, key, scancode, action, mods);
	}

	/**
	 * Triggers the bind context and associated binds with the provided mouse button input.
	 *
	 * @param window The GLFW window id the event occurred in.
	 * @param button The GLFW mouse button code the event refers to.
	 * @param action One of {@link org.lwjgl.glfw.GLFW#GLFW_PRESS} or {@link org.lwjgl.glfw.GLFW#GLFW_RELEASE}
	 * @param mods   The bitmask of any modifier keys held down on the event.
	 */
	public void mouseInput(long window, int button, int action, int mods) {
		if (buttonsToNames[button] == null) return;

		Bind bindToTrigger = namesToBinds.get(buttonsToNames[button]);

		if (bindToTrigger == null) return;

		bindToTrigger.input(window, button, 0, action, mods);
	}

	/**
	 * Binds the provided GLFW button id to the given Bind object.
	 *
	 * @param button The button to associate with the bind.
	 * @param bind   The bind object to trigger when the provided button is updated.
	 * @return Null if the button ID is invalid, otherwise the bind provided.
	 */
	public Bind bind(int button, Bind bind) {
		if (button < 0 || button > GLFW_KEY_LAST) return null;

		this.buttonsToNames[button] = bind.getName();
		this.namesToBinds.put(bind.getName(), bind);

		if (!bindsToButtons.containsKey(bind)) {
			bindsToButtons.put(bind, new HashSet<>(5));
		}
		bindsToButtons.get(bind).add(button);

		return bind;
	}

	/**
	 * Binds the provided GLFW button id to a new bind object with the given name.
	 *
	 * @param button   The button to associate with the bind.
	 * @param bindName The name of the new bind to trigger when the provided button is updated.
	 * @return Null if the button ID is invalid, otherwise the bind created.
	 */
	public Bind bind(int button, String bindName) {
		Bind bind = new Bind(bindName);
		return this.bind(button, bind);
	}

	/**
	 * Binds the provided GLFW button id to a new bind object with the given name and settings.
	 *
	 * @param button             The button to associate with the bind.
	 * @param bindName           The name of the new bind to trigger when the provided button is updated.
	 * @param allowMultipleBinds Whether or not to allow the created bind to have more than one subscriber.
	 * @return Null if the button ID is invalid, otherwise the bind is created.
	 */
	public Bind bind(int button, String bindName, boolean allowMultipleBinds) {
		Bind bind = new Bind(bindName, allowMultipleBinds);
		return this.bind(button, bind);
	}

	/**
	 * Unbinds any associated bind from the provided button.
	 *
	 * @param button The button to remove any binds from.
	 * @return False if a bind does not exist on that button, true otherwise.
	 */
	public boolean unbind(int button) {
		String bindName = buttonsToNames[button];
		if (bindName == null) return false;

		Bind bind = namesToBinds.get(bindName);
		if (bind == null) return false;

		bindsToButtons.get(bind).remove(button);

		// If the bind isn't used any more we can remove it from the stored binds.
		if (bindsToButtons.get(bind).isEmpty()) {
			namesToBinds.remove(bind.getName());
			bindsToButtons.remove(bind);
		}

		buttonsToNames[button] = null;
		return true;
	}

	/**
	 * Removes the provided bind from the context, unbinding it from any keys.
	 *
	 * @param bind The bind to remove.
	 * @return True if successful.
	 */
	public boolean unbind(Bind bind) {
		for (int button : bindsToButtons.get(bind)) {
			unbind(button);
		}
		return true;
	}

	/**
	 * Unbinds the bind associated with the provided name from the context, unbinding it from any keys.
	 *
	 * @param bindName The name of the bind to remove.
	 * @return False if a bind doesn't exist under that name, true otherwise.
	 */
	public boolean unbind(String bindName) {
		Bind bind = namesToBinds.get(bindName);

		if (bind == null) return false;

		return unbind(bind);
	}

	/**
	 * Gets the user defined name of the context.
	 *
	 * @return The name of the context.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Polls the bind associated with the provided name.
	 *
	 * @param bindName The name of the bind to poll.
	 * @param window   The window id to use to check the bind state.
	 * @return True if the bind is pressed, false if not pressed or the bind doesn't exist.
	 */
	public boolean pollBind(String bindName, long window) {
		Bind bind = namesToBinds.get(bindName);

		// TODO Add logging if non existent
		if (bind == null) return false;

		for (int button : bindsToButtons.get(bind)) {
			// If the button is a mouse button
			if (button <= GLFW_MOUSE_BUTTON_LAST) {
				if (glfwGetMouseButton(window, button) == GLFW_PRESS) {
					return true;
				}
				// Otherwise if the button is a key
			} else if (button <= GLFW_KEY_LAST) {
				if (glfwGetKey(window, button) == GLFW_PRESS) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Gets whether the provided bind is in the context.
	 *
	 * @param bind The bind to check for.
	 * @return True if the bind is present, false otherwise.
	 */
	public boolean containsBind(Bind bind) {
		return bindsToButtons.containsKey(bind);
	}

	/**
	 * Gets the bind with the given name if it is present in the context.
	 *
	 * @param bindName The name of the bind to fetch.
	 * @return Null if the bind is not present, otherwise the bind.
	 */
	public Bind getBind(String bindName) {
		return namesToBinds.get(bindName);
	}
}
