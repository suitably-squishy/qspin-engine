package io.input.binds;

/**
 * An object that is able to subscribe to the repeat event of a Bind object.
 */
@FunctionalInterface
public interface BindRepeatSubscriber {

	/**
	 * The method called when a bind is triggered by a repeating key. (Can only be caused by key presses)
	 *
	 * @param bindName The user defined name of the bind triggered.
	 * @param button   The GLFW key code which caused the bind to be triggered.
	 * @param scancode The platform specific scancode of the key.
	 * @param mods     The bitmask for any modifier keys held down when the bind was triggered.
	 * @return True if the repeat has been absorbed and should stop being permeated.
	 */
	boolean onRepeat(String bindName, int button, int scancode, int mods);
}
