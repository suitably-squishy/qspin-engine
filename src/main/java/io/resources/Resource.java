package io.resources;

/**
 * Represents an individual resource that can be loaded by the engine.
 */
public abstract class Resource {

	/** Whether or not the resource has been disposed of. */
	private boolean disposed;

	/**
	 * Disposes of the resource, destroying any non-garbage collectable resources held, such as off heap memory.
	 */
	public void dispose() {
		if (this.disposed) return;

		this.disposed = true;
	}

	/**
	 * Whether or not the resource has already been disposed of.
	 *
	 * @return True if the resource has been disposed, false otherwise.
	 */
	public boolean isDisposed() {
		return disposed;
	}
}
