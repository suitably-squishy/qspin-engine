package io.resources;

import io.resources.loaders.ResourceLoader;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * An atlas for storing loaded resources of the provided type.
 *
 * @param <T> The type of resource to be loaded and stored.
 */
public class ResourceAtlas<T extends Resource> {

	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	//TODO: Change to a hash based system?
	/**
	 * The map containing the loaded resources stored under their paths.
	 */
	private final Map<String, T> loadedResources;

	/**
	 * The resource loader responsible for loading new resources from the disk.
	 */
	private ResourceLoader<T> loader;

	/**
	 * The resource provided by default if loading fails.
	 */
	private T fallbackResource;

	/**
	 * Creates a resource atlas with the provided loader and fallback resource.
	 *
	 * @param loader           The resource loader able to load resources of the provided type.
	 * @param fallbackResource The resource provided by default if loading fails.
	 */
	public ResourceAtlas(ResourceLoader<T> loader, T fallbackResource) {
		this.loadedResources = new HashMap<>();
		this.loader = loader;
		this.fallbackResource = fallbackResource;
	}

	/**
	 * Gets the resource at the provided path and stores it for future retrieval.
	 *
	 * @param path The path to load the resource from.
	 * @return The resource if successfully loaded, otherwise the configured fallback resource.
	 */
	public T get(String path) {
		return get(path, true, false, this.fallbackResource);
	}

	/**
	 * Gets the resource at the provided path using the given settings. Returns the configured fallback resource if loading fails.
	 *
	 * @param path  The path to load the resource from.
	 * @param store Whether to store the loaded resource for future faster loading.
	 * @return The resource if successfully loaded, otherwise the configured fallback resource.
	 */
	public T get(String path, boolean store) {
		return get(path, store, false, this.fallbackResource);
	}

	/**
	 * Gets the resource at the provided path using the given settings. Returns the configured fallback resource if loading fails.
	 *
	 * @param path   The path to load the resource from.
	 * @param store  Whether to store the loaded resource for future faster loading.
	 * @param reload Whether to perform a reload of the resource, replacing any stored version.
	 * @return The resource if successfully loaded, the configured fallback resource if loading fails.
	 */
	public T get(String path, boolean store, boolean reload) {
		return get(path, store, reload, this.fallbackResource);
	}

	/**
	 * Gets the resource at the provided path using the given settings. Returns the provided fallback resource if loading fails.
	 *
	 * @param path             The path to load the resource from.
	 * @param store            Whether to store the loaded resource for future faster loading.
	 * @param reload           Whether to perform a reload of the resource, replacing any stored version.
	 * @param fallbackResource The resource to return if loading fails.
	 * @return The resource if successfully loaded, the fallback resource if loading fails.
	 */
	public T get(String path, boolean store, boolean reload, T fallbackResource) {
		T resource = getWithoutFallback(path, store, reload);
		return resource == null ? fallbackResource : resource;
	}

	/**
	 * Gets the resource at the specified path with the given options, does not return the fallback resource if loading fails.
	 *
	 * @param path   The path to load the resource from.
	 * @param store  Whether to store the loaded resource for future faster loading.
	 * @param reload Whether to perform a reload of the resource, replacing any stored version.
	 * @return The resource if successfully loaded or already stored, null if loading fails.
	 */
	public T getWithoutFallback(String path, boolean store, boolean reload) {
		if (path == null) return null;

		if (!loadedResources.containsKey(path) || reload) {
			T resource = null;
			try {
				resource = loader.loadResource(path);
			} catch (IOException e) {
				LOGGER.warning(e.toString());
			}
			if (store) {
				loadedResources.put(path, resource);
			} else {
				return resource;
			}
		}

		return loadedResources.get(path);
	}

	/**
	 * Gets a live view of the map corresponding to the resource atlas' cached items.
	 *
	 * @return A map from paths to resources.
	 */
	public Map<String, T> getAllCached() {
		return loadedResources;
	}

	/**
	 * Drops the resource at the provided path from the cache store.
	 *
	 * @param path The path of the resource to drop.
	 */
	public void drop(String path) {
		loadedResources.remove(path);
	}

	/**
	 * Gets whether the resource with the specified path is loaded in the cache.
	 *
	 * @param path The path of the resource to check for.
	 * @return True if the resource is loaded, false otherwise.
	 */
	public boolean isLoaded(String path) {
		return loadedResources.get(path) != null;
	}

	/**
	 * Gets the fallback resource the atlas will return if loading fails.
	 *
	 * @return The configured fallback resource.
	 */
	public T getFallbackResource() {
		return fallbackResource;
	}

	/**
	 * Sets the fallback resource the atlas will return if loading fails.
	 *
	 * @param fallbackResource The fallback resource to set.
	 */
	public void setFallbackResource(T fallbackResource) {
		this.fallbackResource = fallbackResource;
	}

	/**
	 * Disposes of all resources held by this atlas.
	 */
	public void dispose() {
		for (Resource resource : loadedResources.values()) {
			resource.dispose();
		}
	}
}
