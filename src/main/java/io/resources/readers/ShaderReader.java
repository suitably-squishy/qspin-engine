package io.resources.readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

/**
 * A class for reading GLSL shader source files.
 */
public class ShaderReader {

	/**
	 * Reads the shader from the shaders/ directory with the provided name.
	 *
	 * @param filename The filename of the shader to read.
	 * @return A string containing the shader source code.
	 */
	public static String readShaderFile(String filename) {
		StringBuilder string = new StringBuilder();
		BufferedReader br;

		try {
			ClassLoader classLoader = ClassLoader.getSystemClassLoader();
			br = new BufferedReader(new InputStreamReader(Objects.requireNonNull(classLoader.getResourceAsStream("shaders/" + filename + ".shader"))));
			String line;
			while ((line = br.readLine()) != null) {
				string.append(line);
				string.append("\n");
			}

			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return string.toString();
	}
}