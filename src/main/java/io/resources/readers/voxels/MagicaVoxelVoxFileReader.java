package io.resources.readers.voxels;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.logging.Logger;

import static io.resources.readers.ResourceUtils.read32;

/**
 * A class to read .vox files as specified in
 * <a href="https://github.com/ephtracy/voxel-model/blob/master/MagicaVoxel-file-format-vox.txt">https://github.com/ephtracy/voxel-model/blob/master/MagicaVoxel-file-format-vox.txt</a>
 */
public class MagicaVoxelVoxFileReader extends VoxelSourceReader {

	/**
	 * The default palette loaded when no palette is present in the file.
	 */
	private static final int[] defaultPalette = {
			0x00000000, 0xffffffff, 0xffccffff, 0xff99ffff, 0xff66ffff, 0xff33ffff, 0xff00ffff, 0xffffccff, 0xffccccff, 0xff99ccff, 0xff66ccff, 0xff33ccff, 0xff00ccff, 0xffff99ff, 0xffcc99ff, 0xff9999ff,
			0xff6699ff, 0xff3399ff, 0xff0099ff, 0xffff66ff, 0xffcc66ff, 0xff9966ff, 0xff6666ff, 0xff3366ff, 0xff0066ff, 0xffff33ff, 0xffcc33ff, 0xff9933ff, 0xff6633ff, 0xff3333ff, 0xff0033ff, 0xffff00ff,
			0xffcc00ff, 0xff9900ff, 0xff6600ff, 0xff3300ff, 0xff0000ff, 0xffffffcc, 0xffccffcc, 0xff99ffcc, 0xff66ffcc, 0xff33ffcc, 0xff00ffcc, 0xffffcccc, 0xffcccccc, 0xff99cccc, 0xff66cccc, 0xff33cccc,
			0xff00cccc, 0xffff99cc, 0xffcc99cc, 0xff9999cc, 0xff6699cc, 0xff3399cc, 0xff0099cc, 0xffff66cc, 0xffcc66cc, 0xff9966cc, 0xff6666cc, 0xff3366cc, 0xff0066cc, 0xffff33cc, 0xffcc33cc, 0xff9933cc,
			0xff6633cc, 0xff3333cc, 0xff0033cc, 0xffff00cc, 0xffcc00cc, 0xff9900cc, 0xff6600cc, 0xff3300cc, 0xff0000cc, 0xffffff99, 0xffccff99, 0xff99ff99, 0xff66ff99, 0xff33ff99, 0xff00ff99, 0xffffcc99,
			0xffcccc99, 0xff99cc99, 0xff66cc99, 0xff33cc99, 0xff00cc99, 0xffff9999, 0xffcc9999, 0xff999999, 0xff669999, 0xff339999, 0xff009999, 0xffff6699, 0xffcc6699, 0xff996699, 0xff666699, 0xff336699,
			0xff006699, 0xffff3399, 0xffcc3399, 0xff993399, 0xff663399, 0xff333399, 0xff003399, 0xffff0099, 0xffcc0099, 0xff990099, 0xff660099, 0xff330099, 0xff000099, 0xffffff66, 0xffccff66, 0xff99ff66,
			0xff66ff66, 0xff33ff66, 0xff00ff66, 0xffffcc66, 0xffcccc66, 0xff99cc66, 0xff66cc66, 0xff33cc66, 0xff00cc66, 0xffff9966, 0xffcc9966, 0xff999966, 0xff669966, 0xff339966, 0xff009966, 0xffff6666,
			0xffcc6666, 0xff996666, 0xff666666, 0xff336666, 0xff006666, 0xffff3366, 0xffcc3366, 0xff993366, 0xff663366, 0xff333366, 0xff003366, 0xffff0066, 0xffcc0066, 0xff990066, 0xff660066, 0xff330066,
			0xff000066, 0xffffff33, 0xffccff33, 0xff99ff33, 0xff66ff33, 0xff33ff33, 0xff00ff33, 0xffffcc33, 0xffcccc33, 0xff99cc33, 0xff66cc33, 0xff33cc33, 0xff00cc33, 0xffff9933, 0xffcc9933, 0xff999933,
			0xff669933, 0xff339933, 0xff009933, 0xffff6633, 0xffcc6633, 0xff996633, 0xff666633, 0xff336633, 0xff006633, 0xffff3333, 0xffcc3333, 0xff993333, 0xff663333, 0xff333333, 0xff003333, 0xffff0033,
			0xffcc0033, 0xff990033, 0xff660033, 0xff330033, 0xff000033, 0xffffff00, 0xffccff00, 0xff99ff00, 0xff66ff00, 0xff33ff00, 0xff00ff00, 0xffffcc00, 0xffcccc00, 0xff99cc00, 0xff66cc00, 0xff33cc00,
			0xff00cc00, 0xffff9900, 0xffcc9900, 0xff999900, 0xff669900, 0xff339900, 0xff009900, 0xffff6600, 0xffcc6600, 0xff996600, 0xff666600, 0xff336600, 0xff006600, 0xffff3300, 0xffcc3300, 0xff993300,
			0xff663300, 0xff333300, 0xff003300, 0xffff0000, 0xffcc0000, 0xff990000, 0xff660000, 0xff330000, 0xff0000ee, 0xff0000dd, 0xff0000bb, 0xff0000aa, 0xff000088, 0xff000077, 0xff000055, 0xff000044,
			0xff000022, 0xff000011, 0xff00ee00, 0xff00dd00, 0xff00bb00, 0xff00aa00, 0xff008800, 0xff007700, 0xff005500, 0xff004400, 0xff002200, 0xff001100, 0xffee0000, 0xffdd0000, 0xffbb0000, 0xffaa0000,
			0xff880000, 0xff770000, 0xff550000, 0xff440000, 0xff220000, 0xff110000, 0xffeeeeee, 0xffdddddd, 0xffbbbbbb, 0xffaaaaaa, 0xff888888, 0xff777777, 0xff555555, 0xff444444, 0xff222222, 0xff111111
	};

	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private final ByteBuffer inputBuffer;

	private boolean hasPalette;

	private final boolean flipYZAxes;

	private static class Chunk {
		long id;
		int contentSize;
		int childSize;
	}

	/**
	 * Creates a new voxel file reader to read the provided byte buffer, flips the y/z axes by default.
	 *
	 * @param inputBuffer The byte buffer representing the file to read.
	 */
	public MagicaVoxelVoxFileReader(ByteBuffer inputBuffer) {
		this(inputBuffer, true);
	}

	/**
	 * Creates a new voxel file reader to read the provided byte buffer, flips the y/z axes by default.
	 *
	 * @param inputBuffer The byte buffer representing the file to read.
	 * @param flipYZAxes  Whether or not to automatically flip the Y/Z axes on load.
	 */
	public MagicaVoxelVoxFileReader(ByteBuffer inputBuffer, boolean flipYZAxes) {
		this.inputBuffer = inputBuffer;
		this.flipYZAxes = flipYZAxes;
	}

	/**
	 * Reads the voxel file, calling the provided functions when information is uncovered.
	 *
	 * @throws IOException If the file is invalid or ends unexpectedly.
	 */
	@Override
	public void read() throws IOException {
		this.hasPalette = false;
		inputBuffer.rewind();
		inputBuffer.order(ByteOrder.LITTLE_ENDIAN);

		try {
			long header = read32(inputBuffer);
			if (header != magicValue('V', 'O', 'X', ' ')) {
				throw new IOException("File is not a valid .vox file.");
			}

			long versionNumber = read32(inputBuffer);
			if (versionNumber < 150) {
				throw new IOException(".vox is an unsupported version.");
			}

			Chunk mainHeader = readChunkHeader();
			if (mainHeader.id != magicValue('M', 'A', 'I', 'N')) {
				throw new IOException("Main chunk expected, got " + Long.toHexString(mainHeader.id));
			}

			skip(mainHeader.contentSize);

			Chunk currentChunk;
			while (true) {
				if (inputBuffer.remaining() == 0) {
					break;
				}
				try {
					currentChunk = readChunkHeader();
				} catch (BufferUnderflowException e) {
					// Exits the loop if the file runs out unexpectedly.
					break;
				}

				if (currentChunk.id == magicValue('P', 'A', 'C', 'K')) {
					readPackChunk();
				} else if (currentChunk.id == magicValue('S', 'I', 'Z', 'E')) {
					readSizeChunk();
				} else if (currentChunk.id == magicValue('X', 'Y', 'Z', 'I')) {
					readVoxelChunk();
				} else if (currentChunk.id == magicValue('R', 'G', 'B', 'A')) {
					readPaletteChunk();
				} else if (currentChunk.id == magicValue('M', 'A', 'T', 'T')) {
					skip(currentChunk.contentSize + currentChunk.childSize);
					//				readMaterialChunk();
				} else {
					// Skips unrecognised chunks.
					skip(currentChunk.contentSize + currentChunk.childSize);
				}
			}

			if (!this.hasPalette) {
				generateDefaultPalette();
			}
		} catch (BufferUnderflowException e) {
			throw new IOException(e);
		}
	}

	/**
	 * Reads a chunk header from the buffer.
	 *
	 * @return A Chunk object representing the header obtained.
	 * @throws IOException If the buffer runs out before the header finishes.
	 */
	private Chunk readChunkHeader() throws IOException {
		Chunk chunk = new Chunk();
		chunk.id = read32(inputBuffer);
		chunk.contentSize = (int) read32(inputBuffer);
		chunk.childSize = (int) read32(inputBuffer);
		return chunk;
	}

	/**
	 * Reads the body of a PACK chunk, contains the number of models in the file.
	 */
	private void readPackChunk() {
		// Currently unused
		// Will be needed to be implemented if models larger than 126x126x126 are needed
		int numModels = (int) read32(inputBuffer);
	}

	/**
	 * Reads the body of a SIZE chunk, containing the overall size of the current model.
	 */
	private void readSizeChunk() {
		int x = (int) read32(inputBuffer);
		int y = (int) read32(inputBuffer);
		int z = (int) read32(inputBuffer);
		if (flipYZAxes) {
			onSize(x, z, y);
		} else {
			onSize(x, y, z);
		}
	}

	/**
	 * Reads the body of a XYZI chunk, containing the position and colour of a model's voxels.
	 */
	private void readVoxelChunk() {
		int numVoxels = (int) read32(inputBuffer);

		for (int i = 0; i < numVoxels; i++) {
			int x = inputBuffer.get();
			int y = inputBuffer.get();
			int z = inputBuffer.get();
			int c = inputBuffer.get();

			if (flipYZAxes) {
				onVoxel(x, z, y, (short) (c & 0xff));
			} else {
				onVoxel(x, y, z, (short) (c & 0xff));
			}
		}
	}

	/**
	 * Reads the body of an RGBA chunk, containing the colours contained in the model's palette.
	 *
	 * @throws IOException If the buffer runs out before the body of the chunk could be read.
	 */
	private void readPaletteChunk() throws IOException {
		int[] colourBuffer = new int[4];

		// Sets the first palette colour to nothing, as in the spec
		extractColour(defaultPalette[0], colourBuffer);
		onPalette((short) 0, colourBuffer[0], colourBuffer[1], colourBuffer[2]);

		for (short i = 1; i < 256; i++) {
			int rgba = (int) read32(inputBuffer);
			extractColour(rgba, colourBuffer);
			onPalette(i, colourBuffer[0], colourBuffer[1], colourBuffer[2]);
		}

		// Last element of palette is never used, as in the spec.
		skip(4);
		this.hasPalette = true;
	}

	/**
	 * Reads the contents of a MATT chunk, containing information on voxel materials.
	 */
	private void readMaterialChunk() {
	}

	/**
	 * Converts the provided characters to a bit sequence.
	 *
	 * @param c0 The first character in the magic value.
	 * @param c1 The second character in the magic value.
	 * @param c2 The third character in the magic value.
	 * @param c3 The fourth and final character in the magic value.
	 * @return A bit sequence representing the provided characters as they would appear in a .vox file.
	 */
	private long magicValue(char c0, char c1, char c2, char c3) {
		return ((c3 << 24) & 0xff000000) | ((c2 << 16) & 0x00ff0000) | ((c1 << 8) & 0x0000ff00) | (c0 & 0x000000ff);
	}

	/**
	 * Skips the provided number of bytes in the buffer.
	 *
	 * @param n The number of bytes to skip.
	 * @throws IOException If the buffer runs out before the number of bytes provided.
	 */
	private void skip(int n) throws IOException {
		if (n <= 0) {
			return;
		}
		if (inputBuffer.remaining() < n) {
			throw new IOException();
		}
		inputBuffer.position(inputBuffer.position() + n);
	}

	/**
	 * Extracts the provided bit sequenced colour into the provided buffer.
	 *
	 * @param rgba   The bit sequenced colour to extract.
	 * @param buffer The array to fill with the colour values, r:0, g:1, b:2, a:3
	 * @return The buffer provided.
	 */
	private int[] extractColour(int rgba, int[] buffer) {
		if (buffer.length < 4) {
			return buffer;
		}
		// Red
		buffer[0] = (rgba & 0x000000ff);
		// Green
		buffer[1] = (rgba & 0x0000ff00) >> 8;
		// Blue
		buffer[2] = (rgba & 0x00ff0000) >> 16;
		// Alpha
		buffer[3] = 0xff;
		return buffer;
	}

	/**
	 * Generates the default palette, as specified in the spec.
	 */
	private void generateDefaultPalette() {
		int[] colourBuffer = new int[4];

		for (short i = 0; i < 256; i++) {
			extractColour(defaultPalette[i], colourBuffer);
			onPalette(i, colourBuffer[0], colourBuffer[1], colourBuffer[2]);
		}
	}
}
