package io.resources.readers.voxels;


import org.joml.Vector3i;
import utils.RGBColour;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static io.resources.readers.ResourceUtils.read32;
import static io.resources.readers.ResourceUtils.readString;

/**
 * A class to read Qubicle Binary format files, as specified in <a href="https://getqubicle.com/learn/article.php?id=22">https://getqubicle.com/learn/article.php?id=22</a>
 */
public class QbFileReader extends VoxelSourceReader {

	/** The .qb file as a byte buffer. */
	private final ByteBuffer inputBuffer;

	private long colourFormat;
	private long zAxisOrientation;
	private long compressed;
	private long visibilityMaskEncoded;
	private long numMatrices;

	// TODO Is having short indexed palettes a good idea when we can have huge files?
	private final Map<RGBColour, Short> paletteCache;
	private short currentPaletteIndex;

	private static final long CODE_FLAG = 2;
	private static final long NEXT_SLICE_FLAG = 6;

	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/**
	 * Creates a reader able to read the provided input buffer as a .qb file.
	 *
	 * @param inputBuffer The input buffer containing the file to read.
	 */
	public QbFileReader(ByteBuffer inputBuffer) {
		this.inputBuffer = inputBuffer;
		this.paletteCache = new HashMap<>();
		this.currentPaletteIndex = 1;
	}

	@Override
	public void read() throws IOException {
		inputBuffer.rewind();

		// Uh why is this required?
		inputBuffer.order(ByteOrder.LITTLE_ENDIAN);
		try {
			readHeader();

			QbMatrix[] matrices = new QbMatrix[(int) numMatrices];

			for (int i = 0; i < numMatrices; i++) {
				int nameLength = inputBuffer.get();

				String name = readString(inputBuffer, nameLength);
				long sizeX = read32(inputBuffer);
				long sizeY = read32(inputBuffer);
				long sizeZ = read32(inputBuffer);

				if (sizeX > Integer.MAX_VALUE || sizeY > Integer.MAX_VALUE || sizeZ > Integer.MAX_VALUE) {
					throw new IOException(
							"That is one FAT voxel file, not going to open that one buddy, size: " +
									sizeX + ", " + sizeY + ", " + sizeZ);
				}

				int posX = inputBuffer.getInt();
				int posY = inputBuffer.getInt();
				int posZ = inputBuffer.getInt();

				matrices[i] = new QbMatrix(
						new Vector3i(posX, posY, posZ),
						new Vector3i((int) sizeX, (int) sizeY, (int) sizeZ)
				);

				if (compressed == 0) {
					for (int x = 0; x < sizeX; x++) {
						for (int y = 0; y < sizeY; y++) {
							for (int z = 0; z < sizeZ; z++) {
								matrices[i].matrix[x][y][z] = read32(inputBuffer);
							}
						}
					}
				} else {
					for (int z = 0; z < sizeZ; z++) {
						int index = 0;
						while (true) {
							long data = read32(inputBuffer);

							if (data == NEXT_SLICE_FLAG) {
								break;
							} else if (data == CODE_FLAG) {
								long count = read32(inputBuffer);
								data = read32(inputBuffer);
								for (int j = 0; j < count; j++) {
									int x = index % (int) (sizeX);
									int y = Math.floorDiv(index, (int) sizeX);
									index++;
									matrices[i].matrix[x][y][z] = data;
								}
							} else {
								int x = index % (int) (sizeX);
								int y = Math.floorDiv(index, (int) sizeX);
								index++;
								matrices[i].matrix[x][y][z] = data;
							}
						}
					}
				}
			}
			// Announces the contents of the matrix cache to the listeners.
			announceMatrixCache(matrices);
		} catch (BufferUnderflowException e) {
			throw new IOException(e);
		}
	}

	/**
	 * Reads the .qb file header, validating whether the file can be parsed.
	 *
	 * @throws IOException If there were any errors reading the file, or the file is unreadable.
	 */
	private void readHeader() throws IOException {
		String versionString = readVersion();
		this.colourFormat = read32(inputBuffer);
		this.zAxisOrientation = read32(inputBuffer);
		this.compressed = read32(inputBuffer);
		this.visibilityMaskEncoded = read32(inputBuffer);

		this.numMatrices = read32(inputBuffer);
		if (numMatrices > Integer.MAX_VALUE) {
			throw new IOException(
					"This .qb file has a stupid number of matrices in it, not reading that one buddy." +
							" Number of matrices: " + numMatrices
			);
		}
	}

	/**
	 * Generates and checks the .qb file version string.
	 *
	 * @return A version string built from the individual version components.
	 * @throws IOException If there were any errors with the version string.
	 */
	private String readVersion() throws IOException {
		short versionMajor = (short) Short.toUnsignedInt(inputBuffer.get());
		short versionMinor = (short) Short.toUnsignedInt(inputBuffer.get());
		short versionRelease = (short) Short.toUnsignedInt(inputBuffer.get());
		short versionBuild = (short) Short.toUnsignedInt(inputBuffer.get());

		String versionString = versionMajor + "." + versionMinor + "." + versionRelease + "." + versionBuild;

		if (versionMajor != 1 || versionMinor != 1)
			throw new IOException("Unsupported .qb file version: " + versionString);

		return versionString;
	}

	/**
	 * Notifies the subscribers of the voxel, palette and optionally the visibility data stored in the provided value.
	 *
	 * @param data The data point extracted from the model.
	 * @param x    The x coordinate of the data point in the model as a whole.
	 * @param y    The y coordinate of the data point in the model as a whole.
	 * @param z    The z coordinate of the data point in the model as a whole.
	 */
	private void getColour(long data, int x, int y, int z) {
		int r, g, b;
		byte a;
		if (colourFormat == 0) {
			a = (byte) ((data & 0xff000000) >> 24);
			b = (int) ((data & 0x00ff0000) >> 16);
			g = (int) ((data & 0x0000ff00) >> 8);
			r = (int) (data & 0x000000ff);
		} else {
			r = (int) ((data & 0xff000000) >> 24);
			g = (int) ((data & 0x00ff0000) >> 16);
			b = (int) ((data & 0x0000ff00) >> 8);
			a = (byte) (data & 0x000000ff);
		}

		RGBColour colour = new RGBColour(r, g, b);

		Short paletteIndex = paletteCache.get(colour);
		if (paletteIndex == null) {
			paletteCache.put(colour, currentPaletteIndex);
			onPalette(currentPaletteIndex, r, g, b);
			paletteIndex = currentPaletteIndex;
			currentPaletteIndex++;
		}
		onVoxel(x, y, z, paletteIndex);

		if (visibilityMaskEncoded != 0) {
			a = convertVisibilityByte(a);
			onVisibility(x, y, z, a);
		}
	}

	/**
	 * Announces the contents of the matrix cache to any listeners as if the file was one continuous matrix,
	 * entirely contained inside positive space.
	 *
	 * @param matrices The matrix cache to announce.
	 */
	private void announceMatrixCache(QbMatrix[] matrices) {
		// Get minimum and maximum extents
		Vector3i minimumExtent = new Vector3i();
		Vector3i maximumExtent = new Vector3i();

		for (QbMatrix matrix : matrices) {
			if (matrix.size.x <= 0 || matrix.size.y <= 0 || matrix.size.z <= 0) continue;

			minimumExtent.x = Math.min(matrix.pos.x, minimumExtent.x);
			maximumExtent.x = Math.max(matrix.pos.x + matrix.size.x, maximumExtent.x);

			minimumExtent.y = Math.min(matrix.pos.y, minimumExtent.y);
			maximumExtent.y = Math.max(matrix.pos.y + matrix.size.y, maximumExtent.y);

			minimumExtent.z = Math.min(matrix.pos.z, minimumExtent.z);
			maximumExtent.z = Math.max(matrix.pos.z + matrix.size.z, maximumExtent.z);
		}

		Vector3i totalSize = maximumExtent.sub(minimumExtent);
		// Announce size first
		onSize(totalSize.x, totalSize.y, totalSize.z);

		// Makes sure the first palette index is full of 0s
		onPalette((short) 0, 0, 0, 0);

		for (QbMatrix matrix : matrices) {
			if (matrix.size.x <= 0 || matrix.size.y <= 0 || matrix.size.z <= 0) continue;

			for (int x = 0; x < matrix.matrix.length; x++) {
				for (int y = 0; y < matrix.matrix[x].length; y++) {
					for (int z = 0; z < matrix.matrix[x][y].length; z++) {
						if (matrix.matrix[x][y][z] == 0) continue;
						getColour(
								matrix.matrix[x][y][z],
								(x - minimumExtent.x) + matrix.pos.x,
								(y - minimumExtent.y) + matrix.pos.y,
								(z - minimumExtent.z) + matrix.pos.z
						);
					}
				}
			}
		}
	}

	/**
	 * Converts the provided byte from a .qb visibility byte to our neighbour byte.
	 *
	 * @param a The visibility byte.
	 * @return The byte represented as our neighbour byte.
	 */
	private static byte convertVisibilityByte(byte a) {
		// Move the byte along by one to the right.
		a = (byte) ((a & 0b01111110) >>> 1);
		// Swap the left/right and back/front bits
		return (byte) (((a & 0b0101) << 1) | ((a & 0b1010) >>> 1) | (a & 0b110000));
	}

	/**
	 * Represents an individual .qb file matrix in a file.
	 */
	private static class QbMatrix {

		/** The voxel matrix of the matrix. */
		private final long[][][] matrix;

		/** The position of 0,0,0 of the matrix in the model. */
		private final Vector3i pos;

		/** The size of the matrix. */
		private final Vector3i size;

		/**
		 * Creates a .qb file matrix with the provided position and size.
		 *
		 * @param pos  The position of 0,0,0 of the matrix in the model.
		 * @param size The size of the matrix.
		 */
		private QbMatrix(Vector3i pos, Vector3i size) {
			this.pos = pos;
			this.size = size;
			this.matrix = new long[size.x][size.y][size.z];
		}
	}
}
