package io.resources.readers.voxels;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * A base abstract class for all voxel source readers, contains holders for subscribers.
 */
public abstract class VoxelSourceReader {

	/** All subscribers to be notified when a voxel source's size in voxels is read. */
	protected final List<SizeReader> sizeSubscribers;

	/** All subscribers to be notified when a voxel in a source is read. */
	protected final List<VoxelReader> voxelSubscribers;

	/** All subscribers to be notified when a colour palette entry is read. */
	protected final List<PaletteReader> paletteSubscribers;

	/** All subscribers to be notified when an individual voxel's visibility information is read. */
	protected final List<VisibilityReader> visibilitySubscribers;

	/**
	 * Creates a voxel source reader with empty subscriber lists.
	 */
	public VoxelSourceReader() {
		this.sizeSubscribers = new LinkedList<>();
		this.voxelSubscribers = new LinkedList<>();
		this.paletteSubscribers = new LinkedList<>();
		this.visibilitySubscribers = new LinkedList<>();
	}

	/**
	 * An interface to represent an object to notify of a voxel file's size.
	 */
	@FunctionalInterface
	public interface SizeReader {
		/**
		 * Gives information of the voxel source's size in voxels.
		 *
		 * @param x The size of the voxel source on the x axis.
		 * @param y The size of the voxel source on the y axis.
		 * @param z The size of the voxel source on the z axis.
		 */
		void size(int x, int y, int z);
	}

	/**
	 * An interface to represent an object to notify of an individual voxel.
	 */
	@FunctionalInterface
	public interface VoxelReader {
		/**
		 * Gives information about a voxel from the voxel source.
		 *
		 * @param x           The x coordinate of the voxel.
		 * @param y           The y coordinate of the voxel.
		 * @param z           The z coordinate of the voxel.
		 * @param colourIndex The palette index of the colour of the voxel.
		 */
		void voxel(int x, int y, int z, short colourIndex);
	}

	/**
	 * An interface to represent an object to notify of a colour in the voxel source's palette.
	 */
	@FunctionalInterface
	public interface PaletteReader {
		/**
		 * Gives information about a colour in the voxel source's palette.
		 *
		 * @param colourIndex The palette index of the colour.
		 * @param r           The red component of the colour.
		 * @param g           The green component of the colour.
		 * @param b           The blue component of the colour.
		 */
		void palette(short colourIndex, int r, int g, int b);
	}

	/**
	 * An interface to represent an object to notify of the visibility of an individual voxel in a model.
	 */
	@FunctionalInterface
	public interface VisibilityReader {
		/**
		 * Gives information about the visibility of an individual voxel.
		 *
		 * @param x              The x coordinate of the voxel.
		 * @param y              The y coordinate of the voxel.
		 * @param z              The z coordinate of the voxel.
		 * @param visibilityByte The visibility of the voxel as defined by {@link utils.CheckNeighbours}
		 */
		void visibility(int x, int y, int z, byte visibilityByte);
	}

	/**
	 * Reads the voxel source, calling any required subscribers.
	 *
	 * @throws IOException If there are any problems reading the voxel source.
	 */
	public abstract void read() throws IOException;

	/**
	 * Notifies all subscribers, in order of their addition, of the size of the voxel source.
	 * <p>
	 * Must be called before {@link VoxelSourceReader#onVoxel} and {@link VoxelSourceReader#onVisibility}.
	 *
	 * @param x The size of the voxel source on the x axis.
	 * @param y The size of the voxel source on the y axis.
	 * @param z The size of the voxel source on the z axis.
	 */
	protected void onSize(int x, int y, int z) {
		this.sizeSubscribers.forEach(s -> s.size(x, y, z));
	}

	/**
	 * Notifies all subscribers, in order of their addition, of an individual voxel.
	 *
	 * @param x           The x coordinate of the voxel.
	 * @param y           The y coordinate of the voxel.
	 * @param z           The z coordinate of the voxel.
	 * @param colourIndex The palette index of the colour of the voxel.
	 */
	protected void onVoxel(int x, int y, int z, short colourIndex) {
		this.voxelSubscribers.forEach(v -> v.voxel(x, y, z, colourIndex));
	}

	/**
	 * Notifies all subscribers, in order of their addition, of a colour in the source's palette.
	 *
	 * @param colourIndex The palette index of the colour.
	 * @param r           The red component of the colour.
	 * @param g           The green component of the colour.
	 * @param b           The blue component of the colour.
	 */
	protected void onPalette(short colourIndex, int r, int g, int b) {
		this.paletteSubscribers.forEach(p -> p.palette(colourIndex, r, g, b));
	}

	/**
	 * Notifies all subscribers, in order of their addition, of the visibility of the provided voxel.
	 *
	 * @param x              The x coordinate of the voxel.
	 * @param y              The y coordinate of the voxel.
	 * @param z              The z coordinate of the voxel.
	 * @param visibilityByte The visibility of the voxel as defined by {@link utils.CheckNeighbours}
	 */
	protected void onVisibility(int x, int y, int z, byte visibilityByte) {
		this.visibilitySubscribers.forEach(v -> v.visibility(x, y, z, visibilityByte));
	}

	/**
	 * Adds the provided size reader to be notified by the source reader.
	 *
	 * @param sizeReader The size reader to receive notifications.
	 */
	public void addSizeSubscriber(SizeReader sizeReader) {
		this.sizeSubscribers.add(sizeReader);
	}

	/**
	 * Adds the provided voxel reader to be notified by the source reader.
	 *
	 * @param voxelReader The voxel reader to receive notifications.
	 */
	public void addVoxelSubscriber(VoxelReader voxelReader) {
		this.voxelSubscribers.add(voxelReader);
	}

	/**
	 * Adds the provided palette reader to be notified by the source reader.
	 *
	 * @param paletteReader The palette reader to receive notifications.
	 */
	public void addPaletteSubscriber(PaletteReader paletteReader) {
		this.paletteSubscribers.add(paletteReader);
	}

	/**
	 * Adds the provided visibility reader to be notified by the source reader.
	 *
	 * @param visibilityReader The visibility reader to receive notifications.
	 */
	public void addVisibilitySubscriber(VisibilityReader visibilityReader) {
		this.visibilitySubscribers.add(visibilityReader);
	}
}
