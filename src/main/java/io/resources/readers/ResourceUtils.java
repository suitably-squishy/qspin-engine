package io.resources.readers;

import org.lwjgl.BufferUtils;

import java.io.*;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.logging.Logger;

/**
 * A class for reading resources into byte buffers.
 */
public class ResourceUtils {

	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/**
	 * <<<<<<< HEAD:src/main/java/io/resources/ResourceReader.java
	 * Resizes the provided buffer to the provided capcaity.
	 * =======
	 * Resizes the provided buffer to the provided capacity.
	 * >>>>>>> feature-qbFiles:src/main/java/io/resources/readers/ResourceUtils.java
	 *
	 * @param buffer      The buffer to resize.
	 * @param newCapacity The new capacity of the buffer.
	 * @return The new resized buffer.
	 */
	private static ByteBuffer resizeBuffer(ByteBuffer buffer, int newCapacity) {
		ByteBuffer newBuffer = BufferUtils.createByteBuffer(newCapacity);
		buffer.flip();
		newBuffer.put(buffer);
		return newBuffer;
	}

	/**
	 * Gets the file at the provided path as a byte buffer.
	 *
	 * @param filename The path of the file to get.
	 * @return A byte buffer representing the file.
	 * @throws IOException If the file isn't found or is interrupted.
	 */
	public static ByteBuffer getResourceAsByteBuffer(String filename) throws IOException {
		ByteBuffer buffer;
		URL url = Thread.currentThread().getContextClassLoader().getResource(filename);
		if (url == null) {
			throw new FileNotFoundException(filename);
		}
		File file = new File(url.getFile());
		if (file.isFile()) {
			FileInputStream fis = new FileInputStream(file);
			FileChannel fc = fis.getChannel();
			buffer = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
			fc.close();
			fis.close();
			LOGGER.info("Loaded " + file.getAbsolutePath() + " (as a file) successfully.");
		} else {
			InputStream source = url.openStream();

			try (source) {
				if (source == null) {
					LOGGER.warning("Failed to load " + file.getAbsolutePath());
					throw new FileNotFoundException(filename);
				}
				buffer = BufferUtils.createByteBuffer(8192);
				byte[] buf = new byte[8192];
				while (true) {
					int bytes = source.read(buf, 0, buf.length);
					if (bytes == -1)
						break;
					if (buffer.remaining() < bytes)
						buffer = resizeBuffer(buffer, buffer.capacity() * 2);
					buffer.put(buf, 0, bytes);
				}
				buffer.flip();
			}
			LOGGER.info("Loaded " + file.getAbsolutePath() + " (as a stream) successfully.");
		}
		return buffer;
	}

	/**
	 * Reads 32 bits from the buffer into a long.
	 *
	 * @param inputBuffer The buffer to read from.
	 * @return A long containing the 32 bits in the reverse order they were read.
	 * @throws java.nio.BufferUnderflowException If the buffer doesn't have enough bytes to read.
	 */
	public static long read32(ByteBuffer inputBuffer) {
		return Integer.toUnsignedLong(inputBuffer.getInt());
	}

	/**
	 * Reads a simple ascii string from the byte buffer of the given length.
	 *
	 * @param inputBuffer The input buffer to read from.
	 * @param length      The number of characters to read.
	 * @return The read string.
	 */
	public static String readString(ByteBuffer inputBuffer, int length) {
		if (length < 0)
			throw new IllegalArgumentException("Can't read a string of negative length you fool. Length: " + length);

		char[] arr = new char[length];
		for (int i = 0; i < length; i++) {
			arr[i] = (char) inputBuffer.get();
		}
		return String.valueOf(arr);
	}
}
