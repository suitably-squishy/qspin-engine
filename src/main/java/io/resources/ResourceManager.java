package io.resources;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * A class for managing multiple ResourceAtlases and simplifying retrieval from them.
 */
public class ResourceManager {

	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/**
	 * The map of resource types to atlases.
	 */
	private final Map<Class<? extends Resource>, ResourceAtlas<? extends Resource>> atlases;

	/**
	 * Creates a new resource manager with a blank map of atlases.
	 */
	public ResourceManager() {
		atlases = new HashMap<>();
	}

	/**
	 * Gets a resource of the specified type from the manager.
	 *
	 * @param resourceType The type of the resource to fetch.
	 * @param location     The path of the resource to fetch.
	 * @param <T>          The type of the resource to fetch.
	 * @return The resource if present in an existing atlas, the fallback resource if the resource fails to load, null if the atlas does not exist.
	 */
	public <T extends Resource> T get(Class<T> resourceType, String location) {
		if (resourceType == null) {
			LOGGER.warning("Tried to load " + location + " with a null type.");
			return null;
		}
		ResourceAtlas<?> atlas = atlases.get(resourceType);

		if (atlas == null) {
			LOGGER.warning("The resource atlas for type " + resourceType.getTypeName() + " was accessed for " + location + " but the atlas does not exist.");
			return null;
		}
		return resourceType.cast(atlas.get(location));
	}

	/**
	 * Gets a resource of the specified type from the manager, without returning a fallback resource if loading fails.
	 *
	 * @param resourceType The type of the resource to fetch.
	 * @param location     The path of the resource to fetch.
	 * @param store        Whether or not the atlas should store the loaded resource.
	 * @param reload       Whether the atlas should reload the resource, overwriting previously loaded values.
	 * @param <T>          The type of the resource to fetch.
	 * @return The resource if present, null otherwise.
	 */
	public <T extends Resource> T getWithoutFallback(Class<T> resourceType, String location, boolean store, boolean reload) {
		if (resourceType == null) {
			LOGGER.warning("Tried to load " + location + " with a null type.");
			return null;
		}
		ResourceAtlas<?> atlas = atlases.get(resourceType);

		if (atlas == null) {
			LOGGER.warning("The resource atlas for type " + resourceType.getTypeName() + " was accessed for " + location + " but the atlas does not exist.");
			return null;
		}
		return resourceType.cast(atlas.getWithoutFallback(location, store, reload));
	}

	/**
	 * Adds the provided atlas to the manager storing it under the given type.
	 *
	 * @param resourceType The type of resource the atlas is responsible for.
	 * @param atlas        The atlas to store.
	 * @param <T>          The type of resource the atlas is responsible for.
	 */
	public <T extends Resource> void putAtlas(Class<T> resourceType, ResourceAtlas<T> atlas) {
		atlases.put(resourceType, atlas);
	}

	/**
	 * Drops the atlas from the manager with the provided type.
	 *
	 * @param resourceType The type of resource to remove the atlas for.
	 * @param <T>          The type of resource to remove the atlas for.
	 */
	public <T extends Resource> void dropAtlas(Class<T> resourceType) {
		atlases.remove(resourceType);
	}

	/**
	 * Gets the atlas responsible for the provided type.
	 *
	 * @param resourceType The type of resource to fetch the atlas for.
	 * @param <T>          The type of resource to fetch the atlas for.
	 * @return The atlas responsible for managing the provided type of resource.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Resource> ResourceAtlas<T> getAtlas(Class<T> resourceType) {
		if (atlases.get(resourceType) == null) {
			return null;
		}
		return (ResourceAtlas<T>) atlases.get(resourceType);
	}

	/**
	 * Disposes of the resource manager, disposing any atlases it holds.
	 */
	public void dispose() {
		for (ResourceAtlas<? extends Resource> atlas : atlases.values()) {
			atlas.dispose();
		}
	}
}
