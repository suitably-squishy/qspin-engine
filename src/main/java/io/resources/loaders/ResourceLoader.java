package io.resources.loaders;

import java.io.IOException;

/**
 * An interface for any classes wishing to load a given type.
 *
 * @param <T> The type the loader is able to load.
 */
public interface ResourceLoader<T> {
	
	/**
	 * Loads an object of type T from the provided location.
	 *
	 * @param path The path to load the resource from.
	 * @return A resource of type T.
	 * @throws IOException If the loading fails in some way.
	 */
	T loadResource(String path) throws IOException;
}
