package io.resources.loaders;

import io.resources.readers.ResourceUtils;
import io.resources.readers.voxels.MagicaVoxelVoxFileReader;
import io.resources.readers.voxels.QbFileReader;
import io.resources.readers.voxels.VoxelSourceReader;
import org.joml.Vector3i;
import render.model.Palette;
import render.model.VoxelModel;
import utils.RGBColour;
import utils.RGBMaterial;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * A class for loading .vox files into a VoxelModel.
 */
public class VoxelModelLoader implements ResourceLoader<VoxelModel> {

	/**
	 * Loads the .qb or .vox file in the voxels/ directory and returns it as a VoxelModel.
	 * Voxel models provided without extensions with identical names will prefer .qb files.
	 *
	 * @param location The filename of the voxel model in voxels/
	 * @return A voxel model if successful, null otherwise.
	 */
	@Override
	public VoxelModel loadResource(String location) throws IOException {
		if (location.endsWith(".qb")) {
			String trimmedLocation = location.substring(0, location.length() - 3);
			return tryLoadQb(trimmedLocation);
		} else if (location.endsWith(".vox")) {
			String trimmedLocation = location.substring(0, location.length() - 4);
			return tryLoadVox(trimmedLocation);
		}

		try {
			return tryLoadQb(location);
		} catch (FileNotFoundException e) {
			try {
				return tryLoadVox(location);
			} catch (FileNotFoundException fnf) {
				throw new FileNotFoundException("Could not find a file with that name.");
			}
		}
	}

	/**
	 * Tries to load a .vox file from the provided location.
	 *
	 * @param location The location and filename of the file to load, without the .vox extension.
	 * @return A voxel model read from the file.
	 * @throws IOException If there are any problems loading the model.
	 */
	private VoxelModel tryLoadVox(String location) throws IOException {
		ByteBuffer buffer = ResourceUtils.getResourceAsByteBuffer("models/" + location + ".vox");
		VoxelSourceReader reader = new MagicaVoxelVoxFileReader(buffer);
		IndividualVoxelModelLoader loader = new IndividualVoxelModelLoader(buffer);
		return loader.readModel(reader);
	}

	/**
	 * Tries to load a .qb file from the provided location.
	 *
	 * @param location The location and filename of the file to load, without the .qb extension.
	 * @return A voxel model read from the file.
	 * @throws IOException If there are any problems loading the model.
	 */
	private VoxelModel tryLoadQb(String location) throws IOException {
		ByteBuffer buffer = ResourceUtils.getResourceAsByteBuffer("models/" + location + ".qb");
		VoxelSourceReader reader = new QbFileReader(buffer);
		IndividualVoxelModelLoader loader = new IndividualVoxelModelLoader(buffer);
		return loader.readModel(reader);
	}

	/**
	 * An internal class to wrap a {@link VoxelSourceReader} and load an individual file.
	 */
	private static class IndividualVoxelModelLoader implements VoxelSourceReader.VoxelReader,
			VoxelSourceReader.SizeReader, VoxelSourceReader.PaletteReader, VoxelSourceReader.VisibilityReader {

		/** The file contents. */
		private final ByteBuffer buffer;

		/** How many voxels have been taken from the file. */
		private long voxelCount;

		/** The voxel array. */
		private short[][][] voxels;

		/** The visibility/neighbour array as defined by {@link utils.CheckNeighbours} */
		private byte[][][] neighbours;
		private boolean hasReceivedVisibility;

		/** The list of materials that have been taken from the source. */
		private final List<RGBMaterial> materials;

		/** The total size of the voxel model. */
		private Vector3i size;

		private boolean hasReceivedSize;

		/**
		 * Creates an model loader for the file contained in the provided byte buffer.
		 *
		 * @param buffer The byte buffer containing the file to read.
		 */
		private IndividualVoxelModelLoader(ByteBuffer buffer) {
			this.buffer = buffer;
			this.voxelCount = 0;
			this.materials = new ArrayList<>();
			this.hasReceivedSize = false;
			this.hasReceivedVisibility = false;
		}

		/**
		 * Reads the loaded file into a voxel model.
		 *
		 * @param reader The source reader to use to read the file.
		 * @return A voxel model read from the file.
		 * @throws IOException If there are any problems reading the file.
		 */
		private VoxelModel readModel(VoxelSourceReader reader) throws IOException {
			reader.addVoxelSubscriber(this);
			reader.addPaletteSubscriber(this);
			reader.addSizeSubscriber(this);
			reader.addVisibilitySubscriber(this);

			reader.read();

			RGBMaterial[] materialsArray = materials.toArray(new RGBMaterial[0]);

			Palette palette = new Palette(materialsArray);

			buffer.clear();

			if (hasReceivedVisibility) {
				return new VoxelModel(voxels, palette, voxelCount, neighbours);
			} else {
				return new VoxelModel(voxels, palette, voxelCount);
			}
		}

		@Override
		public void size(int x, int y, int z) {
			this.voxels = new short[x][y][z];
			this.size = new Vector3i(x, y, z);
			this.hasReceivedSize = true;
		}

		@Override
		public void voxel(int x, int y, int z, short colourIndex) {
			// TODO Maybe allow this and hold a cache?
			if (!hasReceivedSize)
				throw new IllegalStateException("A voxel model loader should not provide a voxel before the model size.");

			this.voxels[x][y][z] = colourIndex;
			this.voxelCount++;
		}

		@Override
		public void palette(short colourIndex, int r, int g, int b) {
			materials.add(colourIndex, new RGBMaterial(new RGBColour(r, g, b)));
		}

		@Override
		public void visibility(int x, int y, int z, byte visibilityByte) {
			if (!hasReceivedVisibility) {
				if (!hasReceivedSize) {
					throw new IllegalStateException("A voxel model loader should not provide a voxel visibility before the model size.");
				}
				this.neighbours = new byte[size.x][size.y][size.z];
			}
			this.hasReceivedVisibility = true;

			this.neighbours[x][y][z] = visibilityByte;
		}
	}
}
