package io.resources.loaders;

import io.resources.readers.ResourceUtils;
import org.lwjgl.stb.STBVorbisInfo;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import sound.SoundBuffer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import static org.lwjgl.stb.STBVorbis.*;

/**
 * A resource loader able to load .ogg files with Vorbis encoding.
 */
public class OggVorbisLoader implements ResourceLoader<SoundBuffer> {

	/**
	 * Loads the given vorbis encoded .ogg file from the provided path into an OpenAL sound buffer.
	 * OpenAL must have been initialised before this is performed.
	 *
	 * @param path The filename in the sounds/ directory.
	 * @return A OpenAL sound buffer.
	 * @throws IOException If an error occurs opening the file.
	 */
	private static SoundBuffer loadOggVorbis(String path) throws IOException {
		SoundBuffer soundBuffer;

		try (MemoryStack stack = MemoryStack.stackPush();
			 STBVorbisInfo info = STBVorbisInfo.malloc()) {
			ByteBuffer buffer = ResourceUtils.getResourceAsByteBuffer("sounds/" + path + ".ogg");
			IntBuffer error = stack.mallocInt(1);
			long decoder = stb_vorbis_open_memory(buffer, error, null);
			if (decoder == MemoryUtil.NULL)
				throw new IOException("Failed to open ogg file at " + path + ", error code: " + error.get(0));

			stb_vorbis_get_info(decoder, info);

			int channels = info.channels();
			if (channels > 2) {
				throw new IOException("Ogg file contained too many channels: " + channels);
			}
			boolean stereo = channels == 2;

			int lengthSamples = stb_vorbis_stream_length_in_samples(decoder);

			ShortBuffer pcmData = MemoryUtil.memAllocShort(lengthSamples);

			pcmData.limit(stb_vorbis_get_samples_short_interleaved(decoder, channels, pcmData) * channels);
			stb_vorbis_close(decoder);

			soundBuffer = new SoundBuffer(pcmData, 16, stereo, info.sample_rate());

			MemoryUtil.memFree(pcmData);
		}
		return soundBuffer;
	}

	@Override
	public SoundBuffer loadResource(String path) throws IOException {
		return loadOggVorbis(path);
	}
}
