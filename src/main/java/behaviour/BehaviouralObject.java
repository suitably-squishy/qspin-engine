package behaviour;

import state.UpdateFunction;


/**
 * An object that can have behaviours attached.
 * Behaviours can be added, removed, activated and deactivated
 * and have priority within the object but not outside the object.
 */
public abstract class BehaviouralObject {
	
	/**
	 * The module responsible for handling behaviours and their functions.
	 */
	protected final BehaviourModule<BehaviouralObject> behaviourModule;
	
	public BehaviouralObject() {
		this.behaviourModule = new BehaviourModule<>(this);
	}
	
	/**
	 * Adds the given behaviour to this entity.
	 *
	 * @param behaviour The behaviour to add.
	 * @see BehaviourModule#add(Behaviour)
	 */
	public void addBehaviour(Behaviour<BehaviouralObject> behaviour) {
		this.behaviourModule.add(behaviour);
	}
	
	/**
	 * Adds a behaviour with the given parameters to this entity.
	 *
	 * @param name           The name of the behaviour to add.
	 * @param updateFunction The update function called when the behaviour is called.
	 * @param priority       The priority of the behaviour, lower number priority behaviours will be called before higher values.
	 * @see BehaviourModule#add(String, UpdateFunction, int)
	 */
	public void addBehaviour(String name, UpdateFunction<BehaviouralObject> updateFunction, int priority) {
		this.behaviourModule.add(name, updateFunction, priority);
	}
	
	/**
	 * Adds a behaviour with the given parameters to this entity.
	 *
	 * @param name              The name the behaviour should be added under.
	 * @param behaviourTemplate The behaviour to add.
	 * @param priority          The priority of the behaviour, lower number priority behaviours will be called before higher values.
	 * @see BehaviourModule#add(String, UpdateFunction, int)
	 */
	public void addBehaviour(String name, BehaviourTemplate<BehaviouralObject> behaviourTemplate, int priority) {
		this.behaviourModule.add(name, behaviourTemplate, priority);
	}
	
	/**
	 * Adds a behaviour with the given parameters to this entity.
	 *
	 * @param name           The name of the behaviour to add.
	 * @param updateFunction The update function called when the behaviour is called.
	 * @see BehaviourModule#add(String, UpdateFunction)
	 */
	public void addBehaviour(String name, UpdateFunction<BehaviouralObject> updateFunction) {
		this.behaviourModule.add(name, updateFunction);
	}
	
	/**
	 * Adds the given behaviour under the given name with the default priority to this module.
	 *
	 * @param name              The name of the behaviour to add.
	 * @param behaviourTemplate The behaviour to be added.
	 * @see BehaviourModule#add(String, BehaviourTemplate)
	 */
	public void addBehaviour(String name, BehaviourTemplate<BehaviouralObject> behaviourTemplate) {
		this.behaviourModule.add(name, behaviourTemplate);
	}
	
	/**
	 * Gets this entities behaviour with the given name, if present.
	 *
	 * @param behaviourName The name of the behaviour to fetch.
	 * @return A behaviour if present, null if not.
	 * @see BehaviourModule#get(String)
	 */
	public Behaviour<BehaviouralObject> getBehaviour(String behaviourName) {
		return this.behaviourModule.get(behaviourName);
	}
	
	/**
	 * Toggles whether the behaviour with the given name is enabled.
	 *
	 * @param behaviourName The name of the behaviour to toggle.
	 * @see BehaviourModule#toggle(String)
	 */
	public void toggleBehaviour(String behaviourName) {
		this.behaviourModule.toggle(behaviourName);
	}
	
	/**
	 * Enables the behaviour with the given name.
	 *
	 * @param behaviourName The name of the behaviour to enable.
	 * @see BehaviourModule#enable(String)
	 */
	public void enableBehaviour(String behaviourName) {
		this.behaviourModule.enable(behaviourName);
	}
	
	/**
	 * Disables the behaviour with the given name.
	 *
	 * @param behaviourName The name of the behaviour to disable.
	 * @see BehaviourModule#disable(String)
	 */
	public void disableBehaviour(String behaviourName) {
		this.behaviourModule.disable(behaviourName);
	}
	
	/**
	 * Removes the behaviour with the given name.
	 *
	 * @param behaviourName The name of the behaviour to remove.
	 */
	public void removeBehaviour(String behaviourName) {
		this.behaviourModule.remove(behaviourName);
	}

	public BehaviourModule<BehaviouralObject> getBehaviours() {
		return behaviourModule;
	}

	/**
	 * Calls the update function for this entity (if any)
	 * then calls the update function for each of the entity's children
	 *
	 * @param delta The time passed in seconds since the last update.
	 */
	public void update(float delta) {
		behaviourModule.update(delta);
	}
}
