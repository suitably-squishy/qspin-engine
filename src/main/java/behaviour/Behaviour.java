package behaviour;

import io.input.binds.Bind;
import io.input.binds.BindPressSubscriber;
import io.input.binds.BindReleaseSubscriber;
import io.input.binds.BindRepeatSubscriber;
import io.input.mouse.MouseInputContext;
import io.input.mouse.MouseMoveSubscriber;
import io.input.mouse.MouseScrollSubscriber;
import state.UpdateFunction;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A module for wrapping {@link BehaviourTemplate}s in order for them to be used in {@link BehaviourModule}s.
 *
 * @param <T> The type of the owner of the behaviour.
 */
public class Behaviour<T> {
	
	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
	/**
	 * The user defined name of the behaviour.
	 */
	protected final String name;
	
	/**
	 * The priority of the behaviour, lower values mean higher priority.
	 */
	protected final int priority;
	
	/**
	 * The wrapped behaviour template.
	 */
	private final BehaviourTemplate<T> behaviourTemplate;
	
	/**
	 * The owner of the behaviour.
	 */
	protected final T owner;
	
	
	/**
	 * Wraps an update function as a behaviour with the given parameters.
	 *
	 * @param name           The user defined name of the behaviour.
	 * @param updateFunction The update function to be wrapped.
	 * @param owner          The owner of the behaviour.
	 * @param priority       The priority of the behaviour, lower values mean higher priority.
	 */
	public Behaviour(String name, UpdateFunction<T> updateFunction, T owner, int priority) {
		this.name = name;
		this.behaviourTemplate = new BehaviourTemplate<>(this, updateFunction);
		this.owner = owner;
		this.priority = priority;
		this.bindTemplate();
	}
	
	/**
	 * Wraps a behaviour with the given parameters.
	 *
	 * @param name              The user defined name of the behaviour.
	 * @param behaviourTemplate The behaviour to be wrapped.
	 * @param owner             The owner of the behaviour.
	 * @param priority          The priority of the behaviour, lower values mean higher priority.
	 */
	public Behaviour(String name, BehaviourTemplate<T> behaviourTemplate, T owner, int priority) {
		this.name = name;
		behaviourTemplate.owner = this;
		this.behaviourTemplate = behaviourTemplate;
		this.owner = owner;
		this.priority = priority;
		this.bindTemplate();
	}
	
	/**
	 * Binds all subscribers specified in the wrapped behaviour template.
	 */
	private void bindTemplate() {
		if (this.behaviourTemplate == null) return;
		for (MouseInputContext mouseContext : behaviourTemplate.moveSubscribers.keySet()) {
			if (mouseContext.isSubscribed((MouseMoveSubscriber) behaviourTemplate)) continue;
			
			mouseContext.subscribeToMove(behaviourTemplate);
		}
		
		for (MouseInputContext mouseContext : behaviourTemplate.scrollSubscribers.keySet()) {
			if (mouseContext.isSubscribed((MouseScrollSubscriber) behaviourTemplate)) continue;
			
			mouseContext.subscribeToScroll(behaviourTemplate);
		}
		
		for (Map.Entry<Bind, BindPressSubscriber> pressSubscriberEntry : behaviourTemplate.pressSubscribers.entries()) {
			pressSubscriberEntry.getKey().subscribe(pressSubscriberEntry.getValue());
		}
		
		for (Map.Entry<Bind, BindRepeatSubscriber> repeatSubscriberEntry : behaviourTemplate.repeatSubscribers.entries()) {
			repeatSubscriberEntry.getKey().subscribe(repeatSubscriberEntry.getValue());
		}
		
		for (Map.Entry<Bind, BindReleaseSubscriber> releaseSubscriberEntry : behaviourTemplate.releaseSubscribers.entries()) {
			releaseSubscriberEntry.getKey().subscribe(releaseSubscriberEntry.getValue());
		}
	}
	
	/**
	 * Unbinds all subscribers in the wrapped behaviour template.
	 */
	private void unbindTemplate() {
		if (this.behaviourTemplate == null) return;
		
		for (MouseInputContext mouseContext : behaviourTemplate.moveSubscribers.keySet()) {
			mouseContext.unsubscribeFromMove(behaviourTemplate);
		}
		
		for (MouseInputContext mouseContext : behaviourTemplate.scrollSubscribers.keySet()) {
			mouseContext.unsubscribeFromScroll(behaviourTemplate);
		}
		
		for (Map.Entry<Bind, BindPressSubscriber> pressSubscriberEntry : behaviourTemplate.pressSubscribers.entries()) {
			pressSubscriberEntry.getKey().unsubscribe(pressSubscriberEntry.getValue());
		}
		
		for (Map.Entry<Bind, BindRepeatSubscriber> repeatSubscriberEntry : behaviourTemplate.repeatSubscribers.entries()) {
			repeatSubscriberEntry.getKey().unsubscribe(repeatSubscriberEntry.getValue());
		}
		
		for (Map.Entry<Bind, BindReleaseSubscriber> releaseSubscriberEntry : behaviourTemplate.releaseSubscribers.entries()) {
			releaseSubscriberEntry.getKey().unsubscribe(releaseSubscriberEntry.getValue());
		}
	}
	
	/**
	 * Updates the behaviour, running the update function if the behaviour is enabled.
	 * <p>
	 * If an exception is raised the behaviour will disable itself.
	 *
	 * @param delta The time in seconds since the last update loop.
	 */
	public void update(float delta) {
		if (this.behaviourTemplate == null) return;
		
		try {
			this.behaviourTemplate.update(delta);
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
			LOGGER.warning("A " + e.getClass() +
					" occurred while running the behaviour " + this.name +
					", disabling the behaviour."
			);
			this.disable();
		}
	}
	
	/**
	 * Toggles whether this behaviour is enabled.
	 */
	public void toggle() {
		behaviourTemplate.toggle();
	}
	
	/**
	 * Enables this behaviour.
	 */
	public void enable() {
		behaviourTemplate.setEnabled(true);
	}
	
	/**
	 * Disables this behaviour.
	 */
	public void disable() {
		behaviourTemplate.setEnabled(false);
	}
	
	/**
	 * Destroys this behaviour, removing any links to binds.
	 */
	public void destroy() {
		this.unbindTemplate();
	}
	
	public boolean isEnabled() {
		return behaviourTemplate.isEnabled();
	}
	
	public void setEnabled(boolean enabled) {
		behaviourTemplate.setEnabled(enabled);
	}
}
