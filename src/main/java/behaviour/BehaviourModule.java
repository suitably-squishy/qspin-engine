package behaviour;

import state.UpdateFunction;

import java.util.*;

/**
 * Holds an objects behaviours and deals with their running and management.
 *
 * @param <T> The type of the owner of the behaviours.
 */
public class BehaviourModule<T> {
	
	/**
	 * A set of all the behaviours, stored in the order they should be called.
	 */
	private final NavigableSet<Behaviour<T>> behaviourSet;
	
	/**
	 * A map of behaviour names to behaviours.
	 */
	private final Map<String, Behaviour<T>> behaviourMap;
	
	/**
	 * The owner of the behaviour module and therefore that of the individual behaviours.
	 */
	private final T owner;
	
	private final static int DEFAULT_PRIORITY = 50;
	
	/**
	 * Creates a behaviour module with the given owner and behaviour comparator.
	 *
	 * @param owner               The owner of the behaviours.
	 * @param behaviourComparator The comparator to use to determine the order behaviours are called in.
	 */
	public BehaviourModule(T owner, Comparator<Behaviour<T>> behaviourComparator) {
		behaviourSet = new TreeSet<>(behaviourComparator);
		
		behaviourMap = new HashMap<>();
		this.owner = owner;
	}
	
	/**
	 * Creates a behaviour module with the given owner, orders the behaviours in order of priority, lowest values to highest.
	 *
	 * @param owner The owner of the behaviours.
	 */
	public BehaviourModule(T owner) {
		this(owner, Comparator.comparingInt(b -> b.priority));
	}
	
	/**
	 * Adds a behaviour with the given parameters to this module.
	 *
	 * @param name           The name of the behaviour to add.
	 * @param updateFunction The update function called when the behaviour is called.
	 * @param priority       The priority of the behaviour, lower number priority behaviours will be called before higher values.
	 */
	public void add(String name, UpdateFunction<T> updateFunction, int priority) {
		Behaviour<T> behaviour = new Behaviour<>(name, updateFunction, owner, priority);
		add(behaviour);
	}
	
	/**
	 * Adds a behaviour with the given parameters to this module.
	 *
	 * @param name              The name of the behaviour to add.
	 * @param behaviourTemplate The behaviour to add.
	 * @param priority          The priority of the behaviour, lower number priority behaviours will be called before higher values.
	 */
	public void add(String name, BehaviourTemplate<T> behaviourTemplate, int priority) {
		Behaviour<T> behaviour = new Behaviour<>(name, behaviourTemplate, owner, priority);
		add(behaviour);
	}
	
	/**
	 * Adds a behaviour with the given update function and name with the {@link BehaviourModule#DEFAULT_PRIORITY} to this module.
	 *
	 * @param name           The name of the behaviour to add.
	 * @param updateFunction The update function called when the behaviour is called.
	 */
	public void add(String name, UpdateFunction<T> updateFunction) {
		add(name, updateFunction, DEFAULT_PRIORITY);
	}
	
	/**
	 * Adds the given behaviour under the given name with the {@link BehaviourModule#DEFAULT_PRIORITY} to this module.
	 *
	 * @param name              The name of the behaviour to add.
	 * @param behaviourTemplate The behaviour to be added.
	 */
	public void add(String name, BehaviourTemplate<T> behaviourTemplate) {
		add(name, behaviourTemplate, DEFAULT_PRIORITY);
	}
	
	/**
	 * Adds the given wrapped behaviour to this module.
	 *
	 * @param behaviour The wrapped behaviour to add.
	 */
	public void add(Behaviour<T> behaviour) {
		behaviourSet.add(behaviour);
		behaviourMap.put(behaviour.name, behaviour);
	}
	
	/**
	 * Gets the behaviour with the given name, if present.
	 *
	 * @param behaviourName The name of the behaviour to fetch.
	 * @return A behaviour if present, null if not.
	 */
	public Behaviour<T> get(String behaviourName) {
		return behaviourMap.get(behaviourName);
	}
	
	/**
	 * Removes the given behaviour from the module.
	 *
	 * @param behaviour The behaviour to remove.
	 */
	public void remove(Behaviour<T> behaviour) {
		behaviour.destroy();
		behaviourSet.remove(behaviour);
		behaviourMap.remove(behaviour.name);
	}
	
	/**
	 * Removes the behaviour with the given name from the module.
	 *
	 * @param behaviourName The name of the behaviour to remove.
	 */
	public void remove(String behaviourName) {
		Behaviour<T> behaviour = behaviourMap.get(behaviourName);
		remove(behaviour);
	}
	
	/**
	 * Toggles whether the behaviour with the given name is enabled.
	 *
	 * @param behaviourName The name of the behaviour to toggle.
	 * @see Behaviour#toggle()
	 */
	public void toggle(String behaviourName) {
		Behaviour<T> behaviour = this.get(behaviourName);
		if (behaviour == null) return;
		
		behaviour.toggle();
	}
	
	/**
	 * Enables the behaviour with the given name.
	 *
	 * @param behaviourName The name of the behaviour to enable.
	 * @see Behaviour#enable()
	 */
	public void enable(String behaviourName) {
		Behaviour<T> behaviour = this.get(behaviourName);
		if (behaviour == null) return;
		
		behaviour.enable();
	}
	
	/**
	 * Disables the behaviour with the given name.
	 *
	 * @param behaviourName The name of the behaviour to disable.
	 * @see Behaviour#disable()
	 */
	public void disable(String behaviourName) {
		Behaviour<T> behaviour = this.get(behaviourName);
		if (behaviour == null) return;
		
		behaviour.disable();
	}
	
	/**
	 * Updates the behaviour module, running any behaviours that need updating.
	 *
	 * @param delta The time in seconds since the last update loop.
	 */
	public void update(float delta) {
		for (Behaviour<T> behaviour : behaviourSet) {
			if (!behaviour.isEnabled()) continue;
			
			behaviour.update(delta);
		}
	}
}
