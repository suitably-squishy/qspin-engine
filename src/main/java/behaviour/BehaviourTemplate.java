package behaviour;

import io.input.binds.Bind;
import io.input.binds.BindPressSubscriber;
import io.input.binds.BindReleaseSubscriber;
import io.input.binds.BindRepeatSubscriber;
import io.input.mouse.MouseInputContext;
import io.input.mouse.MouseMoveSubscriber;
import io.input.mouse.MouseScrollSubscriber;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.joml.Vector2d;
import state.UpdateFunction;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Represents a template of behaviour to exhibit by a {@link BehaviouralObject}
 *
 * @param <T> The type of the owner of the behaviour.
 */
public class BehaviourTemplate<T> implements BindPressSubscriber, BindRepeatSubscriber, BindReleaseSubscriber,
		MouseMoveSubscriber, MouseScrollSubscriber {
	
	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
	/**
	 * The behaviour which owns the instantiated template.
	 */
	protected Behaviour<T> owner;
	
	/**
	 * Whether or not the behaviour is currently enabled.
	 */
	private boolean enabled;
	
	/**
	 * The behaviour's update function to be called every update loop.
	 */
	protected UpdateFunction<T> updateFunction;
	
	/**
	 * A mapping of Binds to the press subscribers the behaviour requires.
	 */
	protected final MultiValuedMap<Bind, BindPressSubscriber> pressSubscribers;
	/**
	 * A mapping of Binds to the repeat subscribers the behaviour requires.
	 */
	protected final MultiValuedMap<Bind, BindRepeatSubscriber> repeatSubscribers;
	/**
	 * A mapping of Binds to the release subscribers the behaviour requires.
	 */
	protected final MultiValuedMap<Bind, BindReleaseSubscriber> releaseSubscribers;
	
	/**
	 * A map of bind names to binds for easier access.
	 */
	private final Map<String, Bind> binds;
	// FIXME Bind storage is overwritten when two binds of the same name are used.
	
	/**
	 * A mapping of mouse input contexts to any move subscribers the behaviour requires.
	 */
	protected final MultiValuedMap<MouseInputContext, MouseMoveSubscriber> moveSubscribers;
	/**
	 * A mapping of mouse input contexts to any scroll subscribers the behaviour requires.
	 */
	protected final MultiValuedMap<MouseInputContext, MouseScrollSubscriber> scrollSubscribers;
	
	/**
	 * Creates a behaviour template with the provided update function and owner.
	 *
	 * @param owner          The behaviour which wraps the template.
	 * @param updateFunction The update function to be called every update loop.
	 */
	public BehaviourTemplate(Behaviour<T> owner, UpdateFunction<T> updateFunction) {
		this(owner);
		this.updateFunction = updateFunction;
	}
	
	/**
	 * Creates an empty behaviour template with the provided owner.
	 *
	 * @param owner The behaviour which wraps the template.
	 */
	public BehaviourTemplate(Behaviour<T> owner) {
		this();
		this.owner = owner;
	}
	
	/**
	 * Creates a behaviour template with the provided update function. The behaviour has no owner.
	 *
	 * @param updateFunction The update function to run every update loop.
	 */
	public BehaviourTemplate(UpdateFunction<T> updateFunction) {
		this();
		this.updateFunction = updateFunction;
	}
	
	/**
	 * Creates an empty behaviour template with no owner.
	 */
	public BehaviourTemplate() {
		this.moveSubscribers = new HashSetValuedHashMap<>();
		this.scrollSubscribers = new HashSetValuedHashMap<>();
		
		this.pressSubscribers = new HashSetValuedHashMap<>();
		this.repeatSubscribers = new HashSetValuedHashMap<>();
		this.releaseSubscribers = new HashSetValuedHashMap<>();
		
		this.binds = new HashMap<>();
		this.setEnabled(true);
	}
	
	//TODO Add more protections to prevent template modification when in use.
	
	/**
	 * Sets the template to, when used, subscribe to the provided mouse context the provided move subscriber.
	 * <p>
	 * The subscriber will be disabled and enabled alongside the behaviour being toggled.
	 *
	 * @param mouseContext   The mouse context to listen to for move events.
	 * @param moveSubscriber The move subscriber to call when the mouse is moved.
	 */
	public void subscribe(MouseInputContext mouseContext, MouseMoveSubscriber moveSubscriber) {
		moveSubscribers.put(mouseContext, moveSubscriber);
	}
	
	/**
	 * Sets the template to, when used, subscribe to the provided mouse context the provided scroll subscriber.
	 * <p>
	 * The subscriber will be disabled and enabled alongside the behaviour being toggled.
	 *
	 * @param mouseContext     The mouse context to listen to for scroll events.
	 * @param scrollSubscriber The scroll subscriber to call when the scrollwheel is moved.
	 */
	public void subscribe(MouseInputContext mouseContext, MouseScrollSubscriber scrollSubscriber) {
		scrollSubscribers.put(mouseContext, scrollSubscriber);
	}
	
	/**
	 * Sets the template to, when used, subscribe to the provided press subscriber to the bind provided.
	 * <p>
	 * The subscriber will be disabled and enabled alongside the behaviour being toggled.
	 *
	 * @param bind            The bind to subscribe to.
	 * @param pressSubscriber The press subscriber to subscribe to the bind.
	 */
	public void subscribe(Bind bind, BindPressSubscriber pressSubscriber) {
		pressSubscribers.put(bind, pressSubscriber);
		binds.put(bind.getName(), bind);
	}
	
	/**
	 * Sets the template to, when used, subscribe to the provided repeat subscriber to the bind provided.
	 * <p>
	 * The subscriber will be disabled and enabled alongside the behaviour being toggled.
	 *
	 * @param bind             The bind to subscribe to.
	 * @param repeatSubscriber The repeat subscriber to subscribe to the bind.
	 */
	public void subscribe(Bind bind, BindRepeatSubscriber repeatSubscriber) {
		repeatSubscribers.put(bind, repeatSubscriber);
		binds.put(bind.getName(), bind);
	}
	
	/**
	 * Sets the template to, when used, subscribe to the provided release subscriber to the bind provided.
	 * <p>
	 * The subscriber will be disabled and enabled alongside the behaviour being toggled.
	 *
	 * @param bind              The bind to subscribe to.
	 * @param releaseSubscriber The release subscriber to subscribe to the bind.
	 */
	public void subscribe(Bind bind, BindReleaseSubscriber releaseSubscriber) {
		releaseSubscribers.put(bind, releaseSubscriber);
		binds.put(bind.getName(), bind);
	}
	
	@Override
	public boolean onPress(String bindName, int button, int scancode, int mods) {
		if (!this.isEnabled()) return false;
		
		// Fetches the bind to be used from the list available
		Bind bindTriggered = binds.get(bindName);
		if (bindTriggered == null) {
			LOGGER.warning("Bind " + bindName + " was sent to a behaviour, the behaviour has no reference to this bind." +
					" This should be impossible.");
			return false;
		}
		Collection<BindPressSubscriber> bindPressSubscribers = pressSubscribers.get(bindTriggered);
		
		boolean absorbed = false;
		for (BindPressSubscriber pressSubscriber : bindPressSubscribers) {
			absorbed = pressSubscriber.onPress(bindName, button, scancode, mods) || absorbed;
		}
		
		return absorbed;
	}
	
	@Override
	public boolean onRelease(String bindName, int button, int scancode, int mods) {
		if (!this.isEnabled()) return false;
		
		Bind bindTriggered = binds.get(bindName);
		if (bindTriggered == null) {
			LOGGER.warning("Bind " + bindName + " was sent to a behaviour, the behaviour has no reference to this bind." +
					" This should be impossible.");
			return false;
		}
		Collection<BindReleaseSubscriber> bindReleaseSubscribers = releaseSubscribers.get(bindTriggered);
		
		boolean absorbed = false;
		for (BindReleaseSubscriber releaseSubscriber : bindReleaseSubscribers) {
			absorbed = releaseSubscriber.onRelease(bindName, button, scancode, mods) || absorbed;
		}
		
		return absorbed;
	}
	
	@Override
	public boolean onRepeat(String bindName, int button, int scancode, int mods) {
		if (!this.isEnabled()) return false;
		
		Bind bindTriggered = binds.get(bindName);
		if (bindTriggered == null) {
			LOGGER.warning("Bind " + bindName + " was sent to a behaviour, the behaviour has no reference to this bind." +
					" This should be impossible.");
			return false;
		}
		Collection<BindRepeatSubscriber> bindRepeatSubscribers = repeatSubscribers.get(bindTriggered);
		
		boolean absorbed = false;
		for (BindRepeatSubscriber repeatSubscriber : bindRepeatSubscribers) {
			absorbed = repeatSubscriber.onRepeat(bindName, button, scancode, mods) || absorbed;
		}
		
		return absorbed;
	}
	
	@Override
	public void onMove(Vector2d mousePos, Vector2d delta) {
		if (!this.isEnabled()) return;
		
		for (MouseMoveSubscriber moveSubscriber : moveSubscribers.values()) {
			moveSubscriber.onMove(mousePos, delta);
		}
	}
	
	@Override
	public boolean onScroll(double xoffset, double yoffset) {
		if (!this.isEnabled()) return false;
		
		boolean absorbed = false;
		for (MouseScrollSubscriber scrollSubscriber : scrollSubscribers.values()) {
			absorbed = scrollSubscriber.onScroll(xoffset, yoffset) || absorbed;
		}
		return false;
	}
	
	/**
	 * Updates the behaviour, running the update function.
	 *
	 * @param delta The time in seconds since the last update.
	 */
	public void update(float delta) {
		if (!this.isEnabled()) return;
		if (this.updateFunction == null) return;
		
		this.updateFunction.update(delta, owner.owner);
	}
	
	public boolean isEnabled() {
		if (this.owner == null) return false;
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	/**
	 * Enables this behaviour.
	 */
	public void enable() {
		this.setEnabled(true);
	}
	
	/**
	 * Disables this behaviour.
	 */
	public void disable() {
		this.setEnabled(false);
	}
	
	/**
	 * Toggles whether this behaviour is enabled.
	 */
	public void toggle() {
		this.setEnabled(!this.enabled);
	}
	
}
