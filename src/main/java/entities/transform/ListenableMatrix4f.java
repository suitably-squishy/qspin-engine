package entities.transform;

/**
 * A {@link org.joml.Matrix4f} capable of providing change events to a listener when the internal state of the matrix changes.
 */
public class ListenableMatrix4f extends OperableMatrix4f implements TransformChangeListener {
	
	/**
	 * The listener to notify when the internal state changes.
	 */
	private TransformChangeListener listener;
	
	/**
	 * The depth of the call stack, an operation is fully complete when the callDepth reaches 0.
	 */
	private short callDepth;
	
	/**
	 * Creates a listenable matrix with the provided listener.
	 *
	 * @param listener The listener to notify when the internal state changes.
	 */
	public ListenableMatrix4f(TransformChangeListener listener) {
		this();
		this.setListener(listener);
	}
	
	/**
	 * Creates a blank listenable matrix without a listener.
	 */
	public ListenableMatrix4f() {
		this.callDepth = 0;
	}
	
	/**
	 * Sets the matrix's listener to the provided object.
	 *
	 * @param listener The listener to notify when the internal state changes.
	 */
	public void setListener(TransformChangeListener listener) {
		this.listener = listener;
	}
	
	@Override
	public ListenableMatrix4f opStart() {
		this.callDepth++;
		return (ListenableMatrix4f) super.opStart();
	}
	
	@Override
	public ListenableMatrix4f opEnd() {
		this.callDepth--;
		if (this.callDepth <= 0) {
			this.callDepth = 0;
			this.onTransformChange();
		}
		return (ListenableMatrix4f) super.opEnd();
	}
	
	@Override
	public void onTransformChange() {
		if (this.listener != null) {
			this.listener.onTransformChange();
		}
	}
}
