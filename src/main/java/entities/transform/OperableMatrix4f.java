package entities.transform;

import org.joml.*;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

/**
 * An {@link Operable} wrapper for a {@link Matrix4f}, allowing operations to be started and finished between function calls.
 * <p>
 * Wraps all state changing functions in {@link Matrix4f} allowing for operations to be monitored, including those that happen to destination {@link OperableMatrix4f}s passed as parameters.
 */
public class OperableMatrix4f extends Matrix4f implements Operable<OperableMatrix4f> {
	
	@Override
	public OperableMatrix4f m00(float m00) {
		this.opStart();
		return ((OperableMatrix4f) super.m00(m00)).opEnd();
	}
	
	@Override
	public Matrix4f m01(float m01) {
		this.opStart();
		return ((OperableMatrix4f) super.m01(m01)).opEnd();
	}
	
	@Override
	public Matrix4f m02(float m02) {
		this.opStart();
		return ((OperableMatrix4f) super.m02(m02)).opEnd();
	}
	
	@Override
	public Matrix4f m03(float m03) {
		this.opStart();
		return ((OperableMatrix4f) super.m03(m03)).opEnd();
	}
	
	@Override
	public Matrix4f m10(float m10) {
		this.opStart();
		return ((OperableMatrix4f) super.m10(m10)).opEnd();
	}
	
	@Override
	public Matrix4f m11(float m11) {
		this.opStart();
		return ((OperableMatrix4f) super.m11(m11)).opEnd();
	}
	
	@Override
	public Matrix4f m12(float m12) {
		this.opStart();
		return ((OperableMatrix4f) super.m12(m12)).opEnd();
	}
	
	@Override
	public Matrix4f m13(float m13) {
		this.opStart();
		return ((OperableMatrix4f) super.m13(m13)).opEnd();
	}
	
	@Override
	public Matrix4f m20(float m20) {
		this.opStart();
		return ((OperableMatrix4f) super.m20(m20)).opEnd();
	}
	
	@Override
	public Matrix4f m21(float m21) {
		this.opStart();
		return ((OperableMatrix4f) super.m21(m21)).opEnd();
	}
	
	@Override
	public Matrix4f m22(float m22) {
		this.opStart();
		return ((OperableMatrix4f) super.m22(m22)).opEnd();
	}
	
	@Override
	public Matrix4f m23(float m23) {
		this.opStart();
		return ((OperableMatrix4f) super.m23(m23)).opEnd();
	}
	
	@Override
	public Matrix4f m30(float m30) {
		this.opStart();
		return ((OperableMatrix4f) super.m30(m30)).opEnd();
	}
	
	@Override
	public Matrix4f m31(float m31) {
		this.opStart();
		return ((OperableMatrix4f) super.m31(m31)).opEnd();
	}
	
	@Override
	public Matrix4f m32(float m32) {
		this.opStart();
		return ((OperableMatrix4f) super.m32(m32)).opEnd();
	}
	
	@Override
	public Matrix4f m33(float m33) {
		this.opStart();
		return ((OperableMatrix4f) super.m33(m33)).opEnd();
	}
	
	@Override
	public OperableMatrix4f identity() {
		this.opStart();
		return ((OperableMatrix4f) super.identity()).opEnd();
	}
	
	@Override
	public OperableMatrix4f set(Matrix4fc m) {
		this.opStart();
		return ((OperableMatrix4f) super.set(m)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setTransposed(Matrix4fc m) {
		this.opStart();
		return ((OperableMatrix4f) super.setTransposed(m)).opEnd();
	}
	
	@Override
	public OperableMatrix4f set(Matrix4x3fc m) {
		this.opStart();
		return ((OperableMatrix4f) super.set(m)).opEnd();
	}
	
	@Override
	public OperableMatrix4f set(Matrix4dc m) {
		this.opStart();
		return ((OperableMatrix4f) super.set(m)).opEnd();
	}
	
	@Override
	public OperableMatrix4f set(Matrix3fc mat) {
		this.opStart();
		return ((OperableMatrix4f) super.set(mat)).opEnd();
	}
	
	@Override
	public OperableMatrix4f set(AxisAngle4f axisAngle) {
		this.opStart();
		return ((OperableMatrix4f) super.set(axisAngle)).opEnd();
	}
	
	@Override
	public OperableMatrix4f set(AxisAngle4d axisAngle) {
		this.opStart();
		return ((OperableMatrix4f) super.set(axisAngle)).opEnd();
	}
	
	@Override
	public OperableMatrix4f set(Quaternionfc q) {
		this.opStart();
		return ((OperableMatrix4f) super.set(q)).opEnd();
	}
	
	@Override
	public OperableMatrix4f set(Quaterniondc q) {
		this.opStart();
		return ((OperableMatrix4f) super.set(q)).opEnd();
	}
	
	@Override
	public Matrix4f set3x3(Matrix4f mat) {
		this.opStart();
		return ((OperableMatrix4f) super.set3x3(mat)).opEnd();
	}
	
	@Override
	public Matrix4f set4x3(Matrix4x3fc mat) {
		this.opStart();
		return ((OperableMatrix4f) super.set4x3(mat)).opEnd();
	}
	
	@Override
	public Matrix4f set4x3(Matrix4f mat) {
		this.opStart();
		return ((OperableMatrix4f) super.set4x3(mat)).opEnd();
	}
	
	@Override
	public OperableMatrix4f mul(Matrix4fc right) {
		this.opStart();
		return ((OperableMatrix4f) super.mul(right)).opEnd();
	}
	
	@Override
	public Matrix4f mul(Matrix4fc right, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mul(right, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mul(right, dest);
		}
	}
	
	@Override
	public OperableMatrix4f mul0(Matrix4fc right) {
		this.opStart();
		return ((OperableMatrix4f) super.mul0(right)).opEnd();
	}
	
	@Override
	public Matrix4f mul0(Matrix4fc right, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mul0(right, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mul0(right, dest);
		}
	}
	
	@Override
	public OperableMatrix4f mul(float r00, float r01, float r02, float r03, float r10, float r11, float r12, float r13, float r20, float r21, float r22, float r23, float r30, float r31, float r32, float r33) {
		this.opStart();
		return ((OperableMatrix4f) super.mul(r00, r01, r02, r03, r10, r11, r12, r13, r20, r21, r22, r23, r30, r31, r32, r33)).opEnd();
	}
	
	@Override
	public Matrix4f mul(float r00, float r01, float r02, float r03, float r10, float r11, float r12, float r13, float r20, float r21, float r22, float r23, float r30, float r31, float r32, float r33, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mul(r00, r01, r02, r03, r10, r11, r12, r13, r20, r21, r22, r23, r30, r31, r32, r33, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mul(r00, r01, r02, r03, r10, r11, r12, r13, r20, r21, r22, r23, r30, r31, r32, r33, dest);
		}
	}
	
	@Override
	public Matrix4f mul3x3(float r00, float r01, float r02, float r10, float r11, float r12, float r20, float r21, float r22) {
		this.opStart();
		return ((OperableMatrix4f) super.mul3x3(r00, r01, r02, r10, r11, r12, r20, r21, r22)).opEnd();
	}
	
	@Override
	public Matrix4f mul3x3(float r00, float r01, float r02, float r10, float r11, float r12, float r20, float r21, float r22, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mul3x3(r00, r01, r02, r10, r11, r12, r20, r21, r22, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mul3x3(r00, r01, r02, r10, r11, r12, r20, r21, r22, dest);
		}
	}
	
	@Override
	public OperableMatrix4f mulLocal(Matrix4fc left) {
		this.opStart();
		return ((OperableMatrix4f) super.mulLocal(left)).opEnd();
	}
	
	@Override
	public Matrix4f mulLocal(Matrix4fc left, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mulLocal(left, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mulLocal(left, dest);
		}
	}
	
	@Override
	public OperableMatrix4f mulLocalAffine(Matrix4fc left) {
		this.opStart();
		return ((OperableMatrix4f) super.mulLocalAffine(left)).opEnd();
	}
	
	@Override
	public Matrix4f mulLocalAffine(Matrix4fc left, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mulLocalAffine(left, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mulLocalAffine(left, dest);
		}
	}
	
	@Override
	public OperableMatrix4f mul(Matrix4x3fc right) {
		this.opStart();
		return ((OperableMatrix4f) super.mul(right)).opEnd();
	}
	
	@Override
	public Matrix4f mul(Matrix4x3fc right, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mul(right, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mul(right, dest);
		}
	}
	
	@Override
	public OperableMatrix4f mul(Matrix3x2fc right) {
		this.opStart();
		return ((OperableMatrix4f) super.mul(right)).opEnd();
	}
	
	@Override
	public Matrix4f mul(Matrix3x2fc right, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mul(right, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mul(right, dest);
		}
	}
	
	@Override
	public OperableMatrix4f mulPerspectiveAffine(Matrix4fc view) {
		this.opStart();
		return ((OperableMatrix4f) super.mulPerspectiveAffine(view)).opEnd();
	}
	
	@Override
	public Matrix4f mulPerspectiveAffine(Matrix4fc view, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mulPerspectiveAffine(view, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mulPerspectiveAffine(view, dest);
		}
	}
	
	@Override
	public OperableMatrix4f mulPerspectiveAffine(Matrix4x3fc view) {
		this.opStart();
		return ((OperableMatrix4f) super.mulPerspectiveAffine(view)).opEnd();
	}
	
	@Override
	public Matrix4f mulPerspectiveAffine(Matrix4x3fc view, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mulPerspectiveAffine(view, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mulPerspectiveAffine(view, dest);
		}
	}
	
	@Override
	public OperableMatrix4f mulAffineR(Matrix4fc right) {
		this.opStart();
		return ((OperableMatrix4f) super.mulAffineR(right)).opEnd();
	}
	
	@Override
	public Matrix4f mulAffineR(Matrix4fc right, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mulAffineR(right, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mulAffineR(right, dest);
		}
	}
	
	@Override
	public OperableMatrix4f mulAffine(Matrix4fc right) {
		this.opStart();
		return ((OperableMatrix4f) super.mulAffine(right)).opEnd();
	}
	
	@Override
	public Matrix4f mulAffine(Matrix4fc right, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mulAffine(right, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mulAffine(right, dest);
		}
	}
	
	@Override
	public Matrix4f mulTranslationAffine(Matrix4fc right, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mulTranslationAffine(right, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mulTranslationAffine(right, dest);
		}
	}
	
	@Override
	public OperableMatrix4f mulOrthoAffine(Matrix4fc view) {
		this.opStart();
		return ((OperableMatrix4f) super.mulOrthoAffine(view)).opEnd();
	}
	
	@Override
	public Matrix4f mulOrthoAffine(Matrix4fc view, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mulOrthoAffine(view, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mulOrthoAffine(view, dest);
		}
	}
	
	@Override
	public Matrix4f fma4x3(Matrix4fc other, float otherFactor) {
		this.opStart();
		return ((OperableMatrix4f) super.fma4x3(other, otherFactor)).opEnd();
	}
	
	@Override
	public Matrix4f fma4x3(Matrix4fc other, float otherFactor, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.fma4x3(other, otherFactor, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.fma4x3(other, otherFactor, dest);
		}
	}
	
	@Override
	public OperableMatrix4f add(Matrix4fc other) {
		this.opStart();
		return ((OperableMatrix4f) super.add(other)).opEnd();
	}
	
	@Override
	public Matrix4f add(Matrix4fc other, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.add(other, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.add(other, dest);
		}
	}
	
	@Override
	public OperableMatrix4f sub(Matrix4fc subtrahend) {
		this.opStart();
		return ((OperableMatrix4f) super.sub(subtrahend)).opEnd();
	}
	
	@Override
	public Matrix4f sub(Matrix4fc subtrahend, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.sub(subtrahend, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.sub(subtrahend, dest);
		}
	}
	
	@Override
	public OperableMatrix4f mulComponentWise(Matrix4fc other) {
		this.opStart();
		return ((OperableMatrix4f) super.mulComponentWise(other)).opEnd();
	}
	
	@Override
	public Matrix4f mulComponentWise(Matrix4fc other, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mulComponentWise(other, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mulComponentWise(other, dest);
		}
	}
	
	@Override
	public Matrix4f add4x3(Matrix4fc other) {
		this.opStart();
		return ((OperableMatrix4f) super.add4x3(other)).opEnd();
	}
	
	@Override
	public Matrix4f add4x3(Matrix4fc other, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.add4x3(other, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.add4x3(other, dest);
		}
	}
	
	@Override
	public Matrix4f sub4x3(Matrix4f subtrahend) {
		this.opStart();
		return ((OperableMatrix4f) super.sub4x3(subtrahend)).opEnd();
	}
	
	@Override
	public Matrix4f sub4x3(Matrix4fc subtrahend, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.sub4x3(subtrahend, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.sub4x3(subtrahend, dest);
		}
	}
	
	@Override
	public Matrix4f mul4x3ComponentWise(Matrix4fc other) {
		this.opStart();
		return ((OperableMatrix4f) super.mul4x3ComponentWise(other)).opEnd();
	}
	
	@Override
	public Matrix4f mul4x3ComponentWise(Matrix4fc other, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.mul4x3ComponentWise(other, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.mul4x3ComponentWise(other, dest);
		}
	}
	
	@Override
	public OperableMatrix4f set(float m00, float m01, float m02, float m03, float m10, float m11, float m12, float m13, float m20, float m21, float m22, float m23, float m30, float m31, float m32, float m33) {
		this.opStart();
		return ((OperableMatrix4f) super.set(m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33)).opEnd();
	}
	
	@Override
	public OperableMatrix4f set(float[] m, int off) {
		this.opStart();
		return ((OperableMatrix4f) super.set(m, off)).opEnd();
	}
	
	@Override
	public OperableMatrix4f set(float[] m) {
		this.opStart();
		return ((OperableMatrix4f) super.set(m)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setTransposed(float[] m, int off) {
		this.opStart();
		return ((OperableMatrix4f) super.setTransposed(m, off)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setTransposed(float[] m) {
		this.opStart();
		return ((OperableMatrix4f) super.setTransposed(m)).opEnd();
	}
	
	@Override
	public OperableMatrix4f set(FloatBuffer buffer) {
		this.opStart();
		return ((OperableMatrix4f) super.set(buffer)).opEnd();
	}
	
	@Override
	public OperableMatrix4f set(ByteBuffer buffer) {
		this.opStart();
		return ((OperableMatrix4f) super.set(buffer)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setTransposed(FloatBuffer buffer) {
		this.opStart();
		return ((OperableMatrix4f) super.setTransposed(buffer)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setTransposed(ByteBuffer buffer) {
		this.opStart();
		return ((OperableMatrix4f) super.setTransposed(buffer)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setFromAddress(long address) {
		this.opStart();
		return ((OperableMatrix4f) super.setFromAddress(address)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setTransposedFromAddress(long address) {
		this.opStart();
		return ((OperableMatrix4f) super.setTransposedFromAddress(address)).opEnd();
	}
	
	@Override
	public OperableMatrix4f set(Vector4fc col0, Vector4fc col1, Vector4fc col2, Vector4fc col3) {
		this.opStart();
		return ((OperableMatrix4f) super.set(col0, col1, col2, col3)).opEnd();
	}
	
	@Override
	public Matrix4f invert(Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.invert(((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.invert(dest);
		}
	}
	
	@Override
	public OperableMatrix4f invert() {
		this.opStart();
		return ((OperableMatrix4f) super.invert()).opEnd();
	}
	
	@Override
	public Matrix4f invertPerspective(Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.invertPerspective(((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.invertPerspective(dest);
		}
	}
	
	@Override
	public OperableMatrix4f invertPerspective() {
		this.opStart();
		return ((OperableMatrix4f) super.invertPerspective()).opEnd();
	}
	
	@Override
	public Matrix4f invertFrustum(Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.invertFrustum(((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.invertFrustum(dest);
		}
	}
	
	@Override
	public OperableMatrix4f invertFrustum() {
		this.opStart();
		return ((OperableMatrix4f) super.invertFrustum()).opEnd();
	}
	
	@Override
	public Matrix4f invertOrtho(Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.invertOrtho(((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.invertOrtho(dest);
		}
	}
	
	@Override
	public OperableMatrix4f invertOrtho() {
		this.opStart();
		return ((OperableMatrix4f) super.invertOrtho()).opEnd();
	}
	
	@Override
	public Matrix4f invertPerspectiveView(Matrix4fc view, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.invertPerspectiveView(view, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.invertPerspectiveView(view, dest);
		}
	}
	
	@Override
	public Matrix4f invertPerspectiveView(Matrix4x3fc view, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.invertPerspectiveView(view, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.invertPerspectiveView(view, dest);
		}
	}
	
	@Override
	public Matrix4f invertAffine(Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.invertAffine(((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.invertAffine(dest);
		}
	}
	
	@Override
	public OperableMatrix4f invertAffine() {
		this.opStart();
		return ((OperableMatrix4f) super.invertAffine()).opEnd();
	}
	
	@Override
	public Matrix4f transpose(Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.transpose(((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.transpose(dest);
		}
	}
	
	@Override
	public Matrix4f transpose3x3() {
		this.opStart();
		return ((OperableMatrix4f) super.transpose3x3()).opEnd();
	}
	
	@Override
	public Matrix4f transpose3x3(Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.transpose3x3(((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.transpose3x3(dest);
		}
	}
	
	@Override
	public Matrix3f transpose3x3(Matrix3f dest) {
		return super.transpose3x3(dest);
	}
	
	@Override
	public OperableMatrix4f transpose() {
		this.opStart();
		return ((OperableMatrix4f) super.transpose()).opEnd();
	}
	
	@Override
	public OperableMatrix4f translation(float x, float y, float z) {
		this.opStart();
		return ((OperableMatrix4f) super.translation(x, y, z)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translation(Vector3fc offset) {
		this.opStart();
		return ((OperableMatrix4f) super.translation(offset)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setTranslation(float x, float y, float z) {
		this.opStart();
		return ((OperableMatrix4f) super.setTranslation(x, y, z)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setTranslation(Vector3fc xyz) {
		this.opStart();
		return ((OperableMatrix4f) super.setTranslation(xyz)).opEnd();
	}
	
	@Override
	public Matrix4f get(Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.get(((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.get(dest);
		}
	}
	
	@Override
	public OperableMatrix4f zero() {
		this.opStart();
		return ((OperableMatrix4f) super.zero()).opEnd();
	}
	
	@Override
	public OperableMatrix4f scaling(float factor) {
		this.opStart();
		return ((OperableMatrix4f) super.scaling(factor)).opEnd();
	}
	
	@Override
	public OperableMatrix4f scaling(float x, float y, float z) {
		this.opStart();
		return ((OperableMatrix4f) super.scaling(x, y, z)).opEnd();
	}
	
	@Override
	public OperableMatrix4f scaling(Vector3fc xyz) {
		this.opStart();
		return ((OperableMatrix4f) super.scaling(xyz)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotation(float angle, Vector3fc axis) {
		this.opStart();
		return ((OperableMatrix4f) super.rotation(angle, axis)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotation(AxisAngle4f axisAngle) {
		this.opStart();
		return ((OperableMatrix4f) super.rotation(axisAngle)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotation(float angle, float x, float y, float z) {
		this.opStart();
		return ((OperableMatrix4f) super.rotation(angle, x, y, z)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotationX(float ang) {
		this.opStart();
		return ((OperableMatrix4f) super.rotationX(ang)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotationY(float ang) {
		this.opStart();
		return ((OperableMatrix4f) super.rotationY(ang)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotationZ(float ang) {
		this.opStart();
		return ((OperableMatrix4f) super.rotationZ(ang)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotationTowardsXY(float dirX, float dirY) {
		this.opStart();
		return ((OperableMatrix4f) super.rotationTowardsXY(dirX, dirY)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotationXYZ(float angleX, float angleY, float angleZ) {
		this.opStart();
		return ((OperableMatrix4f) super.rotationXYZ(angleX, angleY, angleZ)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotationZYX(float angleZ, float angleY, float angleX) {
		this.opStart();
		return ((OperableMatrix4f) super.rotationZYX(angleZ, angleY, angleX)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotationYXZ(float angleY, float angleX, float angleZ) {
		this.opStart();
		return ((OperableMatrix4f) super.rotationYXZ(angleY, angleX, angleZ)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setRotationXYZ(float angleX, float angleY, float angleZ) {
		this.opStart();
		return ((OperableMatrix4f) super.setRotationXYZ(angleX, angleY, angleZ)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setRotationZYX(float angleZ, float angleY, float angleX) {
		this.opStart();
		return ((OperableMatrix4f) super.setRotationZYX(angleZ, angleY, angleX)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setRotationYXZ(float angleY, float angleX, float angleZ) {
		this.opStart();
		return ((OperableMatrix4f) super.setRotationYXZ(angleY, angleX, angleZ)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotation(Quaternionfc quat) {
		this.opStart();
		return ((OperableMatrix4f) super.rotation(quat)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translationRotateScale(float tx, float ty, float tz, float qx, float qy, float qz, float qw, float sx, float sy, float sz) {
		this.opStart();
		return ((OperableMatrix4f) super.translationRotateScale(tx, ty, tz, qx, qy, qz, qw, sx, sy, sz)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translationRotateScale(Vector3fc translation, Quaternionfc quat, Vector3fc scale) {
		this.opStart();
		return ((OperableMatrix4f) super.translationRotateScale(translation, quat, scale)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translationRotateScale(float tx, float ty, float tz, float qx, float qy, float qz, float qw, float scale) {
		this.opStart();
		return ((OperableMatrix4f) super.translationRotateScale(tx, ty, tz, qx, qy, qz, qw, scale)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translationRotateScale(Vector3fc translation, Quaternionfc quat, float scale) {
		this.opStart();
		return ((OperableMatrix4f) super.translationRotateScale(translation, quat, scale)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translationRotateScaleInvert(float tx, float ty, float tz, float qx, float qy, float qz, float qw, float sx, float sy, float sz) {
		this.opStart();
		return ((OperableMatrix4f) super.translationRotateScaleInvert(tx, ty, tz, qx, qy, qz, qw, sx, sy, sz)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translationRotateScaleInvert(Vector3fc translation, Quaternionfc quat, Vector3fc scale) {
		this.opStart();
		return ((OperableMatrix4f) super.translationRotateScaleInvert(translation, quat, scale)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translationRotateScaleInvert(Vector3fc translation, Quaternionfc quat, float scale) {
		this.opStart();
		return ((OperableMatrix4f) super.translationRotateScaleInvert(translation, quat, scale)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translationRotateScaleMulAffine(float tx, float ty, float tz, float qx, float qy, float qz, float qw, float sx, float sy, float sz, Matrix4f m) {
		this.opStart();
		return ((OperableMatrix4f) super.translationRotateScaleMulAffine(tx, ty, tz, qx, qy, qz, qw, sx, sy, sz, m)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translationRotateScaleMulAffine(Vector3fc translation, Quaternionfc quat, Vector3fc scale, Matrix4f m) {
		this.opStart();
		return ((OperableMatrix4f) super.translationRotateScaleMulAffine(translation, quat, scale, m)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translationRotate(float tx, float ty, float tz, float qx, float qy, float qz, float qw) {
		this.opStart();
		return ((OperableMatrix4f) super.translationRotate(tx, ty, tz, qx, qy, qz, qw)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translationRotate(float tx, float ty, float tz, Quaternionfc quat) {
		this.opStart();
		return ((OperableMatrix4f) super.translationRotate(tx, ty, tz, quat)).opEnd();
	}
	
	@Override
	public Matrix4f set3x3(Matrix3fc mat) {
		this.opStart();
		return ((OperableMatrix4f) super.set3x3(mat)).opEnd();
	}
	
	@Override
	public Matrix4f scale(Vector3fc xyz, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.scale(xyz, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.scale(xyz, dest);
		}
	}
	
	@Override
	public OperableMatrix4f scale(Vector3fc xyz) {
		this.opStart();
		return ((OperableMatrix4f) super.scale(xyz)).opEnd();
	}
	
	@Override
	public Matrix4f scale(float xyz, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.scale(xyz, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.scale(xyz, dest);
		}
	}
	
	@Override
	public OperableMatrix4f scale(float xyz) {
		this.opStart();
		return ((OperableMatrix4f) super.scale(xyz)).opEnd();
	}
	
	@Override
	public Matrix4f scaleXY(float x, float y, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.scaleXY(x, y, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.scaleXY(x, y, dest);
		}
	}
	
	@Override
	public OperableMatrix4f scaleXY(float x, float y) {
		this.opStart();
		return ((OperableMatrix4f) super.scaleXY(x, y)).opEnd();
	}
	
	@Override
	public Matrix4f scale(float x, float y, float z, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.scale(x, y, z, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.scale(x, y, z, dest);
		}
	}
	
	@Override
	public OperableMatrix4f scale(float x, float y, float z) {
		this.opStart();
		return ((OperableMatrix4f) super.scale(x, y, z)).opEnd();
	}
	
	@Override
	public Matrix4f scaleAround(float sx, float sy, float sz, float ox, float oy, float oz, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.scaleAround(sx, sy, sz, ox, oy, oz, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.scaleAround(sx, sy, sz, ox, oy, oz, dest);
		}
	}
	
	@Override
	public OperableMatrix4f scaleAround(float sx, float sy, float sz, float ox, float oy, float oz) {
		this.opStart();
		return ((OperableMatrix4f) super.scaleAround(sx, sy, sz, ox, oy, oz)).opEnd();
	}
	
	@Override
	public OperableMatrix4f scaleAround(float factor, float ox, float oy, float oz) {
		this.opStart();
		return ((OperableMatrix4f) super.scaleAround(factor, ox, oy, oz)).opEnd();
	}
	
	@Override
	public Matrix4f scaleAround(float factor, float ox, float oy, float oz, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.scaleAround(factor, ox, oy, oz, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.scaleAround(factor, ox, oy, oz, dest);
		}
	}
	
	@Override
	public Matrix4f scaleLocal(float x, float y, float z, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.scaleLocal(x, y, z, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.scaleLocal(x, y, z, dest);
		}
	}
	
	@Override
	public Matrix4f scaleLocal(float xyz, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.scaleLocal(xyz, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.scaleLocal(xyz, dest);
		}
	}
	
	@Override
	public OperableMatrix4f scaleLocal(float xyz) {
		this.opStart();
		return ((OperableMatrix4f) super.scaleLocal(xyz)).opEnd();
	}
	
	@Override
	public OperableMatrix4f scaleLocal(float x, float y, float z) {
		this.opStart();
		return ((OperableMatrix4f) super.scaleLocal(x, y, z)).opEnd();
	}
	
	@Override
	public Matrix4f scaleAroundLocal(float sx, float sy, float sz, float ox, float oy, float oz, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.scaleAroundLocal(sx, sy, sz, ox, oy, oz, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.scaleAroundLocal(sx, sy, sz, ox, oy, oz, dest);
		}
	}
	
	@Override
	public OperableMatrix4f scaleAroundLocal(float sx, float sy, float sz, float ox, float oy, float oz) {
		this.opStart();
		return ((OperableMatrix4f) super.scaleAroundLocal(sx, sy, sz, ox, oy, oz)).opEnd();
	}
	
	@Override
	public OperableMatrix4f scaleAroundLocal(float factor, float ox, float oy, float oz) {
		this.opStart();
		return ((OperableMatrix4f) super.scaleAroundLocal(factor, ox, oy, oz)).opEnd();
	}
	
	@Override
	public Matrix4f scaleAroundLocal(float factor, float ox, float oy, float oz, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.scaleAroundLocal(factor, ox, oy, oz, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.scaleAroundLocal(factor, ox, oy, oz, dest);
		}
	}
	
	@Override
	public Matrix4f rotateX(float ang, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateX(ang, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateX(ang, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateX(float ang) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateX(ang)).opEnd();
	}
	
	@Override
	public Matrix4f rotateY(float ang, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateY(ang, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateY(ang, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateY(float ang) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateY(ang)).opEnd();
	}
	
	@Override
	public Matrix4f rotateZ(float ang, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateZ(ang, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateZ(ang, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateZ(float ang) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateZ(ang)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotateTowardsXY(float dirX, float dirY) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateTowardsXY(dirX, dirY)).opEnd();
	}
	
	@Override
	public Matrix4f rotateTowardsXY(float dirX, float dirY, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateTowardsXY(dirX, dirY, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateTowardsXY(dirX, dirY, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateXYZ(Vector3fc angles) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateXYZ(angles)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotateXYZ(float angleX, float angleY, float angleZ) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateXYZ(angleX, angleY, angleZ)).opEnd();
	}
	
	@Override
	public Matrix4f rotateXYZ(float angleX, float angleY, float angleZ, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateXYZ(angleX, angleY, angleZ, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateXYZ(angleX, angleY, angleZ, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateAffineXYZ(float angleX, float angleY, float angleZ) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateAffineXYZ(angleX, angleY, angleZ)).opEnd();
	}
	
	@Override
	public Matrix4f rotateAffineXYZ(float angleX, float angleY, float angleZ, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateAffineXYZ(angleX, angleY, angleZ, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateAffineXYZ(angleX, angleY, angleZ, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateZYX(Vector3f angles) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateZYX(angles)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotateZYX(float angleZ, float angleY, float angleX) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateZYX(angleZ, angleY, angleX)).opEnd();
	}
	
	@Override
	public Matrix4f rotateZYX(float angleZ, float angleY, float angleX, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateZYX(angleZ, angleY, angleX, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateZYX(angleZ, angleY, angleX, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateAffineZYX(float angleZ, float angleY, float angleX) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateAffineZYX(angleZ, angleY, angleX)).opEnd();
	}
	
	@Override
	public Matrix4f rotateAffineZYX(float angleZ, float angleY, float angleX, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateAffineZYX(angleZ, angleY, angleX, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateAffineZYX(angleZ, angleY, angleX, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateYXZ(Vector3f angles) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateYXZ(angles)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotateYXZ(float angleY, float angleX, float angleZ) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateYXZ(angleY, angleX, angleZ)).opEnd();
	}
	
	@Override
	public Matrix4f rotateYXZ(float angleY, float angleX, float angleZ, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateYXZ(angleY, angleX, angleZ, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateYXZ(angleY, angleX, angleZ, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateAffineYXZ(float angleY, float angleX, float angleZ) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateAffineYXZ(angleY, angleX, angleZ)).opEnd();
	}
	
	@Override
	public Matrix4f rotateAffineYXZ(float angleY, float angleX, float angleZ, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateAffineYXZ(angleY, angleX, angleZ, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateAffineYXZ(angleY, angleX, angleZ, dest);
		}
	}
	
	@Override
	public Matrix4f rotate(float ang, float x, float y, float z, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotate(ang, x, y, z, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotate(ang, x, y, z, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotate(float ang, float x, float y, float z) {
		this.opStart();
		return ((OperableMatrix4f) super.rotate(ang, x, y, z)).opEnd();
	}
	
	@Override
	public Matrix4f rotateTranslation(float ang, float x, float y, float z, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateTranslation(ang, x, y, z, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateTranslation(ang, x, y, z, dest);
		}
	}
	
	@Override
	public Matrix4f rotateAffine(float ang, float x, float y, float z, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateAffine(ang, x, y, z, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateAffine(ang, x, y, z, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateAffine(float ang, float x, float y, float z) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateAffine(ang, x, y, z)).opEnd();
	}
	
	@Override
	public Matrix4f rotateLocal(float ang, float x, float y, float z, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateLocal(ang, x, y, z, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateLocal(ang, x, y, z, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateLocal(float ang, float x, float y, float z) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateLocal(ang, x, y, z)).opEnd();
	}
	
	@Override
	public Matrix4f rotateLocalX(float ang, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateLocalX(ang, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateLocalX(ang, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateLocalX(float ang) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateLocalX(ang)).opEnd();
	}
	
	@Override
	public Matrix4f rotateLocalY(float ang, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateLocalY(ang, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateLocalY(ang, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateLocalY(float ang) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateLocalY(ang)).opEnd();
	}
	
	@Override
	public Matrix4f rotateLocalZ(float ang, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateLocalZ(ang, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateLocalZ(ang, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateLocalZ(float ang) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateLocalZ(ang)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translate(Vector3fc offset) {
		this.opStart();
		return ((OperableMatrix4f) super.translate(offset)).opEnd();
	}
	
	@Override
	public Matrix4f translate(Vector3fc offset, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.translate(offset, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.translate(offset, dest);
		}
	}
	
	@Override
	public Matrix4f translate(float x, float y, float z, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.translate(x, y, z, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.translate(x, y, z, dest);
		}
	}
	
	@Override
	public OperableMatrix4f translate(float x, float y, float z) {
		this.opStart();
		return ((OperableMatrix4f) super.translate(x, y, z)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translateLocal(Vector3fc offset) {
		this.opStart();
		return ((OperableMatrix4f) super.translateLocal(offset)).opEnd();
	}
	
	@Override
	public Matrix4f translateLocal(Vector3fc offset, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.translateLocal(offset, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.translateLocal(offset, dest);
		}
	}
	
	@Override
	public Matrix4f translateLocal(float x, float y, float z, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.translateLocal(x, y, z, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.translateLocal(x, y, z, dest);
		}
	}
	
	@Override
	public OperableMatrix4f translateLocal(float x, float y, float z) {
		this.opStart();
		return ((OperableMatrix4f) super.translateLocal(x, y, z)).opEnd();
	}
	
	@Override
	public Matrix4f ortho(float left, float right, float bottom, float top, float zNear, float zFar, boolean zZeroToOne, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.ortho(left, right, bottom, top, zNear, zFar, zZeroToOne, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.ortho(left, right, bottom, top, zNear, zFar, zZeroToOne, dest);
		}
	}
	
	@Override
	public Matrix4f ortho(float left, float right, float bottom, float top, float zNear, float zFar, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.ortho(left, right, bottom, top, zNear, zFar, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.ortho(left, right, bottom, top, zNear, zFar, dest);
		}
	}
	
	@Override
	public OperableMatrix4f ortho(float left, float right, float bottom, float top, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.ortho(left, right, bottom, top, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f ortho(float left, float right, float bottom, float top, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.ortho(left, right, bottom, top, zNear, zFar)).opEnd();
	}
	
	@Override
	public Matrix4f orthoLH(float left, float right, float bottom, float top, float zNear, float zFar, boolean zZeroToOne, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.orthoLH(left, right, bottom, top, zNear, zFar, zZeroToOne, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.orthoLH(left, right, bottom, top, zNear, zFar, zZeroToOne, dest);
		}
	}
	
	@Override
	public Matrix4f orthoLH(float left, float right, float bottom, float top, float zNear, float zFar, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.orthoLH(left, right, bottom, top, zNear, zFar, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.orthoLH(left, right, bottom, top, zNear, zFar, dest);
		}
	}
	
	@Override
	public OperableMatrix4f orthoLH(float left, float right, float bottom, float top, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.orthoLH(left, right, bottom, top, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f orthoLH(float left, float right, float bottom, float top, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.orthoLH(left, right, bottom, top, zNear, zFar)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setOrtho(float left, float right, float bottom, float top, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.setOrtho(left, right, bottom, top, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setOrtho(float left, float right, float bottom, float top, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.setOrtho(left, right, bottom, top, zNear, zFar)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setOrthoLH(float left, float right, float bottom, float top, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.setOrthoLH(left, right, bottom, top, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setOrthoLH(float left, float right, float bottom, float top, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.setOrthoLH(left, right, bottom, top, zNear, zFar)).opEnd();
	}
	
	@Override
	public Matrix4f orthoSymmetric(float width, float height, float zNear, float zFar, boolean zZeroToOne, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.orthoSymmetric(width, height, zNear, zFar, zZeroToOne, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.orthoSymmetric(width, height, zNear, zFar, zZeroToOne, dest);
		}
	}
	
	@Override
	public Matrix4f orthoSymmetric(float width, float height, float zNear, float zFar, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.orthoSymmetric(width, height, zNear, zFar, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.orthoSymmetric(width, height, zNear, zFar, dest);
		}
	}
	
	@Override
	public OperableMatrix4f orthoSymmetric(float width, float height, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.orthoSymmetric(width, height, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f orthoSymmetric(float width, float height, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.orthoSymmetric(width, height, zNear, zFar)).opEnd();
	}
	
	@Override
	public Matrix4f orthoSymmetricLH(float width, float height, float zNear, float zFar, boolean zZeroToOne, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.orthoSymmetricLH(width, height, zNear, zFar, zZeroToOne, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.orthoSymmetricLH(width, height, zNear, zFar, zZeroToOne, dest);
		}
	}
	
	@Override
	public Matrix4f orthoSymmetricLH(float width, float height, float zNear, float zFar, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.orthoSymmetricLH(width, height, zNear, zFar, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.orthoSymmetricLH(width, height, zNear, zFar, dest);
		}
	}
	
	@Override
	public OperableMatrix4f orthoSymmetricLH(float width, float height, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.orthoSymmetricLH(width, height, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f orthoSymmetricLH(float width, float height, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.orthoSymmetricLH(width, height, zNear, zFar)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setOrthoSymmetric(float width, float height, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.setOrthoSymmetric(width, height, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setOrthoSymmetric(float width, float height, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.setOrthoSymmetric(width, height, zNear, zFar)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setOrthoSymmetricLH(float width, float height, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.setOrthoSymmetricLH(width, height, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setOrthoSymmetricLH(float width, float height, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.setOrthoSymmetricLH(width, height, zNear, zFar)).opEnd();
	}
	
	@Override
	public Matrix4f ortho2D(float left, float right, float bottom, float top, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.ortho2D(left, right, bottom, top, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.ortho2D(left, right, bottom, top, dest);
		}
	}
	
	@Override
	public Matrix4f ortho2D(float left, float right, float bottom, float top) {
		this.opStart();
		return ((OperableMatrix4f) super.ortho2D(left, right, bottom, top)).opEnd();
	}
	
	@Override
	public Matrix4f ortho2DLH(float left, float right, float bottom, float top, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.ortho2DLH(left, right, bottom, top, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.ortho2DLH(left, right, bottom, top, dest);
		}
	}
	
	@Override
	public Matrix4f ortho2DLH(float left, float right, float bottom, float top) {
		this.opStart();
		return ((OperableMatrix4f) super.ortho2DLH(left, right, bottom, top)).opEnd();
	}
	
	@Override
	public Matrix4f setOrtho2D(float left, float right, float bottom, float top) {
		this.opStart();
		return ((OperableMatrix4f) super.setOrtho2D(left, right, bottom, top)).opEnd();
	}
	
	@Override
	public Matrix4f setOrtho2DLH(float left, float right, float bottom, float top) {
		this.opStart();
		return ((OperableMatrix4f) super.setOrtho2DLH(left, right, bottom, top)).opEnd();
	}
	
	@Override
	public OperableMatrix4f lookAlong(Vector3fc dir, Vector3fc up) {
		this.opStart();
		return ((OperableMatrix4f) super.lookAlong(dir, up)).opEnd();
	}
	
	@Override
	public Matrix4f lookAlong(Vector3fc dir, Vector3fc up, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.lookAlong(dir, up, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.lookAlong(dir, up, dest);
		}
	}
	
	@Override
	public Matrix4f lookAlong(float dirX, float dirY, float dirZ, float upX, float upY, float upZ, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.lookAlong(dirX, dirY, dirZ, upX, upY, upZ, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.lookAlong(dirX, dirY, dirZ, upX, upY, upZ, dest);
		}
	}
	
	@Override
	public OperableMatrix4f lookAlong(float dirX, float dirY, float dirZ, float upX, float upY, float upZ) {
		this.opStart();
		return ((OperableMatrix4f) super.lookAlong(dirX, dirY, dirZ, upX, upY, upZ)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setLookAlong(Vector3fc dir, Vector3fc up) {
		this.opStart();
		return ((OperableMatrix4f) super.setLookAlong(dir, up)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setLookAlong(float dirX, float dirY, float dirZ, float upX, float upY, float upZ) {
		this.opStart();
		return ((OperableMatrix4f) super.setLookAlong(dirX, dirY, dirZ, upX, upY, upZ)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setLookAt(Vector3fc eye, Vector3fc center, Vector3fc up) {
		this.opStart();
		return ((OperableMatrix4f) super.setLookAt(eye, center, up)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setLookAt(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ) {
		this.opStart();
		return ((OperableMatrix4f) super.setLookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ)).opEnd();
	}
	
	@Override
	public Matrix4f lookAt(Vector3fc eye, Vector3fc center, Vector3fc up, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.lookAt(eye, center, up, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.lookAt(eye, center, up, dest);
		}
	}
	
	@Override
	public OperableMatrix4f lookAt(Vector3fc eye, Vector3fc center, Vector3fc up) {
		this.opStart();
		return ((OperableMatrix4f) super.lookAt(eye, center, up)).opEnd();
	}
	
	@Override
	public Matrix4f lookAt(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.lookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.lookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ, dest);
		}
	}
	
	@Override
	public Matrix4f lookAtPerspective(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.lookAtPerspective(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.lookAtPerspective(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ, dest);
		}
	}
	
	@Override
	public OperableMatrix4f lookAt(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ) {
		this.opStart();
		return ((OperableMatrix4f) super.lookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setLookAtLH(Vector3fc eye, Vector3fc center, Vector3fc up) {
		this.opStart();
		return ((OperableMatrix4f) super.setLookAtLH(eye, center, up)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setLookAtLH(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ) {
		this.opStart();
		return ((OperableMatrix4f) super.setLookAtLH(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ)).opEnd();
	}
	
	@Override
	public Matrix4f lookAtLH(Vector3fc eye, Vector3fc center, Vector3fc up, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.lookAtLH(eye, center, up, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.lookAtLH(eye, center, up, dest);
		}
	}
	
	@Override
	public OperableMatrix4f lookAtLH(Vector3fc eye, Vector3fc center, Vector3fc up) {
		this.opStart();
		return ((OperableMatrix4f) super.lookAtLH(eye, center, up)).opEnd();
	}
	
	@Override
	public Matrix4f lookAtLH(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.lookAtLH(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.lookAtLH(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ, dest);
		}
	}
	
	@Override
	public OperableMatrix4f lookAtLH(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ) {
		this.opStart();
		return ((OperableMatrix4f) super.lookAtLH(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ)).opEnd();
	}
	
	@Override
	public Matrix4f lookAtPerspectiveLH(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.lookAtPerspectiveLH(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.lookAtPerspectiveLH(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ, dest);
		}
	}
	
	@Override
	public Matrix4f perspective(float fovy, float aspect, float zNear, float zFar, boolean zZeroToOne, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.perspective(fovy, aspect, zNear, zFar, zZeroToOne, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.perspective(fovy, aspect, zNear, zFar, zZeroToOne, dest);
		}
	}
	
	@Override
	public Matrix4f perspective(float fovy, float aspect, float zNear, float zFar, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.perspective(fovy, aspect, zNear, zFar, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.perspective(fovy, aspect, zNear, zFar, dest);
		}
	}
	
	@Override
	public OperableMatrix4f perspective(float fovy, float aspect, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.perspective(fovy, aspect, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f perspective(float fovy, float aspect, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.perspective(fovy, aspect, zNear, zFar)).opEnd();
	}
	
	@Override
	public Matrix4f perspectiveRect(float width, float height, float zNear, float zFar, boolean zZeroToOne, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.perspectiveRect(width, height, zNear, zFar, zZeroToOne, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.perspectiveRect(width, height, zNear, zFar, zZeroToOne, dest);
		}
	}
	
	@Override
	public Matrix4f perspectiveRect(float width, float height, float zNear, float zFar, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.perspectiveRect(width, height, zNear, zFar, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.perspectiveRect(width, height, zNear, zFar, dest);
		}
	}
	
	@Override
	public OperableMatrix4f perspectiveRect(float width, float height, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.perspectiveRect(width, height, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f perspectiveRect(float width, float height, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.perspectiveRect(width, height, zNear, zFar)).opEnd();
	}
	
	@Override
	public Matrix4f perspectiveOffCenter(float fovy, float offAngleX, float offAngleY, float aspect, float zNear, float zFar, boolean zZeroToOne, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.perspectiveOffCenter(fovy, offAngleX, offAngleY, aspect, zNear, zFar, zZeroToOne, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.perspectiveOffCenter(fovy, offAngleX, offAngleY, aspect, zNear, zFar, zZeroToOne, dest);
		}
	}
	
	@Override
	public Matrix4f perspectiveOffCenter(float fovy, float offAngleX, float offAngleY, float aspect, float zNear, float zFar, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.perspectiveOffCenter(fovy, offAngleX, offAngleY, aspect, zNear, zFar, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.perspectiveOffCenter(fovy, offAngleX, offAngleY, aspect, zNear, zFar, dest);
		}
	}
	
	@Override
	public OperableMatrix4f perspectiveOffCenter(float fovy, float offAngleX, float offAngleY, float aspect, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.perspectiveOffCenter(fovy, offAngleX, offAngleY, aspect, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f perspectiveOffCenter(float fovy, float offAngleX, float offAngleY, float aspect, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.perspectiveOffCenter(fovy, offAngleX, offAngleY, aspect, zNear, zFar)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setPerspective(float fovy, float aspect, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.setPerspective(fovy, aspect, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setPerspective(float fovy, float aspect, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.setPerspective(fovy, aspect, zNear, zFar)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setPerspectiveRect(float width, float height, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.setPerspectiveRect(width, height, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setPerspectiveRect(float width, float height, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.setPerspectiveRect(width, height, zNear, zFar)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setPerspectiveOffCenter(float fovy, float offAngleX, float offAngleY, float aspect, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.setPerspectiveOffCenter(fovy, offAngleX, offAngleY, aspect, zNear, zFar)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setPerspectiveOffCenter(float fovy, float offAngleX, float offAngleY, float aspect, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.setPerspectiveOffCenter(fovy, offAngleX, offAngleY, aspect, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public Matrix4f perspectiveLH(float fovy, float aspect, float zNear, float zFar, boolean zZeroToOne, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.perspectiveLH(fovy, aspect, zNear, zFar, zZeroToOne, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.perspectiveLH(fovy, aspect, zNear, zFar, zZeroToOne, dest);
		}
	}
	
	@Override
	public OperableMatrix4f perspectiveLH(float fovy, float aspect, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.perspectiveLH(fovy, aspect, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public Matrix4f perspectiveLH(float fovy, float aspect, float zNear, float zFar, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.perspectiveLH(fovy, aspect, zNear, zFar, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.perspectiveLH(fovy, aspect, zNear, zFar, dest);
		}
	}
	
	@Override
	public OperableMatrix4f perspectiveLH(float fovy, float aspect, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.perspectiveLH(fovy, aspect, zNear, zFar)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setPerspectiveLH(float fovy, float aspect, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.setPerspectiveLH(fovy, aspect, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setPerspectiveLH(float fovy, float aspect, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.setPerspectiveLH(fovy, aspect, zNear, zFar)).opEnd();
	}
	
	@Override
	public Matrix4f frustum(float left, float right, float bottom, float top, float zNear, float zFar, boolean zZeroToOne, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.frustum(left, right, bottom, top, zNear, zFar, zZeroToOne, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.frustum(left, right, bottom, top, zNear, zFar, zZeroToOne, dest);
		}
	}
	
	@Override
	public Matrix4f frustum(float left, float right, float bottom, float top, float zNear, float zFar, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.frustum(left, right, bottom, top, zNear, zFar, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.frustum(left, right, bottom, top, zNear, zFar, dest);
		}
	}
	
	@Override
	public OperableMatrix4f frustum(float left, float right, float bottom, float top, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.frustum(left, right, bottom, top, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f frustum(float left, float right, float bottom, float top, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.frustum(left, right, bottom, top, zNear, zFar)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setFrustum(float left, float right, float bottom, float top, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.setFrustum(left, right, bottom, top, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setFrustum(float left, float right, float bottom, float top, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.setFrustum(left, right, bottom, top, zNear, zFar)).opEnd();
	}
	
	@Override
	public Matrix4f frustumLH(float left, float right, float bottom, float top, float zNear, float zFar, boolean zZeroToOne, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.frustumLH(left, right, bottom, top, zNear, zFar, zZeroToOne, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.frustumLH(left, right, bottom, top, zNear, zFar, zZeroToOne, dest);
		}
	}
	
	@Override
	public OperableMatrix4f frustumLH(float left, float right, float bottom, float top, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.frustumLH(left, right, bottom, top, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public Matrix4f frustumLH(float left, float right, float bottom, float top, float zNear, float zFar, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.frustumLH(left, right, bottom, top, zNear, zFar, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.frustumLH(left, right, bottom, top, zNear, zFar, dest);
		}
	}
	
	@Override
	public OperableMatrix4f frustumLH(float left, float right, float bottom, float top, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.frustumLH(left, right, bottom, top, zNear, zFar)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setFrustumLH(float left, float right, float bottom, float top, float zNear, float zFar, boolean zZeroToOne) {
		this.opStart();
		return ((OperableMatrix4f) super.setFrustumLH(left, right, bottom, top, zNear, zFar, zZeroToOne)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setFrustumLH(float left, float right, float bottom, float top, float zNear, float zFar) {
		this.opStart();
		return ((OperableMatrix4f) super.setFrustumLH(left, right, bottom, top, zNear, zFar)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setFromIntrinsic(float alphaX, float alphaY, float gamma, float u0, float v0, int imgWidth, int imgHeight, float near, float far) {
		this.opStart();
		return ((OperableMatrix4f) super.setFromIntrinsic(alphaX, alphaY, gamma, u0, v0, imgWidth, imgHeight, near, far)).opEnd();
	}
	
	@Override
	public Matrix4f rotate(Quaternionfc quat, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotate(quat, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotate(quat, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotate(Quaternionfc quat) {
		this.opStart();
		return ((OperableMatrix4f) super.rotate(quat)).opEnd();
	}
	
	@Override
	public Matrix4f rotateAffine(Quaternionfc quat, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateAffine(quat, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateAffine(quat, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateAffine(Quaternionfc quat) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateAffine(quat)).opEnd();
	}
	
	@Override
	public Matrix4f rotateTranslation(Quaternionfc quat, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateTranslation(quat, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateTranslation(quat, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateAround(Quaternionfc quat, float ox, float oy, float oz) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateAround(quat, ox, oy, oz)).opEnd();
	}
	
	@Override
	public Matrix4f rotateAroundAffine(Quaternionfc quat, float ox, float oy, float oz, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateAroundAffine(quat, ox, oy, oz, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateAroundAffine(quat, ox, oy, oz, dest);
		}
	}
	
	@Override
	public Matrix4f rotateAround(Quaternionfc quat, float ox, float oy, float oz, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateAround(quat, ox, oy, oz, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateAround(quat, ox, oy, oz, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotationAround(Quaternionfc quat, float ox, float oy, float oz) {
		this.opStart();
		return ((OperableMatrix4f) super.rotationAround(quat, ox, oy, oz)).opEnd();
	}
	
	@Override
	public Matrix4f rotateLocal(Quaternionfc quat, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateLocal(quat, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateLocal(quat, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateLocal(Quaternionfc quat) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateLocal(quat)).opEnd();
	}
	
	@Override
	public Matrix4f rotateAroundLocal(Quaternionfc quat, float ox, float oy, float oz, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateAroundLocal(quat, ox, oy, oz, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateAroundLocal(quat, ox, oy, oz, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateAroundLocal(Quaternionfc quat, float ox, float oy, float oz) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateAroundLocal(quat, ox, oy, oz)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotate(AxisAngle4f axisAngle) {
		this.opStart();
		return ((OperableMatrix4f) super.rotate(axisAngle)).opEnd();
	}
	
	@Override
	public Matrix4f rotate(AxisAngle4f axisAngle, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotate(axisAngle, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotate(axisAngle, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotate(float angle, Vector3fc axis) {
		this.opStart();
		return ((OperableMatrix4f) super.rotate(angle, axis)).opEnd();
	}
	
	@Override
	public Matrix4f rotate(float angle, Vector3fc axis, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotate(angle, axis, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotate(angle, axis, dest);
		}
	}
	
	@Override
	public OperableMatrix4f unprojectRay(float winX, float winY, int[] viewport, Vector3f originDest, Vector3f dirDest) {
		this.opStart();
		return ((OperableMatrix4f) super.unprojectRay(winX, winY, viewport, originDest, dirDest)).opEnd();
	}
	
	@Override
	public OperableMatrix4f unprojectRay(Vector2fc winCoords, int[] viewport, Vector3f originDest, Vector3f dirDest) {
		this.opStart();
		return ((OperableMatrix4f) super.unprojectRay(winCoords, viewport, originDest, dirDest)).opEnd();
	}
	
	@Override
	public OperableMatrix4f unprojectInvRay(Vector2fc winCoords, int[] viewport, Vector3f originDest, Vector3f dirDest) {
		this.opStart();
		return ((OperableMatrix4f) super.unprojectInvRay(winCoords, viewport, originDest, dirDest)).opEnd();
	}
	
	@Override
	public OperableMatrix4f unprojectInvRay(float winX, float winY, int[] viewport, Vector3f originDest, Vector3f dirDest) {
		this.opStart();
		return ((OperableMatrix4f) super.unprojectInvRay(winX, winY, viewport, originDest, dirDest)).opEnd();
	}
	
	@Override
	public Matrix4f reflect(float a, float b, float c, float d, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.reflect(a, b, c, d, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.reflect(a, b, c, d, dest);
		}
	}
	
	@Override
	public OperableMatrix4f reflect(float a, float b, float c, float d) {
		this.opStart();
		return ((OperableMatrix4f) super.reflect(a, b, c, d)).opEnd();
	}
	
	@Override
	public OperableMatrix4f reflect(float nx, float ny, float nz, float px, float py, float pz) {
		this.opStart();
		return ((OperableMatrix4f) super.reflect(nx, ny, nz, px, py, pz)).opEnd();
	}
	
	@Override
	public Matrix4f reflect(float nx, float ny, float nz, float px, float py, float pz, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.reflect(nx, ny, nz, px, py, pz, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.reflect(nx, ny, nz, px, py, pz, dest);
		}
	}
	
	@Override
	public OperableMatrix4f reflect(Vector3fc normal, Vector3fc point) {
		this.opStart();
		return ((OperableMatrix4f) super.reflect(normal, point)).opEnd();
	}
	
	@Override
	public OperableMatrix4f reflect(Quaternionfc orientation, Vector3fc point) {
		this.opStart();
		return ((OperableMatrix4f) super.reflect(orientation, point)).opEnd();
	}
	
	@Override
	public Matrix4f reflect(Quaternionfc orientation, Vector3fc point, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.reflect(orientation, point, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.reflect(orientation, point, dest);
		}
	}
	
	@Override
	public Matrix4f reflect(Vector3fc normal, Vector3fc point, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.reflect(normal, point, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.reflect(normal, point, dest);
		}
	}
	
	@Override
	public OperableMatrix4f reflection(float a, float b, float c, float d) {
		this.opStart();
		return ((OperableMatrix4f) super.reflection(a, b, c, d)).opEnd();
	}
	
	@Override
	public OperableMatrix4f reflection(float nx, float ny, float nz, float px, float py, float pz) {
		this.opStart();
		return ((OperableMatrix4f) super.reflection(nx, ny, nz, px, py, pz)).opEnd();
	}
	
	@Override
	public OperableMatrix4f reflection(Vector3fc normal, Vector3fc point) {
		this.opStart();
		return ((OperableMatrix4f) super.reflection(normal, point)).opEnd();
	}
	
	@Override
	public OperableMatrix4f reflection(Quaternionfc orientation, Vector3fc point) {
		this.opStart();
		return ((OperableMatrix4f) super.reflection(orientation, point)).opEnd();
	}
	
	@Override
	public Matrix4f setRow(int row, Vector4fc src) throws IndexOutOfBoundsException {
		return super.setRow(row, src);
	}
	
	@Override
	public Matrix4f setColumn(int column, Vector4fc src) throws IndexOutOfBoundsException {
		return super.setColumn(column, src);
	}
	
	@Override
	public OperableMatrix4f set(int column, int row, float value) {
		this.opStart();
		return ((OperableMatrix4f) super.set(column, row, value)).opEnd();
	}
	
	@Override
	public OperableMatrix4f setRowColumn(int row, int column, float value) {
		this.opStart();
		return ((OperableMatrix4f) super.setRowColumn(row, column, value)).opEnd();
	}
	
	@Override
	public OperableMatrix4f normal() {
		this.opStart();
		return ((OperableMatrix4f) super.normal()).opEnd();
	}
	
	@Override
	public Matrix4f normal(Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.normal(((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.normal(dest);
		}
	}
	
	@Override
	public Matrix4f cofactor3x3() {
		this.opStart();
		return ((OperableMatrix4f) super.cofactor3x3()).opEnd();
	}
	
	@Override
	public Matrix4f cofactor3x3(Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.cofactor3x3(((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.cofactor3x3(dest);
		}
	}
	
	@Override
	public Matrix4f normalize3x3() {
		this.opStart();
		return ((OperableMatrix4f) super.normalize3x3()).opEnd();
	}
	
	@Override
	public Matrix4f normalize3x3(Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.normalize3x3(((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.normalize3x3(dest);
		}
	}
	
	@Override
	public OperableMatrix4f shadow(Vector4f light, float a, float b, float c, float d) {
		this.opStart();
		return ((OperableMatrix4f) super.shadow(light, a, b, c, d)).opEnd();
	}
	
	@Override
	public Matrix4f shadow(Vector4f light, float a, float b, float c, float d, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.shadow(light, a, b, c, d, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.shadow(light, a, b, c, d, dest);
		}
	}
	
	@Override
	public OperableMatrix4f shadow(float lightX, float lightY, float lightZ, float lightW, float a, float b, float c, float d) {
		this.opStart();
		return ((OperableMatrix4f) super.shadow(lightX, lightY, lightZ, lightW, a, b, c, d)).opEnd();
	}
	
	@Override
	public Matrix4f shadow(float lightX, float lightY, float lightZ, float lightW, float a, float b, float c, float d, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.shadow(lightX, lightY, lightZ, lightW, a, b, c, d, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.shadow(lightX, lightY, lightZ, lightW, a, b, c, d, dest);
		}
	}
	
	@Override
	public Matrix4f shadow(Vector4f light, Matrix4fc planeTransform, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.shadow(light, planeTransform, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.shadow(light, planeTransform, dest);
		}
	}
	
	@Override
	public OperableMatrix4f shadow(Vector4f light, Matrix4f planeTransform) {
		this.opStart();
		return ((OperableMatrix4f) super.shadow(light, planeTransform)).opEnd();
	}
	
	@Override
	public Matrix4f shadow(float lightX, float lightY, float lightZ, float lightW, Matrix4fc planeTransform, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.shadow(lightX, lightY, lightZ, lightW, planeTransform, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.shadow(lightX, lightY, lightZ, lightW, planeTransform, dest);
		}
	}
	
	@Override
	public OperableMatrix4f shadow(float lightX, float lightY, float lightZ, float lightW, Matrix4f planeTransform) {
		this.opStart();
		return ((OperableMatrix4f) super.shadow(lightX, lightY, lightZ, lightW, planeTransform)).opEnd();
	}
	
	@Override
	public OperableMatrix4f billboardCylindrical(Vector3fc objPos, Vector3fc targetPos, Vector3fc up) {
		this.opStart();
		return ((OperableMatrix4f) super.billboardCylindrical(objPos, targetPos, up)).opEnd();
	}
	
	@Override
	public OperableMatrix4f billboardSpherical(Vector3fc objPos, Vector3fc targetPos, Vector3fc up) {
		this.opStart();
		return ((OperableMatrix4f) super.billboardSpherical(objPos, targetPos, up)).opEnd();
	}
	
	@Override
	public OperableMatrix4f billboardSpherical(Vector3fc objPos, Vector3fc targetPos) {
		this.opStart();
		return ((OperableMatrix4f) super.billboardSpherical(objPos, targetPos)).opEnd();
	}
	
	@Override
	public Matrix4f pick(float x, float y, float width, float height, int[] viewport, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.pick(x, y, width, height, viewport, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.pick(x, y, width, height, viewport, dest);
		}
	}
	
	@Override
	public OperableMatrix4f pick(float x, float y, float width, float height, int[] viewport) {
		this.opStart();
		return ((OperableMatrix4f) super.pick(x, y, width, height, viewport)).opEnd();
	}
	
	@Override
	public OperableMatrix4f swap(Matrix4f other) {
		this.opStart();
		return ((OperableMatrix4f) super.swap(other)).opEnd();
	}
	
	@Override
	public Matrix4f arcball(float radius, float centerX, float centerY, float centerZ, float angleX, float angleY, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.arcball(radius, centerX, centerY, centerZ, angleX, angleY, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.arcball(radius, centerX, centerY, centerZ, angleX, angleY, dest);
		}
	}
	
	@Override
	public Matrix4f arcball(float radius, Vector3fc center, float angleX, float angleY, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.arcball(radius, center, angleX, angleY, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.arcball(radius, center, angleX, angleY, dest);
		}
	}
	
	@Override
	public OperableMatrix4f arcball(float radius, float centerX, float centerY, float centerZ, float angleX, float angleY) {
		this.opStart();
		return ((OperableMatrix4f) super.arcball(radius, centerX, centerY, centerZ, angleX, angleY)).opEnd();
	}
	
	@Override
	public OperableMatrix4f arcball(float radius, Vector3fc center, float angleX, float angleY) {
		this.opStart();
		return ((OperableMatrix4f) super.arcball(radius, center, angleX, angleY)).opEnd();
	}
	
	@Override
	public OperableMatrix4f frustumAabb(Vector3f min, Vector3f max) {
		this.opStart();
		return ((OperableMatrix4f) super.frustumAabb(min, max)).opEnd();
	}
	
	@Override
	public Matrix4f projectedGridRange(Matrix4fc projector, float sLower, float sUpper, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.projectedGridRange(projector, sLower, sUpper, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.projectedGridRange(projector, sLower, sUpper, dest);
		}
	}
	
	@Override
	public Matrix4f perspectiveFrustumSlice(float near, float far, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.perspectiveFrustumSlice(near, far, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.perspectiveFrustumSlice(near, far, dest);
		}
	}
	
	@Override
	public Matrix4f orthoCrop(Matrix4fc view, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.orthoCrop(view, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.orthoCrop(view, dest);
		}
	}
	
	@Override
	public OperableMatrix4f trapezoidCrop(float p0x, float p0y, float p1x, float p1y, float p2x, float p2y, float p3x, float p3y) {
		this.opStart();
		return ((OperableMatrix4f) super.trapezoidCrop(p0x, p0y, p1x, p1y, p2x, p2y, p3x, p3y)).opEnd();
	}
	
	@Override
	public OperableMatrix4f transformAab(float minX, float minY, float minZ, float maxX, float maxY, float maxZ, Vector3f outMin, Vector3f outMax) {
		this.opStart();
		return ((OperableMatrix4f) super.transformAab(minX, minY, minZ, maxX, maxY, maxZ, outMin, outMax)).opEnd();
	}
	
	@Override
	public OperableMatrix4f transformAab(Vector3fc min, Vector3fc max, Vector3f outMin, Vector3f outMax) {
		this.opStart();
		return ((OperableMatrix4f) super.transformAab(min, max, outMin, outMax)).opEnd();
	}
	
	@Override
	public OperableMatrix4f lerp(Matrix4fc other, float t) {
		this.opStart();
		return ((OperableMatrix4f) super.lerp(other, t)).opEnd();
	}
	
	@Override
	public Matrix4f lerp(Matrix4fc other, float t, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.lerp(other, t, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.lerp(other, t, dest);
		}
	}
	
	@Override
	public Matrix4f rotateTowards(Vector3fc dir, Vector3fc up, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateTowards(dir, up, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateTowards(dir, up, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotateTowards(Vector3fc dir, Vector3fc up) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateTowards(dir, up)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotateTowards(float dirX, float dirY, float dirZ, float upX, float upY, float upZ) {
		this.opStart();
		return ((OperableMatrix4f) super.rotateTowards(dirX, dirY, dirZ, upX, upY, upZ)).opEnd();
	}
	
	@Override
	public Matrix4f rotateTowards(float dirX, float dirY, float dirZ, float upX, float upY, float upZ, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.rotateTowards(dirX, dirY, dirZ, upX, upY, upZ, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.rotateTowards(dirX, dirY, dirZ, upX, upY, upZ, dest);
		}
	}
	
	@Override
	public OperableMatrix4f rotationTowards(Vector3fc dir, Vector3fc up) {
		this.opStart();
		return ((OperableMatrix4f) super.rotationTowards(dir, up)).opEnd();
	}
	
	@Override
	public OperableMatrix4f rotationTowards(float dirX, float dirY, float dirZ, float upX, float upY, float upZ) {
		this.opStart();
		return ((OperableMatrix4f) super.rotationTowards(dirX, dirY, dirZ, upX, upY, upZ)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translationRotateTowards(Vector3fc pos, Vector3fc dir, Vector3fc up) {
		this.opStart();
		return ((OperableMatrix4f) super.translationRotateTowards(pos, dir, up)).opEnd();
	}
	
	@Override
	public OperableMatrix4f translationRotateTowards(float posX, float posY, float posZ, float dirX, float dirY, float dirZ, float upX, float upY, float upZ) {
		this.opStart();
		return ((OperableMatrix4f) super.translationRotateTowards(posX, posY, posZ, dirX, dirY, dirZ, upX, upY, upZ)).opEnd();
	}
	
	@Override
	public Vector3f getEulerAnglesZYX(Vector3f dest) {
		return super.getEulerAnglesZYX(dest);
	}
	
	@Override
	public OperableMatrix4f affineSpan(Vector3f corner, Vector3f xDir, Vector3f yDir, Vector3f zDir) {
		this.opStart();
		return ((OperableMatrix4f) super.affineSpan(corner, xDir, yDir, zDir)).opEnd();
	}
	
	@Override
	public OperableMatrix4f obliqueZ(float a, float b) {
		this.opStart();
		return ((OperableMatrix4f) super.obliqueZ(a, b)).opEnd();
	}
	
	@Override
	public Matrix4f obliqueZ(float a, float b, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.obliqueZ(a, b, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.obliqueZ(a, b, dest);
		}
	}
	
	@Override
	public OperableMatrix4f withLookAtUp(Vector3fc up) {
		this.opStart();
		return ((OperableMatrix4f) super.withLookAtUp(up)).opEnd();
	}
	
	@Override
	public Matrix4f withLookAtUp(Vector3fc up, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.withLookAtUp(up, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.withLookAtUp(up, dest);
		}
	}
	
	@Override
	public OperableMatrix4f withLookAtUp(float upX, float upY, float upZ) {
		this.opStart();
		return ((OperableMatrix4f) super.withLookAtUp(upX, upY, upZ)).opEnd();
	}
	
	@Override
	public Matrix4f withLookAtUp(float upX, float upY, float upZ, Matrix4f dest) {
		if (dest instanceof OperableMatrix4f) {
			return ((OperableMatrix4f) super.withLookAtUp(upX, upY, upZ, ((OperableMatrix4f) dest).opStart())).opEnd();
		} else {
			return super.withLookAtUp(upX, upY, upZ, dest);
		}
	}
	
	@Override
	public OperableMatrix4f opStart() {
		return this;
	}
	
	@Override
	public OperableMatrix4f opEnd() {
		return this;
	}
}
