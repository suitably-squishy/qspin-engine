package entities.transform;

/**
 * An interface for classes wanting to listen to changes to transformation matrices.
 */
public interface TransformChangeListener {
	
	/**
	 * Called when any change occurs to the subscribed transformation matrix.
	 */
	void onTransformChange();
}
