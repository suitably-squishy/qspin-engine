package entities.transform;

/**
 * An interface for classes which can start and end operations over one or more function calls.
 *
 * @param <T> The type of the class to return after each operation.
 */
public interface Operable<T> {
	
	/**
	 * Starts an operation.
	 *
	 * @return The object being operated on.
	 */
	T opStart();
	
	
	/**
	 * Ends an operation.
	 *
	 * @return The object that has been operated on.
	 */
	T opEnd();
}
