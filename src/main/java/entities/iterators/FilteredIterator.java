package entities.iterators;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Predicate;

/**
 * A filtered iterator which wraps an existing iterator and only provides elements which pass a provided predicate.
 *
 * @param <T> The type of elements in the iterator.
 */
public class FilteredIterator<T> implements Iterator<T> {

	/** The wrapped iterator. */
	private final Iterator<T> iterator;

	/** The predicate elements need to pass to be provided. */
	private final Predicate<T> predicate;

	/** A buffer of the next element to provide. */
	private T next;

	/** Whether the iterator has a valid next element that matches the predicate. */
	private boolean hasNext = false;

	/**
	 * Wraps the provided iterator, providing only elements from the iterator which pass the provided predicate.
	 *
	 * @param iterator  The iterator to wrap.
	 * @param predicate The predicate elements must pass.
	 */
	public FilteredIterator(Iterator<T> iterator, Predicate<T> predicate) {
		this.iterator = iterator;
		this.predicate = predicate;
		this.updateNextFromInternalIterator();
	}

	@Override
	public boolean hasNext() {
		return this.hasNext;
	}

	@Override
	public T next() {
		if (!this.hasNext()) throw new NoSuchElementException("Out of elements.");

		T returned = next;
		this.updateNextFromInternalIterator();
		return returned;
	}

	/**
	 * Updates the internal buffer with the next element that passes the predicate.
	 * Sets the internal buffer to null and lowers the has next flag if no such element exists.
	 */
	private void updateNextFromInternalIterator() {
		if (this.iterator == null) {
			this.hasNext = false;
			return;
		}

		while (this.iterator.hasNext()) {
			this.next = this.iterator.next();
			if (predicate == null) {
				this.hasNext = true;
				return;
			}
			boolean valid = predicate.test(this.next);
			if (valid) {
				this.hasNext = true;
				return;
			}
		}
		this.hasNext = false;
		this.next = null;
	}
}
