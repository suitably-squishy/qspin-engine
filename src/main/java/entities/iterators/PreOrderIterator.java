package entities.iterators;

import entities.Entity;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Stack;

/**
 * A pre order iterator for entities.
 */
public class PreOrderIterator implements Iterator<Entity> {

	private final Stack<Entity> stack;

	/**
	 * Creates a pre order iterator starting from the provided root entity.
	 *
	 * @param root The entity to start iterating from.
	 */
	public PreOrderIterator(Entity root) {
		stack = new Stack<>();
		if (root != null) {
			stack.push(root);
		}
	}

	@Override
	public boolean hasNext() {
		return !stack.isEmpty();
	}

	@Override
	public Entity next() {
		if (stack.isEmpty()) throw new NoSuchElementException("Out of elements");

		Entity entity = stack.pop();

		// Adds all the children of the current entity to the stack in reverse order
		for (int i = entity.getChildren().size() - 1; i >= 0; i--) {
			Entity child = entity.getChildren().get(i);
			stack.push(child);
		}

		return entity;
	}
}
