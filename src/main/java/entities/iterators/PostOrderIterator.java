package entities.iterators;

import entities.Entity;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Stack;

/**
 * A post order iterator for entities.
 */
public class PostOrderIterator implements Iterator<Entity> {

	private final Stack<Entity> stack;
	private Entity root;

	/**
	 * Creates a post order iterator starting from the provided root entity.
	 *
	 * @param root The entity to start iterating from.
	 */
	public PostOrderIterator(Entity root) {
		stack = new Stack<>();
		this.root = root;
	}

	@Override
	public boolean hasNext() {
		return !(stack.isEmpty() && root == null);
	}

	@Override
	public Entity next() {
		if (!this.hasNext()) throw new NoSuchElementException("No more elements.");

		while (true) {
			while (root != null) {
				// Adds all the current root's children to the stack.
				for (int i = root.getChildren().size() - 1; i > 0; i--) {
					stack.push(root.getChildren().get(i));
				}

				// Adds the current root to the stack
				stack.push(root);

				// If the root has no children, stop
				if (root.getChildren().isEmpty()) {
					root = null;
				} else {
					// Otherwise descend into the root's left child.
					root = root.getChildren().get(0);
				}
			}

			root = stack.pop();

			// If the current root has the top of the stack as a right child
			if (!stack.isEmpty() &&
					root.getChildren().contains(stack.peek()) && root.getChildren().indexOf(stack.peek()) != 0) {
				// Then pop the child from the stack and set it as the new root
				Entity newRoot = stack.pop();
				// Put the old root back on the stack
				stack.push(root);
				root = newRoot;
			} else {
				// Otherwise yield the current root
				Entity returned = root;
				root = null;
				return returned;
			}
		}
	}
}
