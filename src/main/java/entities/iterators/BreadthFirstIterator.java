package entities.iterators;

import entities.Entity;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

/**
 * A breadth first iterator for entities.
 */
public class BreadthFirstIterator implements Iterator<Entity> {

	private final Queue<Entity> queue;

	/**
	 * Creates a breadth first iterator starting at the provided root entity.
	 *
	 * @param root The root entity to start iterating from.
	 */
	public BreadthFirstIterator(Entity root) {
		this.queue = new LinkedList<>();
		if (root != null) {
			this.queue.add(root);
		}
	}

	@Override
	public boolean hasNext() {
		return !(this.queue.isEmpty());
	}

	@Override
	public Entity next() {
		if (!hasNext()) throw new NoSuchElementException("No more elements.");

		Entity next = this.queue.remove();
		this.queue.addAll(next.getChildren());
		return next;
	}
}
