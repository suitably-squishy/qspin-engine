package entities.physics;

import entities.physics.collision.RayEntityIntersection;
import entities.physics.forces.ForceManager;
import org.joml.Vector3f;

/**
 * Represents an entities physical presence in a scene, allowing for behaviour in collisions.
 */
public abstract class PhysicalProperties {

	protected static final float DEFAULT_MASS = 1f;

	/**
	 * Whether or not the entity can be collided with.
	 */
	public boolean collidable;

	/**
	 * Whether or not the entity is logically part of it's parent in relation to collisions.
	 */
	public boolean partOfParent;

	/** The force manager responsible for dealing with forces. */
	private final ForceManager forceManager;

	/**
	 * Creates physical properties with the provided configuration.
	 *
	 * @param collidable   Whether or not the entity can be collided with.
	 * @param partOfParent Whether or not the entity is logically part of it's parent.
	 * @param mass         The mass of the entity.
	 */
	public PhysicalProperties(boolean collidable, boolean partOfParent, float mass) {
		this.collidable = collidable;
		this.partOfParent = partOfParent;
		this.forceManager = new ForceManager(mass);
	}

	/**
	 * Creates physical properties with the provided configuration and default mass.
	 *
	 * @param collidable   Whether or not the entity can be collided with.
	 * @param partOfParent Whether or not the entity is logically part of it's parent.
	 */
	public PhysicalProperties(boolean collidable, boolean partOfParent) {
		this(collidable, partOfParent, DEFAULT_MASS);
	}

	/**
	 * Creates physical properties with the provided configuration, defaults to not being part of it's parent.
	 *
	 * @param collidable Whether or not the entity can be collided with.
	 */
	public PhysicalProperties(boolean collidable) {
		this(collidable, false);
	}

	/**
	 * Tests whether the given ray collides with the entity's physical presence.
	 *
	 * @param rayOrigin    The origin of the ray relative to the root entity.
	 * @param rayDirection The direction of the ray relative to the root entity.
	 * @return A ray entity intersection result if a collision occurs, null if not.
	 */
	public abstract RayEntityIntersection.Result testRayIntersection(Vector3f rayOrigin, Vector3f rayDirection);

	/**
	 * Tests whether the given ray collides with the entity's physical presence.
	 *
	 * @param rayOrigin     The origin of the ray relative to the root entity.
	 * @param rayDirection  The direction of the ray relative to the root entity.
	 * @param currentResult The current minimum distance of a raycast, will discard any results larger than this.
	 * @return A ray entity intersection result if a collision occurs, null if not.
	 */
	public RayEntityIntersection.Result testRayIntersection(Vector3f rayOrigin, Vector3f rayDirection, RayEntityIntersection.Result currentResult) {
		return this.testRayIntersection(rayOrigin, rayDirection);
	}

	/**
	 * Quickly tests whether the given ray can collide with the entity's physical presence, does not perform a full raycast.
	 * <p>
	 * This is to be used for efficiency purposes in order to generate a list of possible candidates for a proper raycast.
	 *
	 * @param rayOrigin    The origin of the ray relative to the root entity.
	 * @param rayDirection The direction of the ray relative to the root entity.
	 * @return A ray entity intersection result if a collision can occur, representing the absolute minimum distance
	 * at which a proper collision (ie one detectable by {@link PhysicalProperties#testRayIntersection(Vector3f, Vector3f)} can occur, null if not.
	 */
	public abstract RayEntityIntersection.Result testRayItersectionRough(Vector3f rayOrigin, Vector3f rayDirection);

	/** Gets the force manager of this entity */
	public ForceManager getForceManager() {
		return forceManager;
	}
}
