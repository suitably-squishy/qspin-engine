package entities.physics.collision;

import org.joml.Matrix4f;
import org.joml.Vector3f;

/**
 * Ray to Oriented Bounding Box collision functions.
 */
public class RayOBBIntersection {

	private static final Vector3f UNIT_CUBE_MIN = new Vector3f(-0.5f);
	private static final Vector3f UNIT_CUBE_MAX = new Vector3f(0.5f);

	/**
	 * Tests whether the provided ray intersects with a centered unit cube of size 1.
	 * Using http://www.opengl-tutorial.org/miscellaneous/clicking-on-objects/picking-with-custom-ray-obb-function/
	 *
	 * @param rayOrigin         The origin of the ray relative to the root entity.
	 * @param rayDirection      The direction of the ray relative to the root entity.
	 * @param targetModelMatrix The transformation matrix of the cube to be tested.
	 * @return The distance of the ray before a collision if one occurs, -1 otherwise.
	 */
	public static float testOBBIntersection(Vector3f rayOrigin, Vector3f rayDirection, Matrix4f targetModelMatrix) {
		return testOBBIntersection(rayOrigin, rayDirection, targetModelMatrix, UNIT_CUBE_MIN, UNIT_CUBE_MAX);
	}

	/**
	 * Tests whether the provided ray intersects with a centered cuboid of the given size.
	 * Using http://www.opengl-tutorial.org/miscellaneous/clicking-on-objects/picking-with-custom-ray-obb-function/
	 *
	 * @param rayOrigin         The origin of the ray relative to the root entity.
	 * @param rayDirection      The direction of the ray relative to the root entity.
	 * @param targetModelMatrix The transformation matrix of the cube to be tested.
	 * @param size              The size of the cuboid relative to the root entity.
	 * @return The distance of the ray before a collision if one occurs, -1 otherwise.
	 */
	public static float testOBBIntersection(Vector3f rayOrigin, Vector3f rayDirection, Matrix4f targetModelMatrix, Vector3f size) {
		if (size.x < 0 || size.y < 0 || size.z < 0)
			throw new IllegalArgumentException("Cannot intersect a cuboid of negative size.");

		Vector3f max = size.div(2, new Vector3f());
		Vector3f min = max.negate(new Vector3f());
		return testOBBIntersection(rayOrigin, rayDirection, targetModelMatrix, min, max);
	}

	/**
	 * Tests whether the provided ray intersects with a cuboid of size (max - min).
	 * Using http://www.opengl-tutorial.org/miscellaneous/clicking-on-objects/picking-with-custom-ray-obb-function/
	 *
	 * @param rayOrigin         The origin of the ray relative to the root entity.
	 * @param rayDirection      The direction of the ray relative to the root entity.
	 * @param targetModelMatrix The transformation matrix of the cube to be tested.
	 * @param unscaledMin       The minimum x,y,z coordinates of the cuboid when not transformed, measured relative to the root entity.
	 * @param unscaledMax       The maximum x,y,z coordinates of the cuboid, normally min * -1 if the cuboid is centered.
	 * @return The distance of the ray before a collision if one occurs, -1 otherwise.
	 */
	public static float testOBBIntersection(Vector3f rayOrigin, Vector3f rayDirection, Matrix4f targetModelMatrix, Vector3f unscaledMin, Vector3f unscaledMax) {
		rayDirection.normalize();

		float tMin = 0f;
		float tMax = 100000f;
		Vector3f scale = targetModelMatrix.getScale(new Vector3f());

		Vector3f targetTransform = targetModelMatrix.getColumn(3, new Vector3f());


		Vector3f min = unscaledMin.mul(scale, new Vector3f());
		Vector3f max = unscaledMax.mul(scale, new Vector3f());

		Vector3f delta = targetTransform.sub(rayOrigin, new Vector3f()).div(scale);

		// Finds X intersections
		{
			Vector3f xAxis = targetModelMatrix.getColumn(0, new Vector3f());
			float e = xAxis.dot(delta);
			float f = rayDirection.dot(xAxis);

			if (Math.abs(f) > 0.001f) {
				float nearIntersection = (e + min.x) / f;
				float farIntersection = (e + max.x) / f;

				// If our near and far intersections are round the wrong way swap em.
				if (nearIntersection > farIntersection) {
					float swap = nearIntersection;
					nearIntersection = farIntersection;
					farIntersection = swap;
				}

				tMax = Math.min(farIntersection, tMax);
				tMin = Math.max(nearIntersection, tMin);

				if (tMax < tMin) {
					return -1;
				}

			} else {
				if (-e + min.x > 0f || -e + max.x < 0f) {
					return -1;
				}
			}
		}

		// Finds Y intersections
		{
			Vector3f yAxis = targetModelMatrix.getColumn(1, new Vector3f());
			float e = yAxis.dot(delta);
			float f = rayDirection.dot(yAxis);

			if (Math.abs(f) > 0.001f) {
				float nearIntersection = (e + min.y) / f;
				float farIntersection = (e + max.y) / f;

				// If our near and far intersections are round the wrong way swap em.
				if (nearIntersection > farIntersection) {
					float swap = nearIntersection;
					nearIntersection = farIntersection;
					farIntersection = swap;
				}

				tMax = Math.min(farIntersection, tMax);
				tMin = Math.max(nearIntersection, tMin);

				if (tMax < tMin) {
					return -1;
				}

			} else {
				if (-e + min.y > 0f || -e + max.y < 0f) {
					return -1;
				}
			}
		}

		// Finds Z intersections
		{
			Vector3f zAxis = targetModelMatrix.getColumn(2, new Vector3f());
			float e = zAxis.dot(delta);
			float f = rayDirection.dot(zAxis);

			if (Math.abs(f) > 0.001f) {
				float nearIntersection = (e + min.z) / f;
				float farIntersection = (e + max.z) / f;

				// If our near and far intersections are round the wrong way swap em.
				if (nearIntersection > farIntersection) {
					float swap = nearIntersection;
					nearIntersection = farIntersection;
					farIntersection = swap;
				}

				tMax = Math.min(farIntersection, tMax);
				tMin = Math.max(nearIntersection, tMin);

				if (tMax < tMin) {
					return -1;
				}

			} else {
				if (-e + min.z > 0f || -e + max.z < 0f) {
					return -1;
				}
			}
		}

		return tMin;
	}
}
