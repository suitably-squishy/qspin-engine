package entities.physics.collision;

import entities.Entity;
import entities.iterators.FilteredIterator;
import entities.iterators.PostOrderIterator;
import org.joml.*;
import render.camera.Camera;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Objects;
import java.util.PriorityQueue;

/**
 * Stores methods for calculating the intersection of entities and rays.
 */
public class RayEntityIntersection {

	/**
	 * Raycasts from a position on the screen, returning a result with the shortest distance to an entity in the descendants of the provided entity.
	 *
	 * @param screenPos       The position in screen coordinates of the origin of the raycast.
	 * @param windowSize      The size of the screen.
	 * @param currentCamera   The camera being used to display the current view.
	 * @param cameraTransform The current transform of the camera in world space relative to the root entity.
	 * @param rootEntity      The entity (and associated children) to raycast.
	 * @return The result with the shortest distance,
	 * if distances are equal the first provided by the iterator,
	 * null if no collisions occur.
	 */
	public static Result raycastFromScreenPosition(Vector2d screenPos, Vector2d windowSize, Camera currentCamera, Matrix4f cameraTransform, Entity rootEntity) {
		Vector4d ray = new Vector4d(
				(2 * screenPos.x) / windowSize.x - 1,
				1 - (2 * screenPos.y) / windowSize.y,
				-1,
				1
		).mul(currentCamera.getInverseCameraProjection());

		ray.z = -1;
		ray.w = 0;
		ray.mul(cameraTransform);

		ray.normalize();

		Vector3f rayFloat = new Vector3f((float) ray.x, (float) ray.y, (float) ray.z);

		return raycast(cameraTransform.getColumn(3, new Vector3f()), rayFloat, rootEntity);
	}

	/**
	 * Raycasts a ray with the given origin and direction,
	 * returning a result with the shortest distance to an entity provided by the given iterator.
	 *
	 * @param rayOrigin      The absolute origin of the ray in world space.
	 * @param rayDirection   The direction of the ray in world space.
	 * @param entityIterator An iterator able to provide entities to test for collisions.
	 * @return The result with the shortest distance,
	 * if distances are equal the first provided by the iterator,
	 * null if no collisions occur.
	 */
	public static Result raycast(Vector3f rayOrigin, Vector3f rayDirection, Iterator<Entity> entityIterator) {
		if (entityIterator == null) return null;

		Iterator<Entity> collidableEntityIterator = new FilteredIterator<>(
				entityIterator,
				e -> e.getPhysicalProperties() != null && e.getPhysicalProperties().collidable
		);

		// Raycast roughly first, generate list of potential collisions/entities
		PriorityQueue<Result> candidateCollisions = new PriorityQueue<>(Comparator.comparingDouble(r -> r.collisionDistance));
		while (collidableEntityIterator.hasNext()) {
			Entity entity = collidableEntityIterator.next();
			Result result = entity.getPhysicalProperties().testRayItersectionRough(rayOrigin, rayDirection);
			if (result != null) {
				candidateCollisions.add(result);
			}
		}

		Result shortestRaycastResult = null;

		// Reraycast again but in order of closeness
		while (!candidateCollisions.isEmpty()) {
			Result roughResult = candidateCollisions.poll();

			// Return the current shortest result if the next in queue has a minimum distance thats larger.
			if (shortestRaycastResult != null) {
				if (roughResult.collisionDistance > shortestRaycastResult.collisionDistance)
					return shortestRaycastResult;
			}
			Entity entity = roughResult.collidedEntity;

			shortestRaycastResult = Result.merge(
					shortestRaycastResult,
					entity.getPhysicalProperties().testRayIntersection(rayOrigin, rayDirection, shortestRaycastResult)
			);
		}

		return shortestRaycastResult;
	}

	// TODO Convert raycast to an interface to allow for different implementations.

	/**
	 * Raycasts a ray with the given origin and direction over the entity provided and recursively over all of it's descendants.
	 * Returns a result with the shortest distance to an entity.
	 *
	 * @param rayOrigin    The absolute origin of the ray in world space.
	 * @param rayDirection The direction of the ray in world space.
	 * @param rootEntity   The entity (and associated children) to raycast.
	 * @return The result with the shortest distance,
	 * if distances are equal the first provided by the iterator,
	 * null if no collisions occur.
	 */
	public static Result raycast(Vector3f rayOrigin, Vector3f rayDirection, Entity rootEntity) {
		return raycast(rayOrigin, rayDirection, new PostOrderIterator(rootEntity));
	}

	/**
	 * Represents the result of a ray entity intersection.
	 */
	public static class Result {

		public float collisionDistance;
		public Entity collidedEntity;
		public Vector3f collisionPosition;

		/**
		 * Merges two intersection results together, choosing the one with the smallest distance.
		 *
		 * @param result1 The first intersection result to compare.
		 * @param result2 The second intersection result to compare.
		 * @return The intersection result with the smallest collision distance, result1 if distances are identical, null if both results are null.
		 */
		public static Result merge(Result result1, Result result2) {
			if (result2 == null) return result1;
			if (result1 == null) return result2;
			if (result1.collisionDistance <= result2.collisionDistance) {
				return result1;
			} else {
				return result2;
			}
		}

		@Override
		public String toString() {
			return "Collision with: " + collidedEntity + " at " + collisionPosition + ", Distance: " + collisionDistance;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (!(o instanceof Result)) return false;
			Result result = (Result) o;
			return Float.compare(result.collisionDistance, collisionDistance) == 0 &&
					Objects.equals(collidedEntity, result.collidedEntity) &&
					Objects.equals(collisionPosition, result.collisionPosition);
		}

		@Override
		public int hashCode() {
			return Objects.hash(collisionDistance, collidedEntity, collisionPosition);
		}
	}

	/**
	 * Represents the result of a ray and voxel entity intersection.
	 */
	public static class VoxelResult extends Result {
		public Vector3i collidedVoxel;

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			if (!super.equals(o)) return false;
			VoxelResult that = (VoxelResult) o;
			return Objects.equals(collidedVoxel, that.collidedVoxel);
		}

		@Override
		public int hashCode() {
			return Objects.hash(super.hashCode(), collidedVoxel);
		}
	}
}
