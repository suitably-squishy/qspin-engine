package entities.physics.forces;

import org.joml.Vector3f;

/**
 * A class for representing
 * contact forces, ie forces that create an normal force (such as resting on a surface)
 * units of arbitrary newtons ANs.
 */
public class ContactForce {

	private Vector3f direction;
	private float contactCoefficient;

	/**
	 * Create a contact force with the given direction and contact coefficient.
	 *
	 * @param direction          The direction of the surface.
	 * @param contactCoefficient The coefficient of restitution,
	 *                           a value between zero and one that gives how strong the frictional force is.
	 */
	public ContactForce(Vector3f direction, float contactCoefficient) {
		this.direction = direction;
		this.contactCoefficient = contactCoefficient;
	}

	public Vector3f getDirection() {
		return direction;
	}

	public void setDirection(Vector3f direction) {
		this.direction = direction;
	}

	public float getContactCoefficient() {
		return contactCoefficient;
	}

	public void setContactCoefficient(float contactCoefficient) {
		this.contactCoefficient = contactCoefficient;
	}
}
