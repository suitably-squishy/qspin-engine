package entities.physics.forces;

import org.joml.Vector3f;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 * A utility class to calculate the force interaction of a massive object
 */
public class ForceManager {

	/**
	 * The forces external forces (that aren't contact forces) on the object
	 * units of blocks of model size blocks (MB) times arbitrary mass units (AMs) per arbitrary time units (ASs) squared (arbitrary newtons ANs)
	 */
	private final Map<String, Vector3f> forces;

	/** Contact forces, ie forces that create an normal force (such as resting on a surface) */
	private final Map<String, ContactForce> contactForces;

	/** An instantaneous application of external force */
	private final Queue<Vector3f> impulses;

	/** The mass of the object */
	private float mass;

	// Total forces, to be used during one step, recalculated every step, kept for efficiency.
	private final Vector3f totalForce;
	private final Vector3f totalForceNormal;
	private final Vector3f totalMovement;

	// Used for calculations of frictional force
	private final Vector3f frictionalForce;
	private final Vector3f normalC;

	/**
	 * Total velocity after the last step,
	 * units of blocks of model size per second.
	 */
	private final Vector3f velocity;

	// Velocities and accelerations of the current step, kept for efficiency purposes.
	private final Vector3f iAcceleration;
	private final Vector3f iVelocity;

	/**
	 * Creates a new force manager with the given mass
	 *
	 * @param mass Mass, in arbitrary mass units, AMs
	 */
	public ForceManager(float mass) {
		this.mass = mass;
		forces = new HashMap<>();
		contactForces = new HashMap<>();
		impulses = new LinkedList<>();

		totalForce = new Vector3f();
		totalMovement = new Vector3f();
		totalForceNormal = new Vector3f();
		frictionalForce = new Vector3f();
		normalC = new Vector3f();
		velocity = new Vector3f();
		iAcceleration = new Vector3f();
		iVelocity = new Vector3f();
	}

	/**
	 * Creates a new force manager with the given mass, and creates gravity with the given magnitude and direction
	 *
	 * @param mass         Mass, in AMs.
	 * @param gravStrength The strength of gravity in ANs.
	 * @param down         The direction of gravity.
	 */
	public ForceManager(float mass, float gravStrength, Vector3f down) {
		//TODO: add variable gravity.
		this(mass);
		this.addGravity(gravStrength, down);
	}

	/**
	 * Adds a contact force to the object.
	 *
	 * @param name               The unique name of the contact force.
	 * @param contactCoefficient The coefficient of restitution,
	 *                           a value between zero and one that gives how strong the frictional force is.
	 * @param direction          The direction of the surface.
	 */
	public void addContactForce(String name, float contactCoefficient, Vector3f direction) {
		this.addContactForce(name, new ContactForce(direction, contactCoefficient));
	}

	/**
	 * Adds a contact force to the object.
	 *
	 * @param name         The unique name of the contact force.
	 * @param contactForce The contact force to add.
	 */
	public void addContactForce(String name, ContactForce contactForce) {
		this.contactForces.put(name, contactForce);
	}

	/**
	 * Creates creates gravity with the given magnitude and direction
	 *
	 * @param gravStrength the strength of gravity in ANs
	 * @param down         the direction of gravity
	 */
	public void addGravity(float gravStrength, Vector3f down) {
		forces.put("gravity", down.normalize(new Vector3f()).mul(gravStrength));
	}

	/**
	 * Adds a force to the list of forces
	 * units of blocks of model size blocks (MB) times arbitrary mass units (AMs) per arbitrary time units (ASs) squared (arbitrary newtons ANs)
	 *
	 * @param name  The unique name of the force
	 * @param force the vector representation of the force
	 */
	public void addForce(String name, Vector3f force) {
		if (name != "gravity") forces.put(name, force);
	}

	/**
	 * Adds a force to the list of forces
	 * units of blocks of model size blocks (MB) times arbitrary mass units (AMs) per arbitrary time units (ASs) squared (arbitrary newtons ANs)
	 *
	 * @param name      The unique name of the force
	 * @param magnitude the magnitude of the force
	 * @param direction the vector direction of the force
	 */
	public void addForce(String name, float magnitude, Vector3f direction) {
		this.addForce(name, direction.normalize().mul(magnitude));
	}

	/**
	 * Adds an impulse (an instantaneous force change) in units of ANs
	 *
	 * @param magnitude the magnitude of the force
	 * @param direction the vector direction of the force
	 */
	public void addImpulse(float magnitude, Vector3f direction) {
		this.addImpulse(direction.normalize().mul(magnitude));
	}

	/**
	 * Adds an impulse (an instantaneous force change) in units of ANs
	 *
	 * @param impulse the vector representation of the impulse
	 */
	public void addImpulse(Vector3f impulse) {
		this.impulses.add(new Vector3f(impulse));
	}

	/**
	 * Removes a force from the list of forces
	 *
	 * @param name The name of the force to remove
	 */
	public void removeForce(String name) {
		forces.remove(name);
	}

	/**
	 * Removes a contact force from the list of forces
	 *
	 * @param name The name of the contact force to remove
	 */
	public void removeContactForce(String name) {
		contactForces.remove(name);
	}

	/**
	 * Calculates the movement of the object the force manager represents.
	 * Calculates using the differential equation F = m d^2 x / dt^2.
	 * Calculates the translation iteratively, and applies any frictional forces.
	 *
	 * @param delta The size of the timestep, in seconds.
	 * @return The total translation in each axis in model size blocks (MB)
	 */
	public Vector3f step(float delta) {
		totalForce.zero();
		totalForceNormal.zero();
		frictionalForce.zero();

		for (Vector3f force : forces.values()) {
			totalForce.add(force);
		}

		while (!impulses.isEmpty()) {
			totalForce.add(impulses.poll());
		}

		for (ContactForce contactForce : contactForces.values()) {
			normalC.set(contactForce.getDirection());
			normalC.mul(normalC.dot(totalForce));
			totalForceNormal.add(normalC);

			velocity.mul(contactForce.getContactCoefficient() * normalC.length(), normalC);
			frictionalForce.add(normalC);
		}

		totalForce.add(totalForceNormal.negate());
		totalForce.add(frictionalForce.negate());

		totalMovement.set(velocity.mul(delta, iVelocity));
		totalMovement.add(totalForce.mul(mass * delta * delta, iAcceleration));
		velocity.add(totalForce.mul(mass * delta));

		return totalMovement;
	}

	/**
	 * Set the mass of the object
	 *
	 * @param mass the mass in arbitrary mass units (AMs)
	 */
	public void setMass(float mass) {
		this.mass = mass;
	}

	/**
	 * Get the mass of the object
	 *
	 * @return the mass in arbitrary mass units (AMs)
	 */
	public float getMass() {
		return mass;
	}
}
