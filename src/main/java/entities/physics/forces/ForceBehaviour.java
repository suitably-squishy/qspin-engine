package entities.physics.forces;

import behaviour.BehaviourTemplate;
import behaviour.BehaviouralObject;
import entities.Entity;

/**
 * A simple behaviour that enables entity position updates based on the physics force manager.
 */
public class ForceBehaviour extends BehaviourTemplate<BehaviouralObject> {

	/**
	 * Creates a template that updates an entities <i>relative</i> position based on the physics force manager.
	 */
	public ForceBehaviour() {
		super();
		this.updateFunction = (delta, owner) -> {
			if (!(owner instanceof Entity)) {
				throw new IllegalArgumentException("Owner of the force behaviour is not an entity!");
			}
			Entity o = (Entity) owner;
			o.getRelativeTransform().translate(o.getPhysicalProperties().getForceManager().step(delta));
		};
	}
}
