package entities.physics;

import entities.VoxelEntity;
import entities.physics.collision.RayEntityIntersection;
import entities.physics.collision.RayOBBIntersection;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector3i;
import render.model.Palette;
import utils.RGBMaterial;

/**
 * Represents the physical presence of a voxel entity.
 */
public class VoxelEntityPhysicalProperties extends PhysicalProperties {

	/**
	 * The voxel entity associated with the physical properties.
	 */
	private final VoxelEntity entity;

	/**
	 * Creates physical properties with the provided configuration.
	 *
	 * @param collidable   Whether or not the entity can be collided with.
	 * @param partOfParent Whether or not the entity is logically part of it's parent.
	 * @param entity       The entity associated with the physical properties.
	 */
	public VoxelEntityPhysicalProperties(boolean collidable, boolean partOfParent, VoxelEntity entity) {
		super(collidable, partOfParent);
		this.entity = entity;
	}

	/**
	 * Creates physical properties with the provided configuration, defaults to not part of it's parent.
	 *
	 * @param collidable Whether or not the entity can be collided with.
	 * @param entity     The entity associated with the physical properties.
	 */
	public VoxelEntityPhysicalProperties(boolean collidable, VoxelEntity entity) {
		super(collidable);
		this.entity = entity;
	}

	@Override
	public RayEntityIntersection.Result testRayIntersection(Vector3f rayOrigin, Vector3f rayDirection) {
		return this.testRayIntersection(rayOrigin, rayDirection, null);
	}

	@Override
	public RayEntityIntersection.Result testRayIntersection(Vector3f rayOrigin, Vector3f rayDirection, RayEntityIntersection.Result currentResult) {
		RayEntityIntersection.VoxelResult result = null;
		rayDirection.normalize();

		RayEntityIntersection.Result boundingBoxResult = this.testRayItersectionRough(rayOrigin, rayDirection);
		if (boundingBoxResult == null) return null;

		if (currentResult != null) {
			if (currentResult.collisionDistance < boundingBoxResult.collisionDistance) {
				return null;
			}
		}

		float minimumCollisionDistance = Float.MAX_VALUE;
		boolean collided = false;
		final Vector3i collidedVoxel = new Vector3i();

		final Palette palette = entity.getModel().getPalette();
		final short[][][] voxels = entity.getModel().getVoxels();

		final Vector3f correction = new Vector3f(
				(voxels.length - 1) / 2f,
				(voxels[0].length - 1) / 2f,
				(voxels[0][0].length - 1) / 2f
		);

		final Vector3f voxelPosition = new Vector3f();
		final Matrix4f voxelTransform = new Matrix4f();

		RGBMaterial voxel;

		for (int x = 0; x < voxels.length; x++) {
			for (int y = 0; y < voxels[x].length; y++) {
				for (int z = 0; z < voxels[x][y].length; z++) {
					// Check voxel present
					if (voxels[x][y][z] == 0) continue;
					voxel = palette.getMaterial(voxels[x][y][z]);
					if (voxel == null) continue;

					voxelPosition.set(x, y, z).sub(correction);

					entity.getAbsoluteTransform().translate(voxelPosition, voxelTransform);

					// Test for collision
					float collisionResponse = RayOBBIntersection.testOBBIntersection(rayOrigin, rayDirection, voxelTransform);
					if (collisionResponse < 0) continue;

					if (collisionResponse < minimumCollisionDistance) {
						collidedVoxel.set(x, y, z);
					}
					minimumCollisionDistance = Math.min(minimumCollisionDistance, collisionResponse);
					collided = true;
				}
			}
		}

		if (collided) {
			result = new RayEntityIntersection.VoxelResult();
			result.collisionDistance = minimumCollisionDistance;
			result.collisionPosition = rayDirection.mul(minimumCollisionDistance, new Vector3f()).add(rayOrigin);
			result.collidedEntity = entity;
			result.collidedVoxel = collidedVoxel;
		}

		return result;
	}

	/**
	 * Tests whether the given ray collides with the entity's bounding box.
	 *
	 * @param rayOrigin    The origin of the ray relative to the root entity.
	 * @param rayDirection The direction of the ray relative to the root entity.
	 * @return A ray entity intersection result if a collision occurs, null if not.
	 */
	@Override
	public RayEntityIntersection.Result testRayItersectionRough(Vector3f rayOrigin, Vector3f rayDirection) {
		Vector3f size = new Vector3f(entity.getModel().getSize());

		float collisionResponse = RayOBBIntersection.testOBBIntersection(rayOrigin, rayDirection, entity.getAbsoluteTransform(), size);

		if (collisionResponse < 0) return null;

		RayEntityIntersection.Result result = new RayEntityIntersection.Result();
		result.collisionDistance = collisionResponse;
		result.collidedEntity = this.entity;

		return result;
	}
}
