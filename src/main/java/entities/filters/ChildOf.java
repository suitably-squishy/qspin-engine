package entities.filters;

import entities.Entity;

import java.util.function.Predicate;

/**
 * A predicate which passes if an entity is a direct child of an entity which passes another predicate.
 */
public class ChildOf implements Predicate<Entity> {

	/**
	 * The predicate the parent must pass.
	 */
	private final Predicate<Entity> parentPredicate;

	/**
	 * Creates a predicate which passes if the parent of the tested entity passes the provided predicate.
	 *
	 * @param parentPredicate The predicate the parent must pass.
	 */
	public ChildOf(Predicate<Entity> parentPredicate) {
		this.parentPredicate = parentPredicate;
	}

	@Override
	public boolean test(Entity entity) {
		if (entity.getParent() == null) return false;

		return parentPredicate.test(entity.getParent());
	}
}
