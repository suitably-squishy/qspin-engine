package entities.filters;

import entities.Entity;

import java.util.function.Predicate;

/**
 * A predicate which passes if an entity is a descendant of another entity which passes another predicate.
 */
public class DescendantOf implements Predicate<Entity> {

	/**
	 * The predicate the ancestor must pass.
	 */
	private final Predicate<Entity> ancestorPredicate;

	/**
	 * Creates a predicate which passes if the passed entity has an ancestor which passes the provided predicate.
	 *
	 * @param ancestorPredicate The predicate an ancestor must pass.
	 */
	public DescendantOf(Predicate<Entity> ancestorPredicate) {
		this.ancestorPredicate = ancestorPredicate;
	}

	@Override
	public boolean test(Entity entity) {
		Entity currentAncestor = entity;
		while ((currentAncestor = currentAncestor.getParent()) != null) {
			if (ancestorPredicate.test(currentAncestor)) return true;
		}
		return false;
	}
}
