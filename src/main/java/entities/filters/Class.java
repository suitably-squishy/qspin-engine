package entities.filters;

import entities.Entity;

import java.util.function.Predicate;

/**
 * A predicate that dictates an entity should have the provided class.
 */
public class Class implements Predicate<Entity> {

	/** The class name to test for. */
	private final String c;

	/**
	 * Creates a predicate that passes if an entity has the provided class.
	 *
	 * @param c The class to test for.
	 */
	public Class(String c) {
		if (c == null) throw new IllegalArgumentException("Class cannot be null");
		this.c = c;
	}

	@Override
	public boolean test(Entity entity) {
		return entity.classList.contains(c);
	}
}
