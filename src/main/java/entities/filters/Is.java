package entities.filters;

import entities.Entity;

import java.lang.ref.WeakReference;
import java.util.function.Predicate;

/**
 * A simple predicate which passes only for a single entity.
 */
public class Is implements Predicate<Entity> {

	/**
	 * A weak reference to the entity, avoids the entity not being garbage collected.
	 */
	private final WeakReference<Entity> entityReference;

	/**
	 * Creates a simple predicate which passes only for the provided entity.
	 *
	 * @param entity The entity to allow.
	 */
	public Is(Entity entity) {
		entityReference = new WeakReference<>(entity);
	}

	@Override
	public boolean test(Entity entity) {
		return entity == entityReference.get();
	}
}
