package entities.filters;

import entities.Entity;

import java.util.function.Predicate;

/**
 * A simple predicate which passes if an entity has the given ID.
 */
public class ID implements Predicate<Entity> {

	/**
	 * The id to test for.
	 */
	private final String id;

	/**
	 * Creates a predicate which passes if the tested entity has the provided ID.
	 *
	 * @param id The ID to test for.
	 */
	public ID(String id) {
		if (id == null) throw new IllegalArgumentException("ID cannot be null");
		this.id = id;
	}

	@Override
	public boolean test(Entity entity) {
		if (entity.getId() == null) return false;

		return entity.getId().equals(this.id);
	}
}
