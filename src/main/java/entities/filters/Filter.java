package entities.filters;

import entities.Entity;

import java.util.function.Predicate;

/**
 * A class to easily create {@link Predicate}s to filter entities.
 */
public class Filter implements Predicate<Entity> {

	/**
	 * The current predicate of the filter.
	 */
	private Predicate<Entity> predicate;

	/**
	 * Creates a filter which returns true on all entities.
	 */
	public Filter() {
		predicate = e -> true;
	}

	@Override
	public boolean test(Entity entity) {
		return predicate.test(entity);
	}

	/**
	 * Modifies the filter to only allow entities through with the provided ID.
	 *
	 * @param id The ID to allow.
	 * @return The current filter.
	 */
	public Filter hasID(String id) {
		this.predicate = this.predicate.and(new ID(id));
		return this;
	}

	/**
	 * Modifies the filter to only allow entities through with the provided class.
	 *
	 * @param c The class to allow.
	 * @return The current filter.
	 */
	public Filter hasClass(String c) {
		if (c == null) return this;
		this.predicate = this.predicate.and(new Class(c));
		return this;
	}

	/**
	 * Modifies the filter to only allow entities through that have all of the provided classes.
	 *
	 * @param classes The classes required to allow an entity.
	 * @return The current filter.
	 */
	public Filter hasClasses(String... classes) {
		Filter filter = this;

		for (String c : classes) {
			filter = filter.hasClass(c);
		}
		return filter;
	}

	/**
	 * Modifies the filter to only allow entities through that are direct children of entities which pass the provided predicate.
	 *
	 * @param parentPredicate The predicate the parent entity is required to pass.
	 * @return The current filter.
	 */
	public Filter isChildOf(Predicate<Entity> parentPredicate) {
		this.predicate = this.predicate.and(new ChildOf(parentPredicate));
		return this;
	}

	/**
	 * Modifies the filter to only allow entities through that are descendants of entities which pass the provided predicate.
	 *
	 * @param ancestorPredicate The predicate the ancestor entity is required to pass.
	 * @return The current filter.
	 */
	public Filter isDescendantOf(Predicate<Entity> ancestorPredicate) {
		this.predicate = this.predicate.and(new DescendantOf(ancestorPredicate));
		return this;
	}

	/**
	 * Modifies the filter to only allow the provided entity.
	 *
	 * @param entity The entity to allow.
	 * @return The current filter.
	 */
	public Filter is(Entity entity) {
		this.predicate = this.predicate.and(new Is(entity));
		return this;
	}

	/**
	 * Gets the predicate created by the filter.
	 *
	 * @return The generated predicate.
	 */
	public Predicate<Entity> getPredicate() {
		return predicate;
	}

	/**
	 * Sets the predicate of the filter to the provided value, ignored if null.
	 *
	 * @param predicate The predicate to set.
	 */
	public void setPredicate(Predicate<Entity> predicate) {
		if (predicate == null) return;
		this.predicate = predicate;
	}
}
