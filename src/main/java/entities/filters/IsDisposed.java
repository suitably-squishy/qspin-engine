package entities.filters;

import entities.Entity;

import java.util.function.Predicate;

/**
 * A simple predicate that passes for disposed entities.
 */
public class IsDisposed implements Predicate<Entity> {
	@Override
	public boolean test(Entity entity) {
		return entity.isDisposed();
	}
}
