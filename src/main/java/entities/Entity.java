package entities;

import behaviour.BehaviouralObject;
import entities.physics.PhysicalProperties;
import entities.selectors.BasicSelector;
import entities.selectors.ClassSet;
import entities.selectors.Selector;
import entities.transform.ListenableMatrix4f;
import entities.transform.OperableMatrix4f;
import entities.transform.TransformChangeListener;
import org.joml.Matrix4f;
import render.RendererData;
import sound.SoundModule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Logger;

/**
 * A class to hold an element in the world, used to generate transforms
 * and to generate the data to be sent to the renderer
 */
public abstract class Entity extends BehaviouralObject implements TransformChangeListener, ClassSet.Listener {

	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/** A list of user defined classes to assign to the entity. */
	public final ClassSet classList;

	/** The user defined unique ID of the entity. */
	private String id;

	/**
	 * A store for the transformation of the entity from it's original position
	 */
	protected List<Entity> children;
	/**
	 * The parent of this entity in the tree, null if this is the root node
	 */
	private Entity parent;

	private Selector selector;

	protected final Matrix4f absoluteTransform;

	protected final Matrix4f relativeTransform;

	private boolean visible = true;

	/**
	 * Used in tree traversal, marks this node as having been updated.
	 */
	private boolean renderUpdateRequired;

	protected PhysicalProperties physicalProperties;

	protected final SoundModule soundModule;

	protected boolean disposed = false;

	/** The entity registry attached to the tree this entity belongs to. */
	private EntityRegistry registry;

	/**
	 * Creates an empty entity with no parent, id or classes.
	 */
	public Entity() {
		this(null, null);
	}

	/**
	 * Creates an empty entity with the provided id.
	 *
	 * @param id The id to assign to the entity.
	 */
	public Entity(String id) {
		this(id, null);
	}

	/**
	 * Creates an empty entity with the provided classes.
	 *
	 * @param classes The classes to assign to the entity.
	 */
	public Entity(Collection<String> classes) {
		this(null, classes);
	}

	/**
	 * Creates an entity with the provided id and classes.
	 *
	 * @param id      The id to assign to the entity.
	 * @param classes The classes to assign to the entity.
	 */
	public Entity(String id, Collection<String> classes) {
		this.setId(id);
		ClassSet classSet = new ClassSet();
		classSet.addClassListener(this);
		this.classList = classSet;

		if (classes != null) {
			this.classList.addAll(classes);
		}

		this.parent = null;
		this.children = new ArrayList<>();

		this.absoluteTransform = new OperableMatrix4f();
		this.relativeTransform = new ListenableMatrix4f(this);

		this.selector = new BasicSelector(this);
		this.soundModule = new SoundModule();
	}

	@Override
	public void update(float delta) {
		if (disposed) {
			LOGGER.warning("Update attempted of disposed entity: " +
					this.toString() +
					", are you sure it has been removed from the tree?");
			return;
		}

		super.update(delta);
		for (Entity child : children) {
			child.update(delta);
		}
	}

	/**
	 * The sub-entities held by this entity.
	 * Overall the entity storage is a tree structure where a root element holds children sub elements
	 *
	 * @return Returns a non-live view of the children of this entity.
	 */
	public List<Entity> getChildren() {
		return new ArrayList<>(children);
	}

	/**
	 * Adds the provided entity as a child to this entity.
	 *
	 * @param child The child entity to add.
	 * @throws MalformedEntityTreeException If adding the given child would result in an invalid entity tree, this can happen in the following circumstances:
	 *                                      <ul>
	 *                                          <li>If either the parent or child entities are disposed.</li>
	 *                                          <li>If the entity being added is an ancestor of the parent entity.</li>
	 *                                          <li>If the added entity is already a child of this entity.</li>
	 *                                          <li>If the added entity already has a parent.</li>
	 *                                      </ul>
	 */
	public void addChild(Entity child) throws MalformedEntityTreeException {
		if (disposed) {
			throw new MalformedEntityTreeException("Cannot add child (" + child.toString() + ") to a disposed entity (" + this.toString() + ").");
		}
		if (child.disposed) {
			throw new MalformedEntityTreeException("Cannot add disposed child (" + child.toString() + ") to entity (" + this.toString() + ").");
		}

		long ancestorDepth = this.getAncestorDepth(child);
		if (ancestorDepth == 0) {
			throw new MalformedEntityTreeException("Cannot add entity (" + this.toString() + ") as it's own child.");
		} else if (ancestorDepth != -1) {
			throw new MalformedEntityTreeException("Cannot add entity (" + child.toString() + ") as a child of " + this.toString() + " as it is an ancestor " + ancestorDepth + " entities up.");
		}

		if (this.hasChild(child)) {
			// Do we really need to crash here?
			throw new MalformedEntityTreeException("Cannot add entity (" + child.toString() + ") as a child of " + this.toString() + " as it is already a child.");
		}
		if (child.getParent() != null) {
			throw new MalformedEntityTreeException("Cannot add entity (" + child.toString() + ") as a child of " + this.toString() + " as it already has a child of " + child.getParent());
		}
		child.setRegistry(registry);
		children.add(child);
		child.setParent(this);
		child.calculateAbsoluteTransform(true);
	}

	/**
	 * Adds the provided children to the entity.
	 *
	 * @param children The children to add.
	 */
	public void addChildren(Entity... children) {
		if (children == null) {
			return;
		}
		for (Entity child : children) {
			this.addChild(child);
		}
	}

	/**
	 * Removes the given child from this entities children.
	 * <p>
	 * Does not dispose of the child, if this is required make sure to perform this yourself by calling {@link Entity#dispose(boolean)}
	 *
	 * @param child The child to remove.
	 * @return True if the child was removed, false otherwise.
	 */
	public boolean removeChild(Entity child) {
		boolean removed = children.remove(child);
		if (removed) {
			child.setParent(null);
			child.setRegistry(null);
		}
		return removed;
	}

	/**
	 * Removes all children from this entities children that pass the provided predicate.
	 * <p>
	 * Does not dispose of the removed children,
	 * if this is required make sure to perform this yourself by calling {@link Entity#dispose(boolean)}
	 *
	 * @param predicate The predicate children must pass to be removed.
	 */
	public void removeChildren(Predicate<Entity> predicate) {
		Collection<Entity> childrenToRemove = new HashSet<>();
		for (Entity child : children) {
			if (predicate.test(child)) {
				childrenToRemove.add(child);
			}
		}
		childrenToRemove.forEach(this::removeChild);
	}

	/**
	 * Removes all provided children from this entities children.
	 * <p>
	 * Does not dispose of the removed children,
	 * if this is required make sure to perform this yourself by calling {@link Entity#dispose(boolean)}
	 *
	 * @param children The children to be removed.
	 */
	public void removeChildren(Entity... children) {
		for (Entity child : children) {
			removeChild(child);
		}
	}

	public boolean hasChild(Entity child) {
		if ((children.contains(child) && child.getParent() != this) || (!children.contains(child) && child.getParent() == this)) {
			throw new MalformedEntityTreeException("Child " + child + " was in an inconsistent state." +
					" This entity had it as a child: " + children.contains(child) + "," +
					" the child had this entity as it's parent: " + (child.getParent() == this) + "." +
					" I don't know how you could achieve this, godspeed.");
		}

		return children.contains(child);
	}

	/**
	 * Get the parent of this entity
	 *
	 * @return null if this entity is the root of the tree or unassigned to the tree,
	 * the parent otherwise
	 */
	public Entity getParent() {
		return parent;
	}

	/**
	 * Sets the model's parent to the model provided.
	 *
	 * @param parent The model to assign as a parent.
	 */
	protected void setParent(Entity parent) {
		if (this.parent != null && this.parent != parent) {
			this.parent.requestRenderUpdate();
		}
		this.parent = parent;
		this.requestRenderUpdate();
	}

	/**
	 * Adds this entity's data to the provided rendererData
	 */
	public void addToRenderData(RendererData data) {
	}

	/**
	 * Gets the transformation matrix of the entity, relative to it's parent.
	 *
	 * @return The transformation matrix.
	 */
	public Matrix4f getRelativeTransform() {
		return this.relativeTransform;
	}

	/**
	 * Updates the absolute transformation matrix of the entity, based on the current relative transformation matrix.
	 *
	 * @param forwardToChildren Whether or not to update the children of this entity's absolute transforms.
	 */
	public void calculateAbsoluteTransform(boolean forwardToChildren) {
		if (this.getParent() != null) {
			this.getParent().getAbsoluteTransform().mul(this.relativeTransform, absoluteTransform);
		}
		if (forwardToChildren) {
			for (Entity child : children) {
				child.calculateAbsoluteTransform(true);
			}
		}
		this.soundModule.updateTransformation(this.absoluteTransform);
	}

	@Override
	public void onTransformChange() {
		this.calculateAbsoluteTransform(true);
	}

	/**
	 * Gets the absolute transform of the entity, relative to the root entity.
	 *
	 * @return The absolute transformation matrix.
	 */
	public Matrix4f getAbsoluteTransform() {
		return absoluteTransform;
	}

	public boolean isVisible() {
		return visible;
	}

	//TODO: make method in element to reflect visibility
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * Requests a render update for this entity.
	 */
	public void requestRenderUpdate() {
		if (this.renderUpdateRequired) return;

		this.renderUpdateRequired = true;
		if (this.parent != null) {
			this.parent.requestRenderUpdate();
		}
	}

	public boolean requiresRenderUpdate() {
		return this.renderUpdateRequired;
	}

	/**
	 * Clears the render update required state through from this node and it's children.
	 */
	public void clearRenderUpdate() {
		this.renderUpdateRequired = false;
		for (Entity child : children) {
			if (child.renderUpdateRequired) {
				child.clearRenderUpdate();
			}
		}
	}

	public PhysicalProperties getPhysicalProperties() {
		return physicalProperties;
	}

	@Override
	public String toString() {
		if (getId() != null) {
			return this.getId() + " {" + this.getClass().getName() + "}";
		} else {
			return "Anonymous {" + super.toString() + "}";
		}
	}

	/**
	 * Gets the user defined ID of the entity. This should not be duplicated by any other entity in the current tree.
	 *
	 * @return The ID of the entity.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the user defined ID of the entity. This should not be duplicated by any other entity in the current tree.
	 *
	 * @param id The ID of the entity.
	 * @throws MalformedEntityTreeException If the entity is used in a registry and the ID chosen is already in use in that registry.
	 */
	public void setId(String id) throws MalformedEntityTreeException {
		if (this.registry != null) {
			registry.updateID(this, id);
		}
		this.id = id;
	}

	/**
	 * Gets the internal selector used by this entity.
	 *
	 * @return The entities selector.
	 */
	public Selector getSelector() {
		return this.selector;
	}

	/**
	 * Gets the descendants of this entity, including the entity itself.
	 *
	 * @return A collection of descendants, ordered according to the internal selector.
	 */
	public Collection<Entity> getDescendants() {
		return getSelector().getDescendants();
	}

	/**
	 * Gets the descendants of this entity, including the entity itself,
	 * filtered to only include those that pass the provided predicate.
	 *
	 * @param predicate The predicate returned entities need to pass to be included.
	 * @return A collection of descendants, ordered according to the internal selector.
	 */
	public Collection<Entity> getDescendants(Predicate<Entity> predicate) {
		return getSelector().getDescendants(predicate);
	}

	public SoundModule getSoundModule() {
		return soundModule;
	}

	/**
	 * Disposes of the entity, freeing any resources it holds. Must be called before the entity becomes unreached.
	 *
	 * @param disposeChildren Whether or not the disposal should be applied to all children of the entity.
	 */
	public void dispose(boolean disposeChildren) {
		if (this.disposed) {
			LOGGER.warning("Attempted disposal of already disposed entity (" + this.toString() + ").");
			return;
		}

		if (disposeChildren) {
			children.forEach(c -> c.dispose(true));
		}

		soundModule.dispose();
		this.disposed = true;
	}

	public boolean isDisposed() {
		return this.disposed;
	}

	/**
	 * Gets the registry current bound to this entity.
	 *
	 * @return An EntityRegistry if one is set, null otherwise.
	 */
	public EntityRegistry getRegistry() {
		return this.registry;
	}

	/**
	 * Sets the registry of the entity to the given registry, adding the entity into the registry.
	 * Also sets the registry of all children to the provided registry.
	 * <p>
	 * If a previous registry was bound,
	 * the entity will be removed from the previous registry before it is swapped.
	 *
	 * @param registry The entity registry to set.
	 */
	public void setRegistry(EntityRegistry registry) {
		if (this.registry != registry && this.registry != null) {
			this.registry.removeEntity(this);
		}
		this.registry = registry;
		if (registry != null) {
			registry.addEntity(this);
		}

		children.forEach(c -> c.setRegistry(registry));
	}

	/**
	 * Gets the depth at which the provided entity is an ancestor of this entity.
	 *
	 * @param ancestor The entity to find the depth of.
	 * @return The depth of the entity up the tree if it is an ancestor,
	 * 0 if the entity is this entity,
	 * -1 if the entity is not an ancestor.
	 */
	private long getAncestorDepth(Entity ancestor) {
		if (this.registry != null) {
			// Perform a check to see if the entity exists in registry
			if (!this.registry.hasEntity(ancestor)) {
				return -1;
			}
		}

		if (ancestor == this) return 0;
		if (this.getParent() == null) return -1;
		long ancestorDepth = getParent().getAncestorDepth(ancestor);
		if (ancestorDepth == -1) return -1;

		return ancestorDepth + 1;
	}

	@Override
	public void onClassAdd(String c) {
		if (this.registry != null) {
			this.registry.addEntityClass(this, c);
		}
	}

	@Override
	public void onClassRemove(String c) {
		if (this.registry != null) {
			this.registry.removeEntityClass(this, c);
		}
	}
}
