package entities;

import render.camera.Camera;

/**
 * A class to represent a Camera within the world
 */
public class CameraEntity extends Entity {

	/**
	 * The camera which will be passed to renderer
	 */
	protected Camera camera;

	/**
	 * Create a new camera entity with the given type of camera
	 *
	 * @param camera The camera to be held,
	 *               different cameras could be different projection matrices
	 */
	public CameraEntity(Camera camera) {
		super();
		this.camera = camera;
	}

	/**
	 * Remake the camera's projection after new settings have been passed
	 */
	public void updateCamera() {
		camera.remakeCameraProjection();
	}

	@Override
	public void calculateAbsoluteTransform(boolean forwardToChildren) {
		super.calculateAbsoluteTransform(forwardToChildren);
		this.camera.setTransform(this.absoluteTransform);
		this.camera.getWorldPos().set(0, 0, 0, 1).mul(this.getAbsoluteTransform());
		this.camera.getWorldDir().set(0, 0, 1, 0).mul(this.getAbsoluteTransform());
	}
}
