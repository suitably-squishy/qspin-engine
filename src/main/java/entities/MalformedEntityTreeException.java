package entities;

/**
 * Thrown when an entity in the engine is put into an entity tree causing an invalid state.
 */
public class MalformedEntityTreeException extends RuntimeException {

	/**
	 * Creates an entity tree exception with the given error message.
	 *
	 * @param s The error message to provide.
	 */
	public MalformedEntityTreeException(String s) {
		super(s);
	}
}
