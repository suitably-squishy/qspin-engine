package entities;

import entities.physics.VoxelEntityPhysicalProperties;
import render.RendererData;
import render.VoxelElement;
import render.model.VoxelModel;

import java.util.Collection;

/**
 * A class to represent an entity with a voxel model
 */
public class VoxelEntity extends Entity {

	/**
	 * The rendering data for the entity.
	 */
	private VoxelElement element;

	/**
	 * A handle to the renderer data to notify if it is updated.
	 */
	private RendererData rendererData;

	/**
	 * The voxel model that the entity is made from.
	 */
	private VoxelModel model;

	/**
	 * Creates a VoxelEntity with the provided model.
	 *
	 * @param model The voxel model to assign.
	 */
	public VoxelEntity(VoxelModel model) {
		this(null, null, model);
	}

	/**
	 * Creates a VoxelEntity with the provided id and model.
	 *
	 * @param id    The id to give the entity.
	 * @param model The voxel model to assign.
	 */
	public VoxelEntity(String id, VoxelModel model) {
		this(id, null, model);
	}

	/**
	 * Creates a VoxelEntity with the provided id, classes and model.
	 *
	 * @param id      The id to give the entity.
	 * @param classes The classes to assign to the entity.
	 * @param model   The voxel model to assign.
	 */
	public VoxelEntity(String id, Collection<String> classes, VoxelModel model) {
		super(id, classes);
		this.model = model;
		this.physicalProperties = new VoxelEntityPhysicalProperties(true, this);
	}

	@Override
	public void addToRenderData(RendererData data) {
		this.rendererData = data;
		if (this.disposed) return;

		element = new VoxelElement(this);
		data.elements.add(element);
		subscribeToRender();
	}

	/**
	 * Adds the rendering element to the list of things to be rendered.
	 */
	public void subscribeToRender() {
		if (rendererData != null) {
			rendererData.elementsToRender.add(element);
		}
	}

	public VoxelModel getModel() {
		return model;
	}

	@Override
	public void calculateAbsoluteTransform(boolean forwardToChildren) {
		super.calculateAbsoluteTransform(forwardToChildren);
		subscribeToRender();
	}
}
