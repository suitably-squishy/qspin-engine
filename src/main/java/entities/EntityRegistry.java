package entities;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;

import java.util.*;

/**
 * A class for storing entities under their IDs and classes for quick access and to prevent tree malformations.
 */
public class EntityRegistry {

	/** Stores entities under their IDs and vice-versa. */
	private final Map<String, Entity> entityIdMap;

	/** Stores entities under their user defined classes. */
	private final MultiValuedMap<String, Entity> entityClassMap;

	private final Set<Entity> entitySet;

	/**
	 * Creates a simple entity registry.
	 */
	public EntityRegistry() {
		entityIdMap = new HashMap<>();
		entityClassMap = new HashSetValuedHashMap<>();
		entitySet = new HashSet<>();
	}

	/**
	 * Adds an entity to the registry, performing checks on the entity.
	 *
	 * @param entity The entity to register.
	 * @throws MalformedEntityTreeException If the entity already exists in the registry (and therefore the tree)
	 *                                      or the entity's ID is already in use.
	 */
	void addEntity(Entity entity) throws MalformedEntityTreeException {
		if (entitySet.contains(entity))
			throw new MalformedEntityTreeException("Entity " + entity.toString() + " already exists in this tree.");

		if (entity.getId() != null) {
			if (!isUniqueID(entity.getId()))
				throw new MalformedEntityTreeException("Entity ID " + entity.getId() + " already exists in this tree.");

			this.entityIdMap.put(entity.getId(), entity);
		}

		for (String c : entity.classList) {
			this.entityClassMap.put(c, entity);
		}

		this.entitySet.add(entity);
	}

	/**
	 * Removes the given entity from the registry.
	 *
	 * @param entity The entity to remove.
	 */
	void removeEntity(Entity entity) {
		if (entity.getId() != null) {
			this.entityIdMap.remove(entity.getId());
		}
		entity.classList.forEach(c -> entityClassMap.get(c).remove(entity));
		entitySet.remove(entity);
	}

	/**
	 * Gets whether the registry contains a record for the provided entity.
	 *
	 * @param entity The entity to check for.
	 * @return True if the entity is present in the registry, false otherwise.
	 */
	boolean hasEntity(Entity entity) {
		return entitySet.contains(entity);
	}

	/**
	 * Checks whether the provided ID is unique in this registry.
	 *
	 * @param id The ID to check.
	 * @return True if the ID is unique (and can be used by an entity in the tree), false otherwise.
	 */
	boolean isUniqueID(String id) {
		return !entityIdMap.containsKey(id);
	}

	/**
	 * Fetches the entity stored under the given ID in the registry.
	 *
	 * @param id The ID to find an entity under.
	 * @return The entity, if found, otherwise null.
	 */
	public Entity getEntityByID(String id) {
		return entityIdMap.get(id);
	}

	/**
	 * Moves the given entity to be stored under the provided new ID.
	 * This does not operate on the entity itself and any entity-side ID updates must be done
	 * <i>after</i> this operation has been performed.
	 *
	 * @param entity The entity who's ID to change.
	 * @param newID  The new ID to store the entity under.
	 * @throws MalformedEntityTreeException If the entity attempts to use an already reserved ID, or the entity is not present in this registry.
	 */
	public void updateID(Entity entity, String newID) throws MalformedEntityTreeException {
		if (!entitySet.contains(entity)) {
			throw new MalformedEntityTreeException("Attempted to update ID of entity " +
					entity.toString() + " which has not been registered (and is therefore unlikely to be in the tree).");
		}

		if (newID != null) {
			Entity currentIDHolder = entityIdMap.get(newID);
			if (currentIDHolder == entity) return;

			if (currentIDHolder != null) {
				throw new MalformedEntityTreeException("Attempted to update ID of entity " + entity.toString() +
						" to " + newID + " but ID was already held by " + currentIDHolder.toString());
			}

			entityIdMap.put(newID, entity);
		}
		
		if (entity.getId() != null) {
			entityIdMap.remove(entity.getId());
		}
	}

	/**
	 * Gets a collection of entities that have the provided class in no particular order.
	 *
	 * @param c The class to get entities with.
	 * @return A collection of entities that have that class.
	 */
	public Collection<Entity> getEntitiesByClass(String c) {
		return entityClassMap.get(c);
	}

	/**
	 * Adds the given class to the entity provided. This does not operate on the entity itself and
	 * any entity-side class updates must be done <i>after</i> this operation has been performed.
	 *
	 * @param entity The entity to register.
	 * @param c      The class to register the entity under.
	 */
	public void addEntityClass(Entity entity, String c) {
		this.entityClassMap.put(c, entity);
	}

	/**
	 * Removes the given class from the entity provided. This does not operate on the entity itself and
	 * any entity-side class updates must be done <i>after</i> this operation has been performed.
	 *
	 * @param entity The entity to unregister.
	 * @param c      The class to remove from the entity's registration.
	 */
	public void removeEntityClass(Entity entity, String c) {
		this.entityClassMap.removeMapping(c, entity);
	}

	/**
	 * Gets a collection of all entities in the registry.
	 *
	 * @return A non-live view of all entities in the registry in no particular order.
	 */
	public Collection<Entity> getAllEntities() {
		return new HashSet<>(entityIdMap.values());
	}
}
