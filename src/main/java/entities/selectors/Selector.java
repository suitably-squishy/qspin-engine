package entities.selectors;

import entities.Entity;
import entities.filters.DescendantOf;
import entities.filters.Is;
import entities.iterators.FilteredIterator;
import entities.iterators.PostOrderIterator;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.function.Predicate;

/**
 * An object that can act as a selector of entities, filterable by predicates.
 */
public interface Selector {

	/**
	 * Gets the entities from the given iterator, filtered by the given predicate.
	 *
	 * @param iterator  The iterator to use to fetch Entities from.
	 * @param predicate The predicate to filter the iterator by.
	 * @return A collection of entities fetched from the provided iterator which pass the provided predicate,
	 * implementing classes need not provide guarantees of order.
	 */
	static Collection<Entity> getEntities(Iterator<Entity> iterator, Predicate<Entity> predicate) {
		return Selector.getEntities(new FilteredIterator<>(iterator, predicate));
	}

	/**
	 * Gets the entities from the given iterator, filtered by the given predicate.
	 *
	 * @param iterator The iterator to use to fetch Entities from.
	 * @return A collection of entities fetched from the provided iterator,
	 * implementing classes need not provide guarantees of order.
	 */
	static Collection<Entity> getEntities(Iterator<Entity> iterator) {
		Collection<Entity> entities = new HashSet<>();
		if (iterator == null) return entities;

		iterator.forEachRemaining(entities::add);
		return entities;
	}

	/**
	 * Gets the provided root entity and it's descendants which pass the provided predicate.
	 *
	 * @param root      The root entity to query.
	 * @param predicate The predicate entities must pass in order to be included.
	 * @return A collection of entities which descend from the root entity (inclusive of the root entity itself)
	 * that pass the provided predicate.
	 */
	static Collection<Entity> getDescendants(Entity root, Predicate<Entity> predicate) {
		return Selector.getEntities(new FilteredIterator<>(new PostOrderIterator(root), predicate));
	}

	/**
	 * Gets the descendants of the selector's owner.
	 *
	 * @param predicate The predicate descendants must pass in order to be returned.
	 * @return A collection of descending entities in no particular order,
	 * possibly including the owner.
	 */
	Collection<Entity> getDescendants(Predicate<Entity> predicate);

	/**
	 * Gets the provided root entity and it's descendants.
	 *
	 * @param root The root entity to query.
	 * @return A collection of entities which descend from the root entity (inclusive of the root entity itself).
	 */
	static Collection<Entity> getDescendants(Entity root) {
		if (root.getRegistry() != null) {
			Collection<Entity> entitiesToReturn = new HashSet<>();

			Collection<Entity> entities = root.getRegistry().getAllEntities();

			Predicate<Entity> descendantPredicate = new DescendantOf(new Is(root));
			for (Entity entity : entities) {
				if (descendantPredicate.test(entity) || entity == root) {
					entitiesToReturn.add(entity);
				}
			}
			return entitiesToReturn;
		} else {
			return getEntities(new PostOrderIterator(root));
		}
	}

	/**
	 * Gets the descendants of the selector's owner.
	 *
	 * @return A collection of descending entities in no particular order,
	 * possibly including the owner.
	 */
	Collection<Entity> getDescendants();

	/**
	 * Gets all entities in the same tree as the owning entity.
	 *
	 * @return A collection of entities in the tree in no particular order.
	 */
	Collection<Entity> getAllEntities();
}
