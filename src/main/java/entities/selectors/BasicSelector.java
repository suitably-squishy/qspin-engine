package entities.selectors;

import entities.Entity;
import entities.iterators.PostOrderIterator;

import java.util.Collection;
import java.util.function.Predicate;

/**
 * A basic selector class that provides entities in a post order fashion where it is able to choose.
 */
public class BasicSelector implements Selector {

	/** The owner of the selector. */
	private final Entity owner;

	/**
	 * Creates a basic selector able to select entities from it's current tree.
	 *
	 * @param owner The owner of the selector and the entity
	 *              to perform any relative operations relative to.
	 */
	public BasicSelector(Entity owner) {
		this.owner = owner;
	}

	@Override
	public Collection<Entity> getDescendants(Predicate<Entity> predicate) {
		return Selector.getDescendants(owner, predicate);
	}

	@Override
	public Collection<Entity> getDescendants() {
		return Selector.getDescendants(owner);
	}

	@Override
	public Collection<Entity> getAllEntities() {
		if (this.owner.getRegistry() != null) {
			return this.owner.getRegistry().getAllEntities();
		} else {
			return Selector.getEntities(new PostOrderIterator(owner));
		}
	}
}
