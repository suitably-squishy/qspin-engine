package entities.selectors;

import java.util.*;

/**
 * A set of user defined classes for entities,
 * capable of providing notifications when changes occur.
 */
public class ClassSet implements Iterable<String> {

	// TODO make this nice and generic, implement collection etc.

	/** All listeners for class changes. */
	private final List<Listener> listeners;
	/** The set that backs the collection. */
	private final Set<String> classSet;

	/**
	 * Creates an empty class set.
	 */
	public ClassSet() {
		classSet = new HashSet<>();
		listeners = new ArrayList<>();
	}

	/**
	 * Adds the provided class to the set.
	 *
	 * @param c The class to add.
	 * @return True if the set did not already contain the class.
	 */
	public boolean add(String c) {
		if (!classSet.contains(c)) {
			for (Listener listener : listeners) {
				listener.onClassAdd(c);
			}
		}
		return classSet.add(c);
	}

	/**
	 * Adds all the classes in the provided collection to the class set.
	 *
	 * @param cs The classes to add.
	 * @return True if a change has occurred.
	 */
	public boolean addAll(Collection<String> cs) {
		for (String c : cs) {
			if (!classSet.contains(c)) {
				for (Listener listener : listeners) {
					listener.onClassAdd(c);
				}
			}
		}
		return classSet.addAll(cs);
	}

	/**
	 * Removes the provided class from the set.
	 *
	 * @param c The class to remove.
	 * @return True if the set contained the class.
	 */
	public boolean remove(String c) {
		if (classSet.contains(c)) {
			for (Listener listener : listeners) {
				listener.onClassRemove(c);
			}
		}
		return classSet.remove(c);
	}

	/**
	 * Removes all of the classes in the collection from the set.
	 *
	 * @param cs The classes to remove.
	 * @return True if a change occurred.
	 */
	public boolean removeAll(Collection<String> cs) {
		for (String c : cs) {
			if (classSet.contains(c)) {
				for (Listener listener : listeners) {
					listener.onClassRemove(c);
				}
			}
		}
		return classSet.removeAll(cs);
	}

	/**
	 * Gets whether the provided class is in the set.
	 *
	 * @param c The class to check for.
	 * @return True if the collection contains the class, false otherwise.
	 */
	public boolean contains(String c) {
		return classSet.contains(c);
	}

	/**
	 * Adds the given listener to the class set.
	 *
	 * @param listener The listener to notify when a change occurs.
	 */
	public void addClassListener(Listener listener) {
		listeners.add(listener);
	}

	/**
	 * Removes the given listener from the class set.
	 *
	 * @param listener The listener to remove.
	 */
	public void removeClassListener(Listener listener) {
		listeners.remove(listener);
	}

	@Override
	public Iterator<String> iterator() {
		return classSet.iterator();
	}

	/**
	 * A listener to notify when a change to the class set occurs.
	 */
	public interface Listener {

		/**
		 * Called when a class is removed from the set.
		 *
		 * @param c The class that has been removed.
		 */
		void onClassRemove(String c);

		/**
		 * Called when a class is added from the set.
		 *
		 * @param c The class that has been added.
		 */
		void onClassAdd(String c);
	}
}
