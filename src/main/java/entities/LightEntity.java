package entities;

import org.joml.Vector4f;
import render.RendererData;
import render.light.DirectionalLight;
import render.light.Light;
import render.light.Light.Type;
import render.light.PointLight;
import render.light.SpotLight;

import java.util.logging.Logger;

/**
 * An entity to hold a light object
 */
public class LightEntity extends entities.Entity {

	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private Light light;

	/**
	 * the direction of the light, used with spot and directional lights
	 */
	private final Vector4f direction = new Vector4f(1, 0, 0, 0);
	/**
	 * the position of the light, used with spot and point lights
	 */
	private final Vector4f position = new Vector4f(0, 0, 0, 1);

	//default values for different lights
	private static final float DEFAULT_CONSTANT = 1f,   //constant part of a reciprocal quadratic equation for attenuation
			DEFAULT_LINEAR = 0.022f,                    //linear part of a reciprocal quadratic equation for attenuation
			DEFAULT_QUADRATIC = 0.0019f,                //quadratic part of a reciprocal quadratic equation for attenuation
	// for more info see http://wiki.ogre3d.org/tiki-index.php?page=-Point+Light+Attenuation
	DEFAULT_CUTOFF = 0.2f,                      // inner cutoff of spotlight, before the light starts fading
			DEFAULT_OUTER_CUTOFF = 0.23f;               // outer cutoff of a spotlight, where the light fades to nothing


	/**
	 * Create a new light entity with a light with default parameters
	 *
	 * @param type the type of light
	 */
	public LightEntity(Type type) {
		switch (type) {
			case DIRECTIONAL:
				this.light = new DirectionalLight(direction);
				break;
			case POINT:
				this.light = new PointLight(position, DEFAULT_CONSTANT, DEFAULT_LINEAR, DEFAULT_QUADRATIC);
				break;
			case SPOT:
				this.light = new SpotLight(position, direction, DEFAULT_CUTOFF, DEFAULT_OUTER_CUTOFF);
				break;
		}
	}

	/**
	 * Create a new light entity with the given light
	 *
	 * @param light          the light object this holds
	 * @param resetPosAndDir whether to chance the light's portion and directing to copy this
	 */
	public LightEntity(Light light, boolean resetPosAndDir) {
		this.light = light;
		if (resetPosAndDir) {
			this.light.setPosition(this.position);
			this.light.setDirection(this.direction);
		}
	}

	/**
	 * Create a new directional light - a light that faces one direction from very far away so the position does not matter
	 */
	public LightEntity() {
		this.light = new DirectionalLight(direction);
	}

	/**
	 * Create a new point light - a light that fades as distance increases
	 *
	 * @param constant  constant part of a reciprocal quadratic equation for attenuation
	 * @param linear    linear part of a reciprocal quadratic equation for attenuation
	 * @param quadratic quadratic part of a reciprocal quadratic equation for attenuation
	 *                  <p>
	 *                  for more info see http://wiki.ogre3d.org/tiki-index.php?page=-Point+Light+Attenuation
	 */
	public LightEntity(float constant, float linear, float quadratic) {
		this.light = new PointLight(position, constant, linear, quadratic);
	}

	/**
	 * Create a new spot light - a cone of light with smooth edges
	 *
	 * @param cutoff      inner cutoff angle of spotlight, before the light starts fading
	 * @param outerCutoff outer cutoff angle of a spotlight, where the light fades to nothing
	 */
	public LightEntity(float cutoff, float outerCutoff) {
		this.light = new SpotLight(position, direction, cutoff, outerCutoff);
	}

	@Override
	public void addToRenderData(RendererData data) {
		data.lightManager.addLight(light);
	}

	@Override
	public void calculateAbsoluteTransform(boolean passToChildren) {
		super.calculateAbsoluteTransform(passToChildren);
		updateLightPosition();
	}

	private void updateLightPosition() {
		direction.set(1, 0, 0, 0).mul(this.absoluteTransform);
		position.set(0, 0, 0, 1).mul(this.absoluteTransform);

		this.light.setDirection(this.direction);
		this.light.setPosition(this.position);
	}

	/**
	 * Gets the entity's light object
	 *
	 * @return A light object
	 */
	public Light getLight() {
		return light;
	}
}
