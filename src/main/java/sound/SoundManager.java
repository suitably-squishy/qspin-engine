package sound;

import org.lwjgl.openal.AL;
import org.lwjgl.openal.ALC;
import org.lwjgl.openal.ALCCapabilities;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.openal.ALC10.*;
import static org.lwjgl.system.MemoryUtil.NULL;

/**
 * Manages the current OpenAL device and context.
 */
public class SoundManager {
	// TODO allow context and device swapping

	/** The OpenAL ID of the current device. */
	private long device;

	/** The OpenAL ID of the current context. */
	private long context;

	/**
	 * Initialises the default sound device, and creates an OpenAL context.
	 */
	public void init() {
		this.device = alcOpenDevice((ByteBuffer) null);
		if (this.device == NULL)
			throw new IllegalStateException("Failed to open the default OpenAL device");

		ALCCapabilities deviceCaps = ALC.createCapabilities(device);
		this.context = alcCreateContext(device, (IntBuffer) null);
		if (this.context == NULL)
			throw new IllegalStateException("Failed to create OpenAL context.");
		alcMakeContextCurrent(context);
		AL.createCapabilities(deviceCaps);

		SoundListener.enable();
	}

	/**
	 * Destroys the current OpenAL device and context.
	 */
	public void dispose() {
		if (context != NULL) alcDestroyContext(context);
		if (device != NULL) alcCloseDevice(device);
	}
}
