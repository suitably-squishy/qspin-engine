package sound;

import io.resources.Resource;

import java.nio.ShortBuffer;

import static org.lwjgl.openal.AL10.*;

/**
 * Represents an OpenAL sound buffer holding a single sound's data.
 */
public class SoundBuffer extends Resource {

	/** The ID given to the buffer by OpenAL, used to reference it in memory. */
	private final int bufferID;

	/**
	 * Creates and uploads the provided sound data to OpenAL.
	 *
	 * @param pcmData    The raw pcm data to upload to openAL.
	 * @param bitDepth   The bit depth of the pcm data, either 8 or 16 bit.
	 * @param stereo     Whether or not the sound should be stereo,
	 *                   stereo will be ignored for sounds when played in a position in space.
	 * @param sampleRate The sample rate of the sound.
	 */
	public SoundBuffer(ShortBuffer pcmData, int bitDepth, boolean stereo, int sampleRate) {
		this.bufferID = alGenBuffers();

		int format = -1;
		if (stereo) {
			if (bitDepth == 8) {
				format = AL_FORMAT_STEREO8;
			} else if (bitDepth == 16) {
				format = AL_FORMAT_STEREO16;
			}
		} else {
			if (bitDepth == 8) {
				format = AL_FORMAT_MONO8;
			} else if (bitDepth == 16) {
				format = AL_FORMAT_MONO16;
			}
		}

		if (format == -1)
			throw new IllegalArgumentException("Unsupported bit depth: " + bitDepth);

		alBufferData(bufferID, format, pcmData, sampleRate);
	}

	@Override
	public void dispose() {
		super.dispose();
		alDeleteBuffers(this.bufferID);
	}

	/**
	 * Gets the ID of the buffer as given by OpenAL.
	 *
	 * @return The OpenAL buffer ID.
	 */
	public int getBufferID() {
		return bufferID;
	}
}
