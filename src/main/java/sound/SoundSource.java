package sound;

import org.joml.Vector3f;

import java.util.logging.Logger;

import static org.lwjgl.openal.AL10.*;

/**
 * Represents an OpenAL sound source, a position in space that can play a given {@link SoundBuffer} object.
 */
public class SoundSource {

	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/** The ID given to this source by OpenAL. Used to reference the source in memory. */
	private final int sourceID;

	/**
	 * Creates a source and uploads it to OpenAL, then sets the source to use the given buffer.
	 *
	 * @param loop     Whether or not the sound should loop.
	 * @param relative Whether or not the sound's position is relative to the listener.
	 * @param buffer   The buffer to bind to the source.
	 */
	public SoundSource(boolean loop, boolean relative, SoundBuffer buffer) {
		this(loop, relative);

		if (buffer != null) this.setBuffer(buffer);
	}

	/**
	 * Creates an empty sound source and uploads it to OpenAL.
	 *
	 * @param loop     Whether or not the sound should loop.
	 * @param relative Whether or not the sound's position is relative to the listener.
	 */
	public SoundSource(boolean loop, boolean relative) {
		sourceID = alGenSources();

		if (loop) alSourcei(sourceID, AL_LOOPING, AL_TRUE);

		if (relative) alSourcei(sourceID, AL_SOURCE_RELATIVE, AL_TRUE);
	}

	/**
	 * Creates an empty sound source and uploads it to OpenAL,
	 * sets the sound to not loop and the sound to not be relative.
	 */
	public SoundSource() {
		this(false, false);
	}

	/**
	 * Creates a sound source and uploads it to OpenAL,
	 * sets the sound to not loop and the sound to not be relative.
	 *
	 * @param buffer The sound buffer to bind.
	 */
	public SoundSource(SoundBuffer buffer) {
		this(false, false, buffer);
	}

	/**
	 * Binds the given sound buffer to this sound source.
	 *
	 * @param buffer The sound buffer to bind.
	 */
	public void setBuffer(SoundBuffer buffer) {
		if (buffer == null) {
			LOGGER.warning("Attempted to assign null SoundBuffer to SoundSource.");
			return;
		}
		this.setBuffer(buffer.getBufferID());
	}

	/**
	 * Binds the sound buffer with the given id to this sound source.
	 *
	 * @param bufferID The OpenAL ID of the buffer to bind.
	 */
	public void setBuffer(int bufferID) {
		stop();
		alSourcei(sourceID, AL_BUFFER, bufferID);
	}

	/**
	 * Sets the position of the sound source to the provided vector.
	 *
	 * @param position The position in 3d space of the sound. Units relative to the root entity.
	 */
	public void setPosition(Vector3f position) {
		if (position == null) {
			LOGGER.warning("Attempted to assign null position to SoundSource.");
			return;
		}
		alSource3f(sourceID, AL_POSITION, position.x, position.y, position.z);
	}

	/**
	 * Sets the speed of the sound source to the provided vector.
	 *
	 * @param speed The speed in 3d space of the sound. Units relative to the root entity.
	 */
	public void setSpeed(Vector3f speed) {
		if (speed == null) {
			LOGGER.warning("Attempted to assign null speed to SoundSource.");
			return;
		}
		alSource3f(sourceID, AL_VELOCITY, speed.x, speed.y, speed.z);
	}

	/**
	 * Sets the gain of the sound to the given value.
	 *
	 * @param gain The gain value to set.
	 */
	public void setGain(float gain) {
		alSourcef(sourceID, AL_GAIN, gain);
	}

	/**
	 * Sets an OpenAL property of the sound source to the given value.
	 *
	 * @param param The parameter index of the property to edit.
	 * @param value The value to assign to the parameter.
	 */
	public void setProperty(int param, float value) {
		alSourcef(sourceID, param, value);
	}

	/**
	 * Plays the sound source.
	 */
	public void play() {
		alSourcePlay(sourceID);
	}

	/**
	 * Gets whether the sound source is currently playing.
	 *
	 * @return True if the source is playing, false otherwise.
	 */
	public boolean isPlaying() {
		return alGetSourcei(sourceID, AL_SOURCE_STATE) == AL_TRUE;
	}

	/**
	 * Pauses the sound source.
	 */
	public void pause() {
		alSourcePause(sourceID);
	}

	/**
	 * Stops the sound source.
	 */
	public void stop() {
		alSourceStop(sourceID);
	}

	/**
	 * Disposes of the source, freeing the source in OpenAL.
	 */
	public void dispose() {
		stop();
		alDeleteSources(sourceID);
	}

	/**
	 * Gets the source's OpenAL ID.
	 *
	 * @return The source's OpenAL ID.
	 */
	public int getSourceID() {
		return sourceID;
	}
}
