package sound;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * A module to handle the holding and playing of sounds in 3D space using OpenAL.
 */
public class SoundModule {

	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/** A map of buffers to the source they are bound to. */
	private final Map<SoundBuffer, SoundSource> buffersToSources;

	/** A map of sound buffers, stored under their user provided names. */
	private final Map<String, SoundBuffer> soundBuffers;

	/** The position at which sounds in this module are positioned. Units relative to the root entity. */
	private final Vector3f position;
	/** The speed at which sounds in this module are moving. Units relative to the root entity. */
	private final Vector3f speed;

	/** Whether or not sounds in the module are positioned relative to the listener. */
	private final boolean relative;

	private boolean disposed = false;

	/**
	 * Creates a sound module stationary at position 0,0,0, makes the module not relative to the listener.
	 */
	public SoundModule() {
		this(false);
	}

	/**
	 * Creates a sound module stationary at position 0,0,0.
	 *
	 * @param relative Whether or not the sounds in the module are positioned relative to the listener.
	 */
	public SoundModule(boolean relative) {
		this.buffersToSources = new HashMap<>();
		this.soundBuffers = new HashMap<>();
		this.position = new Vector3f();
		this.speed = new Vector3f();
		this.relative = relative;
	}

	/**
	 * Creates a sound module with the provided position and speed.
	 *
	 * @param position The position of the sound module in 3D space.
	 * @param speed    The speed of the sound module in 3D space.
	 */
	public SoundModule(Vector3f position, Vector3f speed) {
		this();
		this.position.set(position);
		this.speed.set(speed);
	}

	/**
	 * Plays the sound in the provided sound buffer at the position and speed of the module.
	 *
	 * @param buffer The sound buffer to play.
	 */
	public void play(SoundBuffer buffer) {
		if (this.isDisposed()) {
			LOGGER.warning("Attempted to play sound on a disposed sound module. Ignoring.");
			return;
		}
		if (buffer.isDisposed()) {
			LOGGER.warning("Attempted to play a disposed sound buffer. Ignoring.");
			return;
		}
		if (buffersToSources.containsKey(buffer)) {
			buffersToSources.get(buffer).stop();
		} else {
			buffersToSources.put(buffer, new SoundSource(buffer));
		}
		buffersToSources.get(buffer).play();
	}

	/**
	 * Plays the sound with the given name in the module at the position and speed of the module.
	 *
	 * @param soundName The name of the sound to play.
	 */
	public void play(String soundName) {
		if (!soundBuffers.containsKey(soundName)) {
			LOGGER.warning("Attempt to play sound " + soundName + " failed as no such sound is bound.");
			return;
		}
		this.play(soundBuffers.get(soundName));
	}

	/**
	 * Stops the provided sound in the buffer.
	 *
	 * @param buffer The sound buffer to stop.
	 */
	public void stop(SoundBuffer buffer) {
		if (buffersToSources.containsKey(buffer)) {
			buffersToSources.get(buffer).stop();
		}
	}

	/**
	 * Stops the sound with the provided name.
	 *
	 * @param soundName The name of the sound to stop.
	 */
	public void stop(String soundName) {
		if (soundBuffers.containsKey(soundName)) {
			this.stop(soundBuffers.get(soundName));
		}
	}

	/**
	 * Stops all sounds in the module.
	 */
	public void stopAll() {
		buffersToSources.values().forEach(source -> {
			if (source != null) source.stop();
		});
	}

	/**
	 * Adds the provided sound into the module and returns it's generated name.
	 *
	 * @param buffer The buffer to add.
	 * @return The generated name of the sound, null if the module has been disposed.
	 */
	public String addSound(SoundBuffer buffer) {
		if (this.isDisposed()) {
			LOGGER.warning("Attempted to add a sound to a disposed sound module. Ignoring.");
			return null;
		}
		String uuid = UUID.randomUUID().toString();
		this.addSound(buffer, uuid);
		return uuid;
	}

	/**
	 * Adds the provided sound into the module under the given name. Sets the sound to not loop.
	 *
	 * @param buffer    The sound buffer to add.
	 * @param soundName The name to store the sound under.
	 */
	public void addSound(SoundBuffer buffer, String soundName) {
		this.addSound(buffer, soundName, false);
	}

	/**
	 * Adds the provided sound into the module under the given name.
	 *
	 * @param buffer    The sound buffer to add.
	 * @param soundName The name to store the sound under.
	 * @param loop      Whether or not the sound should loop.
	 */
	public void addSound(SoundBuffer buffer, String soundName, boolean loop) {
		if (this.isDisposed()) {
			LOGGER.warning("Attempted to add a sound to a disposed sound module. Ignoring.");
			return;
		}
		SoundSource soundSource = new SoundSource(loop, relative, buffer);
		this.soundBuffers.put(soundName, buffer);
		this.buffersToSources.put(buffer, soundSource);
		soundSource.setPosition(position);
		soundSource.setSpeed(speed);
	}

	/**
	 * Removes the given sound under the provided name from the module.
	 *
	 * @param soundName The name of the sound to remove.
	 */
	public void removeSound(String soundName) {
		if (!soundBuffers.containsKey(soundName)) {
			LOGGER.warning("Attempted to remove sound \"" + soundName + "\" from sound module with no such sound.");
			return;
		}
		SoundBuffer buffer = soundBuffers.get(soundName);
		SoundSource source = buffersToSources.get(buffer);
		source.dispose();
		buffersToSources.remove(buffer);
		soundBuffers.remove(soundName);
	}

	/**
	 * Disposes this module and cleans up assigned OpenAL memory.
	 */
	public void dispose() {
		buffersToSources.values().forEach(SoundSource::dispose);
		if (SoundListener.getHolder() == this) {
			SoundListener.becomeHolder(null);
		}
		disposed = true;
	}

	/**
	 * Updates the sound module's transformation to the given transformation matrix.
	 * Updates the listener position, if this sound module is the listener.
	 *
	 * @param transform The transformation matrix to use.
	 */
	public void updateTransformation(Matrix4f transform) {
		this.setPosition(transform.getColumn(3, new Vector3f()));

		if (SoundListener.getHolder() == this) {
			Vector4f pos4 = new Vector4f(0, 0, 0, 1).mul(transform);
			Vector4f at4 = new Vector4f(0, 0, -1, 0).mul(transform);
			Vector4f up4 = new Vector4f(0, 1, 0, 0).mul(transform);

			SoundListener.setPosition(pos4.x, pos4.y, pos4.z);
			SoundListener.setOrientation(at4.x, at4.y, at4.z, up4.x, up4.y, up4.z);
		}
	}

	/**
	 * Sets the position of the sound module to the provided vector.
	 *
	 * @param position The position to assign.
	 */
	public void setPosition(Vector3f position) {
		this.position.set(position);
		buffersToSources.values().forEach(source -> source.setPosition(this.position));
	}

	/**
	 * Sets the speed of the sound module to the provided vector.
	 *
	 * @param speed The speed to assign.
	 */
	public void setSpeed(Vector3f speed) {
		this.speed.set(speed);
		buffersToSources.values().forEach(source -> source.setSpeed(this.speed));
	}

	/**
	 * Makes this sound module the global listener.
	 */
	public void becomeListener() {
		SoundListener.becomeHolder(this);
	}

	/**
	 * Gets whether the sound module is disposed or not.
	 *
	 * @return True if the module has been disposed, false otherwise.
	 */
	public boolean isDisposed() {
		return disposed;
	}
}
