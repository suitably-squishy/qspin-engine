package sound;

import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.util.logging.Logger;

import static org.lwjgl.openal.AL10.*;

/**
 * A utility class to represent the single global sound listener in OpenAL.
 */
public class SoundListener {

	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/** The sound module currently set to be the listener. */
	private static SoundModule holder;

	private static boolean enabled = false;

	/**
	 * Sets the provided sound module as the current listener.
	 *
	 * @param holder The sound module to set as listener.
	 */
	public static void becomeHolder(SoundModule holder) {
		SoundListener.holder = holder;
	}

	/**
	 * Gets the sound module currently set as the listener.
	 *
	 * @return The listener sound module.
	 */
	public static SoundModule getHolder() {
		return holder;
	}

	/**
	 * Sets the position of the listener to the provided vector.
	 *
	 * @param position The position of the listener in 3D space, units relative to the root entity.
	 */
	public static void setPosition(Vector3f position) {
		SoundListener.setPosition(position.x, position.y, position.z);
	}

	/**
	 * Sets the position of the listener to the provided values.
	 *
	 * @param x The x coordinate of the listener in 3D space, units relative to the root entity.
	 * @param y The y coordinate of the listener in 3D space, units relative to the root entity.
	 * @param z The z coordinate of the listener in 3D space, units relative to the root entity.
	 */
	public static void setPosition(float x, float y, float z) {
		if (!enabled) {
			LOGGER.warning("Cannot set position of a listener before OpenAL is fully initialised.");
			return;
		}
		alListener3f(AL_POSITION, x, y, z);
	}

	/**
	 * Sets the speed of the listener to the provided vector.
	 *
	 * @param speed The speed of the listener in 3D space, units relative to the root entity.
	 */
	public static void setSpeed(Vector3f speed) {
		SoundListener.setSpeed(speed.x, speed.y, speed.z);
	}

	/**
	 * Sets the speed of the listener to the provided values.
	 *
	 * @param x The x speed of the listener in 3D space, units relative to the root entity.
	 * @param y The y speed of the listener in 3D space, units relative to the root entity.
	 * @param z The z speed of the listener in 3D space, units relative to the root entity.
	 */
	public static void setSpeed(float x, float y, float z) {
		if (!enabled) {
			LOGGER.warning("Cannot set speed of a listener before OpenAL is fully initialised.");
			return;
		}
		alListener3f(AL_VELOCITY, x, y, z);
	}

	/**
	 * Sets the orientation of the listener to the two provided "at" and "up" vectors.
	 *
	 * @param at The direction the listener is looking in.
	 * @param up The direction which is "up" for the listener.
	 */
	public static void setOrientation(Vector3f at, Vector3f up) {
		if (!enabled) {
			LOGGER.warning("Cannot set orientation of a listener before OpenAL is fully initialised.");
			return;
		}
		FloatBuffer buffer = BufferUtils.createFloatBuffer(6);
		at.get(0, buffer);
		up.get(3, buffer);
		alListenerfv(AL_ORIENTATION, buffer);
	}

	/**
	 * Sets the orientation of the listener to the two provided "at" and "up" vectors, in individual floats.
	 *
	 * @param atX The X coordinate of the vector in which the listener is looking.
	 * @param atY The Y coordinate of the vector in which the listener is looking.
	 * @param atZ The Z coordinate of the vector in which the listener is looking.
	 * @param upX The X coordinate of the vector which is "up" for the listener.
	 * @param upY The Y coordinate of the vector which is "up" for the listener.
	 * @param upZ The Z coordinate of the vector which is "up" for the listener.
	 */
	public static void setOrientation(float atX, float atY, float atZ, float upX, float upY, float upZ) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(6);
		buffer.put(atX).put(atY).put(atZ).put(upX).put(upY).put(upZ).rewind();
		alListenerfv(AL_ORIENTATION, buffer);
	}

	/**
	 * Enables writing to the sound listener, should not be performed until OpenAL has been started.
	 */
	protected static void enable() {
		SoundListener.enabled = true;
	}

	/**
	 * Disables writing to the sound listener.
	 */
	protected static void disable() {
		SoundListener.enabled = false;
	}
}
