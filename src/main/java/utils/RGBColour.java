package utils;

import org.joml.Vector4f;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class for representing an RGB colour.
 */
public class RGBColour {

	/**
	 * The red component of the colour 0 to 1.
	 */
	private float red;
	/**
	 * The green component of the colour 0 to 1.
	 */
	private float green;
	/**
	 * The blue component of the colour 0 to 1.
	 */
	private float blue;
	/**
	 * The transparent component of the colour 0 to 1.
	 */
	private float alpha;

	/**
	 * A regex pattern for matching hex colour strings.
	 */
	private final static Pattern colourPattern = Pattern.compile("^#?([A-Fa-f0-9]{2})([A-Fa-f0-9]{2})([A-Fa-f0-9]{2})([A-Fa-f0-9]{2})?$");

	public static final RGBColour WHITE = new RGBColour(1f, 1f, 1f, 1f);
	public static final RGBColour BLACK = new RGBColour(0f, 0f, 0f, 1f);
	public static final RGBColour RED = new RGBColour(1f, 0f, 0f, 1f);
	public static final RGBColour GREEN = new RGBColour(0f, 1f, 0f, 1f);
	public static final RGBColour BLUE = new RGBColour(0f, 0f, 1f, 1f);
	public static final RGBColour TRANSPARENT = new RGBColour(0f, 0f, 0f, 0f);

	/**
	 * Creates a colour with the given component values 0 to 255.
	 *
	 * @param red   The red component 0 to 255.
	 * @param green The green component 0 to 255.
	 * @param blue  The blue component 0 to 255.
	 * @param alpha The transparent component 0 to 255.
	 */
	public RGBColour(int red, int green, int blue, int alpha) {
		setRed(red);
		setGreen(green);
		setBlue(blue);
		setAlpha(alpha);
	}

	/**
	 * Creates a colour with the given component values 0 to 255 with an alpha value of 255.
	 *
	 * @param red   The red component 0 to 255.
	 * @param green The green component 0 to 255.
	 * @param blue  The blue component 0 to 255.
	 */
	public RGBColour(int red, int green, int blue) {
		this(red, green, blue, 255);
	}

	/**
	 * Creates a colour with the given component values 0 to 1.
	 *
	 * @param red   The red component 0 to 1.
	 * @param green The green component 0 to 1.
	 * @param blue  The blue component 0 to 1.
	 * @param alpha The transparent component 0 to 1.
	 */
	public RGBColour(float red, float green, float blue, float alpha) {
		setRed(red);
		setGreen(green);
		setBlue(blue);
		setAlpha(alpha);
	}

	/**
	 * Creates a colour with the given component values 0 to 1 and an alpha value of 1.
	 *
	 * @param red   The red component 0 to 1.
	 * @param green The green component 0 to 1.
	 * @param blue  The blue component 0 to 1.
	 */
	public RGBColour(float red, float green, float blue) {
		this(red, green, blue, 1f);
	}

	/**
	 * Creates the a colour which is a copy of the provided colour.
	 *
	 * @param colour The colour to copy.
	 */
	public RGBColour(RGBColour colour) {
		this.setRed(colour.red);
		this.setGreen(colour.green);
		this.setBlue(colour.blue);
		this.setAlpha(colour.alpha);
	}

	/**
	 * Creates a colour from the provided hex string.
	 *
	 * @param hexString - The hex string to create the colour of in the format
	 *                  RRGGBB or RRGGBBAA.
	 */
	public RGBColour(String hexString) throws IllegalArgumentException {
		Matcher colourMatcher = colourPattern.matcher(hexString);
		if (colourMatcher.find()) {
			this.setRed(Integer.parseInt(colourMatcher.group(1), 16));
			this.setGreen(Integer.parseInt(colourMatcher.group(2), 16));
			this.setBlue(Integer.parseInt(colourMatcher.group(3), 16));

			if (colourMatcher.group(4) == null) {
				this.setAlpha(1f);
			} else {
				this.setAlpha(Integer.parseInt(colourMatcher.group(4), 16));
			}
		} else {
			throw new IllegalArgumentException("The provided hex string is invalid: " + hexString);
		}
	}

	//------------------------- RED -----------------------------//

	/**
	 * Gets the red component of the colour 0 to 1.
	 */
	public float getRed() {
		return red;
	}

	/**
	 * Sets the red component of the colour.
	 *
	 * @param red The red component 0 to 1.
	 */
	public void setRed(float red) {
		if (red < 0f) {
			red = 0f;
		} else if (red > 1f) {
			red = 1f;
		}
		this.red = red;
	}

	/**
	 * Sets the red component of the colour.
	 *
	 * @param red The red component 0 to 255.
	 */
	public void setRed(int red) {
		setRed(red / 255f);
	}

	//------------------------ GREEN --------------------------//

	/**
	 * Gets the green component of the colour 0 to 1.
	 */
	public float getGreen() {
		return green;
	}

	/**
	 * Sets the green component of the colour.
	 *
	 * @param green The green component 0 to 1.
	 */
	public void setGreen(float green) {
		if (green < 0f) {
			green = 0f;
		} else if (green > 1f) {
			green = 1f;
		}
		this.green = green;
	}

	/**
	 * Sets the green component of the colour.
	 *
	 * @param green The green component 0 to 255.
	 */
	public void setGreen(int green) {
		setGreen(green / 255f);
	}

	//----------------------- BLUE -----------------------------//

	/**
	 * Gets the blue component of the colour 0 to 1.
	 */
	public float getBlue() {
		return blue;
	}

	/**
	 * Sets the blue component of the colour.
	 *
	 * @param blue The blue component 0 to 1.
	 */
	public void setBlue(float blue) {
		if (blue < 0f) {
			blue = 0f;
		} else if (blue > 1f) {
			blue = 1f;
		}
		this.blue = blue;
	}

	/**
	 * Sets the blue component of the colour.
	 *
	 * @param blue The blue component 0 to 255.
	 */
	public void setBlue(int blue) {
		setBlue(blue / 255f);
	}

	//----------------------- ALPHA ----------------------------//

	/**
	 * Gets the alpha component of the colour 0 to 1.
	 */
	public float getAlpha() {
		return alpha;
	}

	/**
	 * Sets the alpha component of the colour.
	 *
	 * @param alpha The alpha component 0 to 1.
	 */
	public void setAlpha(float alpha) {
		if (alpha < 0f) {
			alpha = 0f;
		} else if (alpha > 1f) {
			alpha = 1f;
		}
		this.alpha = alpha;
	}

	/**
	 * Sets the alpha component of the colour.
	 *
	 * @param alpha The alpha component 0 to 255.
	 */
	public void setAlpha(int alpha) {
		setAlpha(alpha / 255f);
	}

	@Override
	public String toString() {
		return "R: " + red + ", G: " + green + ", B: " + blue + ", A: " + alpha;
	}

	/**
	 * Gets the colour as a string of the form RRGGBB in hex, rounds any decimal colours.
	 *
	 * @return A string of the form RRGGBB.
	 */
	public String toHexStringNoAlpha() {
		String hexRed = Integer.toHexString((int) (red * 255));
		String hexBlue = Integer.toHexString((int) (blue * 255));
		String hexGreen = Integer.toHexString((int) (green * 255));
		if (hexRed.length() < 2) {
			hexRed = '0' + hexRed;
		}
		if (hexBlue.length() < 2) {
			hexBlue = '0' + hexBlue;
		}
		if (hexGreen.length() < 2) {
			hexGreen = '0' + hexGreen;
		}
		return hexRed + hexBlue + hexGreen;
	}

	/**
	 * Gets the colour as a string of the form RRGGBBAA in hex, rounds any decimal colours.
	 *
	 * @return A string of the form RRGGBBAA.
	 */
	public String toHexString() {
		String hexAlpha = Integer.toHexString((int) (alpha * 255));
		if (hexAlpha.length() < 2) {
			hexAlpha = '0' + hexAlpha;
		}
		return this.toHexStringNoAlpha() + hexAlpha;
	}

	/**
	 * Gets the colour as a vector, used mainly for rendering
	 *
	 * @return The colour in RGBA format in a Vector4f
	 */
	public Vector4f toVector4f() {
		return new Vector4f(red, green, blue, alpha);
	}


	/**
	 * Gets the colour as an array, used mainly for rendering
	 *
	 * @return The colour in RGBA format in a four element float array
	 */
	public float[] toFloatArray() {
		return new float[]{red, green, blue, alpha};
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		RGBColour rgbColour = (RGBColour) o;
		return Float.compare(rgbColour.red, red) == 0 &&
				Float.compare(rgbColour.green, green) == 0 &&
				Float.compare(rgbColour.blue, blue) == 0 &&
				Float.compare(rgbColour.alpha, alpha) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(red, green, blue, alpha);
	}
}
