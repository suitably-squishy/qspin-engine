package utils;

/**
 * A utility class for running functions and timing the time they take to run.
 */
public class TimeMe {
	/**
	 * A utility to check the time it takes to run a function
	 *
	 * @param function a function should be declared as a lambda function to time
	 * @return The length of time the function took to run in ms.
	 */
	public static long timeMe(TimedFunction function) {
		long startTime = System.currentTimeMillis();

		function.run();

		long endTime = System.currentTimeMillis();

		long duration = (endTime - startTime);
		return duration;
	}

	@FunctionalInterface
	public interface TimedFunction {

		/**
		 * The function to run and time.
		 */
		void run();
	}
}
