package utils;

import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

/**
 * A class for representing a material with RGB ambient, diffuse and specular colours.
 */
public class RGBMaterial {

	private RGBColour ambient;
	private RGBColour diffuse;
	private RGBColour specular;
	private float shininess;

	private static final float DEFAULT_SHININESS = 16;

	/**
	 * Creates a new RGB material with the provided ambient, diffuse, specular and shininess values.
	 *
	 * @param ambient   The ambient colour of the material.
	 * @param diffuse   The colour of the material under diffuse lighting.
	 * @param specular  The colour of the material's specular highlight.
	 * @param shininess The shininess of the material.
	 */
	public RGBMaterial(RGBColour ambient, RGBColour diffuse, RGBColour specular, float shininess) {
		this.ambient = ambient;
		this.diffuse = diffuse;
		this.specular = specular;
		this.shininess = shininess;
	}

	/**
	 * Creates an RGB material with the provided colours, using the ambient colour for diffuse as well.
	 *
	 * @param ambient   The ambient and diffuse colour of the material.
	 * @param specular  The colour of the material's specular highlight.
	 * @param shininess The shininess of the material.
	 */
	public RGBMaterial(RGBColour ambient, RGBColour specular, float shininess) {
		this(ambient, ambient, specular, shininess);
	}

	/**
	 * Creates an RGB material with the provided colour and shininess.
	 *
	 * @param ambient   The ambient, diffuse and specular colour of the material.
	 * @param shininess The shininess of the material.
	 */
	public RGBMaterial(RGBColour ambient, float shininess) {
		this(ambient, ambient, ambient, shininess);
	}

	/**
	 * Creates an RGB material with the provided colour and default shininess.
	 *
	 * @param ambient The ambient, diffuse and specular colour of the material.
	 */
	public RGBMaterial(RGBColour ambient) {
		this(ambient, ambient, ambient, DEFAULT_SHININESS);
	}

	/**
	 * Clones the provided RGB material and underlying colours.
	 *
	 * @param material The material to clone.
	 */
	public RGBMaterial(RGBMaterial material) {
		this.ambient = new RGBColour(material.getAmbient());
		this.diffuse = new RGBColour(material.getDiffuse());
		this.specular = new RGBColour(material.getSpecular());
		this.shininess = material.getShininess();
	}

	/**
	 * Puts the material into matrix format in the provided buffer.
	 *
	 * @param target The MatBuffer to add the material to.
	 * @return The RGBMaterial.
	 */
	public RGBMaterial get(RGBMaterial.Buffer target) {
		target.put(this);
		return this;
	}

	// GETTERS AND SETTERS

	public RGBColour getAmbient() {
		return ambient;
	}

	public void setAmbient(RGBColour ambient) {
		this.ambient = ambient;
	}

	public RGBColour getDiffuse() {
		return diffuse;
	}

	public void setDiffuse(RGBColour diffuse) {
		this.diffuse = diffuse;
	}

	public RGBColour getSpecular() {
		return specular;
	}

	public void setSpecular(RGBColour specular) {
		this.specular = specular;
	}

	public float getShininess() {
		return shininess;
	}

	public void setShininess(float shininess) {
		this.shininess = shininess;
	}

	/**
	 * A class for representing an RGBMaterial in a buffer.
	 */
	public static class Buffer {

		// Individual material buffers
		private FloatBuffer materialAmbientBuffer;
		private FloatBuffer materialDiffuseBuffer;
		private FloatBuffer materialSpecularBuffer;
		private FloatBuffer materialShininessBuffer;

		private final static int VECTOR_CAPACITY = 4;
		private final static int FLOAT_CAPACITY = 1;

		public void genMaterialBuffer(int size) {
			materialAmbientBuffer = BufferUtils.createFloatBuffer(VECTOR_CAPACITY * size);
			materialDiffuseBuffer = BufferUtils.createFloatBuffer(VECTOR_CAPACITY * size);
			materialSpecularBuffer = BufferUtils.createFloatBuffer(VECTOR_CAPACITY * size);
			materialShininessBuffer = BufferUtils.createFloatBuffer(FLOAT_CAPACITY * size);
		}

		public void put(RGBMaterial input) {
			materialAmbientBuffer.put(input.getAmbient().toFloatArray());
			materialDiffuseBuffer.put(input.getDiffuse().toFloatArray());
			materialSpecularBuffer.put(input.getSpecular().toFloatArray());
			materialShininessBuffer.put(input.getShininess());
		}

		public void position(int postion) {
			materialAmbientBuffer.position((postion) * VECTOR_CAPACITY);
			materialDiffuseBuffer.position((postion) * VECTOR_CAPACITY);
			materialSpecularBuffer.position((postion) * VECTOR_CAPACITY);
			materialShininessBuffer.position((postion) * FLOAT_CAPACITY);
		}

		public void rewind() {
			materialAmbientBuffer.rewind();
			materialDiffuseBuffer.rewind();
			materialSpecularBuffer.rewind();
			materialShininessBuffer.rewind();
		}

		public int[] capacity() {
			return new int[]{materialAmbientBuffer.capacity(),
					materialDiffuseBuffer.capacity(),
					materialSpecularBuffer.capacity(),
					materialShininessBuffer.capacity()};
		}

		public FloatBuffer[] getBuffers() {
			return new FloatBuffer[]{materialAmbientBuffer,
					materialDiffuseBuffer,
					materialSpecularBuffer,
					materialShininessBuffer};
		}
	}
}
