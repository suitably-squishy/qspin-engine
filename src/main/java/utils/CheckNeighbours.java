package utils;

/**
 * A utility class for helping simplify voxel models by detecting cube faces that are not visible.
 */
public class CheckNeighbours {

	public static final byte EMPTY = 0;
	public static final byte LEFT = 0b00000001;
	public static final byte RIGHT = 0b00000010;
	public static final byte BACK = 0b00000100;
	public static final byte FRONT = 0b00001000;
	public static final byte BOTTOM = 0b00010000;
	public static final byte TOP = 0b00100000;

	/**
	 * Checks the 6 cardinal directions of each point in a 3 dimensional array
	 * and determine if there is a value or a zero there.
	 * <p>
	 * Used to determine if a face can be culled for optimisation and stitching.
	 *
	 * @param intake A 3 dimensional list of points returned from loading a palette
	 * @return A byte with bit flags set for faces which do not back onto another opaque cube.
	 * If equal to {@link CheckNeighbours#EMPTY}, all faces are obscured.
	 */
	public static byte[][][] checkNeighbours(short[][][] intake) {
		byte[][][] directions = new byte[intake.length][intake[0].length][intake[0][0].length];
		for (int x = 0; x < intake.length; x++) {
			for (int y = 0; y < intake[x].length; y++) {
				for (int z = 0; z < intake[x][y].length; z++) {
					byte direction = EMPTY;
					if (intake[x][y][z] != 0) {
						if (x == 0 || intake[x - 1][y][z] == 0) {
							direction |= LEFT;
						}
						if (x == intake.length - 1 || x != intake.length - 1 && intake[x + 1][y][z] == 0) {
							direction |= RIGHT;
						}

						if (y == 0 || intake[x][y - 1][z] == 0) {
							direction |= BACK;
						}
						if (y == intake[x].length - 1 || y != intake[x].length - 1 && intake[x][y + 1][z] == 0) {
							direction |= FRONT;
						}

						if (z == 0 || intake[x][y][z - 1] == 0) {
							direction |= BOTTOM;
						}
						if (z == intake[x][y].length - 1 || z != intake[x][y].length - 1 && intake[x][y][z + 1] == 0) {
							direction |= TOP;
						}
					}
					directions[x][y][z] = direction;
				}
			}
		}
		return directions;
	}
}
