package render;


import java.util.logging.Logger;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;
import static org.lwjgl.opengl.GL33.glVertexAttribDivisor;
import static render.Shader.compileShader;

/**
 * A class for storing information on an OpenGl program containing shaders, VBOs and VAOs.
 */
public class GPUProgram {

	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/**
	 * The files to be used on the gpu
	 */
	private final String vertexShaderFile;
	private final String fragmentShaderFile;

	//the actual identifier of the program as an index returned by openGl
	private int program;

	//the index for the shaders to passed to openGl
	private int vertex_shader;
	private int fragment_shader;

	//the index of the vertex array attribute to pass to openGl
	private int vaoID = -1;

	/**
	 * The individual buffers to pass to openGl
	 * These are bound to the VAO
	 * 0 - vertex buffer
	 * 1 - normals buffer
	 * 2-5 - materials buffers
	 * 6 - transform buffer
	 * 7 - model offset buffer
	 */
	private int[] vboIDs;

	public GPUProgram(String vertexShaderFile, String fragmentShaderFile) {
		this.vertexShaderFile = vertexShaderFile;
		this.fragmentShaderFile = fragmentShaderFile;
	}

	/**
	 * Initialises the GPU program, compiling and linking the shaders.
	 */
	public void startup() {
		LOGGER.info("Gpu Program starting!");

		//compile the shaders to the program
		vertex_shader = compileShader(vertexShaderFile, GL_VERTEX_SHADER);
		fragment_shader = compileShader(fragmentShaderFile, GL_FRAGMENT_SHADER);
		//create the program and attach the shaders, then link the program to openGl and therefore the graphics card
		program = glCreateProgram();
		glAttachShader(program, vertex_shader);
		glAttachShader(program, fragment_shader);
		glLinkProgram(program);

		//validate the program link
		if (glGetProgrami(program, GL_LINK_STATUS) == GL_FALSE) {
			LOGGER.severe("Failed to link shaders");
			System.exit(-1);
		}

		//validate the program itself
		glValidateProgram(program);
		if (glGetProgrami(program, GL_VALIDATE_STATUS) == GL_FALSE) {
			LOGGER.severe("Failed to validate program");
			System.exit(-1);
		}

		//now the shaders are linked, remove them from memory
		glDeleteShader(vertex_shader);
		glDeleteShader(fragment_shader);
	}

	public void bind() {
		glUseProgram(program);
		glBindVertexArray(vaoID);
	}


	/**
	 * Perform initial generation of the openGl buffers
	 * Does not specify the contents of the buffer, only generates the handles.
	 * Must be called AFTER startup()
	 *
	 * @param buffers the number of buffers to set up
	 */
	public void setupBuffers(int buffers) {
		vaoID = glGenVertexArrays();
		glBindVertexArray(vaoID);
		if (vaoID == -1) {
			LOGGER.severe("vaoID Not Initialised");
		}

		vboIDs = new int[buffers];

		for (int i = 0; i < buffers; i++) {
			vboIDs[i] = -1;
			vboIDs[i] = glGenBuffers();
		}

		for (int vboID : vboIDs) {
			if (vboID == -1) {
				LOGGER.severe("vboID Not Initialised");
			}
		}
	}


	/**
	 * Specifies to openGl which attributes are active and which are per-vertex or per-instance.
	 * Must be called AFTER startup() and setupBuffers().
	 *
	 * @param attribs an array of integers signifying the attribute divisor
	 *                with a length corresponding to the number of attributes
	 */
	public void setupAttributes(int[] attribs) {
		for (int i = 0; i < attribs.length; i++) {
			if (attribs[i] != -1) {
				glEnableVertexAttribArray(i);
				glVertexAttribDivisor(i, attribs[i]);
			}
		}
	}

	/**
	 * Gets the location of the named uniform within gl
	 *
	 * @param uniformName the name of the uniform, case sensitive
	 * @return the index of the uniform
	 */
	public int getUniformIndex(String uniformName) {
		return glGetUniformLocation(program, uniformName);
	}

	/**
	 * Shuts down and deletes the GPU program from memory.
	 */
	public void shutdown() {
		glDeleteProgram(program);
	}

	public int getProgram() {
		return program;
	}

	public int getVaoID() {
		return vaoID;
	}

	public int[] getVboIDs() {
		return vboIDs;
	}
}
