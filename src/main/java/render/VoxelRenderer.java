package render;

import org.lwjgl.BufferUtils;
import utils.RGBMaterial;
import window.Window;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL31.glDrawArraysInstanced;

/**
 * A class to render a scene built of 3D cubes constructed from up to six quads.
 */
public class VoxelRenderer {

	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	// The capacity of floatbuffers that hold matrices and vectors.
	private static final int MATRIX_CAPACITY = 16;
	private static final int VECTOR_CAPACITY = 4;

	/** The data that will be rendered. */
	private RendererData data;

	/** The program to run on the gpu. */
	private GPUProgram gpuProgram;

	/** The index of the vertex array attribute to pass to openGl */
	private int vaoID;

	/**
	 * The individual buffers to pass to openGl
	 * These are bound to the VAO
	 * 0 - vertex buffer
	 * 1 - normal buffer
	 * 2 - material ambient buffer
	 * 3 - material diffuse buffer
	 * 4 - material specular buffer
	 * 5 - material shininess buffer
	 * 6 - transform buffer
	 * 7 - model offset buffer
	 */
	private int[] vboIDs;

	/** The amount of element instances. */
	private int elementInstances = 0;

	/** The array of elements to render. */
	private VoxelElement[] voxelElements;

	/** The total number of quads to render. */
	private int totalFaceInstances = 0;

	/** The total size in floats of the transform buffer. */
	private int transformBufferSize;

	/** The buffer of face offsets relative to the middle of each element. */
	private FloatBuffer offsetBuffer;

	/** The buffer of materials to render. */
	private final RGBMaterial.Buffer materialBuffer;

	/** holds the window for the FBO */
	private Window window;

	/** The FrameBufferObject that this renderer will render to */
	private FrameBufferObject fbo;

	/**
	 * Creates a new voxel renderer with the default GPU program
	 */
	public VoxelRenderer(Window window) {
		gpuProgram = new GPUProgram("Vertex", "Fragment");
		fbo = new FrameBufferObject(window);
		// Sets up buffer of the materials
		materialBuffer = new RGBMaterial.Buffer();
		data = null;
	}

	/**
	 * Generates the total number of face elements for array initialisation
	 */
	public void genTotalFaceInstances() {
		totalFaceInstances = 0;
		for (VoxelElement i : voxelElements) {
			totalFaceInstances += i.getSize();
		}
	}

	/**
	 * Generates the offset buffer as empty.
	 * Can be recalled on the fly
	 */
	private void genOffsetBuffer() {
		offsetBuffer = BufferUtils.createFloatBuffer(16 * totalFaceInstances);
	}

	/**
	 * Generates the colour buffer as empty.
	 * Can be recalled on the fly
	 */
	private void genMaterialBuffers() {
		materialBuffer.genMaterialBuffer(totalFaceInstances);
	}

	/**
	 * Loads the rendering elements from the rendering data
	 * Can be recalled on the fly
	 */
	private void loadElementsFromData() {
		voxelElements = new VoxelElement[0];
		voxelElements = data.elements.toArray(new VoxelElement[0]);
		elementInstances = voxelElements.length;
	}

	/**
	 * Sets up the buffer memory offsets for each element.
	 * Can be recalled on the fly
	 */
	private void setupElementMemoryOffsets() {
		int bufferPos = 0;
		for (int i = 0; i < elementInstances; i++) {
			voxelElements[i].setMemOffset(MATRIX_CAPACITY * bufferPos);
			bufferPos += voxelElements[i].getSize();
		}
	}

	/**
	 * Populates the material and offset buffers.
	 * Can be recalled on the fly
	 */
	private void makeOffsetAndMaterialBuffers() {
		int iterator = 0;
		//populate the buffers from the voxelElements
		for (VoxelElement voxelElement : voxelElements) {
			for (int iCube = 0; iCube < voxelElement.getSize(); iCube++) {
				voxelElement.getOffsets().get(iCube).get(offsetBuffer);
				offsetBuffer.position((iterator + 1) * MATRIX_CAPACITY);
				voxelElement.getMaterials().get(iCube).get(materialBuffer);
				materialBuffer.position((iterator + 1));
				iterator++;
			}
			voxelElement.clearMemory();
		}
		//reset the buffers to be read
		offsetBuffer.rewind();
		materialBuffer.rewind();
	}

	/**
	 * Sets up all required buffers for storing data cpu side.
	 * Can be recalled on the fly
	 */
	private void setupRendererBuffers() {
		loadElementsFromData();
		genTotalFaceInstances();
		genOffsetBuffer();
		genMaterialBuffers();
		setupElementMemoryOffsets();
		makeOffsetAndMaterialBuffers();
		//gets the size in bytes of the transform buffer for each element
		transformBufferSize = BufferUtils.createFloatBuffer(16 * totalFaceInstances).capacity() * Float.BYTES;
	}

	/**
	 * Remakes the openGl buffer passing transforms to each instanced cube
	 * MUST be recalled if total number of entities increases or decreases from current value
	 * !Potential performance hit! Potential solution - only call setup memory offsets on decrease
	 */
	public void remakeGLBuffer() {
		setupRendererBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[6]);
		glBufferData(GL_ARRAY_BUFFER, transformBufferSize, GL_DYNAMIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[2]);

		glBufferData(GL_ARRAY_BUFFER, materialBuffer.getBuffers()[0], GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[3]);
		glBufferData(GL_ARRAY_BUFFER, materialBuffer.getBuffers()[1], GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[4]);
		glBufferData(GL_ARRAY_BUFFER, materialBuffer.getBuffers()[2], GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[5]);
		glBufferData(GL_ARRAY_BUFFER, materialBuffer.getBuffers()[3], GL_DYNAMIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[7]);
		glBufferData(GL_ARRAY_BUFFER, offsetBuffer, GL_DYNAMIC_DRAW);
	}

	/**
	 * Binds the transformation buffer to the appropriate vbo.
	 * Must be called before flushing the transform buffer
	 */
	private void bindTransformationBuffer() {
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[6]);
	}

	/**
	 * Flushes the transform buffer of each individual element to the graphics card.
	 * This is sent per instanced face (6 vertexes)
	 * Will only flush if the element has requested a render cycle
	 */
	private void flushTransformBuffer() {
		final List<VoxelElement> elementsRendered = new ArrayList<>();
		bindTransformationBuffer();
		for (VoxelElement voxelElement : data.elementsToRender) {
			glBufferSubData(GL_ARRAY_BUFFER, voxelElement.getMemOffset() * Float.BYTES, voxelElement.getTransformBuffer());
			elementsRendered.add(voxelElement);
		}
		data.elementsToRender.removeAll(elementsRendered);
	}

	public void startup() {
		//setup code for communication with the gpu
		LOGGER.info("Voxel Renderer starting!");

		int[] attributeDivisors = {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

		gpuProgram.startup();
		gpuProgram.setupBuffers(8);
		vaoID = gpuProgram.getVaoID();
		vboIDs = gpuProgram.getVboIDs();

		fbo.startup();
		fbo.attachColourBuffer();
		fbo.attachColourBuffer("BloomBuffer");
		fbo.attachDepthBuffer();
		fbo.prepare();

		//check for rendererData
		if (data == null) {
			LOGGER.severe("Error, no RendererData!");
			System.exit(-1);
			return;
		}

		// Setup the initial data for openGl
		setupRendererBuffers();

		// Setup the structure of the buffers to be sent to openGl
		setupGlBuffers();

		gpuProgram.setupAttributes(attributeDivisors);

	}

	public FrameBufferObject renderScene() {
		// Bind the gpu program to gl
		gpuProgram.bind();

		// Bind the FBO to gl
		fbo.bind();
		if (!FrameBufferObject.validate())
			LOGGER.warning("FBO not complete " + glCheckFramebufferStatus(GL_FRAMEBUFFER));

		// Setup gl state
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glBlendFunc(GL_ONE, GL_ZERO);
		glEnable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

		// Get the location of the camera uniforms
		int cameraProjectionUniformID = gpuProgram.getUniformIndex("cameraProjection");
		int cameraPositionUniformID = gpuProgram.getUniformIndex("cameraPosition");

		// Create a view transform and set it as a uniform to all vertices
		glUniformMatrix4fv(cameraProjectionUniformID, false, data.currentCamera.getTransformBuffer());
		glUniform4fv(cameraPositionUniformID, data.currentCamera.getWorldPosAsArray());

		// Setup lighting in the scene
		data.lightManager.exportLightDataToGL(gpuProgram);

		// Flush the transforms of each of the quads
		flushTransformBuffer();


		// Draw 2 triangles of 3 vertices each
		// Iterate this over totalFaceInstances, passing a new part of the transform, offset and material buffer
		glDrawArraysInstanced(GL_TRIANGLES, 0, 6, totalFaceInstances);

		fbo.drawToBuffers();

		FrameBufferObject.clearBind();

		return fbo;
	}

	public void shutdown() {
		glDeleteBuffers(vaoID);
		glDeleteVertexArrays(vaoID);
		gpuProgram.shutdown();
		gpuProgram = null;
		fbo.destroy();
	}

	/**
	 * Sets up all required buffers for storing data gpu side.
	 */
	public void setupGlBuffers() {
		// Setup of the buffer of cube vertexes to sent to openGl
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[0]);
		glVertexAttribPointer(0, 4, GL_FLOAT, false, 0, 0);
		glBufferData(GL_ARRAY_BUFFER, ShapeGenerator.Cube.faceVertexPositions, GL_STATIC_DRAW);

		// Setup the surface normals of each vertex
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[1]);
		glVertexAttribPointer(1, 4, GL_FLOAT, false, 0, 0);
		glBufferData(GL_ARRAY_BUFFER, ShapeGenerator.Cube.faceVertexNormals, GL_STATIC_DRAW);

		// Setup of the buffers of cube materials to sent to openGl
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[2]);
		glVertexAttribPointer(2, 4, GL_FLOAT, false, 0, 0);
		glBufferData(GL_ARRAY_BUFFER, materialBuffer.getBuffers()[0], GL_DYNAMIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[3]);
		glVertexAttribPointer(3, 4, GL_FLOAT, false, 0, 0);
		glBufferData(GL_ARRAY_BUFFER, materialBuffer.getBuffers()[1], GL_DYNAMIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[4]);
		glVertexAttribPointer(4, 4, GL_FLOAT, false, 0, 0);
		glBufferData(GL_ARRAY_BUFFER, materialBuffer.getBuffers()[2], GL_DYNAMIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[5]);
		glVertexAttribPointer(5, 1, GL_FLOAT, false, 0, 0);
		glBufferData(GL_ARRAY_BUFFER, materialBuffer.getBuffers()[3], GL_DYNAMIC_DRAW);

		// Setup of the buffer of cube transformations to sent to openGl
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[6]);
		glBufferData(GL_ARRAY_BUFFER, transformBufferSize, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(6, 4, GL_FLOAT, false, MATRIX_CAPACITY * Float.BYTES, 0 * Float.BYTES);
		glVertexAttribPointer(7, 4, GL_FLOAT, false, MATRIX_CAPACITY * Float.BYTES, 4 * Float.BYTES);
		glVertexAttribPointer(8, 4, GL_FLOAT, false, MATRIX_CAPACITY * Float.BYTES, 8 * Float.BYTES);
		glVertexAttribPointer(9, 4, GL_FLOAT, false, MATRIX_CAPACITY * Float.BYTES, 12 * Float.BYTES);

		// Setup of the buffer of cube offsets (from the center of the element) to sent to openGl
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[7]);
		glBufferData(GL_ARRAY_BUFFER, offsetBuffer, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(10, 4, GL_FLOAT, false, MATRIX_CAPACITY * Float.BYTES, 0 * Float.BYTES);
		glVertexAttribPointer(11, 4, GL_FLOAT, false, MATRIX_CAPACITY * Float.BYTES, 4 * Float.BYTES);
		glVertexAttribPointer(12, 4, GL_FLOAT, false, MATRIX_CAPACITY * Float.BYTES, 8 * Float.BYTES);
		glVertexAttribPointer(13, 4, GL_FLOAT, false, MATRIX_CAPACITY * Float.BYTES, 12 * Float.BYTES);
	}

	/**
	 * Sets the rendererData of this voxelRenderer.
	 *
	 * @param data The rendererData desired.
	 */
	public void setData(RendererData data) {
		this.data = data;
	}
}
