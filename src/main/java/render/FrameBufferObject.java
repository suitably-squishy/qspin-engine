package render;

import window.Window;

import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL30.*;

/**
 * Represents a OpenGL frame buffer object.
 */
public class FrameBufferObject {

	/** The OpenGL id given to the frame buffer. */
	protected int frameBufferIdentifier;

	/** A map of attachment names to attachments. */
	private final Map<String, Attachment> attachments;

	/** An array representation of attachments. For use by OpenGL. */
	private int[] attachmentArray;

	/** The number of attachments holding colour textures. */
	private int colourAttachments;

	/** The total number of attachments. */
	private int totalAttatchments;

	/** The window the frame buffer is bound to. */
	private Window window;
	
	/**
	 * Creates a frame buffer of the same size as the given window.
	 * <p>
	 * Does not instantiate this in OpenGL until {@link FrameBufferObject#startup()} is called.
	 *
	 * @param window The window who's size to instantiate the buffer with.
	 */
	public FrameBufferObject(Window window) {
		this.window = window;
		attachments = new HashMap<>();
		colourAttachments = 0;
	}

	/**
	 * Creates the frame buffer in OpenGL.
	 */
	public void startup() {
		frameBufferIdentifier = glGenFramebuffers();
	}

	/**
	 * Bind the frameBuffer to the specified target
	 *
	 * @param target One of GL_FRAMEBUFFER, GL_READ_FRAMEBUFFER, or GL_DRAW_FRAMEBUFFER
	 *               GL_READ_FRAMEBUFFER - read from buffer
	 *               GL_DRAW_FRAMEBUFFER - write to buffer
	 *               GL_FRAMEBUFFER - Both read and write
	 */
	public void bind(int target) {
		glBindFramebuffer(target, frameBufferIdentifier);
	}

	/**
	 * Bind to GL_FRAMEBUFFER
	 * to both read and write to the buffer
	 */
	public void bind() {
		bind(GL_FRAMEBUFFER);
	}

	/**
	 * Removes this from the bind of the target.
	 *
	 * @param target The given framebuffer target/operation.
	 */
	public static void clearBind(int target) {
		glBindFramebuffer(target, 0);
	}

	/**
	 * Clears this from the default bind.
	 */
	public static void clearBind() {
		clearBind(GL_FRAMEBUFFER);
	}

	/**
	 * Attach a 2D texture to the framebuffer as a colour buffer
	 *
	 * @param name The name of the buffer. Cannot be called depth or stencil
	 */
	public void attachColourBuffer(String name) {
		if (!name.equals("depth") && !name.equals("stencil") && !attachments.containsKey(name)) {
			if (colourAttachments < GL_MAX_COLOR_ATTACHMENTS) {
				Attachment attachment = new Attachment(GL_COLOR_ATTACHMENT0 + colourAttachments,
						window.getWidth(), window.getHeight(), Attachment.Type.COLOUR);
				attachment.attach(this);
				attachments.put(name, attachment);
				colourAttachments++;
				totalAttatchments++;
			}
		}
	}

	/**
	 * Attach a 2D texture to the framebuffer as a colour buffer named default
	 */
	public void attachColourBuffer() {
		if (!attachments.containsKey("default")) {
			if (colourAttachments < GL_MAX_COLOR_ATTACHMENTS) {
				Attachment attachment = new Attachment(GL_COLOR_ATTACHMENT0 + colourAttachments,
						window.getWidth(), window.getHeight(), Attachment.Type.COLOUR);
				attachment.attach(this);
				attachments.put("default", attachment);
				colourAttachments++;
				totalAttatchments++;
			}
		}
	}

	/**
	 * Attach a 2d texture to the framebuffer as a depth buffer
	 */
	public void attachDepthBuffer() {
		Attachment attachment = new Attachment(GL_DEPTH_ATTACHMENT, window.getWidth(), window.getHeight(), Attachment.Type.DEPTH);
		attachment.attach(this);
		attachments.put("depth", attachment);
		totalAttatchments++;
	}

	/**
	 * Attach a 2d texture to the framebuffer as a stencil buffer
	 */
	public void attachStencilBuffer() {
		Attachment attachment = new Attachment(GL_STENCIL_ATTACHMENT, window.getWidth(), window.getHeight(), Attachment.Type.STENCIL);
		attachment.attach(this);
		attachments.put("stencil", attachment);
		totalAttatchments++;
	}


	/**
	 * Should be called after attaching buffers to tell gl which buffers to draw to
	 */
	public void prepare() {
		attachmentArray = new int[colourAttachments];

		int i = 0;
		for (Attachment a : attachments.values()) {
			if (a.type == Attachment.Type.COLOUR) {
				attachmentArray[i] = a.attachmentPoint;
				i++;
			}
		}
	}

	/**
	 * Draw to the attached buffers
	 * <p>
	 * prepare should be called before this is called
	 */
	public void drawToBuffers() {
		glDrawBuffers(attachmentArray);
	}

	/**
	 * Destroys the framebuffer, clearing it from memory
	 */
	public void destroy() {
		glDeleteFramebuffers(frameBufferIdentifier);
		glDeleteRenderbuffers(attachmentArray);

		for (Attachment attachment : attachments.values()) {
			attachment.destroy();
		}
	}

	/**
	 * Validate the frameBuffer in the specified target
	 *
	 * @param target One of GL_FRAMEBUFFER, GL_READ_FRAMEBUFFER, or GL_DRAW_FRAMEBUFFER
	 *               GL_READ_FRAMEBUFFER - read from buffer
	 *               GL_DRAW_FRAMEBUFFER - write to buffer
	 *               GL_FRAMEBUFFER - Both read and write
	 * @return True if the specified frame buffer is complete
	 */
	public static boolean validate(int target) {
		int validate = glCheckFramebufferStatus(target);
		return validate == GL_FRAMEBUFFER_COMPLETE;
	}

	/**
	 * Validate the frameBuffer in GL_FRAMEBUFFER
	 *
	 * @return True if the specified frame buffer is complete
	 */
	public static boolean validate() {
		int validate = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		return validate == GL_FRAMEBUFFER_COMPLETE;
	}

	/**
	 * Gets the OpenGL identifier of the FBO.
	 *
	 * @return The OpenGL identifier of the FBO.
	 */
	public int getIdentifier() {
		return frameBufferIdentifier;
	}

	/**
	 * Get a texture from a FBO attachment.
	 *
	 * @param name The name of the attachment.
	 * @return The GL id of the texture.
	 */
	public int getTexture(String name) {
		return attachments.get(name).getTextureID();
	}

	/**
	 * Get a default texture from a FBO attachment.
	 *
	 * @return The GL id of the texture.
	 */
	public int getTexture() {
		return attachments.get("default").getTextureID();
	}


	/**
	 * Represents an attached texture to the FBO.
	 */
	protected static class Attachment {

		/** The possible attachment types. */
		enum Type {
			COLOUR,
			DEPTH,
			STENCIL,
		}

		/** The OpenGL id of the texture. */
		private final int textureID;

		/** The OpenGL attachment point on the FBO. */
		private int attachmentPoint;

		/** The attachment type. */
		private Type type;

		/**
		 * Creates an attachment on the given attachment point, with the given type.
		 *
		 * @param attachmentPoint The OpenGL attachment point to fill on an FBO.
		 * @param textureWidth    The width of the attachment texture.
		 * @param textureHeight   The height of the attachment texture.
		 * @param type            The type of the attachment.
		 */
		protected Attachment(int attachmentPoint, int textureWidth, int textureHeight, Type type) {
			this.attachmentPoint = attachmentPoint;
			this.type = type;
			textureID = glGenTextures();
			glBindTexture(GL_TEXTURE_2D, textureID);
			switch (type) {
				case COLOUR:
					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureWidth, textureHeight, 0,
							GL_RGBA, GL_FLOAT, (FloatBuffer) null);
					break;
				case DEPTH:
					glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, textureWidth, textureHeight, 0,
							GL_DEPTH_COMPONENT, GL_FLOAT, (FloatBuffer) null);
					break;
				case STENCIL:
					glTexImage2D(GL_TEXTURE_2D, 0, GL_STENCIL_INDEX, textureWidth, textureHeight, 0,
							GL_STENCIL_INDEX, GL_FLOAT, (FloatBuffer) null);
					break;
			}
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glBindTexture(GL_TEXTURE_2D, 0);
		}

		/**
		 * Gets the OpenGL attachment point of the attachment on an FBO.
		 *
		 * @return An OpenGL attachment point.
		 */
		public int getAttachmentPoint() {
			return attachmentPoint;
		}

		/**
		 * Gets the OpenGL texture ID of the attachment's texture.
		 *
		 * @return An OpenGL texture ID.
		 */
		public int getTextureID() {
			return textureID;
		}

		/**
		 * Gets the type of the attachment.
		 *
		 * @return The attachment type.
		 */
		public Type getType() {
			return type;
		}

		/**
		 * Attaches the attachment onto the given FBO.
		 *
		 * @param fbo The FBO to attach to.
		 */
		public void attach(FrameBufferObject fbo) {
			fbo.bind();
			glFramebufferTexture2D(GL_FRAMEBUFFER, attachmentPoint, GL_TEXTURE_2D, textureID, 0);
			clearBind();
		}

		/**
		 * Destroys the attachment, freeing the texture resources.
		 */
		protected void destroy() {
			glDeleteTextures(this.textureID);
		}
	}
}
