package render;

import entities.Entity;
import render.camera.Camera;
import render.light.LightManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A class to hold the data required to render entities to the screen
 */
public class RendererData {

	public Camera currentCamera;

	//TODO: IF NEW ELEMENTS: Make a superclass of elements
	public final List<VoxelElement> elements = new ArrayList<>();

	/**
	 * A list that is checked each render loop.
	 * Any element within this array will be rendered and then removed
	 * Once an element has been rendered it will stay in the same position in clip space until added back to this list
	 */
	public final List<VoxelElement> elementsToRender = new ArrayList<>();

	/**
	 * a class that holds the lighting of the scene
	 */
	public final LightManager lightManager = new LightManager();

	/**
	 * Fills the renderer data with data fetched from entities supplied by the provided entity iterator.
	 *
	 * @param entityIterator The iterator which will supply entities.
	 */
	public void fillFromEntities(Iterator<Entity> entityIterator) {
		while (entityIterator.hasNext()) {
			entityIterator.next().addToRenderData(this);
		}
	}
}
