package render.camera;

import org.joml.Matrix4f;

/**
 * A class to create a perspective projection,
 * Should be passed to renderer to pass the transform buffer to the uniform representing the projection
 */
public class PerspectiveCamera extends Camera {
	/**
	 * The camera settings
	 */
	private float fov, viewportWidth, viewportHeight, nearDistance, farDistance;

	/**
	 * Holds the projection transform
	 */
	private final Matrix4f projection;

	private final Matrix4f inverseProjection;

	/**
	 * Create a new perspective camera with the specified settings
	 *
	 * @param fov            The field of view in degrees.
	 * @param viewportWidth  The width of the viewport for aspect ratio
	 * @param viewportHeight The height of the viewport for aspect ratio
	 * @param nearDistance   The closest distance rendered
	 * @param farDistance    The furthest distance rendered
	 */
	public PerspectiveCamera(float fov, float viewportWidth, float viewportHeight, float nearDistance, float farDistance) {
		this.fov = fov;
		this.viewportWidth = viewportWidth;
		this.viewportHeight = viewportHeight;
		this.nearDistance = nearDistance;
		this.farDistance = farDistance;

		this.projection = new Matrix4f();
		this.inverseProjection = new Matrix4f();
		this.remakeCameraProjection();
	}

	/**
	 * Update the camera settings
	 * After this is called, updateCamera must be called in {@link entities.CameraEntity}
	 *
	 * @param fov            The field of view
	 * @param viewportWidth  The width of the viewport for aspect ratio
	 * @param viewportHeight The height of the viewport for aspect ratio
	 * @param nearDistance   The closest distance rendered
	 * @param farDistance    The furthest distance rendered
	 */
	public void updateCamera(float fov, float viewportWidth, float viewportHeight, float nearDistance, float farDistance) {
		this.fov = fov;
		this.viewportWidth = viewportWidth;
		this.viewportHeight = viewportHeight;
		this.nearDistance = nearDistance;
		this.farDistance = farDistance;
	}

	@Override
	public Matrix4f getCameraProjection() {
		return projection;
	}

	@Override
	public Matrix4f getInverseCameraProjection() {
		return inverseProjection;
	}

	@Override
	public void remakeCameraProjection() {
		projection.setPerspectiveLH((float) Math.toRadians(fov), viewportWidth / viewportHeight, nearDistance, farDistance);
		projection.invert(inverseProjection);
	}
}
