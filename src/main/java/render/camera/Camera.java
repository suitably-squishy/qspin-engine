package render.camera;

import org.joml.Matrix4f;
import org.joml.Vector4f;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

/**
 * A class for storing information about a camera projection.
 */
public abstract class Camera {

	/**
	 * The floatbuffer to be passed to renderer
	 */
	private final FloatBuffer transformBuffer;

	private final Vector4f worldPos;
	private final Vector4f worldDir;

	/**
	 * The transformation of the camera in absolute world space.
	 */
	private Matrix4f transform;

	/**
	 * Creates a new camera
	 * <p>
	 * do not directly create instances of the abstract camera,
	 * instead use the classes that extend
	 */
	public Camera() {
		transformBuffer = BufferUtils.createFloatBuffer(16);
		transform = new Matrix4f();
		this.worldPos = new Vector4f(0, 0, 0, 1);
		this.worldDir = new Vector4f(0, 0, 1, 0);
	}

	/**
	 * Get the internal transformation buffer.
	 * For renderer usage
	 * .rewind has been called
	 *
	 * @return A Floatbuffer with 16 elements.
	 */
	public FloatBuffer getTransformBuffer() {
		Matrix4f bufferTransform = new Matrix4f();
		bufferTransform.mul(getCameraProjection());
		bufferTransform.mul(transform.invert(new Matrix4f()));
		bufferTransform.get(transformBuffer);
		transformBuffer.rewind();
		return transformBuffer;
	}

	public Vector4f getWorldPos() {
		return this.worldPos;
	}

	public Vector4f getWorldDir() {
		return this.worldDir;
	}

	/**
	 * Get the world pos of this camera
	 *
	 * @return a 4 vector with w = 1 representing the position of the camera
	 */
	public float[] getWorldPosAsArray() {
		return new float[]{
				worldPos.get(0), worldPos.get(1), worldPos.get(2), 1
		};
	}

	/**
	 * Get the world direction of this camera
	 *
	 * @return a 4 vector with w = 0 representing the direction of the camera
	 */
	public float[] getWorldDirAsArray() {
		return new float[]{
				worldDir.get(0), worldDir.get(1), worldDir.get(2), 0
		};
	}

	/**
	 * Sets the transform of the camera
	 * For camera entity usage
	 *
	 * @param transform the transformation matrix, without parent offsets
	 */
	public void setTransform(Matrix4f transform) {
		this.transform = transform;
	}

	/**
	 * Gets the camera projection
	 *
	 * @return a matrix transform representing the projection
	 */
	public abstract Matrix4f getCameraProjection();

	public abstract Matrix4f getInverseCameraProjection();

	/**
	 * Remake the camera projection after a settings change
	 */
	public abstract void remakeCameraProjection();
}
