package render;

import java.util.logging.Logger;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;

/**
 * A renderer for rendering a texture to a 2D rectangle.
 */
public class ScreenRenderer {
	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/** The output FBO made by the blender. */
	private FrameBufferObject fbo;

	/** Whether to render directly to the screen. */
	private boolean renderToScreen;

	/** The underlying screen rendering shader. */
	private GPUProgram gpuProgram;

	/** The index of the vertex array attribute to pass to openGl */
	private int vaoID;

	/**
	 * The individual buffers to pass to openGl
	 * These are bound to the VAO
	 * 0 - vertex buffer
	 * 1 - texture buffer
	 */
	private int[] vboIDs;

	/**
	 * Creates a renderer capable of rendering to the provided FBO.
	 *
	 * @param frameBufferOut The frame buffer to render any input to.
	 */
	public ScreenRenderer(FrameBufferObject frameBufferOut) {
		this.fbo = frameBufferOut;
		gpuProgram = new GPUProgram("ScreenVertex", "ScreenFragment");
		renderToScreen = false;
	}

	/**
	 * Creates a renderer capable of rendering directly to the screen.
	 */
	public ScreenRenderer() {
		gpuProgram = new GPUProgram("ScreenVertex", "ScreenFragment");
		renderToScreen = true;
	}

	/**
	 * Starts the screen renderer, setting up all GL buffers.
	 */
	public void startup() {
		LOGGER.info("Screen Renderer starting!");

		int[] attributeDivisors = {0, 0};

		gpuProgram.startup();
		gpuProgram.setupBuffers(2);
		vaoID = gpuProgram.getVaoID();
		vboIDs = gpuProgram.getVboIDs();

		if (!renderToScreen) {
			fbo.startup();
			fbo.attachColourBuffer();
			fbo.attachDepthBuffer();
			fbo.prepare();
		}

		setupGlBuffers();
		gpuProgram.setupAttributes(attributeDivisors);
	}

	/**
	 * Runs the screen renderer using the default colour attachment on the provided FBO,
	 * rendering out the provided input.
	 *
	 * @param in The FBO to render out.
	 * @return A frame buffer object that has been rendered to, if configured, otherwise null.
	 */
	public FrameBufferObject run(FrameBufferObject in) {
		return run(in, "default");
	}

	/**
	 * Runs the screen renderer using the provided colour attachment on the provided FBO,
	 * rendering out the provided input.
	 *
	 * @param in   The FBO to render out.
	 * @param name The name of the colour buffer attachment to render using.
	 * @return A frame buffer object that has been rendered to, if configured, otherwise null.
	 */
	public FrameBufferObject run(FrameBufferObject in, String name) {
		// Bind the gpu program to gl
		gpuProgram.bind();

		glBindTexture(GL_TEXTURE_2D, in.getTexture(name));

		if (renderToScreen) {
			FrameBufferObject.clearBind();
		} else {
			assert fbo != null;
			fbo.bind();
		}
		if (!FrameBufferObject.validate()) LOGGER.warning("FBO not complete");

		// Setup gl state
		glClear(GL_COLOR_BUFFER_BIT);
		glBlendFunc(GL_ONE, GL_ZERO);
		glDisable(GL_BLEND);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);

		glDrawArrays(GL_TRIANGLES, 0, 6);

		if (!renderToScreen) {
			fbo.drawToBuffers();
		}

		FrameBufferObject.clearBind();

		if (renderToScreen) return null;

		return fbo;
	}

	/**
	 * Shuts down the screen renderer, freeing any held resources.
	 */
	public void shutdown() {
		glDeleteBuffers(vaoID);
		glDeleteVertexArrays(vaoID);
		gpuProgram.shutdown();
		gpuProgram = null;
		if (fbo != null) {
			fbo.destroy();
		}
	}

	/**
	 * Sets up all required buffers for storing data gpu side.
	 */
	public void setupGlBuffers() {
		// Setup of the buffer of cube vertexes to sent to openGl
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[0]);
		glVertexAttribPointer(0, 4, GL_FLOAT, false, 0, 0);
		glBufferData(GL_ARRAY_BUFFER, ShapeGenerator.Screen.screenVertexPos, GL_STATIC_DRAW);

		// Setup the surface normals of each vertex
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[1]);
		glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
		glBufferData(GL_ARRAY_BUFFER, ShapeGenerator.Screen.textureCoords, GL_STATIC_DRAW);
	}

	/**
	 * Sets whether the renderer should render directly to the screen.
	 * <p>
	 * Setting to false will fail if the internal FBO is null.
	 *
	 * @param renderToScreen Whether or not to render directly to the screen.
	 */
	public void setRenderToScreen(boolean renderToScreen) {
		if (!renderToScreen && fbo == null) {
			LOGGER.warning("Can't set a screen renderer to render to a null FBO, ignoring.");
			return;
		}
		this.renderToScreen = renderToScreen;
	}
}
