package render;

import entities.VoxelEntity;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;
import render.model.VoxelModel;
import utils.CheckNeighbours;
import utils.RGBMaterial;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * A class to hold the data of a voxel element for the renderer
 * Currently, the transforms can be edited but not the colours or the offsets.
 * To create independently moving items use a new entity.
 */
public class VoxelElement {
	/**
	 * The VoxelEntity that the element will display
	 */
	private final VoxelEntity entity;

	/**
	 * The amount of cubes within the model
	 */
	private int size;

	private final List<Matrix4f> rotationsList = new ArrayList<>();
	private final List<Matrix4f> offsetsList = new ArrayList<>();
	private final List<RGBMaterial> materialsList = new ArrayList<>();
	private final List<Vector3f> positionsList = new ArrayList<>();

	/**
	 * The memory offset within the buffer that renderer is using.
	 */
	private int memOffset = -1;

	/**
	 * A buffer for storing the transform data for this element.
	 */
	private final FloatBuffer transformBuffer;

	/**
	 * The transform of the element.
	 */
	private final Matrix4f transform = new Matrix4f();

	/**
	 * Capacity of a float buffer containing a Matrix4f.
	 */
	private static final int BUFFER_CAPACITY = 16;

	/**
	 * Creates a VoxelElement from the provided model.
	 *
	 * @param entity The model to create the VoxelElement from.
	 */
	public VoxelElement(VoxelEntity entity) {
		this.entity = entity;

		generateMaterials(entity.getModel());

		generateOffsets();

		transformBuffer = BufferUtils.createFloatBuffer(BUFFER_CAPACITY * size);
	}

	/**
	 * Use the position array to generate the offset transforms for each cube
	 * These are passed initially to the renderer when constructed and can therefore cannot be edited
	 */
	private void generateOffsets() {
		for (int i = 0; i < size; i++) {
			offsetsList.add(new Matrix4f().translate(positionsList.get(i)).mul(rotationsList.get(i)));
		}
	}

	/**
	 * Use the VoxelModel to generate positions in an intermediary stage before proper clip space.
	 * These will be in clip space where 1 cube is 1 unit,
	 * but each element centered at 0,0.
	 * Each element is then transformed to their proper position in clip space,
	 * Via the transformBuffer, by the renderer instance
	 * Only generates faces of cubes that are active.
	 */
	void generateMaterials(VoxelModel model) {
		short[][][] voxels = model.getVoxels();
		byte[][][] neighbours = model.getNeighbours();
		int voxelIterator = 0;
		float correctionX = (voxels.length - 1) / 2f;
		float correctionY = (voxels[0].length - 1) / 2f;
		float correctionZ = (voxels[0][0].length - 1) / 2f;
		for (int i = 0; i < voxels.length; i++) {
			for (int j = 0; j < voxels[i].length; j++) {
				for (int k = 0; k < voxels[i][j].length; k++) {
					if (voxels[i][j][k] != 0) {
						if (neighbours[i][j][k] != CheckNeighbours.EMPTY) {
							List<Matrix4f> sideRotations = ShapeGenerator.Cube.makeCube(neighbours[i][j][k]);
							for (Matrix4f rot : sideRotations) {
								positionsList.add(new Vector3f(i - correctionX, j - correctionY, k - correctionZ));
								materialsList.add(model.getVoxelMaterial(i, j, k));
								rotationsList.add(rot);
								voxelIterator++;
							}
						}
					}
				}
			}
		}
		this.size = voxelIterator;
	}

	/**
	 * Clears the element's internal list of cube positions.
	 */
	void clearMemory() {
		this.offsetsList.clear();
		this.rotationsList.clear();
//        this.materialsList.clear();
	}

	/**
	 * Gets the transform of the cubes in the element as a buffer.
	 *
	 * @return A floatbuffer with Matrix4f's converted to buffer representation.
	 */
	public FloatBuffer getTransformBuffer() {
		transform.identity();
		transform.mul(this.entity.getAbsoluteTransform());
		for (int i = 0; i < size; i++) {
			transform.get(transformBuffer);
			transformBuffer.position((i + 1) * BUFFER_CAPACITY);
		}
		transformBuffer.rewind();

		return transformBuffer;
	}

	/**
	 * @return the amount of cubes contained within the model
	 */
	public int getSize() {
		return size;
	}

	public List<RGBMaterial> getMaterials() {
		return materialsList;
	}

	public List<Matrix4f> getOffsets() {
		return offsetsList;
	}

	public int getMemOffset() {
		return memOffset;
	}

	public void setMemOffset(int memOffset) {
		this.memOffset = memOffset;
	}
}
