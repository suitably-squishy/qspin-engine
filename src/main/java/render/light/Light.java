package render.light;

import org.joml.Vector4f;
import utils.RGBColour;

import java.util.logging.Logger;

/**
 * Represents a light and its associated colours.
 */
public abstract class Light {

	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	/**
	 * The colour of the light that reflects off objects
	 */
	private RGBColour diffuseColour = new RGBColour(0.3f, 0.3f, 0.3f, 1f);
	/**
	 * The colour of the light that creates a specular highlight
	 */
	private RGBColour specularColour = new RGBColour(0.3f, 0.3f, 0.3f, 1f);

	/**
	 * Possible light types
	 */
	public enum Type {
		DIRECTIONAL,
		POINT,
		SPOT
	}

	private final Type type;

	/**
	 * Creates a light of a given type
	 *
	 * @param type Type of light
	 */
	protected Light(Type type) {
		this.type = type;
	}

	/**
	 * Gets the light type
	 *
	 * @return the type of this light
	 */
	public Type getType() {
		return type;
	}

	public RGBColour getSpecularColour() {
		return specularColour;
	}

	public RGBColour getDiffuseColour() {
		return diffuseColour;
	}

	public void setDiffuseColour(RGBColour diffuseColour) {
		this.diffuseColour = diffuseColour;
	}

	public void setSpecularColour(RGBColour specularColour) {
		this.specularColour = specularColour;
	}

	/**
	 * Sets the colours of the light to the provided values.
	 *
	 * @param diff The diffuse colour of the light.
	 * @param spec The specular colour of the light.
	 */
	public void setColours(RGBColour diff, RGBColour spec) {
		this.setDiffuseColour(diff);
		this.setSpecularColour(spec);
	}

	public abstract void setPosition(Vector4f position);

	public abstract void setDirection(Vector4f direction);

}
