package render.light;

import org.joml.Vector4f;

import static render.light.Light.Type.DIRECTIONAL;

/**
 * A light that faces one direction from very far away so the position does not matter
 */
public class DirectionalLight extends Light {
	private Vector4f direction;

	/**
	 * Create a new directional light - a light that faces one direction from very far away so the position does not matter
	 */
	public DirectionalLight(Vector4f direction) {
		super(DIRECTIONAL);
		this.direction = direction;
	}

	public float[] getDirection() {
		return new float[]{
				direction.get(0),
				direction.get(1),
				direction.get(2),
				0
		};
	}

	@Override
	public void setPosition(Vector4f position) {
	}

	@Override
	public void setDirection(Vector4f direction) {
		this.direction = direction;
	}
}
