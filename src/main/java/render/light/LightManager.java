package render.light;

import render.GPUProgram;
import utils.RGBColour;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static org.lwjgl.opengl.GL20.glUniform1f;
import static org.lwjgl.opengl.GL20.glUniform1i;
import static org.lwjgl.opengl.GL20C.glUniform4fv;

public class LightManager {
	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/**
	 * The ambient colour of the light in the scene  - a constant light with no direction, position or attenuation
	 */
	private RGBColour ambientColour = new RGBColour(0.4f, 0.4f, 0.4f, 1);

	//The storage for all the different light types
	private int directionalCount = 0, pointCount = 0, spotCount = 0;

	private final List<DirectionalLight> directionalLights = new ArrayList<>(MAX_D_LIGHTS);
	private final List<PointLight> pointLights = new ArrayList<>(MAX_P_LIGHTS);
	private final List<SpotLight> spotLights = new ArrayList<>(MAX_S_LIGHTS);


	/**
	 * The maximum lights in a scene
	 * To add more lights BOTH LightManager and vertex shader must be edited
	 * Shader arrays must have predefined lengths
	 */
	private static final int MAX_D_LIGHTS = 10;
	private static final int MAX_P_LIGHTS = 10;
	private static final int MAX_S_LIGHTS = 10;

	/**
	 * Add a light to the current scene.
	 * The number of existing lights of the provided type must be lower than the maximum of that type of light.
	 *
	 * @param light The light to be added.
	 */
	public void addLight(Light light) {
		switch (light.getType()) {
			case DIRECTIONAL:
				this.addLight((DirectionalLight) light);
				break;
			case POINT:
				this.addLight((PointLight) light);
				break;
			case SPOT:
				this.addLight((SpotLight) light);
				break;
			default:
				LOGGER.warning("An unrecognised light type was added to LightManager.");
				break;
		}
	}

	/**
	 * Adds the provided directional light to the manager.
	 *
	 * @param light The directional light to add.
	 */
	public void addLight(DirectionalLight light) {
		if (directionalCount < MAX_D_LIGHTS) {
			directionalLights.add(light);
			directionalCount++;
			LOGGER.info("Directional light added to LightManager.");
		} else {
			LOGGER.warning("Too many directional lights! No more lights will be added.");
			LOGGER.info("To add more lights BOTH LightManager and vertex shader must be edited.");
		}
	}

	/**
	 * Adds the provided point light to the manager.
	 *
	 * @param light The point light to add.
	 */
	public void addLight(PointLight light) {
		if (pointCount < MAX_P_LIGHTS) {
			pointLights.add(light);
			pointCount++;
			LOGGER.info("Point light added to LightManager.");
		} else {
			LOGGER.warning("Too many point lights! No more lights will be added.");
			LOGGER.info("To add more lights BOTH LightManager and vertex shader must be edited.");
		}
	}

	/**
	 * Adds the provided spot light to the manager.
	 *
	 * @param light The spot light to add.
	 */
	public void addLight(SpotLight light) {
		if (spotCount < MAX_S_LIGHTS) {
			spotLights.add(light);
			spotCount++;
			LOGGER.info("Spot light added to LightManager.");
		} else {
			LOGGER.warning("Too many spot lights! No more lights will be added.");
			LOGGER.info("To add more lights BOTH LightManager and vertex shader must be edited.");
		}
	}

	/**
	 * Remove a light from the current scene.
	 *
	 * @param light The light to be removed.
	 */
	public void removeLight(Light light) {
		switch (light.getType()) {
			case DIRECTIONAL:
				this.removeLight((DirectionalLight) light);
				break;
			case POINT:
				this.removeLight((PointLight) light);
				break;
			case SPOT:
				this.removeLight((SpotLight) light);
				break;
			default:
				LOGGER.warning("An unrecognised light type was removed from LightManager.");
				break;
		}
	}

	/**
	 * Removes the provided directional light from the manager.
	 *
	 * @param light The directional light to remove.
	 */
	public void removeLight(DirectionalLight light) {
		if (directionalLights.contains(light)) {
			directionalLights.remove(light);
			directionalCount--;
			LOGGER.info("Directional light removed.");
		}
	}

	/**
	 * Removes the provided point light from the manager.
	 *
	 * @param light The point light to remove.
	 */
	public void removeLight(PointLight light) {
		if (pointLights.contains(light)) {
			pointLights.remove(light);
			pointCount--;
			LOGGER.info("point light removed");
		}
	}

	/**
	 * Removes the provided spot light to the manager.
	 *
	 * @param light The spot light to remove.
	 */
	public void removeLight(SpotLight light) {
		if (spotLights.contains(light)) {
			spotLights.remove(light);
			spotCount--;
			LOGGER.info("spot light removed");
		}
	}

	/**
	 * Flushes the light data to the GL pipeline, filling uniform arrays with the light data
	 *
	 * @param gpuProgram The current gpu program running.
	 */
	public void exportLightDataToGL(GPUProgram gpuProgram) {
		//light uniforms for the fragment shader
		int lightPositionUniformID,
				lightAmbientUniformID,
				lightDirectionUniformID,
				lightConstantUniformID,
				lightLinearUniformID,
				lightQuadraticUniformID,
				lightCutoffUniformID,
				lightOuterCutoffUniformID,
				DirLightCountUniformID,
				PointLightCountUniformID,
				SpotLightCountUniformID;

		//get the light ambient uniform and flush the ambient colour
		lightAmbientUniformID = gpuProgram.getUniformIndex("ambient");
		glUniform4fv(lightAmbientUniformID, ambientColour.toFloatArray());

		//get the light count uniforms and flush the light counts
		DirLightCountUniformID = gpuProgram.getUniformIndex("DirLightCount");
		PointLightCountUniformID = gpuProgram.getUniformIndex("PointLightCount");
		SpotLightCountUniformID = gpuProgram.getUniformIndex("SpotLightCount");

		glUniform1i(DirLightCountUniformID, directionalCount);
		glUniform1i(PointLightCountUniformID, pointCount);
		glUniform1i(SpotLightCountUniformID, spotCount);


		// Get the uniforms for each different light type and flush the data

		for (int i = 0; i < directionalCount; i++) {
			lightDirectionUniformID = gpuProgram.getUniformIndex("dirLights[" + i + "].direction");

			DirectionalLight light = directionalLights.get(i);

			glUniform4fv(lightDirectionUniformID, light.getDirection());

			processLightData(light, "dir", i, gpuProgram);
		}

		for (int i = 0; i < pointCount; i++) {
			lightPositionUniformID = gpuProgram.getUniformIndex("pointLights[" + i + "].position");
			lightConstantUniformID = gpuProgram.getUniformIndex("pointLights[" + i + "].constant");
			lightQuadraticUniformID = gpuProgram.getUniformIndex("pointLights[" + i + "].quadratic");
			lightLinearUniformID = gpuProgram.getUniformIndex("pointLights[" + i + "].linear");

			PointLight light = pointLights.get(i);

			glUniform4fv(lightPositionUniformID, light.getPosition());

			glUniform1f(lightConstantUniformID, light.getConstant());
			glUniform1f(lightLinearUniformID, light.getLinear());
			glUniform1f(lightQuadraticUniformID, light.getQuadratic());

			processLightData(light, "point", i, gpuProgram);
		}

		for (int i = 0; i < spotCount; i++) {
			lightPositionUniformID = gpuProgram.getUniformIndex("spotLights[" + i + "].position");
			lightDirectionUniformID = gpuProgram.getUniformIndex("spotLights[" + i + "].direction");
			lightCutoffUniformID = gpuProgram.getUniformIndex("spotLights[" + i + "].cutOff");
			lightOuterCutoffUniformID = gpuProgram.getUniformIndex("spotLights[" + i + "].outerCutOff");

			SpotLight light = spotLights.get(i);

			glUniform4fv(lightPositionUniformID, light.getPosition());

			glUniform4fv(lightDirectionUniformID, light.getDirection());
			glUniform1f(lightCutoffUniformID, light.getInnerCutOff());
			glUniform1f(lightOuterCutoffUniformID, light.getOuterCutOff());

			processLightData(light, "spot", i, gpuProgram);
		}
	}

	/**
	 * Flush the data of a light to the gl pipeline
	 *
	 * @param light      the light to flush
	 * @param type       the name of the light type
	 * @param index      the index of the light in the uniform array
	 * @param gpuProgram the current gpu program running
	 */
	public void processLightData(Light light, String type, int index, GPUProgram gpuProgram) {
		int lightDiffuseUniformID = gpuProgram.getUniformIndex(type + "Lights[" + index + "].diffuse");
		int lightSpecularUniformID = gpuProgram.getUniformIndex(type + "Lights[" + index + "].specular");

		//flush the  light colour data
		glUniform4fv(lightDiffuseUniformID, light.getDiffuseColour().toFloatArray());
		glUniform4fv(lightSpecularUniformID, light.getSpecularColour().toFloatArray());
	}

	/**
	 * Get the underlying light colour of the scene
	 */
	public RGBColour getAmbientColour() {
		return ambientColour;
	}

	/**
	 * Set the underlying light colour of the scene
	 */
	public void setAmbientColour(RGBColour ambientColour) {
		this.ambientColour = ambientColour;
	}
}
