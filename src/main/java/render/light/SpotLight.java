package render.light;

import org.joml.Vector4f;

import static render.light.Light.Type.SPOT;

/**
 * A cone of light with smooth edges
 */
public class SpotLight extends Light {
	private Vector4f position,
			direction;

	private float innerCutOff,
			outerCutOff;

	/**
	 * Create a new spot light - a cone of light with smooth edges
	 *
	 * @param innerCutOff inner cutoff angle of spotlight, before the light starts fading
	 * @param outerCutOff outer cutoff angle of a spotlight, where the light fades to nothing
	 */
	public SpotLight(Vector4f position, Vector4f direction, float innerCutOff, float outerCutOff) {
		super(SPOT);
		this.position = position;
		this.direction = direction;
		this.innerCutOff = innerCutOff;
		this.outerCutOff = outerCutOff;
	}

	public float[] getPosition() {
		return new float[]{
				position.get(0),
				position.get(1),
				position.get(2),
				0
		};
	}

	@Override
	public void setPosition(Vector4f position) {
		this.position = position;
	}

	public float[] getDirection() {
		return new float[]{
				direction.get(0),
				direction.get(1),
				direction.get(2),
				0
		};
	}

	@Override
	public void setDirection(Vector4f direction) {
		this.direction = direction;
	}


	public float getInnerCutOff() {
		return innerCutOff;
	}

	public void setInnerCutOff(float innerCutOff) {
		this.innerCutOff = innerCutOff;
	}

	public float getOuterCutOff() {
		return outerCutOff;
	}

	public void setOuterCutOff(float outerCutOff) {
		this.outerCutOff = outerCutOff;
	}

}
