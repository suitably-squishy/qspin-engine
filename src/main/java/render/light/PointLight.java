package render.light;

import org.joml.Vector4f;

import static render.light.Light.Type.POINT;

/**
 * An omnidirectional light that fades as distance increases.
 */
public class PointLight extends Light {
	private Vector4f position;
	private float constant,
			linear,
			quadratic;

	/**
	 * Create a new point light - a light that fades as distance increases.
	 *
	 * @param constant  constant part of a reciprocal quadratic equation for attenuation
	 * @param linear    linear part of a reciprocal quadratic equation for attenuation
	 * @param quadratic quadratic part of a reciprocal quadratic equation for attenuation
	 *                  <p>
	 *                  for more info see http://wiki.ogre3d.org/tiki-index.php?page=-Point+Light+Attenuation
	 */
	public PointLight(Vector4f position, float constant, float linear, float quadratic) {
		super(POINT);
		this.position = position;
		this.constant = constant;
		this.linear = linear;
		this.quadratic = quadratic;
	}

	public float[] getPosition() {
		return new float[]{
				position.get(0),
				position.get(1),
				position.get(2),
				0
		};
	}

	@Override
	public void setPosition(Vector4f position) {
		this.position = position;
	}

	@Override
	public void setDirection(Vector4f direction) {
	}

	public float getConstant() {
		return constant;
	}

	public void setConstant(float constant) {
		this.constant = constant;
	}

	public float getLinear() {
		return linear;
	}

	public void setLinear(float linear) {
		this.linear = linear;
	}

	public float getQuadratic() {
		return quadratic;
	}

	public void setQuadratic(float quadratic) {
		this.quadratic = quadratic;
	}


}
