package render;

import org.joml.Matrix4f;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.PI;
import static utils.CheckNeighbours.*;

/**
 * A class for storing information on the layout of primitive shapes for instanced rendering.
 */
public class ShapeGenerator {

	public static class Screen {
		public static float[] screenVertexPos = {
				-1, -1, 0, 1,
				1, -1, 0, 1,
				-1, 1, 0, 1,
				1, -1, 0, 1,
				-1, 1, 0, 1,
				1, 1, 0, 1
		};

		public static float[] textureCoords = {
				0, 0,
				1, 0,
				0, 1,
				1, 0,
				0, 1,
				1, 1
		};
	}


	/**
	 * A utility class for dealing with the vertices of cubes.
	 */
	public static class Cube {

		/**
		 * The vertices of a single face.
		 */
		public static float[] faceVertexPositions = {
				-0.5f, -0.5f, -0.5f, 1,
				0.5f, -0.5f, -0.5f, 1,
				0.5f, -0.5f, 0.5f, 1,
				0.5f, -0.5f, 0.5f, 1,
				-0.5f, -0.5f, 0.5f, 1,
				-0.5f, -0.5f, -0.5f, 1
		};

		/**
		 * The normal of each vertex on a face.
		 */
		public static float[] faceVertexNormals = {
				0, -1, 0, 0,
				0, -1, 0, 0,
				0, -1, 0, 0,
				0, -1, 0, 0,
				0, -1, 0, 0,
				0, -1, 0, 0
		};

		/**
		 * Make the transformations for each side of a cube made of quads
		 *
		 * @param sides The bitwise representation of the sides that are active.
		 * @return a list transformation matrices for the appropriate sides
		 * @see utils.CheckNeighbours#checkNeighbours(short[][][])
		 */
		public static List<Matrix4f> makeCube(byte sides) {
			List<Matrix4f> cubeRotations = new ArrayList<>(6);

			if ((sides & RIGHT) != 0) {
				cubeRotations.add(new Matrix4f().rotateXYZ(0, 0, (float) PI / 2));
			}
			if ((sides & LEFT) != 0) {
				cubeRotations.add(new Matrix4f().rotateXYZ(0, 0, (float) -PI / 2));
			}
			if ((sides & FRONT) != 0) {
				cubeRotations.add(new Matrix4f().rotateXYZ(0, 0, (float) PI));
			}
			if ((sides & BACK) != 0) {
				cubeRotations.add(new Matrix4f().rotateXYZ(0, 0, 0));
			}
			if ((sides & BOTTOM) != 0) {
				cubeRotations.add(new Matrix4f().rotateXYZ((float) PI / 2, (float) PI / 2, 0));
			}
			if ((sides & TOP) != 0) {
				cubeRotations.add(new Matrix4f().rotateXYZ((float) PI / 2, (float) PI / 2, (float) PI));
			}

			return cubeRotations;
		}
	}
}
