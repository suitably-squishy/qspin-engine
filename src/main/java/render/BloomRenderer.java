package render;

import window.Window;

import java.util.logging.Logger;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.glUniform1i;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;

/**
 * A renderer that can render out gaussian blurred frames.
 */
public class BloomRenderer {
	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/** The horizontally blurred frame buffer. */
	private FrameBufferObject fboPing;
	/** The vertically blurred frame buffer. */
	private FrameBufferObject fboPong;

	/**
	 * The underlying blur shader program.
	 */
	private GPUProgram gpuProgram;

	/** The index of the vertex array attribute to pass to openGl */
	private int vaoID;

	/**
	 * The individual buffers to pass to openGl
	 * These are bound to the VAO
	 * 0 - vertex buffer
	 * 1 - texture buffer
	 */
	private int[] vboIDs;

	/**
	 * Creates a bloom renderer using frames the same size as the given window.
	 *
	 * @param window The window who's size to use for frames.
	 */
	public BloomRenderer(Window window) {
		this.fboPing = new FrameBufferObject(window);
		this.fboPong = new FrameBufferObject(window);

		gpuProgram = new GPUProgram("ScreenVertex", "Blur");
	}

	/**
	 * Starts the bloom renderer, setting up all OpenGL buffers.
	 */
	public void startup() {
		LOGGER.info("Bloom Renderer starting!");

		int[] attributeDivisors = {0, 0};

		gpuProgram.startup();
		gpuProgram.setupBuffers(2);
		vaoID = gpuProgram.getVaoID();
		vboIDs = gpuProgram.getVboIDs();

		fboPing.startup();
		fboPing.attachColourBuffer();
		fboPing.prepare();
		fboPong.startup();
		fboPong.attachColourBuffer();
		fboPong.prepare();

		setupGlBuffers();
		gpuProgram.setupAttributes(attributeDivisors);
	}

	/**
	 * Performs blurring on the provided frame buffer's default colour attachment.
	 *
	 * @param in The frame buffer containing the default colour attachment to blur.
	 * @return A frame buffer object containing the blurred output.
	 */
	public FrameBufferObject bloom(FrameBufferObject in) {
		return bloom(in, "default");
	}

	/**
	 * Performs blurring on the provided frame buffer's provided attachment.
	 *
	 * @param in   The frame buffer containing the attachment to blur.
	 * @param name The name of the FBO attachment to perform blurring on.
	 * @return A frame buffer object containing the blurred output.
	 */
	public FrameBufferObject bloom(FrameBufferObject in, String name) {
		// Bind the gpu program to gl
		gpuProgram.bind();

		if (!FrameBufferObject.validate()) LOGGER.warning("FBO not complete");

		glBindTexture(GL_TEXTURE_2D, in.getTexture(name));

		// Setup gl state
		glClear(GL_COLOR_BUFFER_BIT);
		glBlendFunc(GL_ONE, GL_ZERO);
		glDisable(GL_BLEND);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);

		boolean horizontal = true;
		int amount = 5;
		for (int i = 0; i < amount * 2; i++) {
			if (horizontal) {
				fboPing.bind();
				glUniform1i(gpuProgram.getUniformIndex("horizontal"), 1);
				glDrawArrays(GL_TRIANGLES, 0, 6);
				glBindTexture(GL_TEXTURE_2D, fboPing.getTexture());
			} else {
				fboPong.bind();
				glUniform1i(gpuProgram.getUniformIndex("horizontal"), 0);
				glDrawArrays(GL_TRIANGLES, 0, 6);
				glBindTexture(GL_TEXTURE_2D, fboPong.getTexture());
			}
			horizontal = !horizontal;
		}

		FrameBufferObject.clearBind();
		glActiveTexture(GL_TEXTURE0);

		return fboPong;
	}

	/**
	 * Shuts down the bloom renderer, freeing any resources held.
	 */
	public void shutdown() {
		glDeleteBuffers(vaoID);
		glDeleteVertexArrays(vaoID);
		gpuProgram.shutdown();
		gpuProgram = null;
		fboPing.destroy();
		fboPong.destroy();
	}

	/**
	 * Sets up all required buffers for storing data gpu side.
	 */
	public void setupGlBuffers() {
		// Setup of the buffer of cube vertexes to sent to openGl
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[0]);
		glVertexAttribPointer(0, 4, GL_FLOAT, false, 0, 0);
		glBufferData(GL_ARRAY_BUFFER, ShapeGenerator.Screen.screenVertexPos, GL_STATIC_DRAW);

		// Setup the surface normals of each vertex
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[1]);
		glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
		glBufferData(GL_ARRAY_BUFFER, ShapeGenerator.Screen.textureCoords, GL_STATIC_DRAW);
	}
}
