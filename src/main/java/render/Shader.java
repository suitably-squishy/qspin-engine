package render;

import java.util.logging.Logger;

import static io.resources.readers.ShaderReader.readShaderFile;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.*;

/**
 * A utility class for dealing with individual shaders.
 */
public class Shader {

	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/**
	 * Loads a shader then validates that the shader compiled.
	 *
	 * @param shaderFileName The file name of the shader to load in the /shaders directory.
	 * @param shaderType     The GL type of the shader.
	 * @return The GL id of the shader.
	 */
	public static int compileShader(String shaderFileName, int shaderType) {
		int shader = glCreateShader(shaderType);
		glShaderSource(shader, readShaderFile(shaderFileName));
		glCompileShader(shader);

		if (glGetShaderi(shader, GL_COMPILE_STATUS) == GL_FALSE) {
			LOGGER.severe("Could not compile " + shaderFileName + " shader");
			System.exit(-1);
		}

		return shader;
	}
}
