package render;

import window.Window;

import java.util.logging.Logger;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;

/**
 * A renderer for combining two frame buffer objects using neutral exposure.
 */
public class Blender {
	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	/** The output FBO made by the blender. */
	private FrameBufferObject fbo;

	/** The underlying blender shader. */
	private GPUProgram gpuProgram;

	/** The index of the vertex array attribute to pass to openGl */
	private int vaoID;

	/**
	 * The individual buffers to pass to openGl
	 * These are bound to the VAO
	 * 0 - vertex buffer
	 * 1 - texture buffer
	 */
	private int[] vboIDs;

	/**
	 * Creates a Blender with a framebuffer with the size of the given window.
	 *
	 * @param window The window to make the framebuffer for.
	 */
	public Blender(Window window) {
		this.fbo = new FrameBufferObject(window);
		gpuProgram = new GPUProgram("ScreenVertex", "Blend");
	}


	/**
	 * Starts the blender, setting up all GL buffers.
	 */
	public void startup() {
		LOGGER.info("Blender starting!");

		int[] attributeDivisors = {0, 0};

		gpuProgram.startup();
		gpuProgram.setupBuffers(2);
		vaoID = gpuProgram.getVaoID();
		vboIDs = gpuProgram.getVboIDs();

		fbo.startup();
		fbo.attachColourBuffer();
		fbo.attachDepthBuffer();
		fbo.prepare();

		setupGlBuffers();
		gpuProgram.setupAttributes(attributeDivisors);
	}

	/**
	 * Blends the two provided frame buffer objects stored by OpenGL together.
	 *
	 * @param scene The GL id of the "below" frame buffer to blend.
	 * @param blend The GL id of the "above" frame buffer to blend.
	 * @return A frame buffer object containing the blended buffers.
	 */
	public FrameBufferObject blend(int scene, int blend) {
		// Bind the gpu program to gl
		gpuProgram.bind();
		fbo.bind();
		if (!FrameBufferObject.validate()) LOGGER.warning("FBO not complete");

		glUniform1i(gpuProgram.getUniformIndex("scene"), 0);
		glUniform1i(gpuProgram.getUniformIndex("blend"), 1);
		glUniform1f(gpuProgram.getUniformIndex("exposure"), 1);

		glActiveTexture(GL_TEXTURE0); // Texture unit 0
		glBindTexture(GL_TEXTURE_2D, scene);

		glActiveTexture(GL_TEXTURE0 + 1); // Texture unit 1
		glBindTexture(GL_TEXTURE_2D, blend);

		// Setup gl state
		glClear(GL_COLOR_BUFFER_BIT);
		glBlendFunc(GL_ONE, GL_ZERO);
		glDisable(GL_BLEND);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);

		glDrawArrays(GL_TRIANGLES, 0, 6);

		fbo.drawToBuffers();

		FrameBufferObject.clearBind();
		glActiveTexture(GL_TEXTURE0);

		return fbo;
	}

	/**
	 * Blends the two provided frame buffer objects together.
	 *
	 * @param scene     The frame buffer object of the "below" frame buffer to blend.
	 * @param sceneName The name of the frame buffer attachment to blend in the "scene" buffer.
	 * @param blend     The frame buffer object of the "above" frame buffer to blend.
	 * @param blendName The name of the frame buffer attachment to blend in the "blend" buffer.
	 * @return A frame buffer object containing the blended buffers.
	 */
	public FrameBufferObject blend(FrameBufferObject scene, String sceneName, FrameBufferObject blend, String blendName) {
		return blend(scene.getTexture(sceneName), blend.getTexture(blendName));
	}

	/**
	 * Blends the two provided frame buffer objects together, using the default attachment in each buffer.
	 *
	 * @param scene The frame buffer object of the "below" frame buffer to blend.
	 * @param blend The frame buffer object of the "above" frame buffer to blend.
	 * @return A frame buffer object containing the blended buffers.
	 */
	public FrameBufferObject blend(FrameBufferObject scene, FrameBufferObject blend) {
		return blend(scene.getTexture(), blend.getTexture());
	}

	/**
	 * Shuts down the blender, freeing any resources held.
	 */
	public void shutdown() {
		glDeleteBuffers(vaoID);
		glDeleteVertexArrays(vaoID);
		gpuProgram.shutdown();
		gpuProgram = null;
		fbo.destroy();
	}

	/**
	 * Sets up all required buffers for storing data gpu side.
	 */
	public void setupGlBuffers() {
		// Setup of the buffer of cube vertexes to sent to openGl
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[0]);
		glVertexAttribPointer(0, 4, GL_FLOAT, false, 0, 0);
		glBufferData(GL_ARRAY_BUFFER, ShapeGenerator.Screen.screenVertexPos, GL_STATIC_DRAW);

		// Setup the surface normals of each vertex
		glBindBuffer(GL_ARRAY_BUFFER, vboIDs[1]);
		glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
		glBufferData(GL_ARRAY_BUFFER, ShapeGenerator.Screen.textureCoords, GL_STATIC_DRAW);
	}
}
