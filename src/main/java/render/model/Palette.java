package render.model;

import utils.RGBMaterial;

/**
 * A class for storing bindings between indices and materials to form a palette.
 */
public class Palette {

	private final RGBMaterial[] materials;

	/**
	 * Creates an empty palette with the given size.
	 *
	 * @param size The size of the palette.
	 */
	public Palette(short size) {
		materials = new RGBMaterial[size];
	}

	/**
	 * Creates a palette with the provided materials.
	 *
	 * @param materials The materials to make a palette out of.
	 */
	public Palette(RGBMaterial... materials) {
		this.materials = new RGBMaterial[materials.length];
		System.arraycopy(materials, 0, this.materials, 0, materials.length);
	}

	/**
	 * Clones the provided palette, also cloning any materials in the palette.
	 *
	 * @param palette The palette to clone.
	 */
	public Palette(Palette palette) {
		this.materials = new RGBMaterial[palette.materials.length];
		for (short i = 0; i < palette.materials.length; i++) {
			this.materials[i] = new RGBMaterial(palette.materials[i]);
		}
	}
	
	/**
	 * Gets all material in the palette.
	 *
	 * @return An array of RGBMaterial in index order.
	 */
	public RGBMaterial[] getMaterials() {
		return materials;
	}

	/**
	 * Gets the material at the provided index.
	 *
	 * @param index The index to get the material at.
	 * @return The material if the index is in the palette, null otherwise.
	 */
	public RGBMaterial getMaterial(short index) {
		if (index < 0 || index >= materials.length) {
			return null;
		}
		return materials[index];
	}

	/**
	 * Sets the material at the provided index to the provided material.
	 *
	 * @param index    The index to set the colour at.
	 * @param material The material to set the index to.
	 */
	public void setMaterials(short index, RGBMaterial material) {
		if (index < 0 || index >= materials.length) {
			return;
		}
		materials[index] = material;
	}
}
