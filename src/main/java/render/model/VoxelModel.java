package render.model;

import io.resources.Resource;
import org.joml.Vector3i;
import utils.CheckNeighbours;
import utils.RGBColour;
import utils.RGBMaterial;

/**
 * A class for representing models made from voxels.
 */
public class VoxelModel extends Resource {

	/**
	 * The voxels making up the model, with 0 representing empty and a number representing the colour in the palette.
	 */
	private short[][][] voxels;

	/**
	 * A byte storing each face as a bit determining if there is a neighbour there,
	 * and should therefore not have a face
	 */
	private byte[][][] neighbours;

	/**
	 * The palette of colours the model uses.
	 */
	private Palette palette;

	/**
	 * The number of voxels in the model.
	 */
	private long volume;

	private Vector3i size;

	public static final VoxelModel fallbackModel = createFallbackModel();

	/**
	 * Creates a new Voxel Model with the provided voxels, palette and size.
	 *
	 * @param voxels     The voxels that make up the model.
	 * @param palette    The palette of colours the model uses.
	 * @param volume     The number of solid voxels in the model.
	 * @param neighbours The neighbour array
	 */
	public VoxelModel(short[][][] voxels, Palette palette, long volume, byte[][][] neighbours) {
		validateVoxelArray(voxels);
		this.voxels = voxels;
		this.palette = palette;
		this.volume = volume;
		this.neighbours = neighbours;
		this.size = new Vector3i(voxels.length, voxels[0].length, voxels[0][0].length);
	}

	/**
	 * Creates a new Voxel Model with the provided voxels, palette and size.
	 *
	 * @param voxels  The voxels that make up the model.
	 * @param palette The palette of colours the model uses.
	 * @param volume  The number of solid voxels in the model.
	 */
	public VoxelModel(short[][][] voxels, Palette palette, long volume) {
		this(voxels, palette, volume, CheckNeighbours.checkNeighbours(voxels));
	}

	/**
	 * Creates a new Voxel Model with the provided voxels and palette.
	 *
	 * @param voxels  The voxels that make up the model.
	 * @param palette The palette of colours the model uses.
	 */
	public VoxelModel(short[][][] voxels, Palette palette) {
		this(voxels, palette, getModelSize(voxels));
	}

	/**
	 * Clones the provided model, also cloning the palette.
	 *
	 * @param model The model to clone.
	 */
	public VoxelModel(VoxelModel model) {
		this(cloneVoxelArrays(model.voxels), new Palette(model.palette), model.volume);
	}

	/**
	 * Gets the material of the voxel at the specified coordinates.
	 *
	 * @param x The x coordinate in the model.
	 * @param y The y coordinate in the model.
	 * @param z The z coordinate in the model.
	 * @return An RGBMaterial if present, null otherwise.
	 */
	public RGBMaterial getVoxelMaterial(int x, int y, int z) {
		if (
				x < 0 || y < 0 || z < 0 ||
						x >= voxels.length ||
						y >= voxels[x].length ||
						z >= voxels[x][y].length
		) {
			return null;
		}
		return palette.getMaterial(voxels[x][y][z]);
	}

	/**
	 * Gets the number of filled voxels in the model.
	 *
	 * @return The number of filled voxels.
	 */
	public long getVolume() {
		return volume;
	}

	/**
	 * Gets the underlying array representing the voxels in the model.
	 *
	 * @return A 3 dimensional array of shorts representing positions in the model's palette.
	 */
	public short[][][] getVoxels() {
		return voxels;
	}

	/**
	 * Gets the neighbour state of the model.
	 *
	 * @return A 3 dimensional array carrying the bitwise state of whether each voxel has neighbours.
	 * @see CheckNeighbours
	 */
	public byte[][][] getNeighbours() {
		return neighbours;
	}

	/**
	 * Gets the number of filled voxels in the model.
	 *
	 * @param voxels The voxels making up the model.
	 * @return The number of filled voxels in the model.
	 */
	private static int getModelSize(short[][][] voxels) {
		int size = 0;
		for (int x = 0; x < voxels.length; x++) {
			for (int y = 0; y < voxels[x].length; y++) {
				for (int z = 0; z < voxels[x][y].length; z++) {
					if (voxels[x][y][z] != 0) size++;
				}
			}
		}
		return size;
	}

	/**
	 * Clones the provided 3D voxel array.
	 *
	 * @param voxels The voxels to copy.
	 * @return A direct copy of the 3D array.
	 */
	private static short[][][] cloneVoxelArrays(short[][][] voxels) {
		short[][][] clonedVoxels = new short[voxels.length][][];
		for (int x = 0; x < voxels.length; x++) {
			for (int y = 0; y < voxels[x].length; y++) {
				clonedVoxels[x][y] = voxels[x][y].clone();
			}
		}
		return clonedVoxels;
	}

	/**
	 * Generates the fallback model of voxels.
	 *
	 * @return A voxel model with a single pink square.
	 */
	private static VoxelModel createFallbackModel() {
		short[][][] voxels = new short[1][1][1];
		voxels[0][0][0] = 1;
		Palette palette = new Palette(new RGBMaterial(new RGBColour(0, 0, 0, 0)), new RGBMaterial(new RGBColour(255, 0, 204)));
		return new VoxelModel(voxels, palette);
	}

	private static void validateVoxelArray(short[][][] voxels) {
		if (voxels.length == 0) throw new IllegalArgumentException("Cannot have a voxel array of 0 x size.");
		if (voxels[0].length == 0) throw new IllegalArgumentException("Cannot have a voxel array of 0 y size.");
		if (voxels[0][0].length == 0) throw new IllegalArgumentException("Cannot have a voxel array of 0 z size.");
	}

	public Palette getPalette() {
		return palette;
	}

	public void setPalette(Palette palette) {
		this.palette = palette;
	}

	public Vector3i getSize() {
		return size;
	}
}
