#version 430 core

in VS_OUT
{
    vec4 colour;
}fs_in;

out vec4 colour;

void main(void)
{
    colour = fs_in.colour;
}