package io.logging;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class LoggingTests {
	
	@Test
	void testLoggingSetup(){
		assertDoesNotThrow(GameLogger::setup);
	}
}
