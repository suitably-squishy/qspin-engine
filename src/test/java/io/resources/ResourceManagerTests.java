package io.resources;

import io.resources.loaders.VoxelModelLoader;
import org.junit.jupiter.api.Test;
import render.model.VoxelModel;

import static org.junit.jupiter.api.Assertions.*;

public class ResourceManagerTests {

	@Test
	void testBasicLoad() {
		ResourceManager manager = new ResourceManager();

		ResourceAtlas<VoxelModel> voxelAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);

		manager.putAtlas(VoxelModel.class, voxelAtlas);

		VoxelModel model = manager.get(VoxelModel.class, "3x3x3");
		assertNotNull(model, "Extracted model is null.");
		assertEquals(20, model.getVolume(), "Extracted model is of wrong size.");
	}

	@Test
	void testNullAtlas() {
		ResourceManager manager = new ResourceManager();

		VoxelModel model = manager.get(VoxelModel.class, "3x3x3");
		assertNull(model, "Model was not null when no atlas was present.");

		ResourceAtlas<VoxelModel> atlas = manager.getAtlas(VoxelModel.class);
		assertNull(atlas, "Atlas was not null when no atlas was present");
	}

	@Test
	void testRepeatedLoad() {
		ResourceManager manager = new ResourceManager();

		ResourceAtlas<VoxelModel> voxelAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);
		manager.putAtlas(VoxelModel.class, voxelAtlas);

		VoxelModel model = manager.get(VoxelModel.class, "3x3x3");
		VoxelModel model2 = manager.get(VoxelModel.class, "3x3x3");
		assertEquals(model, model2, "Models were different.");
	}

	@Test
	void testDrop() {
		ResourceManager manager = new ResourceManager();

		ResourceAtlas<VoxelModel> voxelAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);
		manager.putAtlas(VoxelModel.class, voxelAtlas);

		VoxelModel model = manager.get(VoxelModel.class, "3x3x3");

		manager.dropAtlas(VoxelModel.class);

		VoxelModel model2 = manager.get(VoxelModel.class, "3x3x3");
		assertNull(model2, "Manager got a model even though the atlas was dropped");
	}
}
