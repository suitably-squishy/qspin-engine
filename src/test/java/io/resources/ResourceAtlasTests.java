package io.resources;

import io.resources.loaders.VoxelModelLoader;
import org.junit.jupiter.api.Test;
import render.model.VoxelModel;

import static org.junit.jupiter.api.Assertions.*;

public class ResourceAtlasTests {

	private String realModel = "3x3x3";
	private String fakeModel = "nonexistentmodel";

	@Test
	void testSimpleLoad() {
		ResourceAtlas<VoxelModel> voxelModelResourceAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);
		VoxelModel model = voxelModelResourceAtlas.get(realModel);
		assertNotNull(model, "Extracted model was null.");
		assertEquals(model.getVolume(), 20, "Extracted model was of wrong size.");
	}

	@Test
	void testRepeatedLoad() {
		ResourceAtlas<VoxelModel> voxelModelResourceAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);
		VoxelModel copy1 = voxelModelResourceAtlas.get(realModel);
		VoxelModel copy2 = voxelModelResourceAtlas.get(realModel);
		assertSame(copy1, copy2, "Repeated load does not produce equal models.");
		assertTrue(voxelModelResourceAtlas.isLoaded(realModel));
	}

	@Test
	void testFallbackLoad() {
		ResourceAtlas<VoxelModel> voxelModelResourceAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);
		VoxelModel nonExistentModel = voxelModelResourceAtlas.get(fakeModel);
		assertEquals(nonExistentModel, VoxelModel.fallbackModel, "Model was not the fallback model.");
	}

	@Test
	void testDrop() {
		ResourceAtlas<VoxelModel> voxelModelResourceAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);
		voxelModelResourceAtlas.get(realModel);
		assertTrue(voxelModelResourceAtlas.isLoaded(realModel), "Model was not loaded.");
		voxelModelResourceAtlas.drop(realModel);
		assertFalse(voxelModelResourceAtlas.isLoaded(realModel), "Model was still loaded after being dropped.");
	}

	@Test
	void testLoadNoStore() {
		ResourceAtlas<VoxelModel> voxelModelResourceAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);
		voxelModelResourceAtlas.get(realModel, false);
		assertFalse(voxelModelResourceAtlas.isLoaded(realModel), "Model was still loaded after being told not to be stored.");
	}

	@Test
	void testNullLoad() {
		ResourceAtlas<VoxelModel> voxelModelResourceAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);
		VoxelModel model = voxelModelResourceAtlas.get(null);
		assertEquals(model, VoxelModel.fallbackModel, "Model was not the fallback model.");
	}
}
