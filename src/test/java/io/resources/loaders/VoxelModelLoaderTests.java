package io.resources.loaders;

import org.junit.jupiter.api.Test;
import render.model.VoxelModel;

import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class VoxelModelLoaderTests {

	private final VoxelModelLoader loader = new VoxelModelLoader();

	@Test
	void testSimpleQbFileLoad() {
		try {
			VoxelModel model = loader.loadResource("3x3x3Compressed");

			assertEquals(model.getVolume(), 20);
		} catch (IOException e) {
			fail(e);
		}
	}

	@Test
	void testSimpleVoxFileLoad() {
		try {
			VoxelModel model = loader.loadResource("ColourTestVox");

			assertEquals(model.getVolume(), 7);
		} catch (IOException e) {
			fail(e);
		}
	}

	@Test
	void testExplicitVoxFileLoad() {
		try {
			VoxelModel model = loader.loadResource("3x3x3.vox");

			assertEquals(model.getVolume(), 20);
		} catch (IOException e) {
			fail(e);
		}
	}

	@Test
	void testExplicitQbFileLoad() {
		try {
			VoxelModel model = loader.loadResource("3x3x3.qb");

			assertEquals(model.getVolume(), 20);
		} catch (IOException e) {
			fail(e);
		}
	}

	@Test
	void testFileNotFound() {
		assertThrows(FileNotFoundException.class, () -> loader.loadResource("NonExistentFile"));
	}
}
