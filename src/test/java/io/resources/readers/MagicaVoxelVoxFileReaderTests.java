package io.resources.readers;

import io.resources.readers.voxels.MagicaVoxelVoxFileReader;
import io.resources.readers.voxels.VoxelSourceReader;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class MagicaVoxelVoxFileReaderTests {

	@Test
	void testBasicVoxelReading() {
		try {
			ByteBuffer buffer = ResourceUtils.getResourceAsByteBuffer("models/3x3x3.vox");
			VoxelSourceReader reader = new MagicaVoxelVoxFileReader(buffer);
			AtomicInteger voxelCount = new AtomicInteger();

			AtomicInteger width = new AtomicInteger();
			AtomicInteger depth = new AtomicInteger();
			AtomicInteger height = new AtomicInteger();

			reader.addSizeSubscriber(
					(x, y, z) -> {
						width.set(x);
						depth.set(y);
						height.set(z);
					});
			reader.addVoxelSubscriber(
					(x, y, z, colourIndex) -> voxelCount.getAndIncrement());
			reader.read();
			assertEquals(20, voxelCount.get(), "Incorrect number of voxels extracted.");

			assertEquals(3, width.get(), "Incorrect width.");
			assertEquals(3, depth.get(), "Incorrect depth.");
			assertEquals(3, height.get(), "Incorrect height.");

		} catch (IOException e) {
			fail(e);
		}
	}

	@Test
	void testVoxelColours() {
		try {
			ByteBuffer buffer = ResourceUtils.getResourceAsByteBuffer("models/ColourTestVox.vox");

			VoxelSourceReader reader = new MagicaVoxelVoxFileReader(buffer);

			AtomicInteger width = new AtomicInteger();
			AtomicInteger depth = new AtomicInteger();
			AtomicInteger height = new AtomicInteger();

			int[][][] voxels = new int[10][1][1];

			int[] redPalette = new int[256];
			int[] greenPalette = new int[256];
			int[] bluePalette = new int[256];

			reader.addSizeSubscriber((x, y, z) -> {
				width.getAndSet(x);
				depth.getAndSet(y);
				height.getAndSet(z);
			});
			reader.addVoxelSubscriber((x, y, z, colourIndex) -> voxels[x][y][z] = colourIndex);
			reader.addPaletteSubscriber((colourIndex, r, g, b) -> {
				redPalette[colourIndex] = r;
				greenPalette[colourIndex] = g;
				bluePalette[colourIndex] = b;
			});
			reader.read();

			assertEquals(10, width.get(), "Incorrect width.");
			assertEquals(1, depth.get(), "Incorrect depth.");
			assertEquals(1, height.get(), "Incorrect height.");

			assertEquals(255, redPalette[voxels[0][0][0]], "White voxel incorrect red channel.");
			assertEquals(255, greenPalette[voxels[0][0][0]], "White voxel incorrect green channel.");
			assertEquals(255, bluePalette[voxels[0][0][0]], "White voxel incorrect blue channel.");

			assertEquals(255, redPalette[voxels[1][0][0]], "Red voxel incorrect red channel.");
			assertEquals(0, greenPalette[voxels[1][0][0]], "Red voxel incorrect green channel.");
			assertEquals(0, bluePalette[voxels[1][0][0]], "Red voxel incorrect blue channel.");

			assertEquals(0, redPalette[voxels[2][0][0]], "Green voxel incorrect red channel.");
			assertEquals(255, greenPalette[voxels[2][0][0]], "Green voxel incorrect green channel.");
			assertEquals(0, bluePalette[voxels[2][0][0]], "Green voxel incorrect blue channel.");

			assertEquals(0, redPalette[voxels[3][0][0]], "Blue voxel incorrect red channel.");
			assertEquals(0, greenPalette[voxels[3][0][0]], "Blue voxel incorrect green channel.");
			assertEquals(255, bluePalette[voxels[3][0][0]], "Blue voxel incorrect blue channel.");

			assertEquals(0, redPalette[voxels[4][0][0]], "Black voxel incorrect red channel.");
			assertEquals(0, greenPalette[voxels[4][0][0]], "Black voxel incorrect green channel.");
			assertEquals(0, bluePalette[voxels[4][0][0]], "Black voxel incorrect blue channel.");
		} catch (IOException e) {
			fail(e);
		}
	}
}
