package io.input;

import io.input.binds.Bind;
import io.input.binds.BindContext;
import io.input.binds.BindPressSubscriber;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;

public class BindTests {

    @Test
    void testBasicBinding() {
        BindContext bindContext = new BindContext("testing");
        Bind testBind = new Bind("testBind");
        bindContext.bind(GLFW_KEY_ESCAPE, testBind);

        testBind.subscribe((BindPressSubscriber) (bindName, key, scancode, mods) -> false);

        bindContext.keyInput(0, GLFW_KEY_ESCAPE, 0, 0, 0);
        assertEquals(testBind, bindContext.getBind("testBind"));

        bindContext.unbind("testBind");

        assertNull(bindContext.getBind("testBind"));
    }
}
