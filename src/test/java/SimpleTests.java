import org.joml.Vector2d;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class SimpleTests  {
	@Test
	void testTest(){
		assertEquals(1,1);
	}
	
	@Test
	void testJOML(){
		Vector2d vec = new Vector2d(1,1);
		assertEquals(vec.x, 1);
	}

}
