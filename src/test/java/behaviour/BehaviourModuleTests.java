package behaviour;

import entities.Entity;
import io.input.mouse.MouseInputContext;
import org.joml.Vector2d;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BehaviourModuleTests {

	@Test
	void testBehavouiralObject() {
		AtomicReference<String> testString = new AtomicReference<>("");

		BehaviouralObject object = new BehaviouralObject() {
		};

		object.addBehaviour("test", (delta, owner) -> {
			String string = testString.get();
			testString.set(string + "1");
		});

		object.update(0.1f);

		assertEquals("1", testString.get(), "BehaviouralObject did not add behaviour");
	}

	@Test
	void testBehavioursCalled() {
		AtomicInteger number = new AtomicInteger();
		Entity testEntity = new Entity("test") {
		};

		testEntity.addBehaviour("test", (delta, owner) -> number.getAndIncrement());

		testEntity.update(1f);

		assertEquals(1, number.get(), "Behaviour was not called once.");
	}

	@Test
	void testBehavioursCalledInOrder() {
		AtomicReference<String> testString = new AtomicReference<>("");

		Entity testEntity = new Entity("test") {
		};

		testEntity.addBehaviour("test3", (delta, owner) -> {
			String string = testString.get();
			testString.set(string + "3");
		}, 99);

		testEntity.addBehaviour("test1", (delta, owner) -> {
			String string = testString.get();
			testString.set(string + "1");
		}, 1);

		testEntity.addBehaviour("test2", (delta, owner) -> {
			String string = testString.get();
			testString.set(string + "2");
		}, 50);

		testEntity.update(1f);

		assertEquals("123", testString.get(), "Update functions were called in incorrect order.");
	}

	@Test
	void testBehaviourToggling() {
		AtomicReference<String> testString = new AtomicReference<>("");

		Entity testEntity = new Entity("test") {
		};

		testEntity.addBehaviour(new Behaviour<>("test", (delta, owner) -> {
			String string = testString.get();
			testString.set(string + "1");
		}, testEntity, 50));

		testEntity.update(1f);

		testEntity.disableBehaviour("test");

		testEntity.update(1f);

		assertEquals("1", testString.get(), "Update functions were called in incorrect order.");
	}

	@SuppressWarnings({"ConstantConditions", "MismatchedReadAndWriteOfArray"})
	@Test
	void testBehaviourError() {
		Entity testEntity = new Entity("test") {
		};

		final int[] testArray = new int[2];

		testEntity.addBehaviour("badBehaviour", (delta, owner) -> System.out.println(testArray[4]));

		testEntity.update(1f);
		testEntity.update(1f);
	}

	@Test
	void testBehaviourRemoval() {
		BehaviouralObject testEntity = new BehaviouralObject() {
		};
		AtomicInteger number = new AtomicInteger();

		MouseInputContext inputContext = new MouseInputContext("testContext");

		BehaviourTemplate<BehaviouralObject> behaviourTemplate = new BehaviourTemplate<>();

		behaviourTemplate.subscribe(inputContext, (mousePos, delta) -> {
			number.getAndIncrement();
		});

		Behaviour<BehaviouralObject> behaviour = new Behaviour<>("test", behaviourTemplate, testEntity, 50);

		testEntity.addBehaviour(behaviour);

		inputContext.mousePos(0, new Vector2d(), new Vector2d());

		assertEquals(1, number.get(), "Mouse movement was called incorrect number of times.");

		testEntity.disableBehaviour("test");

		inputContext.mousePos(0, new Vector2d(), new Vector2d());

		assertEquals(1, number.get(), "Mouse movement was probably called when a behaviour is disabled.");

		testEntity.enableBehaviour("test");

		inputContext.mousePos(0, new Vector2d(), new Vector2d());

		assertEquals(2, number.get(), "Mouse movement was probably not called when a behaviour was enabled.");

		testEntity.removeBehaviour("test");

		inputContext.mousePos(0, new Vector2d(), new Vector2d());

		assertEquals(2, number.get(), "Removing behaviour did not stop mouse listeners being called.");
	}
}
