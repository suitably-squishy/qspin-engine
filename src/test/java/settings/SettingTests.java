package settings;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class SettingTests {

    @Test
    void testSettingValueUpdate() {
        BooleanSetting setting = new BooleanSetting("Test", "test", true, true);
        assertTrue(setting.getValue(), "Setting was not true on creation.");
        setting.setValue(false);
        assertFalse(setting.getValue(), "Setting did not update to false.");
    }

    @Test
    void testOptionSettingCorrectValue() {
        OptionSetting<Integer> setting = new OptionSetting<>("Test", "test", true, 0, 0, Arrays.asList(0, 1, 2, 3, 4, 5));
        setting.setValue(5);
        assertEquals(5, setting.getValue(), "Setting did not update to new value.");
    }

    @Test
    void testOptionSettingWrongValue() {
        OptionSetting<Integer> setting = new OptionSetting<>("Test", "test", true, 0, 0, Arrays.asList(0, 1, 2, 3, 4, 5));
        setting.setValue(8);
        assertEquals(0, setting.getValue(), "Setting updated to a value that should not be possible.");
    }

    @Test
    void testSettingNotEditable() {
        BooleanSetting setting = new BooleanSetting("Test", "test", false, true);
        setting.setValue(false);
        assertTrue(setting.getValue(), "Setting updated when it shouldn't have.");
    }

    @Test
    void testContinuousSetting() {
        ContinuousSetting<Integer> setting = new ContinuousSetting<>("Test", "test", true, 5, 5, 0, 10);
        setting.setValue(0);
        assertEquals(0, setting.getValue(), "Setting did not update to minimum.");
        setting.setValue(10);
        assertEquals(10, setting.getValue(), "Setting did not update to maximum.");
        setting.setValue(-5);
        assertEquals(0, setting.getValue(), "Setting did not clamp to minimum.");
        setting.setValue(15);
        assertEquals(10, setting.getValue(), "Setting did not clamp to maximum.");
    }

    @Test
    void testContinuousSettingNull() {
        ContinuousSetting<Integer> setting = new ContinuousSetting<>("Test", "test", true, 5, 5);
        setting.setValue(0);
        assertEquals(0, setting.getValue(), "Setting did not update.");
    }
}
