package utils;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

public class RGBColourTests {

	@ParameterizedTest
	@ValueSource(strings = {"ffffff", "#FFFFFF", "a5b7c2", "#A6B1C8", "00000000"})
	void testValidHexColour(String hexColour) {
		assertDoesNotThrow(() -> {
			RGBColour colour = new RGBColour(hexColour);
		});
	}

	@ParameterizedTest
	@ValueSource(strings = {"fffffff", "fffff", "0000", "ffffgf", "[A0A0A0]", "", "#", "#123456789"})
	void testInvalidHexColour(String hexColour) {
		assertThrows(IllegalArgumentException.class, () -> {
			RGBColour colour = new RGBColour(hexColour);
		});
	}

	@Test
	void testEquality() {
		RGBColour colour1 = new RGBColour(1f, 0f, 0f, 0f);
		RGBColour colour2 = new RGBColour(255, 0, 0, 0);

		assertEquals(colour1, colour2);
		assertEquals(colour1.hashCode(), colour2.hashCode());
	}
}
