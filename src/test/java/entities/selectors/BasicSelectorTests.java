package entities.selectors;

import entities.Entity;
import entities.filters.Class;
import entities.filters.ID;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class BasicSelectorTests extends SelectorTests {

	@Test
	void testBasicIDFilteredSelector() {
		root.addChildren(entity1, entity2, entity3);

		entity1.addChildren(entity1a, entity1b, entity1c);
		entity2.addChildren(entity2a, entity2b, entity2c);
		entity3.addChildren(entity3a);

		Collection<Entity> selectedEntities = root.getDescendants(new ID("entity1"));

		Collection<Entity> expectedEntities = new HashSet<>();
		expectedEntities.add(entity1);

		assertTrue(CollectionUtils.isEqualCollection(selectedEntities, expectedEntities));
	}

	@Test
	void testBasicClassFilteredSelector() {
		root.addChildren(entity1, entity2, entity3);

		entity1.addChildren(entity1a, entity1b, entity1c);
		entity2.addChildren(entity2a, entity2b, entity2c);
		entity3.addChildren(entity3a);

		Collection<Entity> selectedEntities = root.getDescendants(new Class("bean"));

		Collection<Entity> expectedEntities = new HashSet<>();
		expectedEntities.add(root);
		expectedEntities.add(entity2);
		expectedEntities.add(entity1a);
		expectedEntities.add(entity1c);
		expectedEntities.add(entity2b);
		expectedEntities.add(entity3a);

		assertTrue(CollectionUtils.isEqualCollection(selectedEntities, expectedEntities));
	}

	@Test
	void testGeneralSelector() {
		root.addChildren(entity1, entity2, entity3);

		entity1.addChildren(entity1a, entity1b, entity1c);
		entity2.addChildren(entity2a, entity2b, entity2c);
		entity3.addChildren(entity3a);

		Collection<Entity> selectedEntities = root.getDescendants();

		Collection<Entity> expectedEntities = new HashSet<>();
		expectedEntities.add(root);
		expectedEntities.add(entity1);
		expectedEntities.add(entity1a);
		expectedEntities.add(entity1b);
		expectedEntities.add(entity1c);
		expectedEntities.add(entity2);
		expectedEntities.add(entity2a);
		expectedEntities.add(entity2b);
		expectedEntities.add(entity2c);
		expectedEntities.add(entity3);
		expectedEntities.add(entity3a);

		assertTrue(CollectionUtils.isEqualCollection(selectedEntities, expectedEntities));
	}
}
