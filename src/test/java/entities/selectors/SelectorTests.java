package entities.selectors;

import entities.Entity;

import java.util.Collections;

public class SelectorTests {

	Entity root = new Entity("root", Collections.singletonList("bean")) {
	};
	Entity entity1 = new Entity("entity1", Collections.singletonList("apple")) {
	};
	Entity entity2 = new Entity("entity2", Collections.singletonList("bean")) {
	};
	Entity entity3 = new Entity("entity3", Collections.singletonList("apple")) {
	};
	Entity entity1a = new Entity("entity1a", Collections.singletonList("bean")) {
	};
	Entity entity1b = new Entity("entity1b", Collections.singletonList("apple")) {
	};
	Entity entity1c = new Entity("entity1c", Collections.singletonList("bean")) {
	};
	Entity entity2a = new Entity("entity2a", Collections.singletonList("apple")) {
	};
	Entity entity2b = new Entity("entity2b", Collections.singletonList("bean")) {
	};
	Entity entity2c = new Entity("entity2c", Collections.singletonList("apple")) {
	};
	Entity entity3a = new Entity("entity3a", Collections.singletonList("bean")) {
	};
}
