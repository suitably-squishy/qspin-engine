package entities.transform;

import org.joml.Matrix4f;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ListenableMatrix4fTests {
	
	@Test
	void testBasicListener() {
		
		AtomicInteger changeEvents = new AtomicInteger();
		
		final Matrix4f testMatrix = new ListenableMatrix4f(changeEvents::getAndIncrement);
		
		testMatrix.add(new Matrix4f().identity());
		
		assertEquals(1, changeEvents.get(), "Incorrect number of change events occurred.");
	}
	
	@Test
	void testDestinationListener() {
		AtomicInteger changeEvents = new AtomicInteger();
		
		final Matrix4f destination = new ListenableMatrix4f(changeEvents::getAndIncrement);
		
		new OperableMatrix4f().identity().mul(new Matrix4f(), destination);
		assertEquals(1, changeEvents.get(), "Incorrect number of change events occurred.");
	}
	
	@Test
	void testDifferentMatrixTypes() {
		AtomicInteger changeEvents = new AtomicInteger();
		
		final OperableMatrix4f destination = new ListenableMatrix4f(changeEvents::getAndIncrement);
		
		destination.add(new Matrix4f().identity()).add(new ListenableMatrix4f().rotateX(5));
		
		assertEquals(2, changeEvents.get(), "Incorrect number of change events occurred.");
	}
	
	@Test
	void testChainedOperations() {
		AtomicInteger changeEvents = new AtomicInteger();
		
		final OperableMatrix4f destination = new ListenableMatrix4f(changeEvents::getAndIncrement);
		
		destination.opStart().add(new Matrix4f()).m00(2).identity().opEnd();
		assertEquals(1, changeEvents.get(), "Incorrect number of change events occurred.");
	}
}
