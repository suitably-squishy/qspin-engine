package entities.physics.forces;

import org.joml.Vector3f;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ForceTests {

	@Test
	void testNoForces() {
		ForceManager forceManager = new ForceManager(1);

		Vector3f movement = forceManager.step(9);
		assertEquals(new Vector3f(), movement);
	}
}
