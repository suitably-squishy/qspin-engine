package entities.physics.collision;

import entities.Entity;
import entities.VoxelEntity;
import io.resources.ResourceAtlas;
import io.resources.loaders.VoxelModelLoader;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import render.model.VoxelModel;
import utils.TimeMe;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class RayEntityIntersectionTests {

	private static Stream<Arguments> provideResultMergeValues() {
		Entity entity1 = new Entity("Entity1") {
		};

		Entity entity2 = new Entity("Entity2") {
		};


		RayEntityIntersection.Result result1 = new RayEntityIntersection.Result();
		result1.collisionDistance = 50;
		result1.collidedEntity = entity1;

		RayEntityIntersection.Result result2 = new RayEntityIntersection.Result();
		result2.collisionDistance = 60;
		result2.collidedEntity = entity2;

		return Stream.of(
				Arguments.of(result1, result2, result1),
				Arguments.of(result1, null, result1),
				Arguments.of(null, result2, result2),
				Arguments.of(null, null, null)
		);
	}

	@ParameterizedTest
	@MethodSource("provideResultMergeValues")
	void testResultMerges(RayEntityIntersection.Result result1, RayEntityIntersection.Result result2, RayEntityIntersection.Result expectedResult) {
		RayEntityIntersection.Result result = RayEntityIntersection.Result.merge(result1, result2);
		if (expectedResult == null) {
			assertNull(result, "Merged result should have been null.");
		} else {
			assertNotNull(result, "Merged result should not have been null.");
			assertEquals(expectedResult.collidedEntity, result.collidedEntity, "Merged result had incorrect collided entity.");
			assertEquals(expectedResult.collisionDistance, result.collisionDistance, "Merged result had incorrect collision distance.");
		}
	}

	@Test
	void testSimpleRaycast() {
		ResourceAtlas<VoxelModel> voxelModelResourceAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);
		VoxelModel model = voxelModelResourceAtlas.get("ColourTestVox");

		Entity rootEntity = new Entity("root") {
		};

		Entity childEntity1 = new VoxelEntity("child1", model);
		rootEntity.addChild(childEntity1);

		Entity childEntity2 = new VoxelEntity("child2", model);
		rootEntity.addChild(childEntity2);
		childEntity2.getRelativeTransform().set(new Matrix4f().translate(50, 0, 0));

		RayEntityIntersection.Result result = RayEntityIntersection.raycast(new Vector3f(100, 0, 0), new Vector3f(-1, 0, 0), rootEntity);

		assertNotNull(result, "Raycast did not cause a collision.");

		assertEquals(childEntity2, result.collidedEntity, "Raycast collided with the wrong entity.");
		assertEquals(48, result.collisionDistance, "Raycast collided at the wrong length.");
	}

	@Test
	void testPassthroughRaycast() {
		ResourceAtlas<VoxelModel> voxelModelResourceAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);
		VoxelModel model = voxelModelResourceAtlas.get("ColourTestVox");
		VoxelModel hollowModel = voxelModelResourceAtlas.get("3x3x3");

		Entity rootEntity = new Entity("root") {
		};

		Entity childEntity1 = new VoxelEntity("child1", model);
		rootEntity.addChild(childEntity1);

		Entity childEntity2 = new VoxelEntity("child2Hollow", hollowModel);
		rootEntity.addChild(childEntity2);
		childEntity2.getRelativeTransform().set(new Matrix4f().translate(50, 0, 0));

		RayEntityIntersection.Result result = RayEntityIntersection.raycast(new Vector3f(100, 0, 0), new Vector3f(-1, 0, 0), rootEntity);

		assertNotNull(result, "Raycast did not cause a collision.");

		assertEquals(childEntity1, result.collidedEntity, "Raycast collided with the wrong entity.");
		assertEquals(98, result.collisionDistance, "Raycast collided at the wrong length.");
	}

	@Test
	void testRotatedRaycast() {
		ResourceAtlas<VoxelModel> voxelModelResourceAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);
		VoxelModel model = voxelModelResourceAtlas.get("ColourTest");

		Entity rootEntity = new Entity("root") {
		};

		Entity childEntity1 = new VoxelEntity("child1", model);
		rootEntity.addChild(childEntity1);

		Entity childEntity2 = new VoxelEntity("child2", model);
		rootEntity.addChild(childEntity2);
		childEntity2.getRelativeTransform().set(new Matrix4f().translate(50, 0, 0).rotateY((float) (Math.PI / 2)));

		RayEntityIntersection.Result result = RayEntityIntersection.raycast(new Vector3f(100, 0, 0), new Vector3f(-1, 0, 0), rootEntity);

		assertNotNull(result, "Raycast did not cause a collision.");

		assertEquals(childEntity2, result.collidedEntity, "Raycast collided with the wrong entity.");
		assertEquals(49.5, result.collisionDistance, "Raycast collided at the wrong length.");
	}

	@Test
	void testScaledRaycast() {
		ResourceAtlas<VoxelModel> voxelModelResourceAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);
		VoxelModel model = voxelModelResourceAtlas.getWithoutFallback("ColourTestVox.vox", false, false);
		Entity rootEntity = new Entity("root") {
		};

		Entity childEntity2 = new VoxelEntity("child2", model);
		rootEntity.addChild(childEntity2);
		childEntity2.getRelativeTransform().set(new Matrix4f().translate(20, -3, 0).scale(0.5f));

		// Hit surface should be from y -0.5 to -1
		Entity childEntity2_2 = new VoxelEntity("child2-2", model);
		childEntity2.addChild(childEntity2_2);
		childEntity2_2.getRelativeTransform().translate(0, 4.5f, 0);

		RayEntityIntersection.Result[] results = new RayEntityIntersection.Result[7];
		RayEntityIntersection.Result[] expected = new RayEntityIntersection.Result[7];

		for (int i = 0; i < results.length; i++) {
			float y = i * -.25f;
			results[i] = RayEntityIntersection.raycast(new Vector3f(10, y, 0), new Vector3f(1, 0, 0), rootEntity);
			if (results[i] != null)
				if (y <= -0.5f && y >= -1) {
					expected[i] = new RayEntityIntersection.Result();
					expected[i].collidedEntity = childEntity2_2;
					expected[i].collisionDistance = 15;
					expected[i].collisionPosition = new Vector3f(25, y, 0);
				}
		}

		assertArrayEquals(expected, results);
	}

	@Test
	void testAreaPassthroughRaycast() {
		ResourceAtlas<VoxelModel> voxelModelResourceAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);
		VoxelModel model = voxelModelResourceAtlas.get("ColourTest");
		VoxelModel hollowModel = voxelModelResourceAtlas.get("3x3x3");

		Entity rootEntity = new Entity("root") {
		};

		Entity childEntity1 = new VoxelEntity("child1", model);
		rootEntity.addChild(childEntity1);

		Entity childEntity2 = new VoxelEntity("child2Hollow", hollowModel);
		rootEntity.addChild(childEntity2);
		childEntity2.getRelativeTransform().set(new Matrix4f().translate(50, 0, 0));

		RayEntityIntersection.Result result = childEntity1.getPhysicalProperties()
				.testRayItersectionRough(new Vector3f(100, 0, 0), new Vector3f(-1, 0, 0));
		assertNotNull(result, "Raycast should collide with area");

		RayEntityIntersection.Result result1 = childEntity2.getPhysicalProperties()
				.testRayItersectionRough(new Vector3f(100, 0, 0), new Vector3f(-1, 0, 0));
		assertNotNull(result1, "Raycast should collide with area");

		RayEntityIntersection.Result result2 = childEntity1.getPhysicalProperties()
				.testRayItersectionRough(new Vector3f(100, 1, 0), new Vector3f(-1, 0, 0));
		assertNull(result2, "Raycast should not collide with area");

		RayEntityIntersection.Result result3 = childEntity2.getPhysicalProperties()
				.testRayItersectionRough(new Vector3f(100, 0, 3f), new Vector3f(-1, 0, 0));
		assertNull(result3, "Raycast should not collide with area");
	}

	@Test
	@Timeout(5)
	void test1000ModelRaycast() {
		ResourceAtlas<VoxelModel> voxelModelResourceAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);
		VoxelModel bigOlModel = voxelModelResourceAtlas.get("room");

		Entity rootEntity = new Entity() {
		};

		long time = TimeMe.timeMe(() -> {
			for (int i = 0; i < 1000; i++) {
				VoxelEntity entity = new VoxelEntity(bigOlModel);
				rootEntity.addChild(entity);
				entity.getRelativeTransform().translate(i * 10, 0, 0);
			}
		});
//		System.out.println("Setup time: " + time);

		time = TimeMe.timeMe(() -> {
			RayEntityIntersection.Result result = RayEntityIntersection.raycast(
					new Vector3f(-100, 0, 0),
					new Vector3f(1, 0, 0),
					rootEntity
			);
		});
//		System.out.println("Raycast time: " + time);
	}

	@Test
	@Timeout(5)
	void test1000ModelRaycastInverse() {
		ResourceAtlas<VoxelModel> voxelModelResourceAtlas = new ResourceAtlas<>(new VoxelModelLoader(), VoxelModel.fallbackModel);
		VoxelModel bigOlModel = voxelModelResourceAtlas.get("room");

		Entity rootEntity = new Entity() {
		};

		long time = TimeMe.timeMe(() -> {
			for (int i = 1000; i >= 0; i--) {
				VoxelEntity entity = new VoxelEntity(bigOlModel);
				rootEntity.addChild(entity);
				entity.getRelativeTransform().translate(i * 10, 0, 0);
			}
		});
//		System.out.println("Setup time: " + time);

		time = TimeMe.timeMe(() -> {
			RayEntityIntersection.Result result = RayEntityIntersection.raycast(
					new Vector3f(-100, 0, 0),
					new Vector3f(1, 0, 0),
					rootEntity
			);
		});
//		System.out.println("Raycast time: " + time);
	}
}
