package entities.physics.collision;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RayOBBIntersectionTests {

	private static Stream<Arguments> provideSimpleXAxisValues() {
		return Stream.of(
				Arguments.of(new Vector3f(0), new Vector3f(1, 0, 0), new Matrix4f().translate(new Vector3f(5, 0, 0)), true),
				Arguments.of(new Vector3f(2, 0, 0), new Vector3f(-1, 0, 0), new Matrix4f().translate(new Vector3f(5, 0, 0)), false),
				Arguments.of(new Vector3f(-100, 0, 1), new Vector3f(1, 0, 0), new Matrix4f().translate(new Vector3f(0, 0, 1)), true)
		);
	}

	@ParameterizedTest
	@MethodSource("provideSimpleXAxisValues")
	void testSimpleXAxisRaycasts(Vector3f rayOrigin, Vector3f rayDirection, Matrix4f cubeTransformation, boolean expectedCollision) {
		boolean collides = RayOBBIntersection.testOBBIntersection(rayOrigin, rayDirection, cubeTransformation) >= 0;
		assertEquals(expectedCollision, collides);
	}

	private static Stream<Arguments> provideSimpleYAxisValues() {
		return Stream.of(
				Arguments.of(new Vector3f(0), new Vector3f(0, 1, 0), new Matrix4f().translate(new Vector3f(0, 5, 0)), true),
				Arguments.of(new Vector3f(2, 0, 0), new Vector3f(0, 1, 0), new Matrix4f().translate(new Vector3f(0, 5, 0)), false)
		);
	}

	@ParameterizedTest
	@MethodSource("provideSimpleYAxisValues")
	void testSimpleYAxisRaycasts(Vector3f rayOrigin, Vector3f rayDirection, Matrix4f cubeTransformation, boolean expectedCollision) {
		boolean collides = RayOBBIntersection.testOBBIntersection(rayOrigin, rayDirection, cubeTransformation) >= 0;
		assertEquals(expectedCollision, collides);
	}

	private static Stream<Arguments> provideSimpleZAxisValues() {
		return Stream.of(
				Arguments.of(new Vector3f(0), new Vector3f(0, 0, 1), new Matrix4f().translate(new Vector3f(0, 0, 5)), true),
				Arguments.of(new Vector3f(0, 0, 2), new Vector3f(0, 0, -1), new Matrix4f().translate(new Vector3f(0, 0, 5)), false)
		);
	}

	@ParameterizedTest
	@MethodSource("provideSimpleZAxisValues")
	void testSimpleZAxisRaycasts(Vector3f rayOrigin, Vector3f rayDirection, Matrix4f cubeTransformation, boolean expectedCollision) {
		boolean collides = RayOBBIntersection.testOBBIntersection(rayOrigin, rayDirection, cubeTransformation) >= 0;
		assertEquals(expectedCollision, collides);
	}

	private static Stream<Arguments> provideDiagonalValues() {
		return Stream.of(
				Arguments.of(new Vector3f(-1), new Vector3f(1), new Matrix4f(), true),
				Arguments.of(new Vector3f(-1), new Vector3f(-1), new Matrix4f(), false)
		);
	}

	@ParameterizedTest
	@MethodSource("provideDiagonalValues")
	void testDiagonalRaycasts(Vector3f rayOrigin, Vector3f rayDirection, Matrix4f cubeTransformation, boolean expectedCollision) {
		boolean collides = RayOBBIntersection.testOBBIntersection(rayOrigin, rayDirection, cubeTransformation) >= 0;
		assertEquals(expectedCollision, collides);
	}

	private static Stream<Arguments> provideCuboidValues() {
		return Stream.of(
				Arguments.of(new Vector3f(0), new Vector3f(1, 0, 0), new Matrix4f().translate(new Vector3f(5, 2, 0)), new Vector3f(1, 100, 1), true),
				Arguments.of(new Vector3f(5, 0, 0), new Vector3f(0, 1, 0), new Matrix4f().translate(new Vector3f(5, 2, 0)), new Vector3f(0.2f, 0.2f, 1), true),
				Arguments.of(new Vector3f(1), new Vector3f(1), new Matrix4f().translate(new Vector3f(50, 50, 50)), new Vector3f(20, 0.2f, 1), true),
				Arguments.of(new Vector3f(1), new Vector3f(-1), new Matrix4f().translate(new Vector3f(50, 50, 50)), new Vector3f(20, 0.2f, 1), false)
		);
	}

	@ParameterizedTest
	@MethodSource("provideCuboidValues")
	void testCuboidRaycasts(Vector3f rayOrigin, Vector3f rayDirection, Matrix4f targetTransformation, Vector3f size, boolean expectedCollision) {
		boolean collides = RayOBBIntersection.testOBBIntersection(rayOrigin, rayDirection, targetTransformation, size) >= 0;
		assertEquals(expectedCollision, collides);
	}
}
