package entities.iterators;

import entities.Entity;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class FilteredIteratorTests extends IteratorTests {

	@Test
	void testNullPredicate() {
		Set<Entity> expectedElements = new HashSet<>();

		root.addChildren(entity1, entity2, entity3);
		entity1.addChildren(entity1a, entity1b, entity1c);
		entity2.addChildren(entity2a, entity2b, entity2c);
		entity3.addChildren(entity3a);

		expectedElements.add(root);
		expectedElements.add(entity1);
		expectedElements.add(entity2);
		expectedElements.add(entity3);
		expectedElements.add(entity1a);
		expectedElements.add(entity1b);
		expectedElements.add(entity1c);
		expectedElements.add(entity2a);
		expectedElements.add(entity2b);
		expectedElements.add(entity2c);
		expectedElements.add(entity3a);

		Iterator<Entity> iterator = new FilteredIterator<>(new BreadthFirstIterator(root), null);

		Set<Entity> iteratedElements = new HashSet<>();

		while (iterator.hasNext()) {
			iteratedElements.add(iterator.next());
		}

		expectedElements.removeAll(iteratedElements);
		assertTrue(expectedElements.isEmpty(), "Iterator did not cover all elements in the set.");

		assertThrows(NoSuchElementException.class, iterator::next);
	}

	@Test
	void testNullTree() {
		super.testNullTree(new FilteredIterator<>(new BreadthFirstIterator(null), null));
	}

	@Test
	void testSimplePredicate() {
		Set<Entity> expectedElements = new HashSet<>();

		root.addChildren(entity1, entity2, entity3);
		entity1.addChildren(entity1a, entity1b, entity1c);
		entity2.addChildren(entity2a, entity2b, entity2c);
		entity3.addChildren(entity3a);

		expectedElements.add(entity1b);
		expectedElements.add(entity2b);

		Set<Entity> iteratedElements = new HashSet<>();

		Iterator<Entity> iterator = new FilteredIterator<>(new BreadthFirstIterator(root), (e) -> e.getId().contains("b"));

		while (iterator.hasNext()) {
			iteratedElements.add(iterator.next());
		}

		assertEquals(expectedElements, iteratedElements, "Iterated incorrect elements.");
	}

	@Test
	void testNullElement() {
		List<Integer> ints = new ArrayList<>();
		ints.add(-1);
		ints.add(1);
		ints.add(null);
		ints.add(2);
		ints.add(5);

		List<Integer> expectedInts = new ArrayList<>();
		expectedInts.add(1);
		expectedInts.add(null);
		expectedInts.add(2);

		Iterator<Integer> integerIterator = new FilteredIterator<>(
				ints.iterator(),
				integer -> integer == null || (integer > 0 && integer < 3)
		);


		int i = 0;
		while (integerIterator.hasNext()) {
			Integer out = integerIterator.next();
			if (!Objects.equals(out, expectedInts.get(i))) {
				fail("Unexpected element: " + out);
			}
			i++;
		}
		assertEquals(expectedInts.size(), i, "Iterated fewer elements than were expected.");
	}
}
