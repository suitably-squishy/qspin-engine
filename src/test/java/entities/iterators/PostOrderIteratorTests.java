package entities.iterators;

import entities.Entity;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.*;

public class PostOrderIteratorTests extends IteratorTests {

	@Test
	void testBasicTraversal() {
		Queue<Entity> correctOrder = new LinkedList<>();
		
		root.addChildren(entity1, entity2, entity3);

		entity1.addChildren(entity1a, entity1b, entity1c);
		entity2.addChildren(entity2a, entity2b, entity2c);
		entity3.addChildren(entity3a);

		correctOrder.add(entity1a);
		correctOrder.add(entity1b);
		correctOrder.add(entity1c);
		correctOrder.add(entity1);
		correctOrder.add(entity2a);
		correctOrder.add(entity2b);
		correctOrder.add(entity2c);
		correctOrder.add(entity2);
		correctOrder.add(entity3a);
		correctOrder.add(entity3);
		correctOrder.add(root);

		Iterator<Entity> iterator = new PostOrderIterator(root);

		for (Entity next : correctOrder) {
			assertEquals(iterator.next(), next);
		}

		assertFalse(iterator.hasNext());
		assertThrows(NoSuchElementException.class, iterator::next);
	}

	@Test
	void testNullTree() {
		super.testNullTree(new PostOrderIterator(null));
	}
}
