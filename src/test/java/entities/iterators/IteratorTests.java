package entities.iterators;

import entities.Entity;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

public abstract class IteratorTests {
	
	Entity root = new Entity("root") {
	};
	Entity entity1 = new Entity("entity1") {
	};
	Entity entity2 = new Entity("entity2") {
	};
	Entity entity3 = new Entity("entity3") {
	};
	Entity entity1a = new Entity("entity1a") {
	};
	Entity entity1b = new Entity("entity1b") {
	};
	Entity entity1c = new Entity("entity1c") {
	};
	Entity entity2a = new Entity("entity2a") {
	};
	Entity entity2b = new Entity("entity2b") {
	};
	Entity entity2c = new Entity("entity2c") {
	};
	Entity entity3a = new Entity("entity3a") {
	};
	
	void testNullTree(Iterator<?> iterator) {
		assertFalse(iterator.hasNext(), "Iterator claimed it had remaining values.");
		assertThrows(NoSuchElementException.class, iterator::next,
				"Iterator did not throw an exception when calling next without remaining values.");
	}
	
}
