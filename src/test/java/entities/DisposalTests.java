package entities;

import org.junit.jupiter.api.Test;
import render.model.VoxelModel;

import static org.junit.jupiter.api.Assertions.*;

public class DisposalTests {

	@Test
	void testBasicDisposal() {
		Entity entity = new VoxelEntity("disposeyBoi", VoxelModel.fallbackModel);

		assertFalse(entity.disposed, "Entity was created already disposed.");
		assertFalse(entity.soundModule.isDisposed(), "Sound module was created already disposed.");

		Entity child = new VoxelEntity("childBoi", VoxelModel.fallbackModel);
		entity.addChild(child);

		assertEquals(child.getParent(), entity);

		entity.dispose(true);
		assertTrue(entity.soundModule.isDisposed());
		assertTrue(entity.disposed);
		assertTrue(child.disposed);
		assertTrue(child.soundModule.isDisposed());

		Entity child2 = new VoxelEntity("child2Boi", VoxelModel.fallbackModel);

		assertThrows(MalformedEntityTreeException.class, () -> entity.addChild(child2));

		assertNull(child2.getParent(), "Child was added to a disposed entity.");
	}
}
