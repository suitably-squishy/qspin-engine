package entities;

import entities.filters.Class;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class EntityTreeTests {

	@Test
	void testTreeSanity() {
		Entity root = new Entity() {
		};

		assertFalse(root.disposed);

		Entity child1 = new Entity() {
		};

		root.addChild(child1);

		assertTrue(root.hasChild(child1));
		assertEquals(root, child1.getParent());

		assertTrue(root.removeChild(child1));

		Entity impostorEntity = new Entity() {
		};

		assertFalse(root.removeChild(impostorEntity));

		assertNull(child1.getParent());
		assertFalse(root.hasChild(child1));

		assertThrows(MalformedEntityTreeException.class, () -> root.addChild(root));

		assertFalse(root.hasChild(root));

		Entity child2 = new Entity() {
		};
		child2.dispose(false);

		assertThrows(MalformedEntityTreeException.class, () -> root.addChild(child2));
		assertFalse(root.hasChild(child2));
		assertNull(child2.getParent());

		root.dispose(true);

		Entity child3 = new Entity() {
		};

		assertThrows(MalformedEntityTreeException.class, () -> root.addChild(child3));
		assertFalse(root.hasChild(child3));
		assertNull(child3.getParent());
	}

	@Test
	void testTreePredicateRemoval() {
		Entity root = new Entity() {
		};

		Entity child1 = new Entity("child1", Collections.singletonList("group1")) {
		};
		Entity child2 = new Entity("child2", Collections.singletonList("group2")) {
		};
		Entity child3 = new Entity("child3", Collections.singletonList("group1")) {
		};
		Entity child4 = new Entity("child4", Collections.singletonList("group2")) {
		};

		root.addChildren(child1, child2, child3, child4);

		assertTrue(root.hasChild(child1));
		assertTrue(root.hasChild(child2));
		assertTrue(root.hasChild(child3));
		assertTrue(root.hasChild(child4));

		root.removeChildren(new Class("group1"));

		assertFalse(root.hasChild(child1));
		assertTrue(root.hasChild(child2));
		assertFalse(root.hasChild(child3));
		assertTrue(root.hasChild(child4));

		root.addChild(child1);
		root.addChild(child3);

		root.dispose(true);
	}

	@Test
	void testTreeLoopProtection() {
		Entity root = new Entity() {
		};

		Entity child1 = new Entity() {
		};
		root.addChild(child1);

		assertThrows(MalformedEntityTreeException.class, () -> child1.addChild(root));

		assertNotEquals(child1, root.getParent());
	}

	@Test
	void testMovingChild() {
		Entity root = new Entity() {
		};

		Entity child1 = new Entity() {
		};

		Entity child2 = new Entity() {
		};

		root.addChildren(child1, child2);

		Entity movingChild = new Entity() {
		};

		child1.addChild(movingChild);
		assertThrows(MalformedEntityTreeException.class, () -> child2.addChild(movingChild));

		assertEquals(child1, movingChild.getParent());

		assertTrue(child1.hasChild(movingChild));
		assertFalse(child2.hasChild(movingChild));
	}
}
