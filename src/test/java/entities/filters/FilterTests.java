package entities.filters;

import entities.Entity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FilterTests {

	Entity root = new Entity("root", Arrays.asList("1", "2")) {
	};
	Entity entity1 = new Entity("entity1", Arrays.asList("2", "3")) {
	};
	Entity entity2 = new Entity("entity2", Arrays.asList("3", "4")) {
	};
	Entity entity3 = new Entity("entity3", Arrays.asList("4", "5")) {
	};
	Entity entity1a = new Entity("entity1a", Arrays.asList("5", "6")) {
	};
	Entity entity1ai = new Entity("entity1ai", Arrays.asList("5", "6")) {
	};
	Entity entity1b = new Entity("entity1b", Arrays.asList("6", "7")) {
	};
	Entity entity1c = new Entity("entity1c", Arrays.asList("7", "8")) {
	};
	Entity entity2a = new Entity("entity2a", Arrays.asList("8", "9")) {
	};
	Entity entity2b = new Entity("entity2b", Arrays.asList("9", "10")) {
	};
	Entity entity2c = new Entity("entity2c", Arrays.asList("10", "11")) {
	};
	Entity entity3a = new Entity("entity3a", Arrays.asList("11", "12")) {
	};

	@BeforeEach
	void buildTree() {
		root.addChildren(entity1, entity2, entity3);

		entity1.addChildren(entity1a, entity1b, entity1c);
		entity1.addChildren(entity1ai);
		entity2.addChildren(entity2a, entity2b, entity2c);
		entity3.addChildren(entity3a);
	}

	@Test
	void testBasicSelectors() {
		Predicate<Entity> filter = new Filter().hasClass("1");
		assertTrue(filter.test(root));
		assertFalse(filter.test(entity1));

		filter = new Filter().hasClass("8");
		assertTrue(filter.test(entity2a));
		assertFalse(filter.test(root));

		filter = new Filter().hasID("entity1a");
		assertTrue(filter.test(entity1a));
		assertFalse(filter.test(entity1));
	}

	@Test
	void testDescendantSelectors() {
		Predicate<Entity> filter = new Filter().isChildOf(
				new Filter().hasID("root")
		);

		assertFalse(filter.test(root));
		assertTrue(filter.test(entity1));
		assertTrue(filter.test(entity2));
		assertTrue(filter.test(entity3));
		assertFalse(filter.test(entity1a));

		filter = new Filter().isDescendantOf(
				new Filter().hasID("entity1")
		);

		assertFalse(filter.test(root));
		assertFalse(filter.test(entity1));
		assertTrue(filter.test(entity1a));
		assertTrue(filter.test(entity1b));
		assertTrue(filter.test(entity1ai));
	}

	@Test
	void testChainedSelectors() {
		Predicate<Entity> filter = new Filter().hasClass("2").hasClass("3");
		assertTrue(filter.test(entity1));
		assertFalse(filter.test(root));
		assertFalse(filter.test(entity2));

		filter = new Filter().hasClass("7").isChildOf(
				new Filter().hasID("entity1")
		);

		assertFalse(filter.test(entity1));
		assertFalse(filter.test(entity1a));
		assertTrue(filter.test(entity1b));
		assertTrue(filter.test(entity1c));
		assertFalse(filter.test(entity1ai));
	}

	@Test
	void testMultipleClasses() {
		Predicate<Entity> filter = new Filter().hasClasses("2", "3");
		assertTrue(filter.test(entity1));
		assertFalse(filter.test(root));
		assertFalse(filter.test(entity2));
	}

	@Test
	void testIsObject() {
		Predicate<Entity> filter = new Filter().is(entity1ai);
		assertTrue(filter.test(entity1ai));
		assertFalse(filter.test(entity1a));

		entity1a.getChildren().remove(entity1ai);
	}
}
