package entities;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

public class EntityRegistryTests {

	EntityRegistry registry = new EntityRegistry();

	Collection<Entity> entitySet = new HashSet<>();

	Collection<Entity> class0Entities = new HashSet<>();
	Collection<Entity> class1Entities = new HashSet<>();
	Collection<Entity> class2Entities = new HashSet<>();


	Entity root = new Entity("root", Collections.singletonList("0")) {
	};

	Entity child1 = new Entity("child1", Collections.singletonList("1")) {
	};
	Entity child2 = new Entity("child2", Collections.singletonList("1")) {
	};
	Entity child3 = new Entity("child3", Collections.singletonList("1")) {
	};
	Entity child1a = new Entity("child1a", Collections.singletonList("2")) {
	};
	Entity child1b = new Entity("child1b", Collections.singletonList("2")) {
	};
	Entity child2a = new Entity("child2a", Collections.singletonList("2")) {
	};

	private void buildTree() {
		root.addChildren(child1, child2, child3);

		child1.addChildren(child1a, child1b);

		child2.addChildren(child2a);

		entitySet.add(root);
		entitySet.add(child1);
		entitySet.add(child2);
		entitySet.add(child3);
		entitySet.add(child1a);
		entitySet.add(child1b);
		entitySet.add(child2a);

		class0Entities.add(root);

		class1Entities.add(child1);
		class1Entities.add(child2);
		class1Entities.add(child3);

		class2Entities.add(child1a);
		class2Entities.add(child1b);
		class2Entities.add(child2a);
	}

	@Test
	void testPreBuiltTreeRegistry() {
		buildTree();

		root.setRegistry(registry);

		for (Entity entity : entitySet) {
			assertEquals(registry, entity.getRegistry(), entity.toString() + " did not have the right registry.");
		}

		assertEquals(child1a, registry.getEntityByID("child1a"));

		child1.removeChild(child1a);

		assertNull(child1a.getRegistry());

		assertNull(registry.getEntityByID("child1a"));
	}

	@Test
	void testPostBuiltTreeRegistry() {
		root.setRegistry(registry);

		buildTree();

		for (Entity entity : entitySet) {
			assertEquals(registry, entity.getRegistry(), entity.toString() + " did not have the right registry.");
		}

		assertEquals(child1a, registry.getEntityByID("child1a"));

		child1.removeChild(child1a);

		assertNull(child1a.getRegistry());

		assertNull(registry.getEntityByID("child1a"));
	}

	@Test
	void testPreBuiltTreeIDConflict() {
		buildTree();
		root.setRegistry(registry);

		assertFalse(registry.isUniqueID("child2"));

		Entity impostorEntity = new Entity("child2") {
		};

		assertThrows(MalformedEntityTreeException.class, () -> child1a.addChild(impostorEntity));
	}

	@Test
	void testPostBuiltTreeIDConflict() {
		buildTree();

		Entity impostorEntity = new Entity("child2") {
		};

		child1a.addChild(impostorEntity);

		assertThrows(MalformedEntityTreeException.class, () -> root.setRegistry(registry));
	}

	@Test
	void testIDUpdate() {
		buildTree();
		root.setRegistry(registry);

		child1a.setId("beans");

		assertEquals(child1a, registry.getEntityByID("beans"));

		assertThrows(MalformedEntityTreeException.class, () -> child3.setId("child2"));
	}

	@Test
	void testPreBuiltTreeClassRegistry() {
		buildTree();
		root.setRegistry(registry);

		Collection<Entity> class2Entities = registry.getEntitiesByClass("2");

		assertTrue(CollectionUtils.isEqualCollection(class2Entities, this.class2Entities));

		child1.classList.removeAll(Collections.singletonList("1"));

		Collection<Entity> class1Entities = registry.getEntitiesByClass("1");

		Collection<Entity> expectedEntities = new HashSet<>(this.class1Entities);
		expectedEntities.remove(child1);

		assertFalse(child1.classList.contains("1"));

		assertTrue(CollectionUtils.isEqualCollection(class1Entities, expectedEntities));

		child2a.classList.remove("2");

		assertFalse(registry.getEntitiesByClass("2").contains(child2a));

		child1a.classList.addAll(Arrays.asList("5", "6"));

		assertTrue(registry.getEntitiesByClass("5").contains(child1a));
		assertTrue(registry.getEntitiesByClass("6").contains(child1a));
		assertTrue(child1a.classList.contains("5"));

		root.classList.add("toot");

		assertTrue(registry.getEntitiesByClass("toot").contains(root));
		assertTrue(registry.getEntitiesByClass("toot").contains(root));
		assertTrue(root.classList.contains("toot"));
	}

	@Test
	void testPostBuiltTreeClassRegistry() {
		root.setRegistry(registry);
		buildTree();

		Collection<Entity> class2Entities = registry.getEntitiesByClass("2");

		assertTrue(CollectionUtils.isEqualCollection(class2Entities, this.class2Entities));
	}

	@Test
	void testRegistrySwap() {
		EntityRegistry registry2 = new EntityRegistry();

		buildTree();
		root.setRegistry(registry);

		root.setRegistry(registry2);

		assertEquals(registry2, root.getRegistry());

		assertNull(registry.getEntityByID("root"));
		assertEquals(root, registry2.getEntityByID("root"));

		assertNull(registry.getEntityByID("child1a"));
		assertEquals(child1a, registry2.getEntityByID("child1a"));

		assertTrue(registry.getEntitiesByClass("1").isEmpty());
		assertFalse(registry2.getEntitiesByClass("1").isEmpty());

		root.setRegistry(null);

		assertNull(registry2.getEntityByID("root"));
		assertNull(registry2.getEntityByID("child1a"));
		assertTrue(registry2.getEntitiesByClass("1").isEmpty());
	}

	@Test
	void testNullIDDoesNotCountAsDuplicate() {
		Entity root = new Entity((String) null) {
		};

		Entity child1 = new Entity((String) null) {
		};

		root.addChild(child1);

		root.setRegistry(registry);
	}

	@Test
	void testNullIDUpdates() {
		Entity root = new Entity() {
		};

		Entity child = new Entity() {
		};

		root.addChild(child);

		root.setRegistry(registry);

		root.setId("toot");

		assertEquals(root, registry.getEntityByID("toot"));

		root.setId(null);

		assertNull(registry.getEntityByID("toot"));
	}
}
