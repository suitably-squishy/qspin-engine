# Changelog

## [0.2.0] - 2020-11-17
### Added
#### Entity System
- Entity iteration utilities.
- Entity IDs and classes.
- Entity accessor utilities.
- Raycast example to test scene.
- Ability to remove child entities.
- Entity tree structure checks to prevent malformed trees.
#### Sound System
- Simple OpenAL sound system.
#### Render System
- Postprocessing pipeline
- Postprocessing bloom support
#### IO System
- Loading of .qb files
### Changed
- Loading of voxel files now can optionally include the file extension.
- Default coordinate system is now a left handed coordinate system with z depth.
### Fixed
- Fixes high cpu usage when using raycast utilities.

## [0.1.2] - 2020-09-06
### Changed
- Cleans up README.
### Fixed
- Fixes linux failure to load demo files due to directory case sensitivity.

## [0.1.1] - 2020-08-26
### Changed

- Java compile target changed to java 1.11

## [0.1.0] - 2020-08-22
### Added
- Simple .vox file parsing.
- Simple entity storage system.
- Basic lighting system.
- Simple [LEGUI](https://github.com/SpinyOwl/legui) integration.
- Simple camera system
- Raycasting support
- Demo scene

