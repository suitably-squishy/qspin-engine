# qSpin Engine

Welcome to the qSpin engine project; an open source, voxel based, 3D game engine. 
qSpin is written in Java using [LWJGL3](https://www.lwjgl.org/) and [Liquid Engine GUI](https://github.com/SpinyOwl/legui) as a fun project.

## Quickstart guide
qSpin Engine is hosted on the maven central repository. We recommend adding the project through gradle.

To add qSpin to your gradle project add an implementation with the url "uk.co.suitablysquishy.qspin-engine:qspin-engine:(version you would like to add)"  in the dependencies section of your Build.gradle. All current versions can be found at
[The maven repository search](https://search.maven.org/artifact/uk.co.suitablysquishy.qspin-engine/qspin-engine).

You will need to add the repositories mavenCentral, "https://raw.github.com/SpinyOwl/repo/releases" (as a maven repo) and "https://oss.sonatype.org/content/repositories/snapshots/" (also as a maven repo).

Release builds are stable with as much bug fixing and polish as possible, 
whereas Snapshots are likely broken but will be further in development. 
A rough plan of development can be found at our [Trello](https://trello.com/b/Co6TmYW8/qspin-engine-roadmap).

### Quickstart Development guide
qSpin is written in Java and requires Java 11 or higher, to develop using qSpin download a [JDK](https://adoptopenjdk.net/).

To start writing with qSpin you must instance runner, giving the title of the window.
```
Runner runner = new Runner("MyWindow");
```
Then, before startup, you need to set up your game running. Do this by:
 - Loading your textures with resourceManager (voxel models should be placed in the resources/models directory, 
 this allows you to omit the "resources/models" prefix as well as the ".vox" suffix):
 ```
VoxelModel modelX = runner.getResourceManager().get(VoxelModel.class, pathInModelsDirectory/filename);
VoxelModel modelY = runner.getResourceManager().get(VoxelModel.class, pathInModelsDirectory/filename);
```
 - Instantiating your entities
 ```
Entity x = new VoxelEntity(modelX);
Entity y = new VoxelEntity(modelX);
```
 - Deciding a root entity then adding setting the children in the desired layout 
 ```
x.addChild(y)
```
- Setup camera and lights
```
//Binds
MouseInputContext mouseInputContext = new MouseInputContext("default");
BindContext bindContext = new BindContext("default");

//camera
FirstPersonCamera cameraEntity = new FirstPersonCamera(camera);
x.addChild(cameraEntity);
cameraEntity.setUpCamera(bindContext, mouseInputContext);

//Light (this will setup a directional light)
Entity testLight = new LightEntity();
x.addChild(testLight);
```
 - Setting up a GameState that holds the data for the current state of the engine by
      - Instancing GameState
      - Setting the root entity of that state, beginning the tree
      - Setting the camera to render from
 ```
GameState state = new GameState();
state.setRootEntity(cityBase);
state.setCurrentCamera(camera);
runner.setState(state);
runner.startup();
```
 - Adding the entity and state behaviours 
 ```
y.addBehaviour("move", (delta,owner) -> {
    y.getRelativeTransform.translate(0,0,1);
});
```

 - Finally start the Main game loop and add the shutdown code for cleanup
 ```
runner.run();
runner.shutdown();
```
 
 A full example is in demo.TestSceneMain

## Engine Basics
qSpin is designed to load magicaVoxel .vox files into a rendering scene, and then light the scene with 
a dedicated lighting engine. The engine then has several options for interaction with the scene. 
### Setting the scene
qSpin uses an entity system to store data about individual models, lights, and cameras. 
These are stored within a tree system where an entity holds it's children for passthrough of functions.
This means that there is a root entity that determines the coordinate system for all other entities.
Entity transforms can be relative to the entities parent or absolute (relative to the root entity).

- VoxelEntity stores entities with voxel models, loaded from voxel models. 
- CameraEntity stores the data for a camera
- LightEntity stores the data for a single light

There is currently an upper limit to the number of lights capable of being rendered at one time in a scene.
This limit is enforced by the language of the lighting shader, and will be removed in later versions
The lighting is rendered through forward rendering in the fragment shader.

### Interacting with the scene
Several methods of interacting with the rendered scene exist, the simplest is to set the transform of 
an entity. An entity has a transform from it's parent called relative transform, 
and setting that will cause an entity to move.

If you want an entity to move every game loop you can add a behaviour. 
Behaviors hold a function that is run every update loop 
(It should be noted, the update and render loops run separately) and any object that extends 
BehaviouralObject can have behaviours.

To add user interaction there is facility to add key listeners that can activate behaviors. 
There is also facility to ad UI. Raycasting has been added as a utility to determine 
a cube intersection for selection of models or individual cubes.  
